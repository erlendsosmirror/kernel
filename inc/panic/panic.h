/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PANIC_H
#define PANIC_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sched/threadcontrol.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

/*
 * These definitions identifies if the fault occured in the kernel or in
 * userspace.
 */
#define PANIC_STACKTYPE_KERNEL 0
#define PANIC_STACKTYPE_USER 1

/*
 * These definitions identify the fault source. They are mostly relevant
 * for platform specific error handling.
 */
#define PANIC_FAULTTYPE_KERNEL 0
#define PANIC_FAULTTYPE_HARDFAULT 1
#define PANIC_FAULTTYPE_MEMMANAGE 2
#define PANIC_FAULTTYPE_BUSFAULT 3
#define PANIC_FAULTTYPE_USAGEFAULT 4

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

/*
 * Print the specified message and stall the system for easy debug access.
 *
 * WARNING:
 *  Calling this is essentially declaring the system as dead. You cannot return
 *  from this, except for hardware resets.
 */
void Panic (const char *message);

/*
 * The more generic handler for different panic types. Not to be used directly.
 * Should be defined in the platform code.
 */
void PanicInternal (unsigned int *stack, int stacktype, int faulttype);

/*
 * Print crash report of a userspace thread
 */
void PanicPrintReport (struct ThreadControl *ctc);

#endif
