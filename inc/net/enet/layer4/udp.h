/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//
// Created by erlendjs on 10/05/16.
//

#ifndef NETWORK_LAYER4_TCPUDP_H
#define NETWORK_LAYER4_TCPUDP_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "net/enet/layer2/dataframe.h"
#include "posix/arpa/inet.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define NET_IP_BROADCAST 0xFAFA

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////

// This structure defines the data members of a DataFrame when used in UDP
typedef struct UDPCast
{
	unsigned char reserved[6]; 		// layer2 packet header
	unsigned short destination;		// layer3 ip
	unsigned short source;			// layer3 ip
	unsigned char ttl;				// layer3 ip
	unsigned char payloadprotocol;	// layer3 ip
	unsigned char dstport;			// layer4 udp
	unsigned char srcport;			// layer4 udp
	unsigned char payload[242];		// layer4 udp
	unsigned int reserved2;
}UDPCast;

// UDP Socket definition
typedef struct UDPSocket
{
	unsigned short ip;				// Destination IP
	unsigned char dstport;			// Destination port
	unsigned char srcport;			// Source port
	unsigned short hwaddr;			// ARP Cache
	struct UDPSocket *next;		// For internal linked list of sockets
	DataFrame *frames;			// Linked list of frames pending reception
}UDPSocket;

///////////////////////////////////////////////////////////////////////////////
// Functions: UDP Socket member definitions
///////////////////////////////////////////////////////////////////////////////
/*
 * Initializes a socket with the supplied data and adds it to some internal
 * structures, making it usable.
 */
int UDPInit (UDPSocket *s, unsigned short ipaddress, unsigned char dst, unsigned char src);

/*
 * Frees data allocated within the socket. Does not free the socket itself.
 */
void UDPRemove (UDPSocket *s);

int UDPFindFreePort (void);
int UDPSetPort (int port);
int UDPClearPort (int port);

/*
 * Send data of len bytes using socket that, optionally sending it back to
 * receiver defined by reference.
 */
int UDPSend (UDPSocket *s, const unsigned char *data, int len, const struct sockaddr *addr);

int UDPRecv (UDPSocket *s, unsigned char *data, int len, struct sockaddr *addr);

///////////////////////////////////////////////////////////////////////////////
// Functions: General UDP API function definitions
///////////////////////////////////////////////////////////////////////////////
/*
 * Process IP frame.
 */
void IPProcess (DataFrame *f);

#endif
