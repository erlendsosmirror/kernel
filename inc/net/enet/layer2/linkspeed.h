/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//
// Created by erlendjs on 27/04/16.
//

#ifndef NETWORK_LAYER2_LINKSPEED_H
#define NETWORK_LAYER2_LINKSPEED_H

/*
 * States:
 *  0 - Sequence completed, monitoring link->IsUp
 *  1 - Ready to start sequence, still monitoring link->IsUp
 *  2 - Sent discovery, listening for list reply or timeout event
 *  3 - Got list and sent set speed, waiting for Update tick to send data
 *  4 - Sent test packet, waiting for reply
 *
 *  10 - Test packet failed. Waiting for timeout.
 *
 *  254 - Got set speed, speed has been set and is waiting for testdata
 *  255 - Sent speed list, waiting for set speed
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "../config/live/network-config.h"
#include "net/enet/layer2/dataframe.h"
#include "net/enet/layer2/interface.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// This MCU's address
#define LAYER2LINKSPEED_ADDRESS LAYER2_ADDRESS

// Link address
#define LAYER2LINKSPEED_DSTADDRESS 0x3030

// Protocol numer
#define LAYER2LINKSPEED_PROTOCOL '3'

// Packet types
#define LAYER2LINKSPEED_PTYPE_DISCOVERY 'd'
#define LAYER2LINKSPEED_PTYPE_SPEEDLIST 'l'
#define LAYER2LINKSPEED_PTYPE_SETSPEED 's'
#define LAYER2LINKSPEED_PTYPE_TEST 't'
#define LAYER2LINKSPEED_PTYPE_TESTREPLY 'r'
#define LAYER2LINKSPEED_PTYPE_SPEEDCONFIRM 'c'

// Speedlist
#define LAYER2LINKSPEED_SPEEDLIST_DEFAULT 254

// Timeouts
#define LAYER2LINKSPEED_RESTARTTIMEOUT 3000		// Used by discovery
#define LAYER2LINKSPEED_SENDTESTFRAMEWAIT 100	// Between receiving list and first test frame
#define LAYER2LINKSPEED_RECVTESTTIMEOUT 1000	// Timeout from sending test to receiving it
#define LAYER2LINKSPEED_REPLIEDTIMEOUT 3000		// Timeout from replying to test
#define LAYER2LINKSPEED_SETSPEEDTIMEOUT 1000	// Timeout from receiving set speed
#define LAYER2LINKSPEED_YIELDTIMEOUT 5000		// When receiving from higher ranked, stay put

// Sequence
#define LAYER2LINKSPEED_SEQUENCESTART 10

///////////////////////////////////////////////////////////////////////////////
// Protocol structures
///////////////////////////////////////////////////////////////////////////////

// Data rate enumeration
enum DataRate
{
	UARTSPEED_DEFAULT = 19200,
	UARTSPEED_19200 = 19200,
	UARTSPEED_57600 = 57600,
	UARTSPEED_115200 = 115200,
	UARTSPEED_250000 = 250000,
	UARTSPEED_500000 = 500000,
	UARTSPEED_1000000 = 1000000,
	UARTSPEED_1500000 = 1500000,
	UARTSPEED_2000000 = 2000000
};

///////////////////////////////////////////////////////////////////////////////
// Structure definitions
///////////////////////////////////////////////////////////////////////////////
typedef struct Layer2Linkspeed
{
	Layer2 *link;
	unsigned char state;
	unsigned char sequence;
	unsigned short servingremote;
	unsigned int time;
	unsigned int settingspeed;
	unsigned int speeds[8];
}Layer2Linkspeed;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void Layer2LinkspeedConstructor (Layer2Linkspeed *l, Layer2 *interface);
void Layer2LinkspeedUpdate (Layer2Linkspeed *l);
void Layer2LinkspeedProcess (Layer2Linkspeed *l, DataFrame *frame); // Note: takes ownership
int Layer2LinkspeedLinkIsReady (Layer2Linkspeed *l);

#endif
