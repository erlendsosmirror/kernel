/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LAYER2_INTERFACE_H
#define LAYER2_INTERFACE_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "../config/live/network-config.h"
#include "net/enet/layer2/dataframe.h"

#if NETWORK_DEBUG_INTERFACE
#include NETWORK_DEBUG_INC
#endif

#ifdef NETWORK_PLATFORM_INC
#include NETWORK_PLATFORM_INC
#endif

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Statistic recording
#define LAYER2_DOSTATS 1

///////////////////////////////////////////////////////////////////////////////
// Debug definitions
///////////////////////////////////////////////////////////////////////////////
#if NETWORK_DEBUG_INTERFACE
	#define IFDEBUG_OTHER(str) str
#else
	#define IFDEBUG_OTHER(str)
#endif

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#if LAYER2_DOSTATS
	#define IFSTATS(arg) arg
#else
	#define IFSTATS(arg)
#endif

///////////////////////////////////////////////////////////////////////////////
// Physical layer driver structures
///////////////////////////////////////////////////////////////////////////////

// Statistics
typedef struct Layer2Statistics
{
	unsigned int framesreceived;
	unsigned int framessent;

	unsigned int bytesreceived;
	unsigned int bytessent;

	unsigned int error_rxfifofull;
	unsigned int error_pumpallocerr;
	unsigned int error_sendframedma;
	unsigned int error_txfifofull;
	unsigned int error_framesize;
}Layer2Statistics;

// Layer 2 driver type
typedef struct Layer2
{
	// Next inferface
	struct Layer2 *next;

	// Precomputed interface ID
	struct Layer2 *interfaceid;

	// Link state
	unsigned char linkstate;

	// Local receive circular buffer
	unsigned char rxbuffer[RS485_RXBUFF_SIZE];
	unsigned int writepos;
	unsigned int readfrompos;
	unsigned int readtopos;

	// Local transmit FIFO
	DataFrame *txframes[RS485_TXFIFO_SIZE];
	unsigned char txframesread;
	unsigned char txframeswrite;

	// Statistics, included if enabled
	IFSTATS (Layer2Statistics stats);
}Layer2;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void AddInterface (Layer2 *interface);

///////////////////////////////////////////////////////////////////////////////
// Interface
///////////////////////////////////////////////////////////////////////////////

/*
 * Initializer
 * Does UART setup, DMA setup and starts reception.
 */
void Layer2Init (Layer2 *l);

/*
 * Processes incoming data and starts outgoing dma transfers.
 */
void Layer2Pump (Layer2 *l);

/*
 * User implemented
 */
int Layer2SendFrameInternal (Layer2 *l, DataFrame *frame);

/*
 * Set speed
 */
void Layer2SetSpeed (Layer2 *l, unsigned int speed);

/*
 * Interface OK?
 */
int Layer2IsUp (Layer2 *l);

/*
 * Link up?
 */
int Layer2LinkUp (Layer2 *l);

#endif
