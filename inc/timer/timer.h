/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TIMER_H
#define TIMER_H

/*
 * How to Use:
 *  1. Allocate timer handle somewhere.
 *  2. Set all members of the handle to zero, memset recommended.
 *  3. Set the callback, interval (milliseconds) and flags:
 *       TIMER_FLAG_REPEATING: Callback is called every interval milliseconds.
 *       TIMER_FLAG_NOIRQ: Interrupts are disabled when Callback is called.
 *  4. Use TimerRegister to set the timer.
 *  5. Wait for timer to trigger.
 *  6. Use TimerUnregister to stop the timer. If TIMER_FLAG_REPEATING is not
 *     specified, it will be unregistered on trigger automatically. Otherwise,
 *     use it inside the callback. If called from outside the callback, it must
 *     be protected in a critical section.
 *
 * WARNING:
 *  The register and unregister functions are NOT thread safe. If you are
 *  calling them outside of interrupts and such, the calls have to be
 *  protected.
 */

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define TIMER_FLAG_REPEATING 1
#define TIMER_FLAG_NOIRQ 2

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct TimerHandle
{
	int reserved1; // Next
	int reserved2; // Prev
	void (*Callback)(struct TimerHandle*);
	int reserved3; // Container
	int reserved4; // Sort value
	unsigned int interval;
	int flags;
}TimerHandle;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

/*
 * Start the timer worker thread. Only to be called on kernel startup.
 */
void TimerInitialize (void);

/*
 * Register an already initialized timer handle (see above). This function is
 * not thread safe, so protect it in a critical section (interrupts disabled)
 * if required.
 */
void TimerRegister (TimerHandle *handle);

/*
 * Unregister a timer handle. Not thread safe, see TimerRegister.
 */
void TimerUnRegister (TimerHandle *handle);

/*
 * Returns true if the timer is currently registered.
 */
int TimerIsRegistered (TimerHandle *handle);

#endif
