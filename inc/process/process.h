/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sched/threadcontrol.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

/*
 * Allocate a new process control block and initialize it with the supplied
 * function and stack. The process is given a PID, has the root directory
 * as its working directory and is set to privileged access. The process is
 * then started with normal priority.
 *
 * Returns a control block if successful, null otherwise.
 */
struct ProcessControl *ProcessCreateInternal (void(*func)(void),
	unsigned int *stack,
	int stacksize);

/*
 * Close all open file descriptors, including stdin and stdout
 */
void ProcessFileCleanup (struct ProcessControl *p);

/*
 * Notify owner on status change
 */
void ProcessNotifyOnStatusChange (struct ThreadControl *ctc, int terminated);

/*
 * Send a signal to some process
 */
int ProcessSendSignal (int pid, int sig);

/*
 * Handle a signal that was set as ignored
 */
int ProcessHandleIgnoredSignal (struct ProcessControl *p, int sig);

/*
 * Cleanup after crashed process
 */
int ProcessCrashRecover (struct ThreadControl *ctc, unsigned int args);
