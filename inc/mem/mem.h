/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MEM_H
#define MEM_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define MEM_INUSEBIT (1 << 27)
#define MEM_LISTSIZE 128

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
struct MemMpuReturn
{
	unsigned int dataptr;
	unsigned int mpu_rbar;
	unsigned int mpu_rasr;
};

struct MemTableEntry
{
	unsigned int addr;
	unsigned int nextaddr;
};

struct MemBank
{
	unsigned int bankaddress;
	unsigned int banksize;

	unsigned int currentfree;
	unsigned int lowestfree;
	unsigned int lastaddr;

	struct MemTableEntry memlist[MEM_LISTSIZE];
	unsigned char memilist[MEM_LISTSIZE];

	struct MemBank *nextbank;
};

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

/*
 * Initialize a bank "superblock". Do this BEFORE adding it to the memory
 * system.
 */
void MemBankInit (struct MemBank *b, unsigned int address, unsigned int size);

/*
 * Add a bank to the memory system. Note that the order tings are added
 * is important to later "bankno" arguments.
 */
void MemAddBank (struct MemBank *bank);

/*
 * Allocate memory from the given bank.
 */
unsigned int MemAlloc (unsigned int nbytes, unsigned int bankno);

/*
 * MPU Aware allocation from the given bank. The data pointer to use is the one
 * in the mpu structure, while the pointer that must be given to MemFree is the
 * one returned.
 */
unsigned int MemAllocMpu (unsigned int nbytes, unsigned int bankno, struct MemMpuReturn *mpu);

/*
 * Free a block of memory.
 */
int MemFree (unsigned int addr);

/*
 * Destructive test of a memory portion. Memory is divided into 16 blocks,
 * and each block is tested for 512 bytes at a time with 16-bit data.
 *
 * Returns zero if errors are detected.
 */
int MemTest (unsigned int addr, unsigned int nbytes);

#endif
