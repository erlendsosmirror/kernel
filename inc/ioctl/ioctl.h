/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef IOCTL_H
#define IOCTL_H

/*
 * This file defines all ioctl command numbers. All ioctls are defined using a
 * macro just like on linux, in order to make defining unique ioctls easier.
 * Just look below for some examples. The macro assembles a 32-bit integer
 * using a magic byte, the call id and the size of the data argument.
 *
 * The magic byte should be selected for the driver. It can be anything but
 * should be as unique as possible for the driver. The id is a byte-sized
 * integer for use within the driver, you should start with zero. At last
 * the size of the data argument type is included. It helps making the ioctl
 * unique and helps catching errors caused by modifying the size of something
 * unintentionally.
 */

// Definitions for ioctls
#define _IO(magic,id,size)		((0 << 30) | (sizeof(size) << 16) | (magic << 8) | (id << 0))
#define _IOW(magic,id,size)		((1 << 30) | (sizeof(size) << 16) | (magic << 8) | (id << 0))
#define _IOR(magic,id,size)		((2 << 30) | (sizeof(size) << 16) | (magic << 8) | (id << 0))
#define _IORW(magic,id,size)	((3 << 30) | (sizeof(size) << 16) | (magic << 8) | (id << 0))

// Definitions for tty
#define TCGETS		_IOR('t', 0, struct termios)
#define TCSETS		_IOW('t', 1, struct termios)
#define TISATTY		_IO('t', 2, void)
#define TCSBRK		_IO('t', 3, void)
#define TCXONC		_IO('t', 4, void)
#define TCFLSH		_IO('t', 5, void)
#define TCGETPGRP	_IO('t', 6, void)
#define TCSETPGRP	_IO('t', 7, void)

#define TTYSETMODE	_IO('t', 8, int)
#define TTYGETRXBUSY _IO('t', 9, int)

#define TTYSETCBLINK _IO('t', 10, int)

// Definitions for SPI
#define SPIBUSY		_IO('s', 0, void)
#define SPICS		_IOW('s', 1, unsigned int)

// Definitions for SPI FLASH
#define SPIFSETSECT	_IOW('f', 0, unsigned int)
#define SPIFGETSECT	_IOR('f', 1, void)
#define SPIFJEDEC	_IOR('f', 2, void)
#define SPIFGETSECTSIZE _IOR('f', 3, short)
#define SPIFGETSECTCNT _IOR('f', 4, unsigned int)

// Definitions for OWL VHF
#define OWLPTYPE	_IOW('o', 0, unsigned char)
#define OWLSEXT		_IOW('o', 1, unsigned char)
#define OWLCLEAR	_IO('o', 2, void)

// Definitions for sys
#define SYSSHOWREL	_IOW('y', 0, char*)

// Definitions for rtc
#define RTCGETTIME	_IOR('r', 0, int*)
#define RTCSETTIME	_IOW('r', 1, int*)

// Definitions for i2s
#define I2SSTART	_IOW('i', 0, int)
#define I2SSTOP		_IOW('i', 1, void)
#define I2SGETPEAKS	_IOW('i', 2, int)

// General file control
#define F_SETOWN	_IOW('a', 0, int)
#define F_GETFL		_IOW('a', 1, int)
#define F_SETFL		_IOW('a', 2, int)

#endif

