/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef IN_H
#define IN_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "posix/sys/types.h"
#include "posix/sys/socket.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
typedef unsigned short in_port_t;
typedef unsigned int in_addr_t;

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
struct in_addr
{
	in_addr_t s_addr;
};

struct sockaddr_in
{
	sa_family_t sin_family;
	in_port_t sin_port;
	struct in_addr sin_addr;
};

struct in6_addr
{
	unsigned char s6_addr[16];
};

struct sockaddr_in6
{
	sa_family_t sin6_family;
	in_port_t sin6_port;
	unsigned int sin6_flowinfo;
	struct in6_addr sin6_addr;
	unsigned int sin6_scope_id;
};

struct ipv6_mreq
{
	struct in6_addr ipv6mr_multiaddr;
	unsigned int opv6mr_interface;
};

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "posix/arpa/inet.h"

///////////////////////////////////////////////////////////////////////////////
// Externs
///////////////////////////////////////////////////////////////////////////////
extern const struct in6_addr in6addr_any;
extern const struct in6_addr in6addr_loopback;

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Constants (TODO)
#define IN6ADDR_ANY_INIT
#define IN6ADDR_LOOPBACK_INIT

// level argument
#define IPPROTO_IP		1
#define IPPROTO_IPV6	2
#define IPPROTO_ICMP	3
#define IPPROTO_RAW		4
#define IPPROTO_TCP		5
#define IPPROTO_UDP		6

// For passing to bind
#define INADDR_ANY		0

// For destination address passed to connect, sendmsg, sendto
#define INADDR_BROADCAST	1

// Length of ipv4 string
#define INET_ADDRSTRLEN	16

// Length of ipv6 string
#define INET6_ADDRSTRLEN 46

// option_name argument
#define IPV6_JOIN_GROUP
#define IPV6_LEAVE_GROUP
#define IPV6_MULTICAST_HOPS
#define IPV6_MULTICAST_IF
#define IPV6_MULTICAST_LOOP
#define IPV6_UNICAST_HOPS
#define IPV6_V6ONLY

// Macros for testing for special ipv6 addresses
#define IN6_IS_ADDR_UNSPECIFIED
#define IN6_IS_ADDR_LOOPBACK
#define IN6_IS_ADDR_MULTICAST
#define IN6_IS_ADDR_LINKLOCAL
#define IN6_IS_ADDR_SITELOCAL
#define IN6_IS_ADDR_V4MAPPED
#define IN6_IS_ADDR_V4COMPAT
#define IN6_IS_ADDR_MC_NODELOCAL
#define IN6_IS_ADDR_MC_LINKLOCAL
#define IN6_IS_ADDR_MC_SITELOCAL
#define IN6_IS_ADDR_MC_ORGLOCAL
#define IN6_IS_ADDR_MC_GLOBAL

#endif
