/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TYPES_H
#define TYPES_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// For system times in clock ticks
typedef unsigned long clock_t;

// For device IDs
typedef unsigned long dev_t;

// For statvfs
typedef unsigned long fsblkcnt_t;
typedef unsigned long fsfilcnt_t;

// For group IDs
typedef unsigned long gid_t;

// For file serial numbers
typedef unsigned long ino_t;

// Used for file attributes
typedef unsigned long mode_t;

// Used for link counts
typedef unsigned long nlink_t;

// For file sizes
typedef unsigned long off_t;

// Used for process IDS and process group IDs
typedef long pid_t;

// Used for sizes of objects
typedef unsigned long size_t;

// Used for a count of bytes or an error indication
typedef unsigned long ssize_t;

// Used for time in seconds
typedef long time_t;

// Used for time in microseconds
typedef unsigned long useconds_t;
typedef long suseconds_t;

// Used for user IDs
typedef unsigned long uid_t;

// Block size
typedef unsigned long blksize_t;
typedef unsigned long blkcnt_t;

#endif
