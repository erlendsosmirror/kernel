/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SOCKET_H
#define SOCKET_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "posix/sys/types.h"
//#include "sys/uio.h"		// for iovec later on

///////////////////////////////////////////////////////////////////////////////
// Typedefs
///////////////////////////////////////////////////////////////////////////////
typedef unsigned int socklen_t;
typedef unsigned int sa_family_t;

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
struct sockaddr
{
	sa_family_t sa_family;
	char sa_data[12];
};

struct sockaddr_storage
{
	sa_family_t sa_family;
	char sa_data[12+16];
};

struct msghdr
{
	void *msg_name;
	socklen_t msg_namelen;
	struct iovec *msg_iov;
	int msg_iovlen;
	void *msg_control;
	socklen_t msg_controllen;
	int msg_flags;
};

struct cmsghdr
{
	socklen_t cmsg_len;
	int cmsg_level;
	int cmsg_type;
};

struct linger
{
	int l_onoff;
	int l_linger;
};

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Bleh
#define SCM_RIGHTS 0
#define CMSG_DATA(cmsg) 0
#define CMSG_NXTHDR(cmsg) 0
#define CMSG_FIRSTHDR(mhdr) 0

// Socket types
#define SOCK_DGRAM		0	// Datagram socket
#define SOCK_RAW		1	// Raw protocol ingerface
#define SOCK_SEQPACKET	2	// Sequenced-packet socket
#define SOCK_STREAM		3	// Byte-stream socket

// Level argument for setsockopt and getsockopt
#define SOL_SOCKET		1

// option_name argment for getsockopt and setsockopt
#define SO_ACCEPTCONN	1
#define SO_BROADCAST	2
#define SO_DEBUG		3
#define SO_DONTROUTE	4
#define SO_ERROR		5
#define SO_KEEPALIVE	6
#define SO_LINGER		7
#define SO_OOBINLINE	8
#define SO_RCVBUF		9
#define SO_RCVLOWAT		10
#define SO_RCVTIMEO		11
#define SO_REUSEADDR	12
#define SO_SNDBUF		13
#define SO_SNDLOWAT		14
#define SO_SNDTIMEO		15
#define SO_TYPE			16

// Maximum backlog queue length
#define SOMAXCONN		1

// msg_flags in msghdr or flags in function calls
#define MSG_CTRUNC		1
#define MSG_DONTROUTE	2
#define MSG_EOR			3
#define MSG_OOB			4
#define MSG_NOSIGNAL	5
#define MSG_PEEK		6
#define MSG_TRUNC		7
#define MSG_WAITALL		8

// Net types
#define AF_INET			4
#define AF_INET6		6
#define AF_UNIX			1
#define AF_UNSPEC		0

// Other
#define SHUT_RD			1
#define SHUT_RDWR		3
#define SHUT_WR			2

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int accept (int, struct sockaddr *restrict, socklen_t *restrict);
int bind (int, const struct sockaddr *, socklen_t);
int connect (int, const struct sockaddr *, socklen_t);
int getpeername (int, struct sockaddr *restrict, socklen_t *restrict);
int getsockname (int, struct sockaddr *restrict, socklen_t *restrict);
int getsockopt (int, int, int, void *restrict, socklen_t *restrict);
int listen (int, int);
ssize_t recv (int, void *, size_t, int);
ssize_t recvfrom (int, void *restrict, size_t, int, struct sockaddr *restrict, socklen_t *restrict);
ssize_t recvmsg (int, struct msghdr *, int);
ssize_t send (int, const void *, size_t, int);
ssize_t sendmsg (int, const struct msghdr *, int);
ssize_t sendto (int, const void *, size_t, int, const struct sockaddr *, socklen_t);
int setsockopt (int, int, int, const void*, socklen_t);
int shutdown (int, int);
int sockatmark (int);
int socket (int, int, int);
int socketpair (int, int, int, int[2]);

#endif
