/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STAT_H
#define STAT_H

// http://pubs.opengroup.org/onlinepubs/009695399/basedefs/sys/stat.h.html

#include "posix/sys/types.h"

struct stat
{
	dev_t st_dev;
	ino_t st_ino;
	mode_t st_mode;
	nlink_t st_nlink;
	uid_t st_uid;
	gid_t st_gid;
	dev_t st_rdev;
	off_t st_size;
	time_t st_atime;
	time_t st_mtime;
	time_t st_ctime;
	blksize_t st_blksize;
	blkcnt_t st_blocks;
};

#define S_IFMT (0x0F << 16)
#define S_IFBLK (1 << 16)
#define S_IFCHR (2 << 16)
#define S_IFIFO (3 << 16)
#define S_IFREG (4 << 16)
#define S_IFDIR (5 << 16)
#define S_IFLNK (6 << 16)
#define S_IFSOCK (7 << 16)

#define S_IRWXU (S_IRUSR | S_IWUSR | S_IXUSR)
#define S_IRUSR (1 << 8)
#define S_IWUSR (1 << 7)
#define S_IXUSR (1 << 6)

#define S_IRWXG (S_IRGRP | S_IWGRP | S_IXGRP)
#define S_IRGRP (1 << 5)
#define S_IWGRP (1 << 4)
#define S_IXGRP (1 << 3)

#define S_IRWXO (S_IROTH | S_IWOTH | S_IXOTH)
#define S_IROTH (1 << 2)
#define S_IWOTH (1 << 1)
#define S_IXOTH (1 << 0)

#define S_ISUID (1 << 9)
#define S_ISGID (1 << 10)
#define ISVTX (1 << 11)

#define S_ISBLK(m) 0
#define S_ISCHR(m) 0
#define S_ISDIR(m) 0
#define S_ISFIFO(m) 0
#define S_ISREG(m) 0
#define S_ISLNK(m) 0
#define S_ISSOCK(m) 0

#define S_TYPEISMQ(buf) 0
#define S_TYPEISSEM(buf) 0
#define S_TYPEISSHM(buf) 0

#endif
