/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SDIO_H
#define SDIO_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "drivers/dma/dma-stm32f407.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Hardware
#define SDIO_CMD_CPSMEN (1 << 10)
#define SDIO_CMD_WAITRESP_NONE (0 << 6)
#define SDIO_CMD_WAITRESP_SHORT (1 << 6)
#define SDIO_CMD_WAITRESP_LONG (3 << 6)

#define SDIO_STA_CCRCFAIL (1 << 0)
#define SDIO_STA_DCRCFAIL (1 << 1)
#define SDIO_STA_CTIMEOUT (1 << 2)
#define SDIO_STA_DTIMEOUT (1 << 3)
#define SDIO_STA_TXUNDERR (1 << 4)
#define SDIO_STA_RXOVERR (1 << 5)
#define SDIO_STA_CMDREND (1 << 6)
#define SDIO_STA_CMDSENT (1 << 7)
#define SDIO_STA_DATAEND (1 << 8)
#define SDIO_STA_STBITERR (1 << 9)
#define SDIO_STA_TXACT (1 << 12)
#define SDIO_STA_RXACT (1 << 13)

///////////////////////////////////////////////////////////////////////////////
// Registers
///////////////////////////////////////////////////////////////////////////////
typedef struct _SDIO
{
	volatile unsigned int POWER;
	volatile unsigned int CLKCR;
	volatile unsigned int ARG;
	volatile unsigned int CMD;
	volatile unsigned int RESPCMD;
	volatile unsigned int RESP1;
	volatile unsigned int RESP2;
	volatile unsigned int RESP3;
	volatile unsigned int RESP4;
	volatile unsigned int DTIMER;
	volatile unsigned int DLEN;
	volatile unsigned int DCTRL;
	volatile unsigned int DCOUNT;
	volatile unsigned int STA;
	volatile unsigned int ICR;
	volatile unsigned int MASK;
	volatile unsigned int reserved00;
	volatile unsigned int reserved01;
	volatile unsigned int FIFOCNT;
	volatile unsigned int reserved02; // 0x4C
	volatile unsigned int reserved03; // 0x50
	volatile unsigned int reserved04;
	volatile unsigned int reserved05;
	volatile unsigned int reserved06;
	volatile unsigned int reserved07; // 0x60
	volatile unsigned int reserved08;
	volatile unsigned int reserved09;
	volatile unsigned int reserved10;
	volatile unsigned int reserved11; // 0x70
	volatile unsigned int reserved12;
	volatile unsigned int reserved13;
	volatile unsigned int reserved14;
	volatile unsigned int FIFO; // 0x80
} _SDIO;

#define SDIO ((_SDIO*) 0x40012C00)

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct sdio
{
	// Pointer to hardware registers
	_SDIO *instance;
	DMAHandle dmatx;
	DMAHandle dmarx;

	// Current address
	unsigned int address;

	// Transfer state, flags from STA
	volatile unsigned int transferstate;

	// Card ID data
	unsigned int cid[4];
	unsigned int csd[4];
	unsigned int rca;
} sdio;

#endif
