/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef GPIO_H
#define GPIO_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define GPIO_INPUT GPIO_CNF_IN_FLOATING | GPIO_MODE_IN
#define GPIO_OUTPUT GPIO_CNF_OUT_GENERAL_PUSHPULL | GPIO_MODE_OUT_SPEED_HIGH
#define GPIO_AF GPIO_CNF_OUT_AF_PUSHPULL | GPIO_MODE_OUT_SPEED_HIGH
#define GPIO_ANALOG GPIO_CNF_IN_ANALOG | GPIO_MODE_IN

#define GPIO_CNF_IN_ANALOG (0 << 2)
#define GPIO_CNF_IN_FLOATING (1 << 2)
#define GPIO_CNF_IN_PULL (2 << 2)
#define GPIO_CNF_OUT_GENERAL_PUSHPULL (0 << 2)
#define GPIO_CNF_OUT_GENERAL_OPENDRAIN (1 << 2)
#define GPIO_CNF_OUT_AF_PUSHPULL (2 << 2)
#define GPIO_CNF_OUT_AF_OPENDRAIN (3 << 2)

#define GPIO_MODE_IN (0 << 0)
#define GPIO_MODE_OUT_SPEED_LOW (2 << 0)
#define GPIO_MODE_OUT_SPEED_MEDIUM (1 << 0)
#define GPIO_MODE_OUT_SPEED_HIGH (3 << 0)

#define PIN(num) (1 << num)

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct GPIO
{
	volatile unsigned int CRL;
	volatile unsigned int CRH;
	volatile unsigned int IDR;
	volatile unsigned int ODR;
	volatile unsigned int BSRR;
	volatile unsigned int BRR;
	volatile unsigned int LCKR;
} GPIO;

///////////////////////////////////////////////////////////////////////////////
// Registers
///////////////////////////////////////////////////////////////////////////////
#define GPIOA ((GPIO*) 0x40010800)
#define GPIOB ((GPIO*) 0x40010C00)
#define GPIOC ((GPIO*) 0x40011000)
#define GPIOD ((GPIO*) 0x40011400)
#define GPIOE ((GPIO*) 0x40011800)
#define GPIOF ((GPIO*) 0x40011C00)
#define GPIOG ((GPIO*) 0x40012000)

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void GPIOInit (GPIO *gpio, int pins, int mode);

#endif
