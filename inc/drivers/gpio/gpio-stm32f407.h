/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef GPIO_H
#define GPIO_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define GPIO_INPUT (0 << 0)
#define GPIO_OUTPUT (1 << 0)
#define GPIO_AF (2 << 0)
#define GPIO_ANALOG (3 << 0)

#define GPIO_PUSHPULL (0 << 4)
#define GPIO_OPENDRAIN (1 << 4)

#define GPIO_SPEED_LOW (0 << 8)
#define GPIO_SPEED_MEDIUM (1 << 8)
#define GPIO_SPEED_HIGH (2 << 8)
#define GPIO_SPEED_VERYHIGH (3 << 8)

#define GPIO_NOPULL (0 << 12)
#define GPIO_PULLUP (1 << 12)
#define GPIO_PULLDOWN (2 << 12)
#define GPIO_PULLBOTH (3 << 12)

#define GPIO_ALTERNATE(af) (af << 16)

#define PIN(num) (1 << num)

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct GPIO
{
	volatile unsigned int MODER;
	volatile unsigned int OTYPER;
	volatile unsigned int OSPEEDR;
	volatile unsigned int PUPDR;
	volatile unsigned int IDR;
	volatile unsigned int ODR;
	volatile unsigned int BSRR;
	volatile unsigned int LCKR;
	volatile unsigned int AFRL;
	volatile unsigned int AFRH;
} GPIO;

///////////////////////////////////////////////////////////////////////////////
// Registers
///////////////////////////////////////////////////////////////////////////////
#define GPIOA ((GPIO*) 0x40020000)
#define GPIOB ((GPIO*) 0x40020400)
#define GPIOC ((GPIO*) 0x40020800)
#define GPIOD ((GPIO*) 0x40020C00)
#define GPIOE ((GPIO*) 0x40021000)
#define GPIOF ((GPIO*) 0x40021400)
#define GPIOG ((GPIO*) 0x40021800)
#define GPIOH ((GPIO*) 0x40021C00)
#define GPIOI ((GPIO*) 0x40022000)
#define GPIOJ ((GPIO*) 0x40022400)
#define GPIOK ((GPIO*) 0x40022800)

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void GPIOInit (GPIO *gpio, int pins, int mode);

#endif
