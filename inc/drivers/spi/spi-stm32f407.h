/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SPI_H
#define SPI_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "drivers/dma/dma-stm32f407.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Hardware
#define CR1_MSTR (1 << 2)
#define CR1_BRDIV2 (0 << 3)
#define CR1_BRDIV4 (1 << 3)
#define CR1_BRDIV8 (2 << 3)
#define CR1_BRDIV16 (3 << 3)
#define CR1_BRDIV32 (4 << 3)
#define CR1_BRDIV64 (5 << 3)
#define CR1_BRDIV128 (6 << 3)
#define CR1_BRDIV256 (7 << 3)
#define CR1_SPE (1 << 6)
#define CR1_SSI (1 << 8)
#define CR1_SSM (1 << 9)

///////////////////////////////////////////////////////////////////////////////
// Registers
///////////////////////////////////////////////////////////////////////////////
typedef struct _SPI
{
	volatile unsigned int CR1;
	volatile unsigned int CR2;
	volatile unsigned int SR;
	volatile unsigned int DR;
	volatile unsigned int CRCPR;
	volatile unsigned int RXCRCR;
	volatile unsigned int TXCRCR;
	volatile unsigned int I2SCFGR;
	volatile unsigned int I2SPR;
} _SPI;

#define SPI1 ((_SPI*) 0x40013000)
#define SPI2 ((_SPI*) 0x40003800)
#define SPI3 ((_SPI*) 0x40003C00)
#define SPI4 ((_SPI*) 0x40013400)
#define SPI5 ((_SPI*) 0x40015000)
#define SPI6 ((_SPI*) 0x40015400)

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct spi
{
	// Pointer to hardware registers
	_SPI *instance;
	DMAHandle dmatx;
	DMAHandle dmarx;

	// The index number of this instance
	unsigned int spinum;

	// If the spi is busy transferring data or not
	unsigned int busystate;
} spi;

#endif
