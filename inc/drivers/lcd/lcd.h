/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LCD_H
#define LCD_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define LCD_VERTICAL 0
#define LCD_HORIZONTAL 1

#define TFT_BLACK 0x0000
#define TFT_WHITE 0xFFFF

#define TFT_RED 0xF800
#define TFT_GREEN 0x07E0
#define TFT_BLUE 0x001F

#define TFT_YELLOW 0xFFE0
#define TFT_MAGENTA 0xF81F
#define TFT_CYAN 0x07FF

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

// LCD Specific functions
void LCDInit (void);
void LCDRotate (int direction);
void LCDSetAddress (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2);

// Helper function, does not interface LCD
unsigned int LCDColor (unsigned int r, unsigned int g, unsigned int b);

// Generic draw functions
void LCDText (char *str, unsigned int x, unsigned int y, int fontsize, unsigned int fcolor, unsigned int bcolor);
void LCDChar (char c, unsigned int x, unsigned int y, int fontsize, unsigned int fcolor, unsigned int bcolor);
void LCDRectangle (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int color);
void LCDVLine (unsigned int y1, unsigned int y2, unsigned int x, unsigned int color);
void LCDHLine (unsigned int x1, unsigned int x2, unsigned int y, unsigned int color);
void LCDLine (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int color);
void LCDDot (unsigned int x, unsigned int y, unsigned int color);
void LCDBox (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int color);
void LCDFill (unsigned int color);
void LCDData (unsigned short *data, int n);
void LCDGetMetrics (unsigned int *w, unsigned int *h);
int LCDGetOrientation (void);

#endif
