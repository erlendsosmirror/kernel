/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TTY_H
#define TTY_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "../config/live/drivers/tty.h"

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct tty
{
	// Function pointers
	int (*read)(struct tty *t, void*, unsigned int);
	int (*write)(struct tty *t, void*, unsigned int);
	void (*irq)(struct tty *t);

	// Pointer to hardware registers
	_UART *instance;
	DMAHandle dmatx;
	DMAHandle dmarx;

	// The index number of this instance
	unsigned int ttynum;

	// Configuration of the UART
	unsigned int stopbits;
	unsigned int word_parity_mode_sampling;
	unsigned int flowcontrol;
	unsigned int baudrate;

	// If the tty is busy transferring data or not
	unsigned int busystate;

	// Read position
	unsigned int readpos;

	// Internal transfer buffers used for DMA
	unsigned char defrxbuf[TTY_BUF_SIZE];
	unsigned char deftxbuf[TTY_BUF_SIZE];
	unsigned char *rxbuf;
	unsigned int rxbufsize;

	// Position in the read buffer
	short pos_old;
	short irq_pos_old;

	// Process group id used for signals
	short pgid;

	// Error state and mode
	unsigned char errflag;
	unsigned char mode;
} tty;

struct termios
{
	// Standard stuff for the user
	unsigned int c_iflag;		// Input modes
	unsigned int c_oflag;		// Output modes
	unsigned int c_cflag;		// Control modes
	unsigned int c_lflag;		// Local modes
	unsigned char c_cc[NCCS];	// Special characters

	// Non-standard stuff
	unsigned int baudrate;
};

#endif
