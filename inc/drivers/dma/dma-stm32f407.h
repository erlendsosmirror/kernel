/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DMA_H
#define DMA_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define DMA_FEIF (1 << 0)
#define DMA_DMEIF (1 << 2)
#define DMA_TEIF (1 << 3)
#define DMA_HTIF (1 << 4)
#define DMA_TCIF (1 << 5)

#define DMA_CR_EN (1 << 0)
#define DMA_CR_DMEIE (1 << 1)
#define DMA_CR_TEIE (1 << 2)
#define DMA_CR_HTIE (1 << 3)
#define DMA_CR_TCIE (1 << 4)
#define DMA_CR_PFCTRL (1 << 5)
#define DMA_CR_DIR (3 << 6)
#define DMA_CR_CIRC (1 << 8)
#define DMA_CR_PINC (1 << 9)
#define DMA_CR_MINC (1 << 10)
#define DMA_CR_PSIZE (3 << 11)
#define DMA_CR_MSIZE (3 << 13)
#define DMA_CR_PINCOS (1 << 15)
#define DMA_CR_PL (3 << 16)
#define DMA_CR_DBM (1 << 18)
#define DMA_CR_CT (1 << 19)
#define DMA_CR_PBURST (3 << 21)
#define DMA_CR_MBURST (3 << 23)
//#define DMA_CR_CHSEL (7 << 25)

#define DMA_CR_DIR_P2M (0 << 6)
#define DMA_CR_DIR_M2P (1 << 6)
#define DMA_CR_DIR_M2M (2 << 6)

#define DMA_CR_PSIZE_8 (0 << 11)
#define DMA_CR_PSIZE_16 (1 << 11)
#define DMA_CR_PSIZE_32 (2 << 11)

#define DMA_CR_MSIZE_8 (0 << 13)
#define DMA_CR_MSIZE_16 (1 << 13)
#define DMA_CR_MSIZE_32 (3 << 13)

#define DMA_CR_PBURST0 (0 << 21)
#define DMA_CR_PBURST4 (1 << 21)
#define DMA_CR_PBURST8 (2 << 21)
#define DMA_CR_PBURST16 (3 << 21)

#define DMA_CR_MBURST0 (0 << 23)
#define DMA_CR_MBURST4 (1 << 23)
#define DMA_CR_MBURST8 (2 << 23)
#define DMA_CR_MBURST16 (3 << 23)

#define DMA_CR_CHSEL(i) (i << 25)
#define DMA_CR_CHSEL0 (0 << 25)
#define DMA_CR_CHSEL1 (1 << 25)
#define DMA_CR_CHSEL2 (2 << 25)
#define DMA_CR_CHSEL3 (3 << 25)
#define DMA_CR_CHSEL4 (4 << 25)
#define DMA_CR_CHSEL5 (5 << 25)
#define DMA_CR_CHSEL6 (6 << 25)
#define DMA_CR_CHSEL7 (7 << 25)

#define DMA_FCR_FTH_QUARTER (0 << 0)
#define DMA_FCR_FTH_HALF (1 << 0)
#define DMA_FCR_FTH_3QUARTER (2 << 0)
#define DMA_FCR_FTH_FULL (3 << 0)
#define DMA_FCR_DMDIS (1 << 2)
#define DMA_FCR_FEIE (1 << 7)

#define DMA_HAVE_FCR_REG 1

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct DMAChannel
{
	volatile unsigned int CR;
	volatile unsigned int NDTR;
	volatile unsigned int PAR;
	volatile unsigned int M0AR;
	volatile unsigned int M1AR;
	volatile unsigned int FCR;
} DMAChannel;

typedef struct DMA
{
	volatile unsigned int ISR;
	volatile unsigned int reserved;
	volatile unsigned int IFCR;
} DMA;

typedef struct DMAHandle
{
	DMAChannel *chinst;
	DMA *dma;
	int busystate;
	unsigned int config;
	unsigned int fconfig;
	char shift;
} DMAHandle;

///////////////////////////////////////////////////////////////////////////////
// Registers
///////////////////////////////////////////////////////////////////////////////
#define DMAChannel_1_0 ((DMAChannel*) (0x40026000 + 0x0010))
#define DMAChannel_1_1 ((DMAChannel*) (0x40026000 + 0x0028))
#define DMAChannel_1_2 ((DMAChannel*) (0x40026000 + 0x0040))
#define DMAChannel_1_3 ((DMAChannel*) (0x40026000 + 0x0058))
#define DMAChannel_1_4 ((DMAChannel*) (0x40026000 + 0x0070))
#define DMAChannel_1_5 ((DMAChannel*) (0x40026000 + 0x0088))
#define DMAChannel_1_6 ((DMAChannel*) (0x40026000 + 0x00A0))
#define DMAChannel_1_7 ((DMAChannel*) (0x40026000 + 0x00B8))

#define DMAChannel_2_0 ((DMAChannel*) (0x40026400 + 0x0010))
#define DMAChannel_2_1 ((DMAChannel*) (0x40026400 + 0x0028))
#define DMAChannel_2_2 ((DMAChannel*) (0x40026400 + 0x0040))
#define DMAChannel_2_3 ((DMAChannel*) (0x40026400 + 0x0058))
#define DMAChannel_2_4 ((DMAChannel*) (0x40026400 + 0x0070))
#define DMAChannel_2_5 ((DMAChannel*) (0x40026400 + 0x0088))
#define DMAChannel_2_6 ((DMAChannel*) (0x40026400 + 0x00A0))
#define DMAChannel_2_7 ((DMAChannel*) (0x40026400 + 0x00B8))

///////////////////////////////////////////////////////////////////////////////
// Inline functions
///////////////////////////////////////////////////////////////////////////////
inline void DMAEnable (DMAHandle *dma)
{
	dma->chinst->CR |= DMA_CR_EN;
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void DMAInit (DMAHandle *hdma);
void DMAStart (DMAHandle *hdma, void *src, void *dst, unsigned int nbytes);
void DMAIRQHandler (DMAHandle *hdma);

#endif
