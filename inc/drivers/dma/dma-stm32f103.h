/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DMA_H
#define DMA_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define DMA_TCIF (1 << 1)
#define DMA_HTIF (1 << 2)
#define DMA_TEIF (1 << 3)
#define DMA_DMEIF 0
#define DMA_FEIF 0

#define DMA_CR_EN (1 << 0)
#define DMA_CR_TCIE (1 << 1)
#define DMA_CR_HTIE (1 << 2)
#define DMA_CR_TEIE (1 << 3)
#define DMA_CR_DMEIE 0
#define DMA_CR_DIR (3 << 4)
#define DMA_CR_CIRC (1 << 5)
#define DMA_CR_PINC (1 << 6)
#define DMA_CR_MINC (1 << 7)
#define DMA_CR_PSIZE (3 << 8)
#define DMA_CR_MSIZE (3 << 10)
#define DMA_CR_PL (3 << 12)
#define DMA_CR_MEM2MEM (1 << 14)

#define DMA_CR_DIR_P2M (0 << 4)
#define DMA_CR_DIR_M2P (1 << 4)

#define DMA_CR_PSIZE_8 (0 << 8)
#define DMA_CR_PSIZE_16 (1 << 8)
#define DMA_CR_PSIZE_32 (2 << 8)

#define DMA_CR_MSIZE_8 (0 << 10)
#define DMA_CR_MSIZE_16 (1 << 10)
#define DMA_CR_MSIZE_32 (3 << 10)

#define DMA_HAVE_FCR_REG 0

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct DMAChannel
{
	volatile unsigned int CR;
	volatile unsigned int NDTR;
	volatile unsigned int PAR;
	volatile unsigned int M0AR;
} DMAChannel;

typedef struct DMA
{
	volatile unsigned int ISR;
	volatile unsigned int IFCR;
} DMA;

typedef struct DMAHandle
{
	DMAChannel *chinst;
	DMA *dma;
	int busystate;
	unsigned int config;
	char shift;
} DMAHandle;

///////////////////////////////////////////////////////////////////////////////
// Registers
///////////////////////////////////////////////////////////////////////////////
#define DMAChannel_1_1 ((DMAChannel*) (0x40020000 + 0x0008))
#define DMAChannel_1_2 ((DMAChannel*) (0x40020000 + 0x001C))
#define DMAChannel_1_3 ((DMAChannel*) (0x40020000 + 0x0030))
#define DMAChannel_1_4 ((DMAChannel*) (0x40020000 + 0x0044))
#define DMAChannel_1_5 ((DMAChannel*) (0x40020000 + 0x0058))
#define DMAChannel_1_6 ((DMAChannel*) (0x40020000 + 0x006C))
#define DMAChannel_1_7 ((DMAChannel*) (0x40020000 + 0x0080))

#define DMAChannel_2_1 ((DMAChannel*) (0x40020400 + 0x0008))
#define DMAChannel_2_2 ((DMAChannel*) (0x40020400 + 0x001C))
#define DMAChannel_2_3 ((DMAChannel*) (0x40020400 + 0x0030))
#define DMAChannel_2_4 ((DMAChannel*) (0x40020400 + 0x0044))
#define DMAChannel_2_5 ((DMAChannel*) (0x40020400 + 0x0058))
#define DMAChannel_2_6 ((DMAChannel*) (0x40020400 + 0x006C))
#define DMAChannel_2_7 ((DMAChannel*) (0x40020400 + 0x0080))

///////////////////////////////////////////////////////////////////////////////
// Inline functions
///////////////////////////////////////////////////////////////////////////////
inline void DMAEnable (DMAHandle *dma)
{
	dma->chinst->CR |= DMA_CR_EN;
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void DMAInit (DMAHandle *hdma);
void DMAStart (DMAHandle *hdma, void *src, void *dst, unsigned int nbytes);
void DMAIRQHandler (DMAHandle *hdma);

#endif
