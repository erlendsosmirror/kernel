/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ELF_H
#define ELF_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Error values returned by loader functions
#define EELFLDR_OK 0
#define EELFLDR_SEEK 1
#define EELFLDR_READ 2
#define EELFLDR_MAGIC 3
#define EELFLDR_VERSION 4
#define EELFLDR_UNDEFEXTERNAL 5
#define EELFLDR_UNDEFRELOC 6
#define EELFLDR_WOULDOVERFLOW 7

// Options
#define OELFLDR_PHEADER (1 << 0)
#define OELFLDR_SHEADER (1 << 1)
#define OELFLDR_DUMP (1 << 2)
#define OELFLDR_RDUMP (1 << 3)
#define OELFLDR_PMEMUSE (1 << 4)

// Other
#define SELFLDR_NAMES_SIZE 128

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////

/*
 * These first two structs corresponds to the data in the file. Since we do
 * not need all of them, some are defined as "ignored" to make sure we do not
 * use them.
 *
 * Check Wikipedia for more in-depth explaination, and the output of readelf.
 */

struct ElfHeader
{
	char magic[4];				// 0x7F + "ELF"
	char versionpart[5+7+8];	// Members that are only checked for "sameness"
	unsigned int entrypoint;	// Program entry
	unsigned int ignored1;
	unsigned int shoff;			// Start of the section header table
	unsigned int ignored2;
	unsigned short ignored3;
	unsigned short ignored4;
	unsigned short ignored5;
	unsigned short ignored6;
	unsigned short shnum;		// Number of section headers
	unsigned short shstrndx;	// Section header table entry for the names
};

struct ElfSection
{
	unsigned int nameoffset;	// Index into shstrtab
	unsigned int ignored1;
	unsigned int ignored2;
	unsigned int ignored3;
	unsigned int offset;		// Offset in the elf file
	unsigned int size;			// Size in the elf file
	unsigned int ignored4;
	unsigned int ignored5;
	unsigned int ignored6;
	unsigned int ignored7;
};

/*
 * The following are the structures used by the loader itself.
 */

struct LoaderSection
{
	// Index in the section header table for reference from symbol table
	int index;

	// Things kept from the elf section header
	unsigned int offset;		// Offset in the elf file
	unsigned int size;			// Size in the elf file
};

struct Loader
{
	// Things kept from the elf header
	unsigned int entrypoint;	// Program entry
	unsigned int shoff;			// Start of the section header table
	unsigned short shnum;		// Number of section headers
	unsigned short shstrndx;	// Section header table entry for the names

	// File descriptor
	int fd;

	// Offset into the elf file for the section names
	unsigned int shnames_offset;

	// Copy of the names. Size should not be a problem since the files will
	// have few sections.
	char names[SELFLDR_NAMES_SIZE];

	// Section metainfo
	struct LoaderSection text;
	struct LoaderSection rodata;
	struct LoaderSection data;
	struct LoaderSection bss;
	struct LoaderSection rel_text;
	struct LoaderSection rel_rodata;
	struct LoaderSection rel_data;
	struct LoaderSection rel_bss;
	struct LoaderSection symbols;
	struct LoaderSection strings;
};

struct Program
{
	// Process dummy
	unsigned int *text;
	unsigned int *rodata;
	unsigned int *data;
	unsigned int *bss;
};

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

// elf.c
int CheckHeader (struct Loader *l);
int LoadSectionHeader (struct Loader *l, struct ElfSection *sh, int index);
int LoadSectionData (struct Loader *l, unsigned int offset, unsigned int size, void *target);
int FindSizes (struct Loader *l);
int LoadData (struct Loader *l, struct Program *p);

// elf-reloc.c
int Relocate (int fd, struct Loader *l, struct Program *p);

#endif
