/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "arch/stm32/rcc.h"

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct _RCC
{
	volatile unsigned int CR;
	volatile unsigned int PLLCFGR;
	volatile unsigned int CFGR;
	volatile unsigned int CIR;
	volatile unsigned int AHB1RSTR;
	volatile unsigned int AHB2RSTR;
	volatile unsigned int AHB3RSTR;
	volatile unsigned int reserved1;
	volatile unsigned int APB1RSTR;
	volatile unsigned int APB2RSTR;
	volatile unsigned int reserved2;
	volatile unsigned int reserved3;
	volatile unsigned int AHB1ENR;
	volatile unsigned int AHB2ENR;
	volatile unsigned int AHB3ENR;
	volatile unsigned int reserved4;
	volatile unsigned int APB1ENR;
	volatile unsigned int APB2ENR;
	volatile unsigned int reserved5;
	volatile unsigned int reserved6;
	volatile unsigned int AHB1LPENR;
	volatile unsigned int AHB2LPENR;
	volatile unsigned int AHB3LPENR;
	volatile unsigned int reserved7;
	volatile unsigned int APB1LPENR;
	volatile unsigned int APB2LPENR;
	volatile unsigned int reserved8;
	volatile unsigned int reserved9;
	volatile unsigned int BDCR;
	volatile unsigned int CSR;
	volatile unsigned int reserved10;
	volatile unsigned int reserved11;
	volatile unsigned int SSCGR;
	volatile unsigned int PLLI2SCFGR;
	volatile unsigned int PLLSAICFGR;
	volatile unsigned int DCKCFGR;
} _RCC;

typedef struct _SRAMSection
{
	volatile unsigned int BCR;
	volatile unsigned int BTR;
} _SRAMSection;

typedef struct _SRAM
{
	_SRAMSection bank[4];
} _SRAM;

typedef struct exti
{
	volatile unsigned int IMR;
	volatile unsigned int EMR;
	volatile unsigned int RTSR;
	volatile unsigned int FTSR;
	volatile unsigned int SWIER;
	volatile unsigned int PR;
} exti;

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define RCC ((_RCC*) 0x40023800)
#define FLASH ((_FLASH*) 0x40023C00)
#define FSMC ((_SRAM*) 0xA0000000)
#define EXTI ((exti*) 0x40013C00)
#define SYSCFG ((syscfg*) 0x40013800)
