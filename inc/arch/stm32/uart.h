/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef UART_STM32_H
#define UART_STM32_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Hardware
#define OVERSAMPLING_16 (0 << 15)
#define OVERSAMPLING_8 (1 << 15)
#define MODE_TXRX (3 << 2)
#define CR1_RXNEIE (1 << 5)
#define CR1_UE (1 << 13)

#define CR3_EIE (1 << 0)
#define CR3_IREN (1 << 1)
#define CR3_IRLP (1 << 2)
#define CR3_HDSEL (1 << 3)
#define CR3_NACK (1 << 4)
#define CR3_SCEN (1 << 5)
#define CR3_DMAR (1 << 6)
#define CR3_DMAT (1 << 7)
#define CR3_RTSE (1 << 8)
#define CR3_CTSE (1 << 9)
#define CR3_CTSIE (1 << 10)
#define CR3_ONEBIT (1 << 11)

// Default tty buffer size
#define TTY_BUF_SIZE 128
#define TTY_BUF_MASK 0x007F

// Number of special characters
#define NCCS 32

///////////////////////////////////////////////////////////////////////////////
// Registers
///////////////////////////////////////////////////////////////////////////////
typedef struct _UART
{
	volatile unsigned int SR;
	volatile unsigned int DR;
	volatile unsigned int BRR;
	volatile unsigned int CR1;
	volatile unsigned int CR2;
	volatile unsigned int CR3;
	volatile unsigned int GTPR;
} _UART;

#endif
