/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef THREADCONTROL_H
#define THREADCONTROL_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sched/list.h"

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
struct ThreadControl
{
	// Pointer to current top of stack, must be first
	unsigned int *stacktop;

	// Privileged or not, set to 1 for unprivileged, must be second
	unsigned int privileged;

	// MPU data, up to 4 regions, must be third
	unsigned int mpuregs[8];

	// List entry for scheduler lists
	// Note that the parent pointer points back here, not to parent process
	struct ListItem listentry;

	// Program entry point and callback for deferred operations
	union {
		void (*entrypoint)(void);
		int (*callback)(struct ThreadControl*, unsigned int);
	};

	// Parameters initially used for startup. Later reused by signals.
	// In case of a crash, they are reused for hfsr, cfsr and bfar.
	unsigned int param1;
	unsigned int param2;
	unsigned int param3;

	// Priority (see definitions above)
	// In case of a crash, priority is reused and set to 4
	unsigned char priority;

	// Current state of the process, see SCHED_STATUS definitions above
	unsigned char state;
	unsigned char statechanged;

	// Used by SchedSelectNext to either enter (2) or leave (1) the signal handler
	unsigned char sigstate;

	// Signal mask, for enabling signals. 0 is the default ignore action.
	// In case of a crash, sigmask is reused as mmar
	unsigned int sigmask;

	// Signal pending bits
	// In case of a crash, sigpending is reused for faulttype
	unsigned int sigpending;

	// Signal handler and signal trampoline function pointers
	void (*sigh)(int);
	void (*sigt)(int);

	// Copy of stacktop used when leaving a signal handler
	unsigned int *stackstored;

	// Pointer to the ProcessControl structure, containing variables used by
	// the rest of the system but not the scheduler. Keeping it separate allows
	// for lightweight use of the scheduler, possibly threads later on.
	struct ProcessControl *pctrl;

	// TODO: Priority inversion, time counter
};

#endif
