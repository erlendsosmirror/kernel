/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCHEDULER_H
#define SCHEDULER_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sched/threadcontrol.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define SCHED_NPRIORITIES 3
#define SCHED_PRIORITY_LOW 2
#define SCHED_PRIORITY_MEDIUM 1
#define SCHED_PRIORITY_HIGH 0

#define SCHED_STATUS_RUN		0
#define SCHED_STATUS_BLOCKED	1
#define SCHED_STATUS_ZOMBIE		2
#define SCHED_STATUS_SLEEPING	4
#define SCHED_STATUS_STOPPED	8
#define SCHED_STATUS_CRASHED	15

///////////////////////////////////////////////////////////////////////////////
// Exports
///////////////////////////////////////////////////////////////////////////////
extern struct ThreadControl *currentthc;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

/*
 * Initialize the scheduler and its lists. Only to be called once on kernel
 * startup.
 */
void SchedInitialize (void);

/*
 * Start the scheduler. This does not return.
 */
void SchedStart (void);

/*
 * Platform specific critical section start. May disable interrupts completely
 * or raise the base priority.
 */
void SchedEnterCritical (void);

/*
 * Exit a critical section.
 */
void SchedExitCritical (void);

/*
 * Caller has to allocate ThreadControl and stack space. The new task is put
 * on the run list, ready to execute. Privilege level has to be set before
 * calling this.
 */
void SchedAddThread (struct ThreadControl *t,
	void (*entry)(void),
	unsigned int *stack,
	int stacksize,
	int priority);

/*
 * Remove a thread from all lists and set its state to zombie. This is
 * intended for permanent removal. For any other purpose, see SchedState.
 */
void SchedRemoveProcess (struct ThreadControl *thc);

/*
 * Change state, see C file for notes.
 */
void SchedState (
	struct ThreadControl *thc,
	unsigned char newstate,
	int (*callback)(struct ThreadControl*, unsigned int),
	unsigned int args,
	unsigned int time);

/*
 * When a thread is set to blocked state with a callback, it goes on the
 * work list until fetched by this function by a worker thread. Note that
 * if there are no work, this function blocks (sleeps) until there is.
 */
struct ThreadControl *SchedFetchWork (void);

/*
 * This MUST be called whenever the system tick counter is incremented,
 * and ONLY then.
 */
void SchedSystickHandler (void);

/*
 * When a thread returns from a signal handler, it executes a system call.
 * Call this from the system call handler to reset state.
 */
void SchedLeaveCurrentProcessSignal (void);

/*
 * Crash recovery, only for use by MemManage_Handler. Called
 * in the same way as PanicInternal, except stacktype is unused.
 * This function removes the current process from all active lists
 * and marks it as crashed, storing the stack pointer as well as
 * the hfsr, cfsr, bfar, mmar and faulttype. Note that the caller is
 * responsible for any memory that has to be freed.
 */
void SchedCrashRecover (unsigned int *stack, int stacktype, int faulttype);

/*
 * Yield the current process. May not happen until interrupts are enabled
 * or the current SVC returns.
 */
void SchedYield (void);

#endif
