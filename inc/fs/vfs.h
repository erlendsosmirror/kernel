/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef VFS_H
#define VFS_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "posix/sys/stat.h"
#include "posix/sys/statvfs.h"
#include "posix/dirent.h"
#include "../config/live/kernelconfig.h"

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////

/*
 * File node structure. Every open file descriptor will point to an entry
 * like this. It keeps track of the responsible file system, device driver,
 * how many times it is referenced (dup2 etc) and some flags. At last it
 * provides a pointer that a device driver may use for private data.
 *
 * Size: 16 bytes
 */
struct FileNode
{
	const struct VFSDriver *fs;		// File system driver for this node
	const struct DeviceDriver *dev;	// Device driver responsible for this node
	void *priv;						// Private driver data
	short flags;					// Open flags
	short usecounter;				// Number of references
};

/*
 * File system driver definition, providing the pointers to the functions
 * implementing a particular file system. It is recommended to keep this as
 * a static struct in flash. Unused functions can be set to a function
 * that simply returns an error value.
 */
struct VFSDriver
{
	// Basic file operations, aligns with the DeviceDriver structure
	int (*open)(struct FileNode*, const char*, int, int);
	int (*close)(struct FileNode*);
	int (*read)(struct FileNode*, void*, int);
	int (*write)(struct FileNode*, void*, int);
	int (*ioctl_fcntl)(struct FileNode*, unsigned int, void*);

	// Extended file operations
	int (*lseek)(struct FileNode*, int, int);
	int (*fstat)(struct FileNode*, struct stat *st);

	// File system operations
	int (*stat)(const char*, struct stat *st);
	int (*link)(const char*, const char*);
	int (*unlink)(const char*);
	int (*rename)(const char*, const char*);
	int (*mkdir)(const char*, int);
	int (*opendir)(const char*);
	int (*closedir)();
	int (*readdir)();
	int (*statvfs)(const char *path, struct statvfs*);

	// Mount and unmount functions
	int (*mount)(struct FileNode*, char*);
	int (*unmount)(void);
};

///////////////////////////////////////////////////////////////////////////////
// External variables
///////////////////////////////////////////////////////////////////////////////
extern struct FileNode nodetable[NODETABLE_SIZE];

///////////////////////////////////////////////////////////////////////////////
// VFS General Functions
///////////////////////////////////////////////////////////////////////////////
/*
 * Mount a disk (or file) at mountpoint with driver vfs.
 */
int vfs_mount (const char *mountpoint, struct VFSDriver *vfs, struct FileNode *disk);

/*
 * Unmount a disk/file from mountpoint and close disk/file handle.
 */
int vfs_unmount (const char *mountpoint);

/*
 * Relative path to absolute path conversion. Thread-safe as long as all
 * argument strings are private to the caller. The output is string inb
 * relative to the "working" directory string ina.
 */
void vfs_reltoabs (char *out, const char *ina, const char *inb);

///////////////////////////////////////////////////////////////////////////////
// File System Operations
///////////////////////////////////////////////////////////////////////////////
int vfs_open (struct FileNode *fnode, const char *cwd, const char *filename, int flags, int mode);
int vfs_close (struct FileNode *fnode);
int vfs_read (struct FileNode *fnode, char *data, int n);
int vfs_write (struct FileNode *fnode, char *data, int n);
int vfs_stat (const char *cwd, const char *filename, struct stat *st);
int vfs_unlink (const char *cwd, const char *filename);
int vfs_rename (const char *cwd, const char *oldname, const char *newname);
int vfs_mkdir (const char *cwd, const char *name, int mode);
int vfs_opendir (const char *cwd, const char *name);
int vfs_closedir (void);
int vfs_readdir (struct dirent *de);
int vfs_chdir (char *cwd, const char *dst);
int vfs_statvfs (const char *cwd, const char *path, struct statvfs *buf);

#endif
