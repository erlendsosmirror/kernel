/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SOCKET_VFS_H
#define SOCKET_VFS_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "posix/sys/socket.h"

///////////////////////////////////////////////////////////////////////////////
// Non-standard structures
///////////////////////////////////////////////////////////////////////////////
struct recvfrom_args
{
	void *buf;
	unsigned int len;
	int flags;
	struct sockaddr *addr;
	socklen_t *addrlen;
};

struct sendto_args
{
	const void *buf;
	unsigned int len;
	int flags;
	const struct sockaddr *addr;
	socklen_t addrlen;
};

///////////////////////////////////////////////////////////////////////////////
// Network stack declaration structure
///////////////////////////////////////////////////////////////////////////////
struct netstack
{
	int (*bind) (struct FileNode *, const struct sockaddr *, socklen_t);
	int (*recvfrom) (struct FileNode *, struct recvfrom_args *);
	int (*sendto) (struct FileNode *, struct sendto_args *);
	int (*socket) (struct FileNode *, int, int, int);

	int (*close) (struct FileNode *);

	const int domain;
};

///////////////////////////////////////////////////////////////////////////////
// Macros
///////////////////////////////////////////////////////////////////////////////
#define NET_DESCRIPTOR(descriptorname,bind,recvfrom,sendto,socket,close,domain) \
	struct netstack net_##descriptorname \
	__attribute((__section__(".net_list"))) \
	__attribute((__used__)) \
	= {bind, recvfrom, sendto, socket, close, domain};

///////////////////////////////////////////////////////////////////////////////
// Exports
///////////////////////////////////////////////////////////////////////////////
extern const struct VFSDriver vfs_driver_socket;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int vfs_socket_bind (struct FileNode *node, const struct sockaddr *addr, socklen_t addrlen);
int vfs_socket_recvfrom (struct FileNode *node, struct recvfrom_args *args);
int vfs_socket_sendto (struct FileNode *node, struct sendto_args *args);
int vfs_socket_socket (struct FileNode *node, int domain, int type, int protocol);

#endif
