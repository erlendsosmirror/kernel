# Copyright (c) 2018, Erlend Sveen
# All rights reserved.
# This file is licenced under the 2-clause Simplified BSD License, see the
# copying file in the root directory.

###############################################################################
# Project files
###############################################################################
include config/live/arch
include config/live/drivers-enabled
include make/makefiles/files
include config/live/kmod-enabled

###############################################################################
# Main configuration
###############################################################################
include make/makefiles/flags
include make/makefiles/processfiles

###############################################################################
# Build targets
###############################################################################
include make/makefiles/targets

###############################################################################
# Utility targets
###############################################################################

flash:
	@JLinkExe -device $(JLINKDEVICE) -if swd -speed 4000 -CommanderScript make/other/jlink-commandfile.txt | grep -vxFf make/other/jlink-filter.txt

debug:
	JLinkExe -device $(JLINKDEVICE) -if swd -speed 4000

server:
	JLinkGDBServer -device $(JLINKDEVICE) -if swd -speed 4000

kdbg:
	kdbg -r localhost:2331 build/program.elf

asm:
	cd build && ../../../../utils/debughelpers/getasm program.elf
	cd build && ../../../../utils/debughelpers/getsize program.elf
