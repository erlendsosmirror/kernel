/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "ioctl/ioctl.h"
#include "posix/fcntl.h"
#include "mem/string.h"
#include "print/print.h"
#include "drivers/tty/tty-stm32.h"
#include "../config/live/network-config.h"
#include "net/enet/layer2/interface.h"
#include "net/enet/layer2/framepool.h"
#include "net/enet/layer2/manager.h"
#include "net/enet/interfaces/ktty.h"

///////////////////////////////////////////////////////////////////////////////
// Debug definitions
///////////////////////////////////////////////////////////////////////////////
#if NETWORK_DEBUG_INTERFACE
	#define DEBUG(str) str
#else
	#define DEBUG(str)
#endif

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int dev_tty_open (struct FileNode *node, const char *filename);
int dev_tty_close (struct FileNode *node);
int dev_tty_read (struct FileNode *node, void *data, unsigned int nbytes);
int dev_tty_write (struct FileNode *node, void *data, unsigned int nbytes);
int dev_tty_ioctl (struct FileNode *node, unsigned int code, void *data);

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void KTTYInit (Layer2KTTY *t, char *filename)
{
	// Clear object
	MemSet (t, 0, sizeof (Layer2KTTY));

	// Open tty
	t->fd = dev_tty_open (&t->ttynode, filename);

	if (t->fd < 0)
	{
		IFDEBUG_OTHER (NETWORK_PRINTS ("Could not open port\n"));
		return;
	}
}

void Layer2Init (Layer2 *l)
{
	// Cast
	Layer2KTTY *t = (Layer2KTTY*) l;

	// Initialize link speed
	Layer2LinkspeedConstructor (&t->ls, l);

	// Set raw mode
	dev_tty_ioctl (&t->ttynode, TTYSETMODE, (void*)2);

	// Set default baud rate
	Layer2SetSpeed (l, 19200);

	// Store interface ID
	l->interfaceid = l;
}

void Layer2Pump (Layer2 *l)
{
	// Cast
	Layer2KTTY *t = (Layer2KTTY*) l;

	// Data byte
	unsigned char data;

	// Try reading data
	while (dev_tty_read (&t->ttynode, &data, 1) == 1)
	{
		// Write it at the position
		l->rxbuffer[l->writepos] = data;

		// Increment index with wrap-around
		if ((++l->writepos) >= RS485_RXBUFF_SIZE)
			l->writepos = 0;
	}

	// Check if we completed any packets
	DataFrame *frame;

	while ((frame = Layer2CheckPos (l)))
	{
		if (frame->destination == LAYER2LINKSPEED_DSTADDRESS)
			Layer2LinkspeedProcess (&t->ls, frame);
		else if (!t->ls.state)
			Layer2StoreFifo (l, frame);
		else
			FramepoolDelete (frame);
	}

	// Check link speed
	Layer2LinkspeedUpdate (&t->ls);
}

int Layer2SendFrameInternal (Layer2 *l, DataFrame *frame)
{
	// Cast
	Layer2KTTY *t = (Layer2KTTY*) l;

	// Check that the file descriptor is valid
	if (t->fd < 0)
	{
		FramepoolDelete(frame);
		return 0;
	}

	// Write all data
	int len = frame->size;
	int n = len;
	int pos = 0;
	char *str = (char*) frame;

	do
	{
		int diff = dev_tty_write (&t->ttynode, str + pos, n);

		if (diff < 0)
			break;

		pos += diff;
		n -= diff;
	} while (pos != len);

	// Free the frame memory
	FramepoolDelete(frame);

	// Increase statistic values
	IFSTATS (l->stats.framessent++);
	IFSTATS (l->stats.bytessent += frame->size);

	// Done
	return 0;
}

void Layer2SetSpeed (Layer2 *l, unsigned int speed)
{
	// Set speed using termios
	struct termios term;
	MemSet (&term, 0, sizeof (struct termios));
	term.baudrate = speed;

	Layer2KTTY *t = (Layer2KTTY*) l;
	dev_tty_ioctl (&t->ttynode, TCSETS, (void*)&term);
}

int Layer2IsUp (Layer2 *l)
{
	// Use getrxbusy ioctl to check for errors
	Layer2KTTY *t = (Layer2KTTY*) l;
	int ret = dev_tty_ioctl (&t->ttynode, TTYGETRXBUSY, 0);

	// Value 2 is an error, bring the link down
	if (ret == 2)
	{
		DEBUG (Print ("KTTY Error detected\n"));
		l->linkstate = 0;
	}

	// Return linkstate, make it 1 from here on
	unsigned char old = l->linkstate;
	l->linkstate = 1;
	return old;
}

int Layer2LinkUp (Layer2 *l)
{
	// Check state
	return Layer2LinkspeedLinkIsReady (&((Layer2KTTY*) l)->ls);
}
