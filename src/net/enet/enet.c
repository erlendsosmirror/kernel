/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sched/scheduler.h"
#include "fs/socket/socket.h"
#include "net/enet/network.h"
#include "net/enet/interfaces/ktty.h"
#include "print/print.h"
#include "mem/mem.h"
#include "posix/errno.h"
#include "kmod/kmod.h"

///////////////////////////////////////////////////////////////////////////////
// Debug definitions
///////////////////////////////////////////////////////////////////////////////
#define NETWORK_DEBUG_ENET 0

#if NETWORK_DEBUG_ENET
	#define DEBUG(str) str
#else
	#define DEBUG(str)
#endif

///////////////////////////////////////////////////////////////////////////////
// Network worker thread
///////////////////////////////////////////////////////////////////////////////
void ENetThread (void)
{
	while (1)
	{
		NetPump ();
		SchedState (currentthc, SCHED_STATUS_SLEEPING, 0, 0, 1);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Network stack entry points
///////////////////////////////////////////////////////////////////////////////
int enet_bind (struct FileNode *node, const struct sockaddr *addr, socklen_t addrlen)
{
	DEBUG (Print ("enet_bind\n"));

	struct sockaddr_in *in = (struct sockaddr_in*) addr;

	if (node->priv || !in || addrlen != sizeof (struct sockaddr_in))
		return -EINVAL;

	UDPSocket *s = (UDPSocket*) MemAlloc (sizeof (UDPSocket), 0);

	if (!s)
	{
		DEBUG (Print ("bind allocation error\n"));
		return -ENOMEM;
	}

	node->priv = s;
	int ipaddr = ntohl (in->sin_addr.s_addr);
	int port = in->sin_port >> 8;

	DEBUG (Print ("enet_bind port incoming %i outgoing %i\n", port, 0));

	return UDPInit (s, ipaddr, 0, port);
}

int enet_connect (struct FileNode *node, const struct sockaddr *addr, socklen_t addrlen)
{
	DEBUG (Print ("enet_connect\n"));

	struct sockaddr_in *in = (struct sockaddr_in*) addr;

	if (node->priv || !in || addrlen != sizeof (struct sockaddr_in))
		return -EINVAL;

	UDPSocket *s = (UDPSocket*) MemAlloc (sizeof (UDPSocket), 0);

	if (!s)
	{
		DEBUG (Print ("bind allocation error\n"));
		return -ENOMEM;
	}

	node->priv = s;
	int ipaddr = ntohl (in->sin_addr.s_addr);
	int sinport = in->sin_port >> 8;

	int port = UDPFindFreePort ();

	UDPSetPort (port);

	DEBUG (Print ("enet_connect port incoming %i outgoing %i\n", port, sinport));

	return UDPInit (s, ipaddr, sinport, port);
}

int enet_recvfrom (struct FileNode *node, struct recvfrom_args *args)
{
	// Debug trace
	DEBUG (Print ("enet_recvfrom\n"));

	// recvfrom on an unbound socket is not allowed
	if (!node->priv)
		return -EINVAL;

	// Get data from the stack
	int ret = UDPRecv ((UDPSocket*)node->priv, (unsigned char*)args->buf, args->len, args->addr);

	DEBUG (if (ret < 0) Print ("Recv failed\n"));

	if (args->addr && ret > 0)
	{
		struct sockaddr_in *ipaddr = (struct sockaddr_in*) args->addr;
		ipaddr->sin_addr.s_addr = htonl (ipaddr->sin_addr.s_addr);
		ipaddr->sin_port = ipaddr->sin_port << 8;
	}

	return ret;
}

int enet_sendto (struct FileNode *node, struct sendto_args *args)
{
	// Debug trace
	DEBUG (Print ("enet_sendto\n"));

	// Do "connect" if the socket is not bound
	if (!node->priv)
	{
		int ret = enet_connect (node, args->addr, args->addrlen);

		if (ret)
			return ret;
	}

	// Return value
	int ret;

	// Endianness
	if (args->addr)
	{
		struct sockaddr_in *ipaddr = (struct sockaddr_in*) args->addr;
		struct sockaddr_in target;
		target.sin_addr.s_addr = ntohl (ipaddr->sin_addr.s_addr);
		target.sin_port = ipaddr->sin_port >> 8;
		ret = UDPSend ((UDPSocket*)node->priv, (unsigned char*)args->buf, args->len, (struct sockaddr*) &target);
	}
	else
		ret = UDPSend ((UDPSocket*)node->priv, (unsigned char*)args->buf, args->len, 0);

	DEBUG (if (ret <= 0) Print ("Send failed\n"));

	return ret;
}

int enet_socket (struct FileNode *node, int domain, int type, int protocol)
{
	// Debug trace
	DEBUG (Print ("enet_socket\n"));

	// Make sure the priv (socket) pointer is cleared
	node->priv = 0;
	return 0;
}

int enet_close (struct FileNode *node)
{
	// Debug trace
	DEBUG (Print ("enet_close\n"));

	// Free data inside the socket (UDPRemove), then free the socket itself
	if (node->priv)
	{
		UDPClearPort (((UDPSocket*) node->priv)->srcport);
		UDPRemove ((UDPSocket*) node->priv);
		MemFree ((unsigned int)node->priv);
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Initialize
///////////////////////////////////////////////////////////////////////////////
void EnetInitialize (void)
{
	NET_INIT

	NetInit (IP_DEFAULT_ADDRESS);

	static struct ThreadControl enetthread;
	static unsigned int enetstack[256];

	enetthread.privileged = 0;
	SchedAddThread (&enetthread, &ENetThread, enetstack, 256, SCHED_PRIORITY_MEDIUM);
}

///////////////////////////////////////////////////////////////////////////////
// Declare network stack to the OS
///////////////////////////////////////////////////////////////////////////////
NET_DESCRIPTOR (enet,
	enet_bind,
	enet_recvfrom,
	enet_sendto,
	enet_socket,
	enet_close,
	4);

KMOD_DESCRIPTOR (enet, EnetInitialize, "enet-init");
