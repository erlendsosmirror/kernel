/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//
// Created by erlendjs on 10/05/16.
//

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "platform/platform.h"
#include "net/enet/network.h"
#include "net/enet/layer2/arp.h"
#include "net/enet/layer2/switch.h"
#include "net/enet/layer2/framepool.h"
#include "net/enet/layer2/manager.h"

///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////
struct ENetGlobals eglobals;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void NetInit (unsigned short myipaddress)
{
	// Initialize frame pool
	FramepoolInit ();

	// Initialize hardware drivers
	Layer2GlobalInit ();

	// Add interfaces
	for (struct Layer2 *i = eglobals.iflist; i; i = i->next)
		Layer2Init (i);

	// Initialize switch
	Layer2SwitchInit ();

	// Initialize ARP
	ARPInit (myipaddress);

	// Vars
	eglobals.myip = myipaddress;
	eglobals.defaultgateway = IP_DEFAULT_GATEWAY;
}

void NetPump (void)
{
	// Process interface data
	for (struct Layer2 *i = eglobals.iflist; i; i = i->next)
		Layer2Pump (i);

	// Process datagrams
	DataFrame *frame;

	while ((frame = Layer2GlobalPull ()))
	{
		// Kill null frames early, otherwise transfer to switch
		if (frame->destination == 0)
			FramepoolDelete (frame);
		else
			Layer2SwitchProcessFrame(frame);
	}

	// Call switch list maintenance every second
	static unsigned int start = 0;

	if ((PlatformGetTick () - start) > 1000)
	{
		start = PlatformGetTick ();
		Layer2SwitchMaintainList ();
	}
}

int NetLinkUp (void)
{
	// Check if at least one interface is up
	for (struct Layer2 *i = eglobals.iflist; i; i = i->next)
		if (Layer2LinkUp (i))
			return 1;

	// No interfaces up
	return 0;
}
