/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//
// Created by erlendjs on 10/05/16.
//

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/string.h"
#include "sched/scheduler.h"
#include "posix/errno.h"
#include "fs/socket/socket.h"
#include "../config/live/network-config.h"
#include "net/enet/layer4/udp.h"
#include "net/enet/layer2/framepool.h"
#include "net/enet/layer2/switch.h"
#include "net/enet/layer2/arp.h"
#include "net/enet/network.h"

#if NETWORK_DEBUG_IP
#include NETWORK_DEBUG_INC
#endif

///////////////////////////////////////////////////////////////////////////////
// Debug definitions
///////////////////////////////////////////////////////////////////////////////
#if NETWORK_DEBUG_IP
	#define IPDEBUG(str) str
#else
	#define IPDEBUG(str)
#endif

///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////

// Linked list of udp sockets
UDPSocket *socklist;

// Bit set for port that is in use
unsigned int portinuse[256/4];

///////////////////////////////////////////////////////////////////////////////
// Helper functions
///////////////////////////////////////////////////////////////////////////////
int UDPPack (UDPCast *ip, const unsigned char *data, int len)
{
	// Index into packet, will hold the final payload size
	int j = 0;

	// Pack up to len data, or payload size of 231
	for (int i = 0; i < len && j < 230; i++, j++)
	{
		if (data[i] < 254)
			ip->payload[j] = data[i];
		else
		{
			ip->payload[j++] = 254;
			ip->payload[j] = data[i] - 254;
		}
	}

	return j;
}

int UDPUnPack (UDPCast *ip)
{
	// Size to process, substract layer2 header, layer4 header and separator
	int n = ((DataFrame*)ip)->size - 6 - 8 - 1;

	// Unpack data
	int j = 0;

	for (int ii = 0; ii < n; ii++, j++)
	{
		if (ip->payload[ii] == 254)
			ip->payload[j] = ip->payload[++ii] + 254;
		else
			ip->payload[j] = ip->payload[ii];
	}

	return j;
}

///////////////////////////////////////////////////////////////////////////////
// Functions: UDP Socket member definitions
///////////////////////////////////////////////////////////////////////////////
int UDPInit (UDPSocket *s, unsigned short ipaddress, unsigned char dst, unsigned char src)
{
	// Clear the structure
	MemSet (s, 0, sizeof(UDPSocket));

	// Set ip and ports
	s->ip = ipaddress;
	s->dstport = dst;
	s->srcport = src;

	// Add to internal linked list
	if (socklist)
		s->next = socklist;

	socklist = s;
	return 0;
}

void UDPRemove (UDPSocket *s)
{
	// Delete all frames
	for (DataFrame *f = s->frames; f; f = f->next)
		FramepoolDelete (f);

	// Remove from list
	for (UDPSocket *sock = socklist; sock; sock = sock->next)
	{
		if (sock->next == s)
		{
			sock->next = s->next;
			break;
		}
	}

	if (socklist == s)
		socklist = s->next;
}

int UDPFindFreePort (void)
{
	for (int i = 120; i < 240; i++)
	{
		int ix = i / 32;
		int iy = i % 32;

		if (!(portinuse[ix] & (1 << iy)))
			return i;
	}

	return 0;
}

int UDPSetPort (int port)
{
	if (port < 0 || port > 255)
		return -1;

	int ix = port / 32;
	int iy = port % 32;

	portinuse[ix] |= 1 << iy;
	return 0;
}

int UDPClearPort (int port)
{
	if (port < 0 || port > 255)
		return -1;

	int ix = port / 32;
	int iy = port % 32;

	portinuse[ix] &= ~(1 << iy);
	return 0;
}

int UDPSend (UDPSocket *s, const unsigned char *data, int len, const struct sockaddr *addr)
{
	// If we are sending to some special destination, resolve its address.
	// Otherwise, use the ip in the socket. As a last resort try the default
	// gateway.
	struct sockaddr_in *ipaddr = (struct sockaddr_in*) addr;

	if (ipaddr || !s->hwaddr)
	{
		if (ipaddr)
			s->hwaddr = ipaddr->sin_addr.s_addr;
		else if (!s->hwaddr)
			s->hwaddr = s->ip;

		s->hwaddr = ARPResolve (s->hwaddr);

		if (!s->hwaddr && !(s->hwaddr = ARPResolve (eglobals.defaultgateway)))
			return -EHOSTUNREACH;
	}

	// Allocate a frame
	DataFrame *f = FramepoolNew ();

	if (!f)
		return -ENOMEM;

	// Cast to UDP datagram
	UDPCast *ip = (UDPCast*) f;

	// Sanitize data with escape sequences
	int j = UDPPack (ip, data, len);

	// Set IP data
	ip->source = eglobals.myip;
	ip->ttl = 30;
	ip->payloadprotocol = 1;
	ip->srcport = s->srcport;

	if (ipaddr)
	{
		ip->destination = ipaddr->sin_addr.s_addr;
		ip->dstport = ipaddr->sin_port;
	}
	else
	{
		ip->destination = s->ip;
		ip->dstport = s->dstport;
	}

	// Set layer2 data
	f->destination = s->hwaddr;
	f->source = LAYER2_ADDRESS;
	f->protocol = 1;
	f->checksum = ' ';
	f->payload[8 + j] = PACKETSEPARATOR;
	f->size = (unsigned char) (DATAFRAME_SIZE + 8 + j);
	f->originatinginterface = 0;

	// Transfer ownership to the switch, return number of bytes written
	Layer2SwitchProcessFrame (f);
	return j;
}

int UDPRecv (UDPSocket *s, unsigned char *data, int len, struct sockaddr *addr)
{
	// No data ready
	if (!s->frames)
		return 0;

	// Dequeue one data frame
	DataFrame *f = s->frames;
	s->frames = f->next;

	// Copy data
	UDPCast *ip = (UDPCast*) f;
	int n = f->size < len ? f->size : len;
	MemCpy (data, ip->payload, n);

	// Supply the address to the caller if wanted
	if (addr)
	{
		struct sockaddr_in *ipaddr = (struct sockaddr_in*) addr;
		ipaddr->sin_addr.s_addr = ip->source;
		ipaddr->sin_port = ip->srcport;
	}

	// Free the frame and return number of bytes read
	FramepoolDelete (f);
	return n;
}

///////////////////////////////////////////////////////////////////////////////
// Functions: General UDP API function definitions
///////////////////////////////////////////////////////////////////////////////
void IPProcess (DataFrame *f)
{
	// Cast to UDP datagram
	UDPCast *c = (UDPCast*) f;

	// If we are to receive it, attach it to the correct socket in the list
	if (c->destination == eglobals.myip ||
		c->destination == NET_IP_BROADCAST ||
		c->destination == 0)
	{
		SchedEnterCritical ();

		for (UDPSocket *sock = socklist; sock; sock = sock->next)
		{
			// Check if packet destination port matches socket reception port
			if (c->dstport != sock->srcport)
				continue;

			// Check if this is our specific connection by ip and port
			if (c->source == sock->ip && c->srcport == sock->dstport)
			{
				IPDEBUG (NETWORK_PRINTS ("Accepting packet\n"));
			}
			// It was not our specific connection even if it matched
			// our reception port, check if this is a server
			// socket. If not, ignore the current position entirely and cont.
			else if (sock->ip)
			{
				IPDEBUG (NETWORK_PRINTS ("Error: Port match but not server socket\n"));
				continue;
			}
			// Since this is a server socket, we need to check the rest too
			// in case the specific connection comes later on in the array.
			else
			{
				IPDEBUG (NETWORK_PRINTS ("Accepting packet as server\n"));
			}

			// Unpack
			f->size = UDPUnPack (c);

			// Put on the pending list, keep the memory in the list
			f->next = sock->frames;
			sock->frames = f;
			SchedExitCritical ();
			return;
		}

		SchedExitCritical ();
	}

	// Destination is not for us or it is a broadcast. It has to be routed.
	// Note: packets broadcasted in layer 2 is let in here, needs fix.
	if (c->destination != eglobals.myip)
	{
		// Decrease the time to live counter
		c->ttl--;

		// If still valid
		if (c->ttl)
		{
			// Route and send the packet
			// TODO: Implement packet routing
			IPDEBUG (NETWORK_PRINTS ("IP Packet not routed\n"));
		}
	}

	// Free the memory now that we are done
	FramepoolDelete(f);
}
