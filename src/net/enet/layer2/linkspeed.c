/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//
// Created by erlendjs on 27/04/16.
//

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/string.h"
#include "net/enet/layer2/linkspeed.h"
#include "net/enet/layer2/switch.h"
#include "net/enet/layer2/framepool.h"
#include "net/enet/layer2/manager.h"
#include NETWORK_DELAY_INC

#if NETWORK_DEBUG_LINKSPEED
#include NETWORK_DEBUG_INC
#endif

///////////////////////////////////////////////////////////////////////////////
// Debug definitions
///////////////////////////////////////////////////////////////////////////////
#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_DISCOVERYSEND
	#define LSDEBUG_DISCOVERYSEND(str) str
#else
	#define LSDEBUG_DISCOVERYSEND(str)
#endif

#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_STATEIS3
	#define LSDEBUG_STATEIS3(str) str
#else
	#define LSDEBUG_STATEIS3(str)
#endif

#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_TESTFAILED
	#define LSDEBUG_TESTFAILED(str) str
#else
	#define LSDEBUG_TESTFAILED(str)
#endif

#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_YIELDONDISCOVER
	#define LSDEBUG_YIELDONDISCOVER(str) str
#else
	#define LSDEBUG_YIELDONDISCOVER(str)
#endif

#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_SENDINGLIST
	#define LSDEBUG_SENDINGLIST(str) str
#else
	#define LSDEBUG_SENDINGLIST(str)
#endif

#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_YIELDONLISTREPLY
	#define LSDEBUG_YIELDONLISTREPLY(str) str
#else
	#define LSDEBUG_YIELDONLISTREPLY(str)
#endif

#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_SENDINGSETSPEED
	#define LSDEBUG_SENDINGSETSPEED(str) str
#else
	#define LSDEBUG_SENDINGSETSPEED(str)
#endif

#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_SETTINGSPEED
	#define LSDEBUG_SETTINGSPEED(str) str
#else
	#define LSDEBUG_SETTINGSPEED(str)
#endif

#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_TEST
	#define LSDEBUG_TEST(str) str
#else
	#define LSDEBUG_TEST(str)
#endif

#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_TESTREPLY
	#define LSDEBUG_TESTREPLY(str) str
#else
	#define LSDEBUG_TESTREPLY(str)
#endif

#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_SPEEDCONFIRMED
	#define LSDEBUG_SPEEDCONFIRMED(str) str
#else
	#define LSDEBUG_SPEEDCONFIRMED(str)
#endif

#if NETWORK_DEBUG_LINKSPEED && DEBUG_LINKSPEED_TIMEOUT
	#define LSDEBUG_TIMEOUT(str) str
#else
	#define LSDEBUG_TIMEOUT(str)
#endif

///////////////////////////////////////////////////////////////////////////////
// Private functions: Outgoing packets
///////////////////////////////////////////////////////////////////////////////
void LinkSpeedSendDiscovery (Layer2Linkspeed *l)
{
	// Current state: Ready to start sequence, still monitoring link->IsUp
	// Send discovery packet if link seems to be up
	if (Layer2IsUp (l->link))
	{
		// Allocate frame
		DataFrame *frame = FramepoolNew ();

		// If allocation failed
		if (!frame)
		{
			LSDEBUG_DISCOVERYSEND (NETWORK_PRINTS ("LSO Discovery no frame\n"));
			return;
		}

		// Set frame data members
		frame->destination = LAYER2LINKSPEED_DSTADDRESS;
		frame->source = LAYER2LINKSPEED_ADDRESS;
		frame->protocol = LAYER2LINKSPEED_PROTOCOL;
		frame->checksum = ' ';
		frame->payload[0] = LAYER2LINKSPEED_PTYPE_DISCOVERY;
		frame->payload[1] = PACKETSEPARATOR;
		frame->size = DATAFRAME_HEADSIZE + 2;

		// Print debug message
		LSDEBUG_DISCOVERYSEND (NETWORK_PRINTS ("LSO Discovery send\n"));

		// Try sending it. If it failed, free the memory.
		if (Layer2SendFrame (l->link, frame))
		{
			LSDEBUG_DISCOVERYSEND (NETWORK_PRINTS ("LSO Discovery send error\n"));
			FramepoolDelete (frame);
		}
		else
		{
			// Success, start listening for list reply or timeout event
			l->time = PlatformGetTick () + LAYER2LINKSPEED_RESTARTTIMEOUT;
			l->state = 2;
		}
	}
	else
	{
		// Print debug message
		LSDEBUG_DISCOVERYSEND (NETWORK_PRINTS ("LSO Discovery aborted\n"));
	}
}

void LinkSpeedSendTestFrame (Layer2Linkspeed *l)
{
	// Current state: (3) Got list and sent set speed, waiting for Update tick to send data
	// Set this device's speed when about to send the first frame
	if (l->sequence == LAYER2LINKSPEED_SEQUENCESTART)
	{
		LSDEBUG_STATEIS3 (NETWORK_PRINTI ("LSO State 3, speed %i\n", l->speeds[l->settingspeed]));
		Layer2SetSpeed (l->link, l->speeds[l->settingspeed]);
	}

	// Get frame and send test data or confirm
	DataFrame *frame = FramepoolNew();

	// Check allocation
	if (!frame)
	{
		LSDEBUG_STATEIS3 (NETWORK_PRINTS ("LSO No frame\n"));
		return;
	}

	// Set frame data
	frame->destination = LAYER2LINKSPEED_DSTADDRESS;
	frame->source = LAYER2LINKSPEED_ADDRESS;
	frame->protocol = LAYER2LINKSPEED_PROTOCOL;
	frame->checksum = ' ';

	// Confirmation or not
	if (!l->sequence)
	{
		// Reached sequence 0, set payload as speed confirmation with speed
		frame->payload[0] = LAYER2LINKSPEED_PTYPE_SPEEDCONFIRM;
		frame->payload[1] = ((unsigned char *) &l->speeds[l->settingspeed])[0];
		frame->payload[2] = ((unsigned char *) &l->speeds[l->settingspeed])[1];
		frame->payload[3] = ((unsigned char *) &l->speeds[l->settingspeed])[2];
		frame->payload[4] = ((unsigned char *) &l->speeds[l->settingspeed])[3];
		frame->payload[5] = PACKETSEPARATOR;
		frame->size = DATAFRAME_HEADSIZE + 6;

		// Set state to completed
		l->state = 0;

		// Print debug
		LSDEBUG_STATEIS3 (NETWORK_PRINTI ("LSO State 3 ch to 0, speed %i\n", l->speeds[l->settingspeed]));
	}
	else
	{
		// Set payload as test data
		frame->payload[0] = LAYER2LINKSPEED_PTYPE_TEST;
		frame->payload[1] = l->sequence;
		frame->payload[2] = PACKETSEPARATOR;
		frame->size = DATAFRAME_HEADSIZE + 3;

		// Set state: Sent test packet, waiting for reply
		l->state = 4;

		// Print debug
		LSDEBUG_STATEIS3 (NETWORK_PRINTI ("LSO State 3 ch to 4, sequence %i\n", l->sequence));
	}

	// Send packet and set time
	if (Layer2SendFrame (l->link, frame))
		FramepoolDelete (frame);
	else
		l->time = PlatformGetTick () + LAYER2LINKSPEED_RECVTESTTIMEOUT;
}

void LinkSpeedTestFailed (Layer2Linkspeed *l)
{
	// Print debug message
	LSDEBUG_TESTFAILED (NETWORK_PRINTS ("LSO LinkSpeedTestFailed, 19200\n"));

	// Start over again
	Layer2SetSpeed (l->link, UARTSPEED_19200);

	// Reply with set speed, reusing frame argument (default list here)
	DataFrame *frame = FramepoolNew ();

	// Check allocation
	if (!frame)
	{
		LSDEBUG_TESTFAILED (NETWORK_PRINTS ("LSO No frame\n"));
		return;
	}

	// Set frame contents
	frame->destination = LAYER2LINKSPEED_DSTADDRESS;
	frame->source = LAYER2LINKSPEED_ADDRESS;
	frame->protocol = LAYER2LINKSPEED_PROTOCOL;
	frame->checksum = ' ';
	frame->payload[0] = LAYER2LINKSPEED_PTYPE_SETSPEED;
	frame->payload[1] = ((unsigned char *) &l->speeds[l->settingspeed])[0];
	frame->payload[2] = ((unsigned char *) &l->speeds[l->settingspeed])[1];
	frame->payload[3] = ((unsigned char *) &l->speeds[l->settingspeed])[2];
	frame->payload[4] = ((unsigned char *) &l->speeds[l->settingspeed])[3];
	frame->payload[5] = PACKETSEPARATOR;
	frame->size = DATAFRAME_HEADSIZE + 6;

	// Try sending it
	if (Layer2SendFrame (l->link, frame))
		FramepoolDelete (frame);

	// Set state to send test frame
	l->state = 3;
	l->time = PlatformGetTick () + LAYER2LINKSPEED_SENDTESTFRAMEWAIT;

	// If we are at the lowest speed, complete the sequence
	if (l->settingspeed == 0)
		l->sequence = 0;
	else
		l->sequence = LAYER2LINKSPEED_SEQUENCESTART;
}

///////////////////////////////////////////////////////////////////////////////
// Private functions: Incoming packets
///////////////////////////////////////////////////////////////////////////////
void LinkSpeedProcessDiscovery (Layer2Linkspeed *l, DataFrame *frame)
{
	// Received discovery packet
	// Check if we have to yield and abort our quest if we do
	if (frame->source > LAYER2LINKSPEED_ADDRESS)
	{
		// Print debug message
		LSDEBUG_YIELDONDISCOVER (NETWORK_PRINTI ("LSI Yield on discovery, sending list (to %i)\n", frame->source));

		// Reply with speed list, reusing frame argument (default list here)
		frame->source = LAYER2LINKSPEED_ADDRESS;
		frame->payload[0] = LAYER2LINKSPEED_PTYPE_SPEEDLIST;

	#if LAYER2LINKSPEED_USE_DEFAULT_SPEEDS
		frame->payload[1] = LAYER2LINKSPEED_SPEEDLIST_DEFAULT;
		frame->payload[2] = PACKETSEPARATOR;
		frame->size = DATAFRAME_HEADSIZE + 3;
	#else
		unsigned int speed = LAYER2LINKSPEED_USE_SPEED;
		frame->payload[1] = 1;
		frame->payload[2] = speed >> 0;
		frame->payload[3] = speed >> 8;
		frame->payload[4] = speed >> 16;
		frame->payload[5] = speed >> 24;
		frame->payload[6] = PACKETSEPARATOR;
		frame->size = DATAFRAME_HEADSIZE + 7;
	#endif

		// Send the data message
		if (Layer2SendFrame (l->link, frame))
			FramepoolDelete (frame);

		// Set state: Sent speed list, waiting for set speed
		l->state = 255;
		l->time = PlatformGetTick () + LAYER2LINKSPEED_YIELDTIMEOUT;
	}
	else
	{
		// Debug message
		LSDEBUG_SENDINGLIST (NETWORK_PRINTS ("LSI Error: List to lower ranked\n"));
		FramepoolDelete (frame);

		// The other node probably restarted and wants to re-negotiate, reset
		l->state = 1;
	}
}

void LinkSpeedProcessSpeedlist (Layer2Linkspeed *l, DataFrame *frame)
{
	// Received list reply packet
	// Check if we have to yield and abort our quest if we do
	if (frame->source > LAYER2LINKSPEED_ADDRESS)
	{
		// Print debug message
		LSDEBUG_YIELDONLISTREPLY (NETWORK_PRINTS ("LSI Abort on list reply\n"));

		// Set state to 255. Should make main check set baud to default and
		// then to state 1.
		l->state = 255;
		l->time = PlatformGetTick () + LAYER2LINKSPEED_YIELDTIMEOUT;
		FramepoolDelete (frame);
		return;
	}

	// Match the list
	if (frame->payload[1] == LAYER2LINKSPEED_SPEEDLIST_DEFAULT)
	{
		// Build the standard list and start off with the highest
		l->speeds[0] = UARTSPEED_19200;
		l->speeds[1] = UARTSPEED_57600;
		l->speeds[2] = UARTSPEED_115200;
		l->speeds[3] = UARTSPEED_250000;
		l->speeds[4] = UARTSPEED_500000;
		l->speeds[5] = UARTSPEED_1000000;
		l->speeds[6] = UARTSPEED_1500000;
		l->speeds[7] = UARTSPEED_2000000;
		l->settingspeed = 7;

		// Print debug
		LSDEBUG_SENDINGSETSPEED (NETWORK_PRINTS ("LSI Using default speeds\n"));
	}
	else
	{
		// The received baud rates
		unsigned int *array = ((unsigned int *) (&frame->payload[2]));

		// True if there is a mutually supported rate
		int foundsupported = 0;

		//
		l->settingspeed = 0xFFFFFFFF;

		// Get the array size
		int n = frame->payload[1];

		// Search the array
		for (int i = 0; i < n; i++)
		{
			if (array[i] == UARTSPEED_19200 ||
				 array[i] == UARTSPEED_57600 ||
				 array[i] == UARTSPEED_115200 ||
				 array[i] == UARTSPEED_250000 ||
				 array[i] == UARTSPEED_500000 ||
				 array[i] == UARTSPEED_1000000 ||
				 array[i] == UARTSPEED_1500000 ||
				 array[i] == UARTSPEED_2000000)
			{
				// Build our own array
				l->settingspeed++;
				l->speeds[l->settingspeed] = array[i];

				// Set the flag
				foundsupported = 1;
			}
		}

		// If there are no mutually supported speeds
		if (!foundsupported)
		{
			// Print debug message
			LSDEBUG_SENDINGSETSPEED (NETWORK_PRINTS ("LSI No mutual speeds\n"));

			// Set to failed state and wait for timeout
			l->settingspeed = 0;
			l->state = 1;
			FramepoolDelete (frame);
			return;
		}
		else
		{
			// Print debug message
			LSDEBUG_SENDINGSETSPEED (NETWORK_PRINTS ("LSI Built speed list\n"));
		}
	}

	// Store the remotes address
	l->servingremote = frame->source;

	// Reply with set speed, reusing frame argument (default list here)
	frame->source = LAYER2LINKSPEED_ADDRESS;
	frame->payload[0] = LAYER2LINKSPEED_PTYPE_SETSPEED;
	frame->payload[1] = ((unsigned char *) &l->speeds[l->settingspeed])[0];
	frame->payload[2] = ((unsigned char *) &l->speeds[l->settingspeed])[1];
	frame->payload[3] = ((unsigned char *) &l->speeds[l->settingspeed])[2];
	frame->payload[4] = ((unsigned char *) &l->speeds[l->settingspeed])[3];
	frame->payload[5] = PACKETSEPARATOR;
	frame->size = DATAFRAME_HEADSIZE + 6;

	// Print debug message
	LSDEBUG_SENDINGSETSPEED (NETWORK_PRINTI ("LSI Sending set speed %i\n", l->speeds[l->settingspeed]));

	// Send the frame
	if (Layer2SendFrame (l->link, frame))
		FramepoolDelete (frame);

	// Set state: Got list and sent set speed, waiting for Update tick to send data
	l->state = 3;
	l->time = PlatformGetTick () + LAYER2LINKSPEED_SENDTESTFRAMEWAIT;

	// Set start sequence
	l->sequence = LAYER2LINKSPEED_SEQUENCESTART;
}

void LinkSpeedProcessSetSpeed (Layer2Linkspeed *l, DataFrame *frame)
{
	// Store the speed
	((unsigned char *) &l->settingspeed)[0] = frame->payload[1];
	((unsigned char *) &l->settingspeed)[1] = frame->payload[2];
	((unsigned char *) &l->settingspeed)[2] = frame->payload[3];
	((unsigned char *) &l->settingspeed)[3] = frame->payload[4];

	// Set the speed
	Layer2SetSpeed (l->link, l->settingspeed);

	// Debug message
	LSDEBUG_SETTINGSPEED (NETWORK_PRINTI ("LSI Setting speed %i\n", l->settingspeed));

	// Set state: Got set speed, speed has been set and is waiting for testdata
	l->state = 255;
	l->time = PlatformGetTick () + LAYER2LINKSPEED_SETSPEEDTIMEOUT;
}

void LinkSpeedProcessTest (Layer2Linkspeed *l, DataFrame *frame)
{
	// Received test packet, reuse data frame.
	// Reply protocol and testdata sequence is already set.
	// Correct sequence number is handled by the sender.
	frame->source = LAYER2LINKSPEED_ADDRESS;
	frame->payload[0] = LAYER2LINKSPEED_PTYPE_TESTREPLY;
	frame->payload[2] = PACKETSEPARATOR;
	frame->size = DATAFRAME_HEADSIZE + 3;

	// Print debug message
	LSDEBUG_TEST (NETWORK_PRINTI ("LSI Test %i\n", (int) frame->payload[1]));

	// Send data frame
	if (Layer2SendFrame (l->link, frame))
		FramepoolDelete (frame);

	// Set delay
	l->time = PlatformGetTick () + LAYER2LINKSPEED_REPLIEDTIMEOUT;
}

void LinkSpeedProcessTestReply (Layer2Linkspeed *l, DataFrame *frame)
{
	// Received test packet reply, check the reply
	if (frame->source != l->servingremote || frame->payload[1] != l->sequence)
	{
		// Print debug message
		LSDEBUG_TESTREPLY (NETWORK_PRINTI ("LSI Incorrect sequence %i\n", l->sequence));

		// Set state: Test error.
		l->state = 4;
		l->time = PlatformGetTick ();
	}
	else
	{
		// Print debug message
		LSDEBUG_TESTREPLY (NETWORK_PRINTI ("LSI Test reply %i\n", l->sequence));

		// Decrement sequence number
		l->sequence--;

		// Set state: Got list and sent set speed, waiting for Update tick to send data
		// It causes a wait and then sends the next test in the sequence
		l->state = 3;
		l->time = PlatformGetTick ();
	}
}

void LinkSpeedProcessSpeedConfirm (Layer2Linkspeed *l, DataFrame *frame)
{
	// Received set speed confirmation
	((unsigned char *) &l->settingspeed)[0] = frame->payload[1];
	((unsigned char *) &l->settingspeed)[1] = frame->payload[2];
	((unsigned char *) &l->settingspeed)[2] = frame->payload[3];
	((unsigned char *) &l->settingspeed)[3] = frame->payload[4];

	// Print debug message
	LSDEBUG_SPEEDCONFIRMED (NETWORK_PRINTI ("LSI Speed confirmed %i\n", l->settingspeed));

	// Set speed and state
	Layer2SetSpeed (l->link, l->settingspeed);
	l->state = 0;
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void Layer2LinkspeedConstructor (Layer2Linkspeed *l, Layer2 *interface)
{
	MemSet (l, 0, sizeof (Layer2Linkspeed));
	l->link = interface;
	l->state = 1;
	l->time = PlatformGetTick () + LAYER2LINKSPEED_RESTARTTIMEOUT;
}

void Layer2LinkspeedUpdate (Layer2Linkspeed *l)
{
	if (l->state == 0)
	{
		// We're in the good state, check if link went down
		if (!Layer2IsUp (l->link))
		{
			// Reset link and clear switch cache
			l->state = 1;
			Layer2SwitchClearInterface (l->link->interfaceid);
		}
	}
	else if (PlatformGetTick () > l->time)
	{
		if (l->state == 1)
			LinkSpeedSendDiscovery (l); // State 1 -> 2, timeout if no reply
		else if (l->state == 3)
			LinkSpeedSendTestFrame (l); // State 3 -> 0 or 4.
		else if (l->state == 10)
			LinkSpeedTestFailed (l); // State 10 -> 3
		else if (l->state == 4)
		{
			// We get here if we timed out waiting for a test packet reply
			// Step to a lower speed
			l->settingspeed--;

			// If it rolled over (settingspeed is unsigned), set to lowest
			if (l->settingspeed > 7)
				l->settingspeed = 0;

			// Try again with the lower speed
			l->state = 10;
			l->time = PlatformGetTick () + LAYER2LINKSPEED_RESTARTTIMEOUT;
		}
		else
		{
			// We get here on timeouts, print message
			LSDEBUG_TIMEOUT (NETWORK_PRINTS ("LSO Operation timeout, 19200\n"));

			// Set speed back to default
			Layer2SetSpeed (l->link, UARTSPEED_19200);

			// Go back to the initial state (sending discovery)
			l->state = 1;
		}
	}
}

void Layer2LinkspeedProcess (Layer2Linkspeed *l, DataFrame *frame)
{
	// The frame is likely to be reused
	// Set its members but wait with source as it has to be checked first
	frame->destination = LAYER2LINKSPEED_DSTADDRESS;
	frame->protocol = LAYER2LINKSPEED_PROTOCOL;
	frame->checksum = ' ';

	// Switch on the message type
	switch (frame->payload[0])
	{
	case LAYER2LINKSPEED_PTYPE_DISCOVERY:
		LinkSpeedProcessDiscovery (l, frame); // State x -> 255 (yield) or x (we are higher ranked)
		return;
	case LAYER2LINKSPEED_PTYPE_SPEEDLIST:
		LinkSpeedProcessSpeedlist (l, frame); // State x -> 255 (yield) or 1 (no speeds) or 3 (start sequence)
		return;
	case LAYER2LINKSPEED_PTYPE_SETSPEED:
		LinkSpeedProcessSetSpeed (l, frame); // 255 -> 255 (speed accepted) or 253 (not applicable)
		break;
	case LAYER2LINKSPEED_PTYPE_TEST:
		LinkSpeedProcessTest (l, frame); // State unmodified, but timeout set
		return;
	case LAYER2LINKSPEED_PTYPE_TESTREPLY:
		LinkSpeedProcessTestReply (l, frame); // State x -> 3
		break;
	case LAYER2LINKSPEED_PTYPE_SPEEDCONFIRM:
		LinkSpeedProcessSpeedConfirm (l, frame); // State x -> 0 (assume link ok)
		break;
	}

	// If we get here the frame has to be freed
	FramepoolDelete (frame);
}

int Layer2LinkspeedLinkIsReady (Layer2Linkspeed *l)
{
	return l->state == 0 ? 1 : 0;
}
