/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//
// Created by erlendjs on 05/05/16.
//

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/string.h"
#include "net/enet/network.h"
#include "net/enet/layer2/switch.h"
#include "net/enet/layer2/framepool.h"
#include "net/enet/layer2/arp.h"
#include "net/enet/layer2/manager.h"

#if NETWORK_DEBUG_SWITCH
#include NETWORK_DEBUG_INC
#endif

///////////////////////////////////////////////////////////////////////////////
// Debug definitions
///////////////////////////////////////////////////////////////////////////////
#if NETWORK_DEBUG_SWITCH && DEBUG_SWITCH_ADDLIST
	#define SWDEBUG_ADDLIST(str) str
#else
	#define SWDEBUG_ADDLIST(str)
#endif

#if NETWORK_DEBUG_SWITCH && DEBUG_SWITCH_UPDATELIST
	#define SWDEBUG_UPDATELIST(str) str
#else
	#define SWDEBUG_UPDATELIST(str)
#endif

#if NETWORK_DEBUG_SWITCH && DEBUG_SWITCH_DELETELIST
	#define SWDEBUG_DELETELIST(str) str
#else
	#define SWDEBUG_DELETELIST(str)
#endif

#if NETWORK_DEBUG_SWITCH && DEBUG_SWITCH_SWITCH
	#define SWDEBUG_BROADCASTING(str) str
#else
	#define SWDEBUG_BROADCASTING(str)
#endif

///////////////////////////////////////////////////////////////////////////////
// Structure definitions
///////////////////////////////////////////////////////////////////////////////

// Switch cache table entry
typedef struct SwitchListEntry
{
	Layer2 *interface;
	unsigned short address;
	unsigned char timeout;
}SwitchListEntry;

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////

// Static cache table
SwitchListEntry switchlist[SWITCH_LISTSIZE];

///////////////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////////////
void Layer2VirtualSendFrame (DataFrame *frame)
{
	// Give frame to the correct protocol handler, or free it if unknown
	if (frame->protocol == 1)
		IPProcess (frame);
	else if (frame->protocol == 2)
		ARPProcess (frame);
	else
		FramepoolDelete (frame);
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void Layer2SwitchInit (void)
{
	// Clear switch cache table
	MemSet (switchlist, 0, sizeof(switchlist));
}

void Layer2SwitchProcessFrame (DataFrame *frame)
{
	// This is set if the address is in the cache
	int found = 0;

	// Check the switch cache for updates
	for (int i = 0; i < SWITCH_LISTSIZE; i++)
	{
		if (frame->source == switchlist[i].address)
		{
			// Update where it came from and reset the timeout
			switchlist[i].interface = frame->originatinginterface;
			switchlist[i].timeout = 255;
			found = 1;

			// Print debug message
			SWDEBUG_UPDATELIST (NETWORK_PRINTI ("SW Update addr at idx %i\n", i));
			break;
		}
	}

	// If we did not have it in the table we need to add it
	if (!found)
	{
		// Search for a free entry
		for (int i = 0; i < SWITCH_LISTSIZE; i++)
		{
			if (!switchlist[i].address)
			{
				// Set the address, interface and timeout
				switchlist[i].address = frame->source;
				switchlist[i].interface = frame->originatinginterface;
				switchlist[i].timeout = 255;

				// Print debug message
				SWDEBUG_ADDLIST (NETWORK_PRINTI ("SW Add addr %i\n", (int) frame->source));
				SWDEBUG_ADDLIST (NETWORK_PRINTI ("   at index %i\n", i));
				break;
			}
		}
	}

	// If not explicit broadcast
	if (frame->destination != LAYER2_BROADCAST)
	{
		// Search the table to get the correct outgoing interface
		for (int i = 0; i < SWITCH_LISTSIZE; i++)
		{
			if (frame->destination == switchlist[i].address)
			{
				// If it resolved to ourself, process it, otherwise send it
				if (!switchlist[i].interface)
					Layer2VirtualSendFrame (frame);
				else if (!Layer2LinkUp (switchlist[i].interface))
					FramepoolDelete(frame);
				else if (Layer2SendFrame (switchlist[i].interface, frame))
					FramepoolDelete (frame);

				return;
			}
		}
	}

	// Could not find the outgoing interface, broadcast instead
	// Print debug message
	SWDEBUG_BROADCASTING (NETWORK_PRINTS ("SW Broadcasting frame\n"));

	// For every interface
	for (Layer2 *j = eglobals.iflist; j; j = j->next)
	{
		// Attempt sending if it is not the originating one and if the link is up
		if (frame->originatinginterface != j && Layer2LinkUp (j))
		{
			// Allocate for copy
			DataFrame *a = FramepoolNew ();

			// Check allocation
			if (a)
			{
				// Copy contents
				for (int i = 0; i < frame->size; i++)
					((unsigned char *) a)[i] = ((unsigned char *) frame)[i];

				// Set size
				a->size = frame->size;

				// Send frame on the interface
				if (Layer2SendFrame (j, a))
					FramepoolDelete (a);
			}
		}
	}

	// Broadcasts should also be received by self
	Layer2VirtualSendFrame(frame);
}

void Layer2SwitchMaintainList (void)
{
	// Loop through cache to delete timed out entries
	// This loop seems to take between 10 and 20 microseconds when list is empty
	for (int i = 0; i < SWITCH_LISTSIZE; i++)
	{
		// If valid entry
		if (switchlist[i].timeout)
		{
			// Decrement time value
			switchlist[i].timeout--;

			// Now if it timed out
			if (!switchlist[i].timeout)
			{
				// Clear the entry
				switchlist[i].address = 0;
				switchlist[i].interface = 0;

				// Print debug message
				SWDEBUG_DELETELIST (NETWORK_PRINTI ("SW Cleared index %i\n", i));
			}
		}
	}
}

void Layer2SwitchClearInterface (Layer2 *interface)
{
	for (int i = 0; i < SWITCH_LISTSIZE; i++)
	{
		if (switchlist[i].interface == interface)
		{
			switchlist[i].address = 0;
			switchlist[i].interface = 0;
			switchlist[i].timeout = 0;
		}
	}
}
