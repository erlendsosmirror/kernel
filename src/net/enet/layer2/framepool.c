/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//
// Created by erlendjs on 14/04/16.
//

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "net/enet/layer2/framepool.h"
#include "../config/live/network-config.h"

#if NETWORK_DEBUG_FRAMEPOOL
#include NETWORK_DEBUG_INC
#endif

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#if NETWORK_DEBUG_FRAMEPOOL
	#define FRAMEPOOLDEBUG(str) str
#else
	#define FRAMEPOOLDEBUG(str)
#endif

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
DataFrame framepool[FRAMEPOOL_POOLSIZE];
unsigned char usage[FRAMEPOOL_POOLSIZE];

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void FramepoolInit (void)
{
	// Loop through slots and clear the usage flag
	for (int i = 0; i < FRAMEPOOL_POOLSIZE; i++)
		usage[i] = 0;
}

DataFrame *FramepoolNew (void)
{
	// Loop through slots to find a free one
	for (int i = 0; i < FRAMEPOOL_POOLSIZE; i++)
	{
		// Check flag
		if (!usage[i])
		{
			// Set flag
			usage[i] = 1;

			// Print debug message
			FRAMEPOOLDEBUG (NETWORK_PRINTI ("FP Allocating %i\n", i));

			// Clear the contents if enabled
#if FRAMEPOOL_ERASE
			for (int j = 0; j < 256+4; j++)
				((unsigned char*)&framepool[i])[j] = 0xFA;
#endif

			// Return its pointer
			return &framepool[i];
		}
	}

	// If there are no free slots, print message and return 0
	FRAMEPOOLDEBUG (NETWORK_PRINTS ("FP Allocation failed\n"));
	return 0;
}

void FramepoolDelete (DataFrame *frame)
{
	// Recalculate the index
#if __LP64__ == 1
	unsigned int i = (((unsigned long long) frame - (unsigned long long) framepool) / sizeof (DataFrame));
#else
	unsigned int i = (((unsigned int) frame - (unsigned int) framepool) / sizeof (DataFrame));
#endif

	// Print debug message
	FRAMEPOOLDEBUG (NETWORK_PRINTI ("FP Deleting %i\n", i));

	// Clear usage flag
	usage[i] = 0;
}
