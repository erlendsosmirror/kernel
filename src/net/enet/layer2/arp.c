/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//
// Created by erlendjs on 06/05/16.
//

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/string.h"
#include "net/enet/network.h"
#include "net/enet/layer2/framepool.h"
#include "net/enet/layer2/switch.h"
#include "net/enet/layer2/arp.h"
#include NETWORK_DELAY_INC

#if NETWORK_DEBUG_ARP
#include NETWORK_DEBUG_INC
#endif

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#if NETWORK_DEBUG_ARP
	#define ARPDEBUG(str) str
#else
	#define ARPDEBUG(str)
#endif

///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////

// Replies are communicated through these globals
unsigned char arp_gotflag;
unsigned short arp_foundaddress;

// The remote we are searching for
unsigned short arp_remoteprotocoladdress;

// My "IP" address
unsigned short arp_myprotocoladdress;

// Typecast structure for the payload segment in a frame
typedef struct ArpCast
{
	unsigned char hwtype;					// Always 0 (RS485)
	unsigned char protocoltype;				// Always 1 ("IP")
	unsigned char operation;				// 1 for request 2 for reply
	unsigned char reserved;					// Alignment, set to 0 to avoid illegal symbols (254 & 255)
	unsigned short senderhwaddress;			// When sending, set to my hardware address
	unsigned short senderprotocoladdress;	// When sending, set to my "ip" address
	unsigned short targethwaddress;			// When sending, set to 0 (unknown)
	unsigned short targetprotocoladdress;	// When sending, set to target "ip" address
	unsigned char eof;						// Set to PACKETSEPARATOR, added here for convenience
}ArpCast;

// If the cache table is enabled, define the entry type and a static table
typedef struct ArpTable
{
	unsigned short remoteprotocoladdress;
	unsigned short remotehardwareaddress;
}ArpTable;

#if ARP_USE_TABLE
ArpTable arptable[ARP_TABLE_SIZE];
#endif

///////////////////////////////////////////////////////////////////////////////
// Private function prototypes
///////////////////////////////////////////////////////////////////////////////
void ARPInit (unsigned short myprotocoladdress)
{
#if ARP_USE_TABLE
	MemSet (arptable, 0, sizeof (arptable));
#endif
	arp_myprotocoladdress = myprotocoladdress;
	arp_remoteprotocoladdress = 0;
}

unsigned short ARPResolve (unsigned short remoteprotocoladdress)
{
	// Do not try to resolve localhost!
	if (remoteprotocoladdress == 0)
		return LAYER2_ADDRESS;

	// Search the table if enabled
#if ARP_USE_TABLE
	for (int i = 0; i < ARP_TABLE_SIZE; i++)
		if (arptable[i].remoteprotocoladdress == remoteprotocoladdress)
			return arptable[i].remotehardwareaddress;
#endif

	// Found nothing in the table, do request for the address, allocate frame
	DataFrame *req = FramepoolNew ();

	// Fail if zero
	if (!req)
		return 0;

	// Set layer 2 data: Broadcast ARP packet from us
	req->destination = LAYER2_BROADCAST;
	req->source = LAYER2_ADDRESS;
	req->protocol = 2;
	req->checksum = ' ';

	// Set ARP data: Request hw address
	ArpCast *ptr = (ArpCast*) req->payload;
	ptr->hwtype = 0;
	ptr->protocoltype = 1;
	ptr->operation = 1;
	ptr->reserved = 0;
	ptr->senderhwaddress = LAYER2_ADDRESS;
	ptr->senderprotocoladdress = arp_myprotocoladdress;
	ptr->targethwaddress = 0;
	ptr->targetprotocoladdress = remoteprotocoladdress;
	ptr->eof = PACKETSEPARATOR;

	// Set layer 2 metadata
	req->size = DATAFRAME_HEADSIZE + sizeof(ArpCast);
	req->originatinginterface = 0;

	// Clear flag and set address
	arp_gotflag = 0;
	arp_remoteprotocoladdress = remoteprotocoladdress;

	// Print debug message
	ARPDEBUG (NETWORK_PRINTI ("ARP Sending request for %i\n", remoteprotocoladdress));

	// Give the frame to the switch, ownership transfer
	Layer2SwitchProcessFrame (req);

	// Wait for reply or timeout while pumping network
	unsigned int arp_timeout = PlatformGetTick();

	while ((PlatformGetTick () - arp_timeout) < ARP_TIMEOUT && !arp_gotflag)
		NetPump ();

	// Check for success
	if (arp_gotflag)
	{
		// Add to table or update if enabled
#if ARP_USE_TABLE
		for (int i = 0; i < ARP_TABLE_SIZE; i++)
		{
			if (arptable[i].remoteprotocoladdress == 0 ||
				arptable[i].remoteprotocoladdress == remoteprotocoladdress)
			{
				arptable[i].remoteprotocoladdress = remoteprotocoladdress;
				arptable[i].remotehardwareaddress = arp_foundaddress;
				break;
			}
		}
#endif

		// Print debug message and return the address
		ARPDEBUG (NETWORK_PRINTI ("ARP got hw %x\n", arp_foundaddress));
		return arp_foundaddress;
	}
	else
	{
		// Timeout, print debug message and return nothing
		ARPDEBUG (NETWORK_PRINTI ("ARP nothing for ip %i\n", remoteprotocoladdress));
		return 0;
	}
}

void ARPProcess (DataFrame *frame)
{
	// Cast the payload data
	ArpCast *ptr = (ArpCast*) frame->payload;

	// Print debug information: source hw and ip address
	ARPDEBUG (NETWORK_PRINTI ("ARP frame from %x\n", frame->source));
	ARPDEBUG (NETWORK_PRINTI ("   with ip %i\n", ptr->senderprotocoladdress));

	// Check for valid packet and if it is intended for this node
	if (ptr->hwtype == 0 &&
		 ptr->protocoltype == 1 &&
		 ptr->targetprotocoladdress == arp_myprotocoladdress)
	{
		// Check for request or reply
		if (ptr->operation == 1)
		{
			// Set layer 2 data: broadcast from us
			frame->destination = LAYER2_BROADCAST;
			frame->source = LAYER2_ADDRESS;

			// Set ARP data for reply with our information
			ptr->operation = 2;
			ptr->reserved = 0;
			ptr->targethwaddress = ptr->senderhwaddress;
			ptr->targetprotocoladdress = ptr->senderprotocoladdress;
			ptr->senderhwaddress = LAYER2_ADDRESS;
			ptr->senderprotocoladdress = arp_myprotocoladdress;
			ptr->eof = PACKETSEPARATOR;

			// Set layer 2 metadata
			frame->size = DATAFRAME_HEADSIZE + sizeof(ArpCast);
			frame->originatinginterface = 0;

			// Give frame to the switch, ownership transfer
			Layer2SwitchProcessFrame (frame);
			return;
		}
		else if (ptr->operation == 2 &&
			ptr->targethwaddress == LAYER2_ADDRESS &&
			ptr->senderprotocoladdress == arp_remoteprotocoladdress)
		{
			// Got reply, store the address and set the flag
			arp_foundaddress = ptr->senderhwaddress;
			arp_gotflag = 1;
		}
	}

	// The frame was not reused, free the memory
	FramepoolDelete (frame);
}
