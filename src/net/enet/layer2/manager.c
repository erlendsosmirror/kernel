/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/string.h"
#include "net/enet/layer2/manager.h"
#include "net/enet/layer2/framepool.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////

// Global receive fifo for pointers
DataFrame *rxframes[RS485_RXFIFO_SIZE];

// Indexes into the fifo
unsigned char rxframesread;
unsigned char rxframeswrite;

///////////////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////////////
void memcpy_helper (unsigned char *dst, unsigned char *src, unsigned int n)
{
	do
	{
		*dst = *src;
		dst++;
		src++;
		n--;
	} while (n);
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void Layer2GlobalInit (void)
{
	// Clear the pointer array and indexes
	MemSet (rxframes, 0, sizeof (rxframes));
	rxframesread = 0;
	rxframeswrite = 0;
}

DataFrame *Layer2GlobalPull (void)
{
	// Fetch the pointer at the read position
	DataFrame *frame = rxframes[rxframesread];

	// If there is anything at the read position
	if (frame)
	{
		// Clear position in the array
		rxframes[rxframesread] = 0;

		// Increment the read position with wrap-around
		if ((++rxframesread) == RS485_RXFIFO_SIZE)
			rxframesread = 0;
	}

	// Done
	return frame;
}

/*
 * Extract data from readfrompos (including) to readtopos (including)
 * readtopos points to the end-of-packet symbol, and readfrompos
 * points to the first byte in the packet.
 */
DataFrame *Layer2Extract (Layer2 *l)
{
	// Allocate frame
	DataFrame *frame = FramepoolNew ();

	// Did it work?
	if (frame)
	{
		// Number of bytes to memcpy
		unsigned int count;

		// If the packet wraps around the end
		if (l->readtopos < l->readfrompos)
		{
			// Count is the first part
			count = sizeof (l->rxbuffer) - l->readfrompos;

			// Calculate size of the packet
			unsigned int size = count + l->readtopos + 1;
			frame->size = (unsigned char) (size);

			// If too large, discard it
			if (size > 256)
			{
				FramepoolDelete (frame);
				return 0;
			}

			// Copy last part
			memcpy_helper((unsigned char*) frame + count, l->rxbuffer, l->readtopos + 1);
		}
		else
		{
			// Calculate size
			count = l->readtopos - l->readfrompos + 1;
			frame->size = (unsigned char) count;

			// If too large, discard it
			if (count > 256)
			{
				FramepoolDelete (frame);
				return 0;
			}
		}

		// Check size again
		if (frame->size < 7 || frame->size > 256)
		{
			// Report error and discard
			IFSTATS (l->stats.error_framesize++);
			FramepoolDelete(frame);
			return 0;
		}

		// Copy packet contents
		memcpy_helper((unsigned char*) frame, l->rxbuffer + l->readfrompos, count);

		// Check for invalid destination, discard
		if (frame->destination == 0)
		{
			FramepoolDelete (frame);
			return 0;
		}

		// Set the name of the interface it came from
		frame->originatinginterface = l->interfaceid;

		// Return the frame
		return frame;
	}
	else
	{
		// Allocation failed, log error
		IFDEBUG_OTHER (NETWORK_PRINTS ("Pump alloc err\n"));
		IFSTATS (l->stats.error_pumpallocerr++);

		// Done
		return 0;
	}
}

DataFrame *Layer2CheckPos (Layer2 *l)
{
	// DataFrame pointer
	DataFrame *frame = 0;

	// If read and write positions are inequal we need to process
	while (l->readtopos != l->writepos)
	{
		// Search for packet separator
		if (l->rxbuffer[l->readtopos] == PACKETSEPARATOR)
		{
			// Found separator, try extracting the packet
			// The packet starts at readfrompos and ends at readtopos
			frame = Layer2Extract (l);

			// Update readfrompos to the byte after the separator
			l->readfrompos = l->readtopos + 1;

			// Check wrap-around
			if (l->readfrompos == sizeof(l->rxbuffer))
				l->readfrompos = 0;
		}

		// Increment read position with wrap-around
		if ((++l->readtopos) == sizeof(l->rxbuffer))
			l->readtopos = 0;

		// Return the frame
		if (frame)
			return frame;
	}

	// Send packets in the transmit fifo if we can
	while (l->txframes[l->txframesread] && !Layer2SendFrameInternal (l, l->txframes[l->txframesread]))
	{
		// Clear that pointer
		l->txframes[l->txframesread] = 0;

		// Increment read position with wrap-around
		if ((++l->txframesread) == RS485_TXFIFO_SIZE)
			l->txframesread = 0;
	}

	// Done
	return 0;
}

int Layer2SendFrame (Layer2 *l, DataFrame *frame)
{
	// Set end
	((unsigned char*) frame)[frame->size-1] = PACKETSEPARATOR;

	// Send immediately if the transmitter is ready
	if (!l->txframes[l->txframesread] && !Layer2SendFrameInternal (l, frame))
		return 0;
	else
	{
		// If list has a free spot
		if (!l->txframes[l->txframeswrite])
		{
			// Add to list
			l->txframes[l->txframeswrite] = frame;

			// Increment index with wrap-around
			if ((++l->txframeswrite) == RS485_TXFIFO_SIZE)
				l->txframeswrite = 0;

			// Done
			return 0;
		}
		else
		{
			IFDEBUG_OTHER (NETWORK_PRINTS ("TX FIFO full\n"));
			IFSTATS (l->stats.error_txfifofull++);
			return 1;
		}
	}
}

void Layer2StoreFifo (Layer2 *l, DataFrame *frame)
{
	// Can we add to receive fifo?
	if (!rxframes[rxframeswrite])
	{
		// Yes, store it
		rxframes[rxframeswrite] = frame;

		// Increment the write index with wrap-around
		if ((++rxframeswrite) == RS485_RXFIFO_SIZE)
			rxframeswrite = 0;

		// Log statistics
		IFSTATS (l->stats.framesreceived++);
		IFSTATS (l->stats.bytesreceived += frame->size);
	}
	else
	{
		// No, report error and discard
		IFDEBUG_OTHER (NETWORK_PRINTS ("RX FIFO full\n"));
		IFSTATS (l->stats.error_rxfifofull++);
		FramepoolDelete(frame);
	}
}
