/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "process/process.h"
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "fs/pipe/pipe.h"
#include "fs/socket/socket.h"
#include "syscall/svc-basicfileops.h"
#include "syscall/svc-procops.h"
#include "print/print.h"
#include "kmod/kmod.h"
#include "posix/spawn.h"
#include "posix/unistd.h"
#include "posix/sys/wait.h"
#include "panic/panic.h"
#include "worker/worker.h"
#include "platform/platform.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
struct ProcessControl *prc;

///////////////////////////////////////////////////////////////////////////////
// Forward declarations
///////////////////////////////////////////////////////////////////////////////
void main (void);

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void InitProcess (void)
{
	// Re-open standard io
#if INIT_REOPENSTDIO
	close (1);
	close (0);
	open (INIT_REOPEN_FILENAME, 0);
	int fakeerrno = 0;
	int dup2args[4] = {0, 1, 0, (int) &fakeerrno};
	svc_dup2 (&prc->trc, dup2args);
#endif

	// Mount pipe
	vfs_mount ("/pipe", (struct VFSDriver*) &vfs_driver_pipe, 0);

	// Mount socket
	vfs_mount ("/socket", (struct VFSDriver*) &vfs_driver_socket, 0);

	// Initialize kernel modules
	for (struct KernelModule *i = &__kmod_list_start; i < &__kmod_list_end; ++i)
		i->init ();

	// Start kernel worker
	WorkerStart (prc);

	// For minimal configurations, go to a custom main func
#if INIT_CUSTOM_MINIMAL_MAIN == 1
	main ();
#endif

	// Start root shell
#if INIT_CUSTOM_MINIMAL_MAIN == 0
	pid_t pid;
	posix_spawnattr_t spawnattr = {POSIX_SPAWN_SETPGROUP, 0};
	char *argv[] = {"sh", 0};
	char *env[] = {"PATH=/bin", 0};
	int status;

	while (1)
	{
		if (posix_spawn (&pid, "/bin/sh", 0, &spawnattr, argv, env))
			Panic ("No shell\n");

		tcsetpgrp (0, pid);

		waitpid (pid, &status, 0);

		Print ("Root sh terminated, restarting\n");
	}
#endif
}

void InitInitialize (void)
{
	// Start init as the first proper process
	static unsigned int initstack[192];
	prc = ProcessCreateInternal (&InitProcess, initstack, 192);

	// Mount dev
	vfs_mount ("/dev", (struct VFSDriver*) &vfs_driver_dev, 0);

	// Open tty0
	int fakeerrno = 0;
	const char openfile[] = "/dev/tty0";
	int openargs[4] = {(int)openfile, 0, 0, (int) &fakeerrno};
	svc_open (&prc->trc, openargs);

	// dup2 0 -> 1
	int dup2args[4] = {0, 1, 0, (int) &fakeerrno};
	svc_dup2 (&prc->trc, dup2args);

	// Initialize memory
	PlatformInitMemory ();
}
