/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sched/list.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void ListInit (List *l)
{
	l->nitems = 0;
	l->current = 0;
	l->last = 0;
	l->stepback = 0;
}

void ListAdd (List *l, ListItem *it)
{
	if (l->nitems)
	{
		it->next = l->last->next;
		it->previous = l->last;
		l->last->next->previous = it;
		l->last->next = it;
	}
	else
	{
		it->next = it;
		it->previous = it;
		l->current = it;
		l->last = it;
	}

	it->container = l;
	l->nitems++;
}

void ListAddSorted (List *l, ListItem *it, unsigned int sortvalue)
{
	if (l->nitems)
	{
		ListItem *cur = l->current;

		while (cur->sortvalue < sortvalue)
		{
			cur = cur->next;
			if (cur == l->current)
				break;
		}

		// At this point, cur is either:
		//  * the only item with lower sort value, add after it, do not set current
		//  * the only item with higher sort value, add before it and set current
		//  * the first item because all items has lower values, add before it and do not set current
		//  * some item with higher or equal sort value, add before it and do not set current

		it->next = cur;
		it->previous = cur->previous;
		cur->previous->next = it;
		cur->previous = it;

		// Because the above code adds the item before the current item,
		// l->current has to be set when their sort values are equal also.
		// Otherwise the added item will appear at the end of the list
		// and the process will appear to "lag".
		if (sortvalue <= l->current->sortvalue)
			l->current = it;
	}
	else
	{
		it->next = it;
		it->previous = it;
		l->current = it;
		l->last = it;
	}

	it->sortvalue = sortvalue;
	it->container = l;
	l->nitems++;
}

void ListRemove (ListItem *it)
{
	List *l = it->container;

	if (l->nitems == 1)
	{
		l->nitems = 0;
		l->current = 0;
		l->last = 0;
	}
	else
	{
		it->previous->next = it->next;
		it->next->previous = it->previous;

		if (l->last == it)
			l->last = it->previous;

		if (l->current == it)
		{
			// Bugfix: Prevent double-jump by going back instead of forwards
			// Problem: If SchedState removes the current process from the run
			// list it also triggers a reschedule. If SchedState advanced the
			// 'current' by one spot the scheduler will do it again,
			// effectively skipping over a thread. In bad cases this may cause
			// a deadlock. The run lists should have stepback set true so that
			// removing a thread sets the current one step back, and the
			// scheduler steps one step forward before context switching.
			if (l->stepback)
				l->current = it->previous;
			else
				l->current = it->next;
		}

		l->nitems--;
	}
}
