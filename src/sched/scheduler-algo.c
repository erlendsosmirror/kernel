/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sched/scheduler.h"
#include "sched/scheduler-signal.h"
#include "panic/panic.h"

///////////////////////////////////////////////////////////////////////////////
// External variables
///////////////////////////////////////////////////////////////////////////////
extern List listrun[SCHED_NPRIORITIES];

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

/*
 * Used by the context switch interrupt to select the next process to execute
 */
void SchedSelectNext (void)
{
	// Always do the selection in a critical section
	SchedEnterCritical ();

	// Special handling if signal state is set
	if (currentthc->sigstate)
	{
		// Process must be in the run list
		// TODO: Handle problems here
		if (currentthc->state != SCHED_STATUS_RUN)
			Panic ("Not in run state");

		// Leave signal processing if 1, enter if 2
		if (currentthc->sigstate == 1)
		{
			// Restore stack
			currentthc->stacktop = currentthc->stackstored;
			currentthc->stackstored = 0;
			currentthc->sigstate = 0;

			// If there are more signals pending, we need to re-enter
			if (currentthc->sigpending)
			{
				for (int i = 0; i < 32; i++)
				{
					if (currentthc->sigpending & (1 << i))
					{
						currentthc->param1 = i + 1;
						SchedSignalStackSetup (currentthc);
						break;
					}
				}
			}
		}
		else
			SchedSignalStackSetup (currentthc);
	}
	else
	{
		// Regular scheduling, select next of highest priority
		unsigned int toppri = 0;

		// TODO: Fix bad stuff happening if medium pri is empty but high is not
		while (listrun[toppri].nitems == 0)
			toppri++;

		listrun[toppri].current = listrun[toppri].current->next;
		currentthc = listrun[toppri].current->parent;
	}

	// Exit critical section
	SchedExitCritical ();
}
