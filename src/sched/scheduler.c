/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sched/scheduler.h"
#include "mpu/mpu.h"
#include "platform/platform.h"
#include "panic/panic.h"
#include "process/process.h"
#include "worker/worker.h"

///////////////////////////////////////////////////////////////////////////////
// Local variables
///////////////////////////////////////////////////////////////////////////////
List listrun[SCHED_NPRIORITIES];
List listblocked;
List listsleeping;
List listwork;
struct ThreadControl *currentthc;

///////////////////////////////////////////////////////////////////////////////
// External platform specific functions
///////////////////////////////////////////////////////////////////////////////
void SchedStartAsm (void);
void InitializeStack (struct ThreadControl *t);

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void SchedInitialize (void)
{
	for (int i = 0; i < SCHED_NPRIORITIES; i++)
	{
		ListInit (&listrun[i]);
		listrun[i].stepback = 1;
	}

	ListInit (&listblocked);
	ListInit (&listsleeping);
	ListInit (&listwork);

	currentthc = 0;
}

void SchedStart (void)
{
	// MPU Setup
	MpuInitialSetup ();

	// Start system tick
	PlatformInitTick ();

	// Set the stack back to reset state and call svc 0
	// This does not return
	SchedStartAsm ();
}

void SchedAddThread (struct ThreadControl *t,
	void (*entry)(void),
	unsigned int *stack,
	int stacksize,
	int priority)
{
	t->stacktop = stack + stacksize - 1;
	t->listentry.parent = t;

	t->entrypoint = entry;

	t->priority = priority;

	t->sigh = 0;
	t->sigt = 0;
	t->stackstored = 0;
	t->sigstate = 0;
	t->sigmask = 0;
	t->sigpending = 0;

	t->state = SCHED_STATUS_RUN;
	t->statechanged = 0;

	InitializeStack (t);
	t->entrypoint = 0;

	SchedEnterCritical ();

	ListAdd (&listrun[priority], &t->listentry);

	if (!currentthc)
		currentthc = t;

	SchedExitCritical ();
}

void SchedRemoveProcess (struct ThreadControl *thc)
{
	SchedEnterCritical ();

	if (thc->listentry.container)
		ListRemove (&thc->listentry);

	SchedExitCritical ();

	thc->listentry.container = 0;
	thc->state = SCHED_STATUS_ZOMBIE;
	thc->statechanged = 1;

	if (currentthc == thc)
		SchedYield ();
}

void SchedState (
	struct ThreadControl *thc,
	unsigned char newstate,
	int (*callback)(struct ThreadControl*, unsigned int),
	unsigned int args,
	unsigned int time)
{
	// Disable IRQ prior to modifying lists
	SchedEnterCritical ();

	// Illegal state check. Index with current, output are legal state mask
	static const char legal[16] = {
		0x0F, 0x18, 0, 0,	// R/H, B, Z, illegal
		0x18, 0, 0, 0,		// S, illegal, illegal, illegal
		0x10, 0x09, 0, 0,	// T, B&T, illegal, illegal
		0, 0, 0, 0};		// illegal, illegal, illegal, illegal (change from crashed)

	if (!(legal[thc->state] & (newstate ? newstate : 0x10)))
		Panic ("Illegal state change");

	// Check wanted state change
	if (newstate == SCHED_STATUS_STOPPED)
	{
		// Legal entry states: run/handler, blocked, sleeping, blocked&stopped
		// If old state is sleeping, we need to abort that by calling the callback
		if (thc->state & SCHED_STATUS_SLEEPING)
		{
			if (thc->callback)
				thc->callback (thc, thc->param2);
			thc->callback = 0;
			thc->state &= ~SCHED_STATUS_SLEEPING;
		}

		// Change the state, to T or B&T
		thc->state |= SCHED_STATUS_STOPPED;

		// Remove ourselves from list and add ourselves to blocked list
		ListRemove (&thc->listentry);
		ListAddSorted (&listblocked, &thc->listentry, args);
	}
	else if (newstate == SCHED_STATUS_BLOCKED)
	{
		// Legal entry states: run/handler, stopped
		// These state changes are results of syscalls, set the callback
		thc->callback = callback;
		thc->param2 = args;

		// Use param3/time to identify the syscall that blocked
		thc->param3 = time;

		// Change the state to B
		thc->state = SCHED_STATUS_BLOCKED;

		// Remove ourselves from ready list and add ourselves to blocked list
		ListRemove (&thc->listentry);

		if (callback)
			ListAddSorted (&listwork, &thc->listentry, args);
		else
			ListAddSorted (&listblocked, &thc->listentry, args);
	}
	else if (newstate == SCHED_STATUS_SLEEPING)
	{
		// These changes are the result of either nanosleep or kernel threads sleeping
		// themselves, and may only change from the running state. Callback is optional,
		// if not used set to zero. Argument should be the time to wait.
		thc->callback = callback;
		thc->param2 = args;

		// Change state to S
		thc->state = SCHED_STATUS_SLEEPING;

		// Remove ourselves from ready list and add ourselves to sleeping list
		ListRemove (&thc->listentry);

		if (time != 0xFFFFFFFF)
			ListAddSorted (&listsleeping, &thc->listentry, systemTick + time);
		else
			ListAddSorted (&listblocked, &thc->listentry, args);
	}
	else if (newstate == SCHED_STATUS_RUN)
	{
		// Want to go back to running, but we need to check that we are not in the
		// B&S state. If registered callback is nonzero, we know that we came from
		// S, B or B&T. If we are in the B&T state we cannot know if we are
		// continuing from SIGCONT or syscall, so we use args to indicate that
		// the callback should be called. Argument should be set to the state we
		// are leaving.
		if (thc->callback && args)
		{
			thc->callback (thc, thc->param2);
			thc->callback = 0;
		}

		thc->state &= ~args;

		// At this point we're either ready for the running state, or still in the
		// blocked state. So we check if the state is indeed running, and put
		// it in the running list if that is the case. Otherwise just stay in the
		// T or B list.
		if (thc->state == SCHED_STATUS_RUN)
		{
			if (thc->listentry.container)
				ListRemove (&thc->listentry);
			ListAdd (&listrun[thc->priority], &thc->listentry);
		}
	}

	// Set our state
	thc->statechanged = 1;

	// Enable IRQ
	SchedExitCritical ();

	// Yield
	if (currentthc == thc)
		SchedYield ();
}

struct ThreadControl *SchedFetchWork (void)
{
	struct ThreadControl *ret = 0;

	SchedEnterCritical ();

	do
	{
		if (listwork.nitems)
		{
			ListItem *current = listwork.current;
			ListRemove (current);
			current->container = 0;
			ret = current->parent;
		}
		else
		{
			SchedState (currentthc, SCHED_STATUS_SLEEPING, 0, 0, 0xFFFFFFFF);
			SchedExitCritical ();
		}
	}
	while (!ret);

	SchedExitCritical ();

	return ret;
}

void SchedSystickHandler (void)
{
	SchedEnterCritical ();

	while (listsleeping.nitems)
	{
		ListItem *it = listsleeping.current;

		// This holds because we increment it syncronously
		if (systemTick == it->sortvalue)
			SchedState (it->parent, SCHED_STATUS_RUN, 0, SCHED_STATUS_SLEEPING, 0);
		else
			break;
	}

	SchedExitCritical ();
	SchedYield ();
}

void SchedLeaveCurrentProcessSignal (void)
{
	// Ensure that we are in signal processing state
	if (currentthc->stackstored)
	{
		// Tell the scheduler algorithm that we need to set the stack back
		currentthc->sigstate = 1;
		SchedYield ();
	}
}

void SchedCrashRecover (unsigned int *stack, int stacktype, int faulttype)
{
	// Delete from all lists. Yield and zombie state is done here.
	SchedRemoveProcess (currentthc);

	// Mark as crashed
	currentthc->state = SCHED_STATUS_CRASHED;

	// Make sure top of stack is stored
	currentthc->stacktop = stack;

	// Store platform specific registers
	PlatformStoreCrashRegs (currentthc);

	// Re-use sigpending as faulttype
	currentthc->sigpending = faulttype;

	// Add deferred handler to work list
	currentthc->callback = ProcessCrashRecover;
	ListAddSorted (&listwork, &currentthc->listentry, 0);
	WorkerUnblock ();
}
