/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sched/scheduler.h"
#include "sched/scheduler-signal.h"
#include "panic/panic.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int SchedPostSignal (struct ThreadControl *thc, int sig)
{
	// Calculate signal bit
	int signal = 1 << (sig - 1);

	// Check mask and function pointers, return 1 if signaling is not possible
	if (!(thc->sigmask & signal) || !thc->sigh || !thc->sigt)
		return 1;

	// Disable interrupts
	SchedEnterCritical ();

	// Set signal to pending
	thc->sigpending |= signal;

	// Enter signal
	if (thc->stackstored)
	{
		// The thread is already in the signal processing state, so set the
		// pending bit. sigstate can be in either 0 state or 1 state. If 0,
		// it will eventually be set 1 by the trampoline and the scheduler
		// will check for pending signals after restoring the stack. If 1,
		// the same will happen. It cannot be 2 because if it is, the scheduler
		// has not been involved yet and has not gotten to setting the stack.
	}
	else if (thc == currentthc)
	{
		// The stack has not been stored (not in signal processing state), and
		// the target is also the current process. Set sigstate to 2 so that
		// the scheduler will set the stack up for us when "clean", and yield.
		// sigstate may already be 2, in that case the last one is delivered
		// first and the rest is up to the scheduler when it checks for pending
		// signals. sigstate may be 1 if the signal was posted after the
		// trampoline but before the scheduler was invoked. In that case, do
		// not set sigstate and let the scheduler set us up after restoring the
		// stack. sigstate will be 0 if in normal processing state, when in
		// signal processing state the previous "if" would have been invoked.
		if (thc->sigstate != 1)
			thc->sigstate = 2;

		thc->param1 = (unsigned int) sig;
		SchedYield ();
	}
	else
	{
		// The stack has not been stored and the process sending the signal
		// (current) is not the target. We may the set the stack up immediately
		// and the process will continue normally, but execute the signal once
		// it is scheduled. In the meantime, any further calls to PostSignals
		// will end up in the first "if" check.
		thc->param1 = (unsigned int) sig;
		SchedSignalStackSetup (thc);
	}

	// Wake the thread if sleeping
	if (thc->state & SCHED_STATUS_SLEEPING)
		SchedState (thc, SCHED_STATUS_RUN, 0, SCHED_STATUS_SLEEPING, 0);

	SchedExitCritical ();
	return 0;
}
