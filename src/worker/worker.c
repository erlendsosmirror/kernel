/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WORKER_H
#define WORKER_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "worker/worker.h"
#include "sched/scheduler.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
// TODO: Check required stack space
unsigned int workerstack[256];
struct ThreadControl workerthread;

///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
void WorkerThread (void)
{
	while (1)
	{
		// Fetch work from queue, note that this function blocks until
		// work is available.
		struct ThreadControl *prc = SchedFetchWork ();

		// Call the work function
		if (!prc->callback (prc, prc->param2))
		{
			// If not crashed:
			// Erase the function pointer so that SchedState does not call it
			prc->callback = 0;

			// Unblock the thread we did work for
			SchedState (prc, SCHED_STATUS_RUN, 0, SCHED_STATUS_BLOCKED, 0);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void WorkerStart (struct ProcessControl *parent)
{
	// Privileged thread
	workerthread.privileged = 0;

	// Create and start worker thread
	workerthread.pctrl = parent;
	SchedAddThread (&workerthread, &WorkerThread, workerstack, 256, SCHED_PRIORITY_MEDIUM);
}

void WorkerUnblock (void)
{
	SchedEnterCritical ();

	if (workerthread.state & SCHED_STATUS_SLEEPING)
		SchedState (&workerthread, SCHED_STATUS_RUN, 0, SCHED_STATUS_SLEEPING, 0);

	SchedExitCritical ();
}

#endif
