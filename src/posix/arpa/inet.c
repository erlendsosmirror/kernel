/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "posix/arpa/inet.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
unsigned int htonl (unsigned int in)
{
	unsigned int ret = 0;
	ret |= (in & 0xFF000000) >> 24;
	ret |= (in & 0x00FF0000) >> 8;
	ret |= (in & 0x0000FF00) << 8;
	ret |= (in & 0x000000FF) << 24;
	return ret;
}

unsigned short htons (unsigned short in)
{
	unsigned short ret = 0;
	ret |= (in & 0xFF00) >> 8;
	ret |= (in & 0x00FF) << 8;
	return ret;
}

unsigned int ntohl (unsigned int in)
{
	unsigned int ret = 0;
	ret |= (in & 0xFF000000) >> 24;
	ret |= (in & 0x00FF0000) >> 8;
	ret |= (in & 0x0000FF00) << 8;
	ret |= (in & 0x000000FF) << 24;
	return ret;
}

unsigned short ntohs (unsigned short in)
{
	unsigned short ret = 0;
	ret |= (in & 0xFF00) >> 8;
	ret |= (in & 0x00FF) << 8;
	return ret;
}

in_addr_t inet_addr (const char *str)
{
	// TODO
	return 0;
}

char *inet_ntoa (struct in_addr in)
{
	// TODO
	return "0.0.0.0";
}

const char *inet_ntop (int, const void *restrict, char *restrict, socklen_t);
int inet_pton (int, const char *restrict, void *restirct);
