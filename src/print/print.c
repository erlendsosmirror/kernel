/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdarg.h>
#include "mem/string.h"
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "process/process-allocate.h"

// Note: If we do not want to depend on the stdarg.h file in the future,
// we could just prepend the va_ stuff with __builtin_

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void IntToStr (int n, char *str)
{
	int sign = n;

	if (n < 0)
		n = -n;

	int i = 0;

	do {
		str[i++] = '0' + n % 10;
	} while ((n /= 10));

	if (sign < 0)
		str[i++] = '-';

	str[i] = 0;
	Reverse(str);
}

void UintToHex (unsigned int in, char *out)
{
	// http://stackoverflow.com/questions/311165/how-do-you-convert-byte-array-to-hexadecimal-string-and-vice-versa
	unsigned char b;

	b = (in >> 28) & 0x0F;
	out[0] = (55 + b + (((b-10)>>31)&-7));

	b = (in >> 24) & 0x0F;
	out[1] = (55 + b + (((b-10)>>31)&-7));

	b = (in >> 20) & 0x0F;
	out[2] = (55 + b + (((b-10)>>31)&-7));

	b = (in >> 16) & 0x0F;
	out[3] = (55 + b + (((b-10)>>31)&-7));

	b = (in >> 12) & 0x0F;
	out[4] = (55 + b + (((b-10)>>31)&-7));

	b = (in >> 8) & 0x0F;
	out[5] = (55 + b + (((b-10)>>31)&-7));

	b = (in >> 4) & 0x0F;
	out[6] = (55 + b + (((b-10)>>31)&-7));

	b = (in >> 0) & 0x0F;
	out[7] = (55 + b + (((b-10)>>31)&-7));

	out[8] = 0;
}

void PrintString (char *str)
{
	// Get length
	int len = StrLen (str);

	// Working vars
	int n = len;
	int pos = 0;

	/*
	 * Get file node and function pointer. The output should be at nodetable
	 * index 0 or 1, depending on dup2 being used or not. Use PID 1's file
	 * descriptor 1 (standard output). Since the idle thread is started as a
	 * thread, the init process will be at index 0.
	 */
	struct FileNode *fnode = &nodetable[processdata[0].fnodes[1]];
	int (*write)(struct FileNode*, void*, unsigned int) = fnode->dev->write;

	// Write while there is more data
	do
	{
		int diff = write (fnode, str + pos, n);

		if (diff < 0)
			return;

		pos += diff;
		n -= diff;
	} while (pos != len);
}

void Print (const char *format, ...)
{
	// Initialize argument list
	va_list list;
	va_start (list, format);

	// Initialize output data and duplicate a pointer
	char output[128];
	MemSet (output, 0, sizeof (output));
	char *out = output;

	// Process the format string
	for (; *format; format++)
	{
		if (*format == '%')
		{
			format++;

			if (*format == 'i')
			{
				int arg = va_arg (list, int);
				IntToStr (arg, out);
			}
			else if (*format == 'x')
			{
				unsigned int arg = va_arg (list, unsigned int);
				UintToHex (arg, out);
			}
			else
				*out = *format;
		}
		else
			*out = *format;

		while (*out) out++;
	}

	// Print the string and exit
	PrintString (output);
	va_end (list);
}
