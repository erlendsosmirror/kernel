/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void StrCpy (char *dst, const char *src)
{
	while (*src)
	{
		*dst++ = *src++;
	}

	*dst = 0;
}

void StrCat (char *dst, const char *src)
{
	while (*dst) dst++;

	StrCpy (dst, src);
}

void MemSet (void *dst, int val, unsigned int nbytes)
{
	for (unsigned int i = 0; i < nbytes; i++)
		((unsigned char*)dst)[i] = (unsigned char) val;
}

void *memset (void *dst, int val, unsigned int nbytes)
{
	MemSet (dst, val, nbytes);
	return dst;
}

void MemCpy (void *dst, void *src, unsigned int nbytes)
{
	for (unsigned int i = 0; i < nbytes; i++)
		((unsigned char*)dst)[i] = ((unsigned char*)src)[i];
}

int memcpy (void *dst, void *src, unsigned int nbytes)
{
	MemCpy (dst, src, nbytes);
	return nbytes;
}

int MemCmp (const void *dst, const void *src, unsigned int nbytes)
{
	const char *a = (const char*) dst;
	const char *b = (const char*) src;

	for (unsigned int i = 0; i < nbytes; i++)
	{
		if (a[i] < b[i])
			return -1;
		else if (a[i] > b[i])
			return 1;
	}

	return 0;
}

int StrCmp (const char *a, const char *b)
{
	while (*a == *b && *a)
	{
		a++;
		b++;
	}

	if (!*a && !*b)
		return 0;
	else if (*a < *b)
		return -1;
	else
		return 1;
}

int StrLen (const char *s)
{
	int l = 0;
	while (*s++) l++;
	return l;
}

int strlen (const char *s)
{
	return StrLen (s);
}

void Reverse (char *s)
{
	int l = StrLen (s);

	for (int i = 0, j = l-1; i < j; i++, j--)
	{
		char c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

char *strtok (char *in, char *match)
{
	static char *current;

	if (in)
		current = in;

	char *out = current;

	while (*current && *current != *match)
		current++;

	if (*current == *match)
	{
		*current = 0;
		current++;
		return out;
	}
	else
		return out;
}

int atoi (char *in)
{
	Reverse (in);

	int len = StrLen (in);
	int ret = 0;
	int po = 1;

	for (int i = 0; i < len; i++)
	{
		if (in[i] == '-')
			return -ret;
		else
			ret += (in[i] - '0') * po;

		po *= 10;
	}

	return ret;
}
