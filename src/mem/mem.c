/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/mem.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
struct MemBank *membanklist = 0;

///////////////////////////////////////////////////////////////////////////////
// Helper functions
///////////////////////////////////////////////////////////////////////////////
struct MemBank *MemGetBank (unsigned int bankno)
{
	struct MemBank *membank = membanklist;

	for (int i = 0; i <= bankno; i++)
		if (i == bankno)
			return membank;
		else if (membank->nextbank)
			membank = membank->nextbank;

	return membank;
}

struct MemBank *MemFindBank (unsigned int address)
{
	struct MemBank *membank = membanklist;

	do
	{
		if (address >= membank->bankaddress && address < membank->lastaddr)
			return membank;

		membank = membank->nextbank;
	} while (membank);

	return 0;
}

unsigned int MemSplit (struct MemBank *b, struct MemTableEntry *current,
	unsigned int nbytes, int pos)
{
	// Find an unused entry to split into
	int foundi = 0;

	for (int i = 0; i < MEM_LISTSIZE; i++)
	{
		if (!b->memlist[i].addr)
		{
			foundi = i;
			break;
		}
	}

	// Check
	if (!foundi)
		return 0;

	// Get the entry
	struct MemTableEntry *found = &b->memlist[foundi];

	// Calculate the middle address
	unsigned int middle = current->addr + nbytes;

	// Tricky part: New entry has to point to our current next
	found->nextaddr = current->nextaddr;
	found->addr = middle;
	b->memilist[foundi] = b->memilist[pos];

	// Point ourselves to the new entry
	current->nextaddr = middle;
	b->memilist[pos] = foundi;
	return 1;
}

unsigned int MemAllocEntry (struct MemBank *b, struct MemTableEntry *current,
	unsigned int blocksize, unsigned int nbytes, int pos)
{
	// Copy
	unsigned int addr = current->addr;

	// Check if we can split the block
	if (blocksize >= nbytes)
		if (!MemSplit (b, current, nbytes, pos))
			return 0;

	// Set bit to indicate that the block is used
	current->addr |= MEM_INUSEBIT;

	// Update statistics and return
	b->currentfree -= nbytes;

	if (b->currentfree < b->lowestfree)
		b->lowestfree = b->currentfree;

	return addr;
}

unsigned int MemAllocMpuFindMinimal (unsigned int nbytes)
{
	// Increment size by power of two
	for (int i = 0; i < 32; i++)
	{
		// Calculate actual size
		int regionsize = 1 << i;

		// Return if it is large enough
		if (regionsize >= nbytes)
			return i;
	}

	// Error
	return 0;
}

unsigned int MemAllocMpuCalcRASR (unsigned int dataaddr,
	unsigned int regionaddr,
	unsigned int subregsize,
	unsigned int nspare)
{
	// Calculate subregion disable bits
	unsigned int start = (dataaddr - regionaddr) / subregsize;
	unsigned int nreg = 8 - nspare;
	unsigned int reg = 0;

	for (int i = 0; i < nreg; i++)
		reg |= 0x0100 << (start + i);

	return ~reg & 0x0000FF00;
}

void MemMerge (struct MemBank *b, unsigned char from, unsigned char to)
{
	// Get current
	struct MemTableEntry *current = &b->memlist[from];

	// Get target entry
	struct MemTableEntry *foundentry = &b->memlist[to];

	// Check that they are in use
	if (!(foundentry->addr & MEM_INUSEBIT) && !(current->addr & MEM_INUSEBIT))
	{
		// 'Consume' the next entry, set our nextaddr and ilist to its, and invalidate it
		current->nextaddr = foundentry->nextaddr;
		b->memilist[from] = b->memilist[to];

		foundentry->addr = 0;
		foundentry->nextaddr = 0;
		b->memilist[to] = 255;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void MemBankInit (struct MemBank *b, unsigned int address, unsigned int size)
{
	b->bankaddress = address;
	b->banksize = size;

	for (int i = 0; i < MEM_LISTSIZE; i++)
	{
		b->memlist[i].addr = 0;
		b->memlist[i].nextaddr = 0;
		b->memilist[i] = 255;
	}

	b->currentfree = size;
	b->lowestfree = size;
	b->lastaddr = address + size;

	b->memlist[0].addr = address;
	b->memlist[0].nextaddr = b->lastaddr;

	b->nextbank = 0;
}

void MemAddBank (struct MemBank *bank)
{
	if (!membanklist)
		membanklist = bank;
	else
	{
		struct MemBank *membank = membanklist;

		do
		{
			if (!membank->nextbank)
			{
				membank->nextbank = bank;
				return;
			}

			membank = membank->nextbank;
		} while (membank);
	}
}

unsigned int MemAlloc (unsigned int nbytes, unsigned int bankno)
{
	// Check for invalid argument
	if (!nbytes)
		return 0;

	// Get bank
	struct MemBank *b = MemGetBank (bankno);

	if (!b)
		return 0;

	// Align the size
	nbytes = (nbytes + 3) & ~3;

	// Search list for free entries
	// TODO: Use for loop
	int pos = 0;

	do
	{
		// Get the current memory block
		struct MemTableEntry *current = &b->memlist[pos];

		// Check if it is used and if it is the last block
		if (current->addr & MEM_INUSEBIT)
		{
			if (current->nextaddr == b->lastaddr)
				return 0;

			pos = b->memilist[pos];
			continue;
		}

		// Get the size of the block and allocate if large enough
		unsigned int blocksize = (current->nextaddr & ~MEM_INUSEBIT) - current->addr;

		if (blocksize >= nbytes)
			return MemAllocEntry (b, current, blocksize, nbytes, pos);

		// Next
		pos = b->memilist[pos];
	} while (pos != 255);

	// No good block found
	return 0;
}

unsigned int MemAllocMpu (unsigned int nbytes, unsigned int bankno, struct MemMpuReturn *mpu)
{
	// Check for invalid argument
	if (!nbytes)
		return 0;

	// Get bank
	struct MemBank *b = MemGetBank (bankno);

	if (!b)
		return 0;

	// Align the size
	nbytes = (nbytes + 3) & ~3;

	// Get the nearest power of two that will fit nbytes
	// Worst case it is nbytes*2-1 bytes
	unsigned int mpuminimal = MemAllocMpuFindMinimal (nbytes);
	unsigned int regionsize = 1 << mpuminimal;

	// Minimal allocation size and its subregion size
	unsigned int minalloc = regionsize;
	unsigned int subreg = regionsize / 8;

	// We may disable unused subregions to save memory, but
	// the allocation has to fill the entire last subregion
	if (minalloc > 128)
	{
		// Round nbytes to nearest multiple of subregion size
		unsigned int n = nbytes / subreg;
		n = n * subreg;

		// If it was rounded down, add a subregions worth of memory
		if (n < nbytes)
			n += subreg;

		minalloc = n;
	}

	// At this point, we know that
	//  1. The memory block has to be aligned with regionsize
	//  2. minalloc has been rounded to subregion border size

	// Number of free subregions
	unsigned int nspare = (regionsize - minalloc) / subreg;

	// Calculate mask
	unsigned int mask = ~(regionsize - 1);

	// Search list for free entries
	// TODO: Use for loop
	int pos = 0;

	do
	{
		// Get the current memory block
		struct MemTableEntry *current = &b->memlist[pos];

		// Check if it is used and if it is the last block
		if (current->addr & MEM_INUSEBIT)
		{
			if (current->nextaddr == b->lastaddr)
				return 0;

			pos = b->memilist[pos];
			continue;
		}

		// Get the size of the block and allocate if large enough
		unsigned int blocksize = (current->nextaddr & ~MEM_INUSEBIT) - current->addr;

		// Copy address
		unsigned int addr = current->addr;

		// Calculate region start address
		unsigned int regionaddr = addr & mask;

redo:
		// Check if it will fit
		if (blocksize >= minalloc)
		{
			// Add up to nspare subregions until we are past the block start
			unsigned int addrc = regionaddr;

			for (int i = 0; i < nspare; i++)
				if (addrc < addr)
					addrc += subreg;

			// If addrc is still less than addr, we cannot use this block since
			// it does not align with the MPU start address
			if (addrc < addr)
			{
				// Go to the next region block, and add the gap to the allocation size
				regionaddr += regionsize;
				minalloc += regionaddr - addr;
				goto redo;
			}

			// Add wasted subregions to the allocation amount and check
			// that the block is still large enough
			minalloc += addrc - regionaddr;

			if (blocksize < minalloc)
				goto redo;

			// Set output
			mpu->dataptr = addrc;
			mpu->mpu_rbar = regionaddr;
			mpu->mpu_rasr = MemAllocMpuCalcRASR (addrc, regionaddr, subreg, nspare);
			mpu->mpu_rasr |= ((mpuminimal - 1) << 1);

			// Check for split
			if (blocksize >= minalloc)
				if (!MemSplit (b, current, minalloc, pos))
					return 0;

			// Set bit in structure
			current->addr |= MEM_INUSEBIT;

			// Update stats
			b->currentfree -= nbytes;

			if (b->currentfree < b->lowestfree)
				b->lowestfree = b->currentfree;

			// Clear memory and return
			for (unsigned int i = addr; i < (addr + minalloc); i += 4)
				*((unsigned int*)i) = 0;

			return addr;
		}

		// Next
		pos = b->memilist[pos];
	} while (pos != 255);

	// No good block found
	return 0;
}

int MemFree (unsigned int addr)
{
	// Find the memory bank
	struct MemBank *b = MemFindBank (addr);

	if (!b)
		return 1;

	// Loop for the address
	for (int i = 0; i < MEM_LISTSIZE; i++)
	{
		// Get current entry and address
		struct MemTableEntry *current = &b->memlist[i];
		unsigned int entryaddr = current->addr & ~MEM_INUSEBIT;

		// Is it our memory block?
		if (entryaddr == addr)
		{
			// Clear in-use bit and update stats
			current->addr = entryaddr;
			b->currentfree += current->nextaddr - entryaddr;

			// Get the next index and check for merge
			unsigned char nexti = b->memilist[i];

			if (nexti != 255)
				MemMerge (b, i, nexti);

			// Also check for backwards merge
			for (int j = 0; j < MEM_LISTSIZE; j++)
			{
				if (b->memilist[j] == i)
				{
					MemMerge (b, j, i);
					break;
				}
			}

			return 0;
		}
	}

	return 0;
}
