/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/mem.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
unsigned short MemTestBlock (unsigned int start, unsigned int end, unsigned short *testarr)
{
	for (unsigned int i = start; i < end; i += 512)
	{
		volatile unsigned short *r = (volatile unsigned short*) i;

		for (unsigned int data = 0; data < 256; data++)
			r[data] = testarr[data];

		for (unsigned int data = 0; data < 256; data++)
		{
			if (r[data] != testarr[data])
				return r[data] ^ testarr[data];
		}
	}

	return 0;
}

int MemTest (unsigned int addr, unsigned int nbytes)
{
	unsigned short testarr[256];
	unsigned int div = nbytes / 16;

	for (unsigned int i = 0; i < 256; i++)
		testarr[i] = (~i & 0x00FF) | (i << 8);

	for (unsigned int s = 0; s < 16; s++)
	{
		unsigned int start = addr + (s * div);
		unsigned int end = addr + ((s + 1) * div);

		unsigned short result = MemTestBlock (start, end, testarr);

		if (result)
			return 0;
	}

	return 1;
}
