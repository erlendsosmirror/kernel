/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "posix/errno.h"
#include "mem/string.h"
#include "fs/vfs.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////

// Pointers to vfs drivers, that may be constants
const struct VFSDriver *vfsdrivers[VFS_MAX_DRIVERS];

// Flags indicating if the pointers above are in use
char vfsdriverinuse[VFS_MAX_DRIVERS] = {0};

// Mount point strings
char mountpoints[VFS_MAX_DRIVERS][VFS_MOUNTPOINT_MAX_SIZE];

// Some drivers may need to prepend something to their path.
// e.g. fatfs expects "0:/" for the first drive.
char prepends[VFS_MAX_DRIVERS][VFS_PREPEND_MAX_SIZE];

// These are for opendir, closedir and readdir
// To be replaced by a proper thread-safe api
const struct VFSDriver *dirdrv = 0;
int vfsdircur;
char vfsabs[VFS_MAX_PATH];

// Node table
struct FileNode nodetable[NODETABLE_SIZE];

///////////////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////////////
/*
 * Special string compare function that returns the number of matching
 * characters, considering forward slashes.
 */
int vfs_samefolder (const char *a, const char *b)
{
	// Counter
	int n = 0;

	// Regular string compare that counts for every equal character
	while (*a && *a == *b)
	{
		a++;
		b++;
		n++;
	}

	// Return number of matching characters
	if (!*a && (!*b || *b == '/' || n == 1))
		return n;
	return 0;
}

/*
 * Resolve the given path into a vfs driver and the appropriate path for
 * that driver.
 */
const struct VFSDriver *vfs_resolve (const char *pathin, char *pathout)
{
	// Number of matched characters in the path
	int bestmatch = 0;

	// The index of the best matched driver
	int bestmatchindex = 0;

	// Search for drivers
	for (int i = 0; i < VFS_MAX_DRIVERS; i++)
	{
		if (vfsdriverinuse[i])
		{
			int ret = vfs_samefolder (mountpoints[i], pathin);

			if (ret > bestmatch)
			{
				bestmatch = ret;
				bestmatchindex = i;
			}
		}
	}

	// If we found a driver
	if (bestmatch)
	{
		// Copy the prepend path and concatenate the input path
		pathout[0] = 0;

		if (prepends[bestmatchindex][0])
			StrCat (pathout, prepends[bestmatchindex]);

		if (pathin[bestmatch] != '/')
			StrCat (pathout, "/");

		StrCat (pathout, pathin + bestmatch);

		// Return the driver pointer
		return vfsdrivers[bestmatchindex];
	}

	// Nothing found
	return 0;
}

const struct VFSDriver *vfs_resolve_complete (const char *cwd, const char *pathin, char *pathout)
{
	// Convert filename to absolute path
	char abs[VFS_MAX_PATH];
	vfs_reltoabs (abs, cwd, pathin);

	// Resolve that into a file system driver
	return vfs_resolve (abs, pathout);
}

///////////////////////////////////////////////////////////////////////////////
// VFS General Functions
///////////////////////////////////////////////////////////////////////////////
int vfs_mount (const char *mountpoint, struct VFSDriver *vfs, struct FileNode *disk)
{
	// Find empty slot
	for (int i = 0; i < VFS_MAX_DRIVERS; i++)
	{
		if (!vfsdriverinuse[i])
		{
			vfsdrivers[i] = vfs;
			vfsdriverinuse[i] = 1;
			StrCpy (mountpoints[i], mountpoint);
			vfs->mount (disk, prepends[i]);
			return 0;
		}
	}

	// No empty slots
	return -1;
}

int vfs_unmount (const char *mountpoint)
{
	// Find the mount
	for (int i = 0; i < VFS_MAX_DRIVERS; i++)
	{
		if (vfsdriverinuse[i] && !StrCmp (mountpoints[i], mountpoint))
		{
			vfsdriverinuse[i] = 0;
			return 0;
		}
	}

	// Could not find it
	return -1;
}

void vfs_reltoabs (char *out, const char *ina, const char *inb)
{
	// If the path is absolute, just copy the drive from inb. Copy ina if not
	if (inb[0] == '/')
	{
		StrCpy (out, inb);
		return;
	}
	else
		StrCpy (out, ina);

	// Create end pointer pointing to start of out
	char *end = out;

	// While we have chars in inb
	while (*inb)
	{
		// Find end (null terminator) of out
		while (*end) end++;

		// tok and str points to current pos in inb
		const char *tok = inb;
		const char *str = inb;

		// Search for '/' or null terminator
		while (*tok && *tok != '/')
			tok++;

		// Set inb to the next string or just the null terminator
		if (*tok)
			inb = tok + 1;
		else
			inb = tok;

		// Terminate tok (and str), overwriting already existing terminator or '/'
		const char *tokend = tok;

		// Compare for known values that need special handling
		if (str[0] == '.' && str[1] == '.' && (str+2 == tokend))
		{
			// If we are not at root, backpedal until '/' is found, erasing with null
			if ((end - out) > 2)
				while (end != out && *end != '/')
					{*end = 0; end--;}

			// Then erase the '/'
			if ((end - out) > 2)
				{*end = 0; end--;}
		}
		else if (str[0] == '.' && (str+1 == tokend))
		{
			// Just discard these
		}
		else
		{
			// If we're not at /, add the '/'
			if ((end - out) > 1)
			{
				end[0] = '/';
				end[1] = 0;
				end++;
			}

			// Copy the new path part
			char *tmp = end;
			while (*str && str != tokend) *tmp++ = *str++;
			*tmp = 0;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// File System Operations
///////////////////////////////////////////////////////////////////////////////
int vfs_open (struct FileNode *fnode, const char *cwd, const char *filename, int flags, int mode)
{
	// Resolve path into a file system driver
	char target[VFS_MAX_PATH];
	const struct VFSDriver *drv = vfs_resolve_complete (cwd, filename, target);

	// Check for validity
	if (!drv)
		return -ENOENT;

	// Set the file system in the file node
	fnode->fs = drv;

	// Run the file system open function
	return drv->open (fnode, target, flags, mode);
}

int vfs_close (struct FileNode *fnode)
{
	return fnode->fs->close (fnode);
}

int vfs_read (struct FileNode *fnode, char *data, int n)
{
	return fnode->fs->read (fnode, data, n);
}

int vfs_write (struct FileNode *fnode, char *data, int n)
{
	return fnode->fs->write (fnode, data, n);
}

int vfs_stat (const char *cwd, const char *filename, struct stat *st)
{
	// Convert filename to absolute path
	char abs[VFS_MAX_PATH];
	vfs_reltoabs (abs, cwd, filename);

	// Resolve that into a file system driver
	char target[VFS_MAX_PATH];
	const struct VFSDriver *drv = vfs_resolve (abs, target);

	// Check for validity
	if (!drv)
		return -ENOENT;

	// Erase it
	MemSet (st, 0, sizeof(struct stat));

	// Check if stat-ing a mountpoint, in that case return directory
	for (int i = 0; i < VFS_MAX_DRIVERS; i++)
	{
		if (vfsdriverinuse[i] && !StrCmp (mountpoints[i], abs))
		{
			st->st_mode = S_IFDIR;
			st->st_size = 0;
			return 0;
		}
	}

	// Do the file system stat function
	return drv->stat (target, st);
}

int vfs_unlink (const char *cwd, const char *filename)
{
	// Resolve path into a file system driver
	char target[VFS_MAX_PATH];
	const struct VFSDriver *drv = vfs_resolve_complete (cwd, filename, target);

	// Check for validity
	if (!drv)
		return -ENOENT;

	// Do the file system unlink
	return drv->unlink (target);
}

int vfs_rename (const char *cwd, const char *oldname, const char *newname)
{
	// Convert filename to absolute path
	char abs[VFS_MAX_PATH];

	// Resolve that into a file system driver
	char target[VFS_MAX_PATH];
	vfs_reltoabs (abs, cwd, oldname);
	const struct VFSDriver *drv = vfs_resolve (abs, target);

	char target2[VFS_MAX_PATH];
	vfs_reltoabs (abs, cwd, newname);
	const struct VFSDriver *drv2 = vfs_resolve (abs, target2);

	// Check for validity
	if (!drv)
		return -ENOENT;

	// Cannot do a rename across devices
	if (drv != drv2)
		return -EXDEV;

	// Do the file system rename
	return drv->rename (target, target2);
}

int vfs_mkdir (const char *cwd, const char *name, int mode)
{
	// Resolve path into a file system driver
	char target[VFS_MAX_PATH];
	const struct VFSDriver *drv = vfs_resolve_complete (cwd, name, target);

	// Check for validity
	if (!drv)
		return -ENOENT;

	// Do the file system mkdir
	return drv->mkdir (target, mode);
}

int vfs_opendir (const char *cwd, const char *name)
{
	// Convert filename to abs
	vfs_reltoabs (vfsabs, cwd, name);

	// Resolve path into a file system driver
	char target[VFS_MAX_PATH];
	const struct VFSDriver *drv = vfs_resolve_complete (cwd, name, target);

	// Check
	if (!drv)
		return -ENOENT;

	// Set the directory variables
	dirdrv = drv;
	vfsdircur = 0;

	// Do the file system opendir
	return drv->opendir (target);
}

int vfs_closedir (void)
{
	if (dirdrv)
		return dirdrv->closedir ();
	else
		return -EINVAL;
}

int vfs_readdir (struct dirent *de)
{
	// Opendir has to be called previously
	if (!dirdrv)
		return -1;

	// Call the file system readdir
	int ret = dirdrv->readdir (de);

	// Return if valid
	if (ret != -1)
		return ret;

	// End of dir, can we tell about mount points?
	for (; vfsdircur < VFS_MAX_DRIVERS; vfsdircur++)
	{
		if (vfsdriverinuse[vfsdircur])
		{
			char abs[VFS_MAX_PATH];
			vfs_reltoabs (abs, mountpoints[vfsdircur], "..");

			if (!StrCmp (abs, vfsabs))
			{
				char *s = mountpoints[vfsdircur];
				while (*s) s++;
				while (*s != '/') s--;
				s++;
				if (!*s)
					continue;
				StrCpy (de->d_name, s);
				vfsdircur++;
				return 0;
			}
		}
	}

	// Done
	return -1;
}

int vfs_chdir (char *cwd, const char *dst)
{
	// Convert filename
	char abs[VFS_MAX_PATH];
	char target[VFS_MAX_PATH];
	vfs_reltoabs (abs, cwd, dst);
	const struct VFSDriver *drv = vfs_resolve (abs, target);

	// Check
	if (!drv)
		return -ENOENT;

	// Test opendir, since it fails for invalid directories
	int ret = drv->opendir (target);

	// Check
	if (!ret)
	{
		StrCpy (cwd, abs);
		drv->closedir ();
		return 0;
	}
	else
		return ret;
}

int vfs_statvfs (const char *cwd, const char *path, struct statvfs *buf)
{
	// Resolve path into a file system driver
	char target[VFS_MAX_PATH];
	const struct VFSDriver *drv = vfs_resolve_complete (cwd, path, target);

	// Check for validity
	if (!drv)
		return -ENOENT;

	// Call file system statvfs
	return drv->statvfs (target, buf);
}
