/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/string.h"
#include "posix/errno.h"
#include "posix/sys/statvfs.h"
#include "fs/socket/socket.h"
#include "print/print.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// External variables
///////////////////////////////////////////////////////////////////////////////
extern struct netstack __net_list_start;
extern struct netstack __net_list_end;

///////////////////////////////////////////////////////////////////////////////
// "internal" socket api
///////////////////////////////////////////////////////////////////////////////
int vfs_socket_bind (struct FileNode *node, const struct sockaddr *addr, socklen_t addrlen)
{
	return ((struct netstack*)node->dev)->bind (node, addr, addrlen);
}

int vfs_socket_recvfrom (struct FileNode *node, struct recvfrom_args *args)
{
	return ((struct netstack*)node->dev)->recvfrom (node, args);
}

int vfs_socket_sendto (struct FileNode *node, struct sendto_args *args)
{
	return ((struct netstack*)node->dev)->sendto (node, args);
}

int vfs_socket_socket (struct FileNode *node, int domain, int type, int protocol)
{
	// Set the driver to this vfs
	node->fs = &vfs_driver_socket;

	// Find the stack responsible for the domain
	for (struct netstack *i = &__net_list_start; i < &__net_list_end; ++i)
	{
		if (i->domain == domain)
		{
			// Found match, store pointer and try opening
			node->dev = (struct DeviceDriver*) i;
			return i->socket (node, domain, type, protocol);
		}
	}

	return -ENOSYS;
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int vfs_socket_open (struct FileNode *node, const char *filename, int flags, int mode)
{
	// Skip forward slash
	if (*filename == '/')
		filename++;

	// TODO: Use this to create named sockets
	Print ("vfs_socket_open\n");
	return -ENOSYS;
}

int vfs_socket_close (struct FileNode *node)
{
	return ((struct netstack*)node->dev)->close (node);
}

int vfs_socket_read (struct FileNode *node, void *data, int nbytes)
{
	struct recvfrom_args a;
	a.buf = data;
	a.len = nbytes;
	a.flags = 0;
	a.addr = 0;
	a.addrlen = 0;

	return vfs_socket_recvfrom (node, &a);
}

int vfs_socket_write (struct FileNode *node, void *data, int nbytes)
{
	struct sendto_args a;
	a.buf = data;
	a.len = nbytes;
	a.flags = 0;
	a.addr = 0;
	a.addrlen = 0;

	return vfs_socket_sendto (node, &a);
}

int vfs_socket_ioctl_fcntl (struct FileNode *node, unsigned int code, void *data)
{
	// TODO
	Print ("vfs_socket_ioctl_fcntl\n");
	return -ENOSYS;
}

int vfs_socket_stat (const char *filename, struct stat *st)
{
	// TODO
	Print ("vfs_socket_stat\n");
	return -ENOSYS;
}

int vfs_socket_unlink (const char *filename)
{
	// TODO
	Print ("vfs_socket_unlink\n");
	return -ENOSYS;
}

int vfs_socket_opendir (const char *name)
{
	// TODO
	Print ("vfs_socket_opendir\n");
	return -ENOSYS;
}

int vfs_socket_closedir (void)
{
	// TODO
	Print ("vfs_socket_closedir\n");
	return -ENOSYS;
}

int vfs_socket_readdir (struct dirent *de)
{
	// TODO
	Print ("vfs_socket_readdir\n");
	return -ENOSYS;
}

int vfs_socket_statvfs (const char *path, struct statvfs *st)
{
	MemSet (st, 0, sizeof (struct statvfs));
	return 0;
}

int vfs_socket_mount (struct FileNode *fnode, char *drvpath)
{
	drvpath[0] = 0;
	return 0;
}

int vfs_socket_unsupported (void)
{
	return -ENOSYS;
}

///////////////////////////////////////////////////////////////////////////////
// Driver struct
///////////////////////////////////////////////////////////////////////////////
const struct VFSDriver vfs_driver_socket =
{
	.open = vfs_socket_open,
	.close = vfs_socket_close,
	.read = vfs_socket_read,
	.write = vfs_socket_write,
	.ioctl_fcntl = vfs_socket_ioctl_fcntl,
	.lseek = (int(*)(struct FileNode*, int, int)) vfs_socket_unsupported,
	.fstat = (int(*)(struct FileNode*, struct stat*)) vfs_socket_unsupported,
	.stat = vfs_socket_stat,
	.link = (int(*)(const char*, const char*)) vfs_socket_unsupported,
	.unlink = vfs_socket_unlink,
	.rename = (int(*)(const char*, const char*)) vfs_socket_unsupported,
	.mkdir = (int(*)(const char*, int)) vfs_socket_unsupported,
	.opendir = vfs_socket_opendir,
	.closedir = vfs_socket_closedir,
	.readdir = vfs_socket_readdir,
	.statvfs = vfs_socket_statvfs,
	.mount = vfs_socket_mount,
	.unmount = vfs_socket_unsupported
};
