/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/string.h"
#include "posix/errno.h"
#include "posix/sys/statvfs.h"
#include "fs/dev/dev.h"
#include "print/print.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define PIPE_FLAG_INUSE	0x0001
#define PIPE_FLAG_NAMED	0x0002
#define PIPE_FLAG_MSG	0x0004
#define PIPE_FLAG_BLOCK	0x0008

#define PIPE_BUF_SIZE	512
#define PIPE_BUF_MASK	0x01FF
#define PIPE_ALLOC_N	2

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct pipe_data
{
	unsigned char contents[PIPE_BUF_SIZE];
	unsigned short readpos;
	unsigned short writepos;
	unsigned short flags;
	unsigned short nbytesinpipe;
	unsigned short usecounter;
	char name[16];
}pipe_data;

typedef struct pipe_alloc
{
	pipe_data pipes[PIPE_ALLOC_N];
	struct pipe_alloc *next;
}pipe_alloc;

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
pipe_alloc pipes;
int vfs_pipe_curi;
pipe_alloc *vfs_pipe_a;

///////////////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////////////
pipe_data *vfs_pipe_findpipe (const char *named, unsigned int searchflags)
{
	// Initialize
	pipe_alloc *a = &pipes;
	int i = 0;

	// Search
	while (1)
	{
		// Get the pipe flags
		unsigned int f = a->pipes[i].flags;

		// Ignore flags other than INUSE and NAMED
		f = f & (PIPE_FLAG_INUSE | PIPE_FLAG_NAMED);

		// Check if the current pipe matches our search
		if (f == searchflags)
		{
			// If we are searching for a named pipe, we also have to check the name
			if (searchflags & PIPE_FLAG_NAMED)
			{
				if (!StrCmp (a->pipes[i].name, named))
					return &a->pipes[i];
			}
			else
				return &a->pipes[i];
		}

		// Go to the next pipe
		i++;

		if (i >= PIPE_ALLOC_N)
		{
			a = a->next;
			i = 0;

			if (!a)
				return 0;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int vfs_pipe_open (struct FileNode *node, const char *filename, int flags, int mode)
{
	// Skip forward slash
	if (*filename == '/')
		filename++;

	// Pointer to found pipe
	pipe_data *p = 0;

	// Flags
	int f = PIPE_FLAG_INUSE;
	f |= flags & (PIPE_FLAG_MSG | PIPE_FLAG_BLOCK);

	// Check for named pipe
	if (StrCmp (filename, "unnamed"))
	{
		// Search for a matching pipe
		p = vfs_pipe_findpipe (filename, PIPE_FLAG_INUSE | PIPE_FLAG_NAMED);

		// Set the NAMED flag
		f |= PIPE_FLAG_NAMED;
	}

	// At this point, p is either an existing named pipe or null
	// If none found, create it
	if (!p)
	{
		// Find slot
		p = vfs_pipe_findpipe (filename, 0);

		if (p)
		{
			// Set its flags, accounting for MSG and BLOCKING flags specified
			p->flags = f;

			// Clear data, read/write positions and store name
			MemSet (p->contents, 0, PIPE_BUF_SIZE);
			p->readpos = 0;
			p->writepos = 0;
			p->nbytesinpipe = 0;
			p->usecounter = 0;
			StrCpy (p->name, filename);
		}
	}

	// If we dont have any pipe now, there was an error
	if (!p)
	{
		Print ("No memory for pipe\n");
		return -ENOMEM;
	}

	// Set node members
	node->priv = (void*) p;

	// Increase use counter
	p->usecounter++;

	// Return OK
	return 0;
}

int vfs_pipe_close (struct FileNode *node)
{
	pipe_data *p = (pipe_data*) node->priv;

	p->usecounter--;

	if (!p->usecounter && !(p->flags & PIPE_FLAG_NAMED))
		p->flags = 0;

	return 0;
}

int vfs_pipe_read (struct FileNode *node, void *data, int nbytes)
{
	pipe_data *p = (pipe_data*) node->priv;

	int n = p->nbytesinpipe;

	if (nbytes < n)
		n = nbytes;

	for (int i = 0; i < n; i++)
		((char*)data)[i] = p->contents[(p->readpos + i) & PIPE_BUF_MASK];

	p->readpos = (p->readpos + n) & PIPE_BUF_MASK;
	p->nbytesinpipe -= n;

	return n;
}

int vfs_pipe_write (struct FileNode *node, void *data, int nbytes)
{
	pipe_data *p = (pipe_data*) node->priv;

	int n = PIPE_BUF_SIZE - p->nbytesinpipe;

	if ((p->flags & PIPE_FLAG_MSG) && (n < nbytes))
		return 0;

	if (nbytes < n)
		n = nbytes;

	for (int i = 0; i < n; i++)
		p->contents[(p->writepos + i) & PIPE_BUF_MASK] = ((char*)data)[i];

	p->writepos = (p->writepos + n) & PIPE_BUF_MASK;
	p->nbytesinpipe += n;

	return n;
}

int vfs_pipe_ioctl_fcntl (struct FileNode *node, unsigned int code, void *data)
{
	return -1;
}

int vfs_pipe_stat (const char *filename, struct stat *st)
{
	if (*filename == '/')
		filename++;

	pipe_data *p = vfs_pipe_findpipe (filename, PIPE_FLAG_INUSE | PIPE_FLAG_NAMED);

	if (p)
	{
		st->st_mode = S_IFIFO;
		st->st_size = PIPE_BUF_SIZE;
		return 0;
	}

	return -ENOENT;
}

int vfs_pipe_unlink (const char *filename)
{
	// Remove starting slash
	if (*filename == '/')
		filename++;

	// Search for a named pipe in use with the filename
	pipe_data *p = vfs_pipe_findpipe (filename, PIPE_FLAG_INUSE | PIPE_FLAG_NAMED);

	// If found
	if (p)
	{
		if (!p->usecounter)
		{
			p->flags = 0;
			return 0;
		}
		else
			return -EBUSY;
	}

	// Not found
	return -ENOENT;
}

int vfs_pipe_opendir (const char *name)
{
	vfs_pipe_curi = 0;
	vfs_pipe_a = &pipes;
	return 0;
}

int vfs_pipe_closedir (void)
{
	return 0;
}

int vfs_pipe_readdir (struct dirent *de)
{
	// Check
	if (vfs_pipe_curi >= PIPE_ALLOC_N)
	{
		vfs_pipe_a = vfs_pipe_a->next;
		vfs_pipe_curi = 0;

		if (!vfs_pipe_a)
			return -1;
	}

	unsigned int mask = PIPE_FLAG_INUSE | PIPE_FLAG_NAMED;

	while ((vfs_pipe_a->pipes[vfs_pipe_curi].flags & mask) != mask)
	{
		vfs_pipe_curi++;

		if (vfs_pipe_curi >= PIPE_ALLOC_N)
		{
			vfs_pipe_a = vfs_pipe_a->next;
			vfs_pipe_curi = 0;

			if (!vfs_pipe_a)
				return -1;
		}
	}

	// Copy up to 16 chars
	if (vfs_pipe_a->pipes[vfs_pipe_curi].flags & PIPE_FLAG_INUSE)
	{
		for (int i = 0; i < 16; i++)
		{
			de->d_name[i] = vfs_pipe_a->pipes[vfs_pipe_curi].name[i];

			if (!de->d_name[i])
				break;
		}

		de->d_name[15] = 0;
	}

	// Done
	vfs_pipe_curi++;
	return 0;
}

int vfs_pipe_statvfs (const char *path, struct statvfs *st)
{
	MemSet (st, 0, sizeof (struct statvfs));
	st->f_bsize = PIPE_BUF_SIZE;
	return 0;
}

int vfs_pipe_mount (struct FileNode *fnode, char *drvpath)
{
	drvpath[0] = 0;
	MemSet (&pipes, 0, sizeof (pipe_alloc));
	return 0;
}

int vfs_pipe_unsupported (void)
{
	return -1;
}

///////////////////////////////////////////////////////////////////////////////
// Driver struct
///////////////////////////////////////////////////////////////////////////////
const struct VFSDriver vfs_driver_pipe =
{
	.open = vfs_pipe_open,
	.close = vfs_pipe_close,
	.read = vfs_pipe_read,
	.write = vfs_pipe_write,
	.ioctl_fcntl = vfs_pipe_ioctl_fcntl,
	.lseek = (int(*)(struct FileNode*, int, int)) vfs_pipe_unsupported,
	.fstat = (int(*)(struct FileNode*, struct stat*)) vfs_pipe_unsupported,
	.stat = vfs_pipe_stat,
	.link = (int(*)(const char*, const char*)) vfs_pipe_unsupported,
	.unlink = vfs_pipe_unlink,
	.rename = (int(*)(const char*, const char*)) vfs_pipe_unsupported,
	.mkdir = (int(*)(const char*, int)) vfs_pipe_unsupported,
	.opendir = vfs_pipe_opendir,
	.closedir = vfs_pipe_closedir,
	.readdir = vfs_pipe_readdir,
	.statvfs = vfs_pipe_statvfs,
	.mount = vfs_pipe_mount,
	.unmount = vfs_pipe_unsupported
};
