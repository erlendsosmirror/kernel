/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/string.h"
#include "posix/errno.h"
#include "posix/sys/statvfs.h"
#include "fs/dev/dev.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
int vfs_dev_curdir;

///////////////////////////////////////////////////////////////////////////////
// External variables
///////////////////////////////////////////////////////////////////////////////
extern struct DeviceDriver __dev_list_start;
extern struct DeviceDriver __dev_list_end;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int vfs_dev_open (struct FileNode *node, const char *filename, int flags, int mode)
{
	// We may get a filename with a slash, skip over it
	if (*filename == '/')
		filename++;

	// Loop through list of devices to find it
	for (struct DeviceDriver *i = &__dev_list_start; i < &__dev_list_end; ++i)
	{
		if (!StrCmp (filename, i->name))
		{
			// Found match, store pointer and try opening
			node->dev = i;
			return node->dev->open (node, filename);
		}
	}

	// Device not found
	return -1;
}

int vfs_dev_close (struct FileNode *node)
{
	return node->dev->close (node);
}

int vfs_dev_read (struct FileNode *node, void *data, int nbytes)
{
	return node->dev->read (node, data, nbytes);
}

int vfs_dev_write (struct FileNode *node, void *data, int nbytes)
{
	return node->dev->write (node, data, nbytes);
}

int vfs_dev_ioctl_fcntl (struct FileNode *node, unsigned int code, void *data)
{
	return node->dev->ioctl (node, code, data);
}

int vfs_dev_stat (const char *filename, struct stat *st)
{
	// We may get a filename with a slash, skip over it
	if (*filename == '/')
		filename++;

	// Loop through list of devices to find it
	for (struct DeviceDriver *i = &__dev_list_start; i < &__dev_list_end; ++i)
	{
		if (!StrCmp (filename, i->name))
		{
			// It exists, set mode and size to char device
			st->st_mode = S_IFCHR;
			st->st_size = 0;
			return 0;
		}
	}

	// Device not found
	return -1;
}

int vfs_dev_opendir (const char *name)
{
	vfs_dev_curdir = 0;
	return 0;
}

int vfs_dev_closedir (void)
{
	return 0;
}

int vfs_dev_readdir (struct dirent *de)
{
	// Create pointer
	struct DeviceDriver *d = &__dev_list_start;
	d += vfs_dev_curdir;

	// Check
	if (d >= &__dev_list_end)
		return -1;

	// Copy up to VFS_MAX_PATH chars
	for (int i = 0; i < VFS_MAX_PATH; i++)
	{
		de->d_name[i] = d->name[i];

		if (!d->name[i])
			break;
	}

	// Done
	vfs_dev_curdir++;
	return 0;
}

int vfs_dev_statvfs (const char *path, struct statvfs *st)
{
	// Dev does not have any interesting attributes
	MemSet (st, 0, sizeof (struct statvfs));
	st->f_bsize = 512;
	return 0;
}

int vfs_dev_mount (struct FileNode *fnode, char *drvpath)
{
	drvpath[0] = 0;
	return 0;
}

int vfs_dev_unsupported (void)
{
	return -1;
}

///////////////////////////////////////////////////////////////////////////////
// Driver struct
///////////////////////////////////////////////////////////////////////////////
const struct VFSDriver vfs_driver_dev =
{
	.open = vfs_dev_open,
	.close = vfs_dev_close,
	.read = vfs_dev_read,
	.write = vfs_dev_write,
	.ioctl_fcntl = vfs_dev_ioctl_fcntl,
	.lseek = (int(*)(struct FileNode*, int, int)) vfs_dev_unsupported,
	.fstat = (int(*)(struct FileNode*, struct stat*)) vfs_dev_unsupported,
	.stat = vfs_dev_stat,
	.link = (int(*)(const char*, const char*)) vfs_dev_unsupported,
	.unlink = (int(*)(const char*)) vfs_dev_unsupported,
	.rename = (int(*)(const char*, const char*)) vfs_dev_unsupported,
	.mkdir = (int(*)(const char*, int)) vfs_dev_unsupported,
	.opendir = vfs_dev_opendir,
	.closedir = vfs_dev_closedir,
	.readdir = vfs_dev_readdir,
	.statvfs = vfs_dev_statvfs,
	.mount = vfs_dev_mount,
	.unmount = vfs_dev_unsupported
};
