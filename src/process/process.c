/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "process/processcontrol.h"
#include "process/process.h"
#include "process/process-allocate.h"
#include "process/process-loader.h"
#include "sched/scheduler.h"
#include "sched/scheduler-signal.h"
#include "panic/panic.h"
#include "syscall/svc-basicfileops.h"
#include "posix/sys/signal.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
int currentmaxpid;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
struct ProcessControl *ProcessCreateInternal (void(*func)(void),
	unsigned int *stack,
	int stacksize)
{
	// Allocate process data
	struct ProcessControl *slot = ProcessAllocate ();

	if (!slot)
		return 0;

	// Set pid and set current directory to root
	slot->pid = currentmaxpid++;
	slot->cwd[0] = '/';
	slot->cwd[1] = 0;

	// Start internal processes as privileged
	slot->trc.privileged = 0;

	// Add to schedulers run list
	SchedAddThread (&slot->trc, func, stack, stacksize, SCHED_PRIORITY_MEDIUM);
	return slot;
}

void ProcessFileCleanup (struct ProcessControl *p)
{
	for (int i = 0; i < MAX_NODES; i++)
	{
		// Just close all the files. EBADF will be returned if it was not in
		// use, but that does not matter
		int derrno = 0;
		int closeargs[] = {i, 0, 0, (int)&derrno};
		svc_close (&p->trc, (void*) closeargs);
	}
}

void ProcessNotifyOnStatusChange (struct ThreadControl *ctc, int terminated)
{
	// Get structures of child and parent
	struct ProcessControl *child = ctc->pctrl;

	if (!child)
		Panic ("No child pointer");

	struct ProcessControl *parent = ProcessFindByPid (child->ownerpid);

	if (!parent)
		Panic ("No parent pointer");

	// If the process terminated, we have to terminate its subprocesses too
	if (terminated)
	{
		for (int i = 0; i < MAX_PROCESSDATA; i++)
		{
			if (processdata[i].inuse && processdata[i].ownerpid == child->pid)
			{
				SchedRemoveProcess (&processdata[i].trc);
				ProcessFileCleanup (&processdata[i]);
				ProcessSpawnFree (&processdata[i]);
				ProcessFree (&processdata[i]);
			}
		}
	}

	// Check the state of the parent. Is it blocked because of waitpid?
	if ((parent->trc.state & SCHED_STATUS_BLOCKED) && parent->trc.param3 == 32)
	{
		// Get the arguments
		int *r = (int*) parent->trc.param2;

		// If it is blocking on any pid or this child
		if (r[0] < 0 || r[0] == child->pid)
		{
			// Unblock it with the appropriate pid and exit code
			r[0] = child->pid;
			*((int*)r[1]) = child->returncode;
			SchedState (&parent->trc, SCHED_STATUS_RUN, 0, SCHED_STATUS_BLOCKED, 0);

			// Because this ends the waitpid call we have to do more cleanup also
			if (terminated)
			{
				ProcessSpawnFree (child);
				ProcessFree (child);
			}

			// Debug message
			//PrintValue ("Exit wake", prc->pid);

			// Done
			return;
		}
	}

	// Just notify owner
	SchedPostSignal (&parent->trc, SIGCHLD);
}

int ProcessSendSignal (int pid, int sig)
{
	if (pid > 0)
	{
		// We are singaling a single process, find it
		struct ProcessControl *p = ProcessFindByPid (pid);

		if (p)
		{
			// Attempt sending the signal to the process
			int ret = SchedPostSignal (&p->trc, sig);

			// If it was ignored, handle it accordingly
			if (ret == 1)
				return ProcessHandleIgnoredSignal (p, sig);

			return ret;
		}

		// No process with that pid
		return 2;
	}
	else if (pid < 0)
	{
		// We are signaling a group, negate the argument to get the group id
		pid = -pid;

		// Search in all processes, and signal the first process found
		for (int i = 0; i < MAX_PROCESSDATA; i++)
		{
			if (processdata[i].inuse && processdata[i].gid == pid)
			{
				int ret = SchedPostSignal (&processdata[i].trc, sig);

				if (ret == 1)
					return ProcessHandleIgnoredSignal (&processdata[i], sig);

				return ret;
			}
		}

		// No process with that group id
		return 0;
	}
	else
	{
		// Cannot signal process 0
		return 2;
	}
}

int ProcessHandleIgnoredSignal (struct ProcessControl *p, int sig)
{
	if (sig == SIGKILL || sig == SIGINT)
	{
		// Terminate the process
		SchedRemoveProcess (&p->trc);
		ProcessFileCleanup (p);
		p->returncode = 2;
		ProcessNotifyOnStatusChange (&p->trc, 1);
		return 0;
	}
	else if (sig == SIGTSTP)
	{
		// Stop/suspend the process
		SchedState (&p->trc, SCHED_STATUS_STOPPED, 0, 0, 0);
		p->returncode = 1;
		ProcessNotifyOnStatusChange (&p->trc, 0);
		return 0;
	}
	else if (sig == SIGCONT)
	{
		// Resume the process
		if (currentthc == &p->trc)
			Panic ("Cant resume current process");

		SchedState (&p->trc, SCHED_STATUS_RUN, 0, SCHED_STATUS_STOPPED, 0);
		return 0;
	}

	// No default action for this signal
	return 1;
}

int ProcessCrashRecover (struct ThreadControl *ctc, unsigned int args)
{
	// Store its return value with WIFSIGNALED
	ctc->pctrl->returncode = 42 << 8 | 2;

	// Delete leftover resources
	ProcessFileCleanup (ctc->pctrl);

	// Print crash report
	PanicPrintReport (ctc);

	// Signal the parent
	ProcessNotifyOnStatusChange (ctc, 1);

	// Return true so that the dead thread is not woken
	return 1;
}
