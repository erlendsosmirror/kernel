/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "process/processcontrol.h"
#include "process/process.h"
#include "process/process-allocate.h"
#include "sched/scheduler.h"
#include "mem/mem.h"
#include "mem/string.h"
#include "elf/elf.h"
#include "posix/errno.h"
#include "posix/spawn.h"
#include "posix/unistd.h"
#include "posix/fcntl.h"
#include "syscall/svc-basicfileops.h"
#include "syscall/svc-procops.h"
#include "fs/vfs.h"
#include "mpu/mpu.h"
#include "print/print.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define ARG_MAX_COUNT 16

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
extern int currentmaxpid;
char loader_printrel[16];

///////////////////////////////////////////////////////////////////////////////
// Local functions
///////////////////////////////////////////////////////////////////////////////
int ProcessSpawnCountArgs (char **args, int *argcount)
{
	int argscount = 0;
	int argsize = 0;

	for (int i = 0; i < ARG_MAX_COUNT; i++)
	{
		if (args[i])
		{
			argscount++;
			argsize += StrLen (args[i]) + 1;
		}
		else
			break;
	}

	*argcount = argscount;
	return argsize + (argscount + 1) * 4;
}

void ProcessSpawnCopyArgs (char **dst, char **src, unsigned int count)
{
	unsigned int pos = (count + 1) * 4;

	for (int i = 0; i < count; i++)
	{
		dst[i] = (char*) (((unsigned int)dst) + pos);
		StrCpy (dst[i], src[i]);
		pos += StrLen (src[i]) + 1;
	}
}

int ProcessSpawnAllocate (struct ProcessControl *slot, struct Loader *loader, unsigned int argvsize, unsigned int envsize, unsigned int stacksize, struct Program *p)
{
	// Force word allocation (TODO: Check if this can be done by mem system)
	loader->text.size = (loader->text.size + 3) & ~3;
	loader->rodata.size = (loader->rodata.size + 3) & ~3;
	loader->data.size = (loader->data.size + 3) & ~3;
	loader->bss.size = (loader->bss.size + 3) & ~3;

	argvsize = (argvsize + 3) & ~3;
	envsize = (envsize + 3) & ~3;

	stacksize = (stacksize + 3) & ~3;

	// Clear pointers
	slot->alloc[0] = 0;
	slot->alloc[1] = 0;
	slot->alloc[2] = 0;
	slot->alloc[3] = 0;
	slot->text = 0;

	// Set as unprivileged
	slot->trc.privileged = 1;

	// MPU Allocation
	return MpuSpawnAllocate (slot, loader, argvsize, envsize, stacksize, p);
}

int ProcessSpawnLoad (struct ProcessControl *slot, struct Loader *loader, unsigned int argvsize, unsigned int envsize)
{
	// Return value
	int r;

	// Check the header
	if ((r = CheckHeader (loader)))
		return r;

	// Load names section header
	struct ElfSection shn;

	if ((r = LoadSectionHeader (loader, &shn, loader->shstrndx)))
		return r;

	loader->shnames_offset = shn.offset;

	if (shn.size >= sizeof (loader->names))
		shn.size = sizeof (loader->names) - 1;

	// Load names
	if ((r = LoadSectionData (loader, shn.offset, shn.size, loader->names)))
		return r;

	loader->names[sizeof (loader->names) - 1] = 0;

	// Calculate the required sizes
	if ((r = FindSizes (loader)))
		return r;

	// Allocation
	struct Program p;

	if ((r = ProcessSpawnAllocate (slot, loader, argvsize, envsize, 2048, &p)))
		return r;

	// Optional debug
	if (loader_printrel[0])
	{
		Print ("text   at %x entry %x\n", (unsigned int)p.text, loader->entrypoint);
		Print ("rodata at %x\n", (unsigned int)p.rodata);
		Print ("data   at %x\n", (unsigned int)p.data);
		Print ("bss    at %x\n", (unsigned int)p.bss);
	}

	// Load data
	if ((r = LoadData (loader, &p)))
		return r;

	// Relocate
	if ((r = Relocate (loader->fd, loader, &p)))
		return r;

	return 0;
}

void ProcessSpawnAttr (struct ThreadControl *cur, struct ProcessControl *slot, int *arg)
{
	posix_spawnattr_t *spawnattr = (posix_spawnattr_t*) arg[1];

	if (spawnattr && spawnattr->flags & POSIX_SPAWN_SETPGROUP)
	{
		if (spawnattr->pgroup)
			slot->gid = spawnattr->pgroup;
		else
			slot->gid = slot->pid;
	}
	else
		slot->gid = cur->pctrl->gid;
}

void ProcessSpawnFileActions (struct ThreadControl *cur, struct ProcessControl *slot, int *arg)
{
	// The default action is to copy file descriptors that are open in the
	// parent process, so do this first.
	for (int i = 0; i < MAX_NODES; i++)
	{
		int kfd = cur->pctrl->fnodes[i];
		slot->fnodes[i] = kfd;

		if (kfd != 255)
			nodetable[kfd].usecounter++;
	}

	// Only process file actions if supplied
	if (!arg[0])
		return;

	// Convert to structure pointer
	posix_spawn_file_actions_t *factions = (posix_spawn_file_actions_t*) arg[0];

	// Get number of file actions in structure
	int n = factions->nfileactions;

	// Dummy errno
	int derrno = 0;

	// Loop through the file actions
	for (int i = 0; i < n; i++)
	{
		// Simple pointer
		file_action *f = &factions->fa[i];

		// Check for addopen, adddup2 and addclose
		if (f->inuse == 1)
		{
			// Close the file descriptor
			int closeargs[] = {(int)f->fildes, 0, 0, (int)&derrno};
			svc_close (&slot->trc, (void*) closeargs);

			// Set dir for the worker thread
			MemCpy (currentthc->pctrl->cwd, slot->cwd, 64);

			// Open the file (Note: Not sure about calling this in thread mode)
			int openargs[] = {(int)f->path, (int)f->oflag, (int)f->mode, (int)&derrno};
			svc_open (&slot->trc, (void*) openargs);
		}
		else if (f->inuse == 2)
		{
			// Close the file descriptor
			int closeargs[] = {(int)f->fildes, 0, 0, (int)&derrno};
			svc_close (&slot->trc, (void*) closeargs);

			// Call dup2 in the context of the child
			int dupargs[] = {(int)f->fildes, (int)f->oflag, 0, (int)&derrno};
			svc_dup2 (&slot->trc, (void*) dupargs);
		}
		else if (f->inuse == 3)
		{
			// Close the file descriptor
			int closeargs[] = {(int)f->fildes, 0, 0, (int)&derrno};
			svc_close (&slot->trc, (void*) closeargs);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void ProcessSpawnFree (struct ProcessControl *slot)
{
	if (slot->alloc[0]) MemFree (slot->alloc[0]);
	if (slot->alloc[1]) MemFree (slot->alloc[1]);
	if (slot->alloc[2]) MemFree (slot->alloc[2]);
	if (slot->alloc[3]) MemFree (slot->alloc[3]);

	slot->alloc[0] = 0;
	slot->alloc[1] = 0;
	slot->alloc[2] = 0;
	slot->alloc[3] = 0;
}

/*
 * The posix_spawn implementation.
 *  - args: The arguments from the calling thread. Note that the
 *    first argument must be fetched from cur->param3 instead of args[0].
 *  - cur: The calling threads ProcessControl.
 */
int ProcessSpawn (struct ThreadControl *cur, void *args)
{
	// Get the arguments
	int *pid = (int*) cur->param3;
	char *name = (char*) ((int*)args)[1];
	int *arg = (int*) ((int*)args)[2];
	char **argv = (char**) arg[2];
	char **env = (char**) arg[3];

	// Calculate argument counts and sizes
	int argvcount = 0;
	int envcount = 0;
	int argvsize = ProcessSpawnCountArgs (argv, &argvcount);
	int envsize = ProcessSpawnCountArgs (env, &envcount);

	// Allocate and check process data structure
	struct ProcessControl *slot = ProcessAllocate ();

	if (!slot)
		return -ENOMEM;

	// Copy the current working directory
	StrCpy (slot->cwd, cur->pctrl->cwd);

	// Allocate elf loader and open the input file
	struct Loader loader;
	MemSet (&loader, 0, sizeof (struct Loader));

	loader.fd = open (name, O_RDONLY);

	if (loader.fd < 0)
	{
		ProcessFree (slot);
		return loader.fd;
	}

	// Load the executable
	int ret = ProcessSpawnLoad (slot, &loader, argvsize, envsize);

	close (loader.fd);

	if (ret)
	{
		ProcessSpawnFree (slot);
		ProcessFree (slot);
		return -1;
	}

	// Copy arguments
	ProcessSpawnCopyArgs (slot->argv, argv, argvcount);
	ProcessSpawnCopyArgs (slot->environ, env, envcount);

	// Set start parameters
	slot->trc.param1 = (unsigned int) argvcount;
	slot->trc.param2 = (unsigned int) slot->argv;
	slot->trc.param3 = (unsigned int) slot->environ;

	// Do a simple entry check
	if (loader.entrypoint < 0x20000000)
	{
		ProcessSpawnFree (slot);
		ProcessFree (slot);
		return -1;
	}

	// Set the pid values
	slot->ownerpid = cur->pctrl->pid;
	slot->pid = currentmaxpid++;
	*pid = slot->pid;

	// Process spawn attr and file actions
	ProcessSpawnAttr (cur, slot, arg);
	ProcessSpawnFileActions (cur, slot, arg);

	// Start the process and return
	SchedAddThread (&slot->trc, (void(*)(void)) loader.entrypoint, slot->stack, 512, SCHED_PRIORITY_MEDIUM);
	return 0;
}
