/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/mem.h"
#include "mem/string.h"
#include "posix/errno.h"
#include "mpu/mpu.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
// Definitions for region manipulation
#define XN (1 << 28)
#define AP(x) ((x) << 24)
#define TEX(x) ((x) << 19)
#define S (1 << 18)
#define C (1 << 17)
#define B (1 << 16)
#define SIZE(x) ((x) << 1)
#define E 1

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int MpuSpawnAllocate (struct ProcessControl *slot, struct Loader *loader, unsigned int argvsize, unsigned int envsize, unsigned int stacksize, struct Program *p)
{
	// Calculate size of allocation regions
	unsigned int rs0 = loader->text.size;
	unsigned int rs1 = stacksize;
	unsigned int rs2 = loader->rodata.size;
	unsigned int rs3 = loader->data.size + loader->bss.size + argvsize + envsize;

	// Data
	unsigned int *mpuregs = slot->trc.mpuregs;
	struct MemMpuReturn mpu;

	// Allocate text from bank 0
	slot->alloc[0] = MemAllocMpu (rs0, 0, &mpu);
	mpuregs[0] = mpu.mpu_rbar;
	mpuregs[1] = mpu.mpu_rasr;
	p->text = (unsigned int*) mpu.dataptr;
	slot->text = mpu.dataptr;

	// Allocate stack from bank 0
	slot->alloc[1] = MemAllocMpu (rs1, 0, &mpu);
	mpuregs[2] = mpu.mpu_rbar;
	mpuregs[3] = mpu.mpu_rasr;
	slot->stack = (unsigned int*) mpu.dataptr;

	// Allocate rodata from bank 0
	slot->alloc[2] = MemAllocMpu (rs2, 0, &mpu);
	mpuregs[4] = mpu.mpu_rbar;
	mpuregs[5] = mpu.mpu_rasr;
	p->rodata = (unsigned int*) mpu.dataptr;

	// Allocate data, bss, argv and environ from bank 0
	slot->alloc[3] = MemAllocMpu (rs3, 0, &mpu);
	mpuregs[6] = mpu.mpu_rbar;
	mpuregs[7] = mpu.mpu_rasr;
	unsigned int allocptr = mpu.dataptr;

	// Check allocations for success now, will be freed later
	if (!slot->alloc[0] || !slot->alloc[1] || !slot->alloc[2] || !slot->alloc[3])
		return -ENOMEM;

	// Clear the data regions
	MemSet (p->text, 0, rs0);
	MemSet (slot->stack, 0, rs1);
	MemSet (p->rodata, 0, rs2);
	MemSet ((int*)allocptr, 0, rs3);

	// Set data, bss, argv and environ
	p->data = (unsigned int*) allocptr;
	allocptr += loader->data.size;

	p->bss = (unsigned int*) allocptr;
	allocptr += loader->bss.size;

	slot->argv = (char**) allocptr;
	allocptr += argvsize;

	slot->environ = (char**) allocptr;

	// Adjust entry point
	loader->entrypoint += (unsigned int) p->text;

	// Enable auto set of regions
	mpuregs[0] |= 0x00000010 | 0;
	mpuregs[2] |= 0x00000010 | 1;
	mpuregs[4] |= 0x00000010 | 2;
	mpuregs[6] |= 0x00000010 | 3;

	// Set region access
	mpuregs[1] |=      AP(2) | TEX(0) |C  |E; // Exec,    RO, Outer write back, Cacheable, Write back, Shareable
	mpuregs[3] |= XN | AP(3) | TEX(0) |C|S|E; // No exec, RW, Outer write back, Cacheable, Write back, Shareable
	mpuregs[5] |= XN | AP(2) | TEX(0) |C|S|E; // No exec, RO, Outer write back, Cacheable, Write back, Shareable
	mpuregs[7] |= XN | AP(3) | TEX(0) |C|S|E; // No exec, RW, Outer write back, Cacheable, Write back, Shareable

	// Done
	return 0;
}
