/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "process/processcontrol.h"
#include "fs/vfs.h"
#include "posix/errno.h"
#include "posix/sys/stat.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// For manipulating stack arguments
#define RET ((int*)args)[0]
#define ERRN ((int**)args)[3]

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int svc_stat (struct ThreadControl *ctc, void *args)
{
	// Get the name argument
	const char *name = (const char*)((int*)args)[0];

	// Get the stat argument
	struct stat *st = (struct stat *)((int*)args)[1];

	// Do the stat operation
	int ret = vfs_stat (ctc->pctrl->cwd, name, st);

	// Check for errors and return
	if (ret < 0)
	{
		*ERRN = -ret;
		return -1;
	}

	return ret;
}

int svc_link (struct ThreadControl *ctc, void *args)
{
	// The link syscall is currently not supported, use rename instead
	(void)ctc;
	(void)args;
	return -1;
}

int svc_unlink (struct ThreadControl *ctc, void *args)
{
	// Get the name argument
	const char *name = (const char*)((int*)args)[0];

	// Do the unlink operation
	int ret = vfs_unlink (ctc->pctrl->cwd, name);

	// Check for errors and return
	if (ret < 0)
	{
		*ERRN = -ret;
		return -1;
	}

	return ret;
}

int svc_rename (struct ThreadControl *ctc, void *args)
{
	// Get the name arguments
	const char *namea = (const char*)((int*)args)[0];
	const char *nameb = (const char*)((int*)args)[1];

	// Do the rename operation
	int ret = vfs_rename (ctc->pctrl->cwd, namea, nameb);

	// Check for errors and return
	if (ret < 0)
	{
		*ERRN = -ret;
		return -1;
	}

	return ret;
}

int svc_mkdir (struct ThreadControl *ctc, void *args)
{
	// Get the name and mode flags
	const char *name = (const char*)((int*)args)[0];
	int mode = ((int*)args)[1];

	// Do the mkdir operation
	int ret = vfs_mkdir (ctc->pctrl->cwd, name, mode);

	// Check for errors and return
	if (ret < 0)
	{
		*ERRN = -ret;
		return -1;
	}

	return ret;
}

int svc_opendir (struct ThreadControl *ctc, void *args)
{
	// Get the name
	const char *name = (const char*)((int*)args)[0];

	// Open the directory
	int ret = vfs_opendir (ctc->pctrl->cwd, name);

	// Check for errors and return
	if (ret < 0)
	{
		*ERRN = -ret;
		return -1;
	}

	return ret;
}

int svc_closedir (struct ThreadControl *ctc, void *args)
{
	// CTC Is not used!
	(void)ctc;

	// Close the directory
	int ret = vfs_closedir ();

	// Check for errors and return
	if (ret < 0)
	{
		*ERRN = -ret;
		return -1;
	}

	return ret;
}

int svc_readdir (struct ThreadControl *ctc, void *args)
{
	// CTC Is not used!
	(void)ctc;

	// Read the directory
	int ret = vfs_readdir ((struct dirent*)((int*)args)[1]);

	// Check for errors and return
	if (ret < 0)
	{
		*ERRN = -ret;
		return -1;
	}

	return ret;
}

int svc_statvfs (struct ThreadControl *ctc, void *args)
{
	// Get the path and statvfs structure
	const char *path = (const char*)((int*)args)[0];
	struct statvfs *buf = (struct statvfs*)((int*)args)[1];

	// Do the statvfs operation
	int ret = vfs_statvfs (ctc->pctrl->cwd, path, buf);

	// Check for errors and return
	if (ret < 0)
	{
		*ERRN = -ret;
		return -1;
	}

	return ret;
}
