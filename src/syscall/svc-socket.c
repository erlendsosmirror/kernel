/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "syscall/svc-basicfileops.h"
#include "fs/vfs.h"
#include "fs/socket/socket.h"
#include "posix/errno.h"
#include "process/process-allocate-file.h"
#include "print/print.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// For manipulating stack arguments
#define RET ((int*)args)[0]
#define ERRN ((int**)args)[3]

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int svc_bind (struct ThreadControl *ctc, void *args)
{
	// File descriptor index from the caller
	unsigned int pfd = ((unsigned int*)args)[0];

	// Index to file descriptor in the kernel
	int kfd;

	// Check for validity and get kfd
	if (pfd < MAX_NODES && (kfd = ctc->pctrl->fnodes[pfd]) != 255)
	{
		int ret = vfs_socket_bind (&nodetable[kfd], (const struct sockaddr *)((int*)args)[1], (socklen_t)((int*)args)[2]);

		if (ret < 0)
		{
			*ERRN = -ret;
			return -1;
		}

		return ret;
	}

	// Invalid descriptor
	*ERRN = EBADF;
	return -1;
}

int svc_recvfrom (struct ThreadControl *ctc, void *args)
{
	// File descriptor index from the caller
	unsigned int pfd = ((unsigned int*)args)[0];

	// Index to file descriptor in the kernel
	int kfd;

	// Check for validity and get kfd
	if (pfd < MAX_NODES && (kfd = ctc->pctrl->fnodes[pfd]) != 255)
	{
		int ret = vfs_socket_recvfrom (&nodetable[kfd], (struct recvfrom_args*)((int*)args)[1]);

		if (ret < 0)
		{
			*ERRN = -ret;
			return -1;
		}

		return ret;
	}

	// Invalid descriptor
	*ERRN = EBADF;
	return -1;
}

int svc_sendto (struct ThreadControl *ctc, void *args)
{
	// File descriptor index from the caller
	unsigned int pfd = ((unsigned int*)args)[0];

	// Index to file descriptor in the kernel
	int kfd;

	// Check for validity and get kfd
	if (pfd < MAX_NODES && (kfd = ctc->pctrl->fnodes[pfd]) != 255)
	{
		int ret = vfs_socket_sendto (&nodetable[kfd], (struct sendto_args*)((int*)args)[1]);

		if (ret < 0)
		{
			*ERRN = -ret;
			return -1;
		}

		return ret;
	}

	// Invalid descriptor
	*ERRN = EBADF;
    return -1;
}

int svc_socket (struct ThreadControl *ctc, void *args)
{
	// Allocate a file descriptor in the process
	struct FileNode *fn;
	int pfd = ProcessAllocateFileDesc (ctc, &fn);

	// Check if it is valid
	if (pfd < 0)
	{
		*ERRN = -pfd;
		return -1;
	}

	// Attempt open
	int ret = vfs_socket_socket (fn, ((int*)args)[0], ((int*)args)[1], ((int*)args)[2]);

	// Check for error, deallocate and return error
	if (ret < 0)
	{
		ctc->pctrl->fnodes[pfd] = 255;
		fn->usecounter = 0;
		*ERRN = -ret;
		return -1;
	}

	// Success
	return pfd;
}
