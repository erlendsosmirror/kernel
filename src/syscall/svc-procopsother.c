/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "process/process-allocate.h"
#include "process/process-loader.h"
#include "process/process.h"
#include "sched/scheduler.h"
#include "worker/worker.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// For manipulating stack arguments
#define RET ((int*)args)[0]
#define ERRN ((int**)args)[3]

///////////////////////////////////////////////////////////////////////////////
// Local functions
///////////////////////////////////////////////////////////////////////////////
int svc_spawn_rethelper (struct ThreadControl *ctc, unsigned int args)
{
	RET = ProcessSpawn (ctc, (void*)args);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int svc_spawn (struct ThreadControl *ctc, void *args)
{
	// Postpone processing of this syscall
	SchedState (ctc, SCHED_STATUS_BLOCKED,
		&svc_spawn_rethelper, (unsigned int) args, ((int*)args)[0]);

	// Wake the worker
	WorkerUnblock ();

	return 0;
}

int svc_waitpid (struct ThreadControl *ctc, void *args)
{
	// Get the arguments
	int pid = ((int*)args)[0];
	int *status = (int*)((int*)args)[1];
	int options = ((int*)args)[2];

	// Vars
	int found = 0;

	// Search list for the child we're waiting for
	for (int i = 0; i < MAX_PROCESSDATA; i++)
	{
		// Is it a child of ours?
		if (processdata[i].inuse && processdata[i].ownerpid == ctc->pctrl->pid)
		{
			// Mark presence of any child
			found = 1;

			// Skip if positive and it is not the pid we're looking for.
			// Otherwise, it is a specific child or any child
			if (pid > 0 && pid != processdata[i].pid)
				continue;

			// Check state change
			if (!processdata[i].trc.statechanged)
				continue;

			processdata[i].trc.statechanged = 0;

			// Check if regular stopped
			if (processdata[i].trc.state == SCHED_STATUS_BLOCKED)
			{
				// Set status to stopped and return pid
				*status = 1;
				return processdata[i].pid;
			}

			// Skip if it's status isnt zombie
			if (processdata[i].trc.state == SCHED_STATUS_ZOMBIE ||
				processdata[i].trc.state == SCHED_STATUS_CRASHED)
			{
				// Set the status code according to the return code
				*status = processdata[i].returncode;

				// Free the childs memory now and return its pid
				int retpid = processdata[i].pid;
				ProcessSpawnFree (&processdata[i]);
				ProcessFree (&processdata[i]);
				return retpid;
			}
		}
	}

	// At this point, the process we're looking for is either still active,
	// or does not exist. Check if there are no children.
	if (!found)
	{
		*ERRN = ECHILD;
		return -1;
	}

	// Now, we know children exist but all are still active. When WNOHANG is
	// set we should just return 0.
	if (options & 1)
		return 0;

	// It is okay to block
	SchedState (ctc, SCHED_STATUS_BLOCKED, 0, (unsigned int) args, 32);

	// This return should always be "manipulated" on the stack later on
	// It does not actually matter what it is set to here
	return pid;

}

int svc_kill (struct ThreadControl *ctc, void *args)
{
	int pid = ((int*)args)[0];
	int sig = ((int*)args)[1];

	int ret = ProcessSendSignal (pid, sig);

	if (!ret)
		return 0;
	else if (ret == 1)
		*ERRN = EINVAL;
	else if (ret == 2)
		*ERRN = ESRCH;

	return -1;
}
