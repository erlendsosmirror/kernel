/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "syscall/svc-basicfileops.h"
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// For manipulating stack arguments
#define RET ((int*)args)[0]
#define ERRN ((int**)args)[3]

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int svc_open (struct ThreadControl *ctc, void *args)
{
	// Find a free file descriptor in the process
	int pfd = 255;

	for (int i = 0; i < MAX_NODES; i++)
		if (ctc->pctrl->fnodes[i] == 255)
			{pfd = i; break;}

	// Check if it is valid
	if (pfd == 255)
	{
		*ERRN = ENOMEM;
		return -1;
	}

	// It is valid, find a kernel file descriptor
	int kfd = -1;

	for (int i = 0; i < NODETABLE_SIZE; i++)
		if (!nodetable[i].usecounter)
			{kfd = i; break;}

	// Check if it is valid
	if (kfd < 0)
	{
		*ERRN = ENOMEM;
		return -1;
	}

	// Cast variables
	const char *path = (const char*)((int*)args)[0];
	int flags = ((int*)args)[1];
	int mode = ((int*)args)[2];

	// Set dev and flags
	nodetable[kfd].dev = 0;
	nodetable[kfd].flags = flags;

	// Attempt open
	int ret = vfs_open (&nodetable[kfd], ctc->pctrl->cwd, path, flags, mode);

	// Check for error
	if (ret < 0)
	{
		*ERRN = -ret;
		return -1;
	}

	// Success
	nodetable[kfd].usecounter++;
	ctc->pctrl->fnodes[pfd] = kfd;
	return pfd;
}

int svc_close (struct ThreadControl *ctc, void *args)
{
	// File descriptor index from the caller
	unsigned int pfd = ((unsigned int*)args)[0];

	// Index to file descriptor in the kernel
	int kfd;

	// Check for validity and get kfd
	if (pfd < MAX_NODES && (kfd = ctc->pctrl->fnodes[pfd]) != 255)
	{
		// If this is the last use of the descriptor
		if (nodetable[kfd].usecounter == 1)
		{
			// Close it properly
			int ret = vfs_close (&nodetable[kfd]);

			if (ret < 0)
			{
				*ERRN = -ret;
				return -1;
			}
		}

		// It is still used elsewhere, decrement use counter
		nodetable[kfd].usecounter--;
		ctc->pctrl->fnodes[pfd] = 255;
		return 0;
	}

	// Invalid descriptor
	*ERRN = EBADF;
	return -1;
}

int svc_read (struct ThreadControl *ctc, void *args)
{
	// File descriptor index from the caller
	unsigned int pfd = ((unsigned int*)args)[0];

	// Index to file descriptor in the kernel
	int kfd;

	// Check for validity and get kfd
	if (pfd < MAX_NODES && (kfd = ctc->pctrl->fnodes[pfd]) != 255)
	{
		int ret = vfs_read (&nodetable[kfd], (char*)((int*)args)[1], ((int*)args)[2]);

		if (ret < 0)
		{
			*ERRN = -ret;
			return -1;
		}

		return ret;
	}

	// Invalid descriptor
	*ERRN = EBADF;
	return -1;
}

int svc_write (struct ThreadControl *ctc, void *args)
{
	// File descriptor index from the caller
	unsigned int pfd = ((unsigned int*)args)[0];

	// Index to file descriptor in the kernel
	int kfd;

	// Check for validity and get kfd
	if (pfd < MAX_NODES && (kfd = ctc->pctrl->fnodes[pfd]) != 255)
	{
		int ret = vfs_write (&nodetable[kfd], (char*)((int*)args)[1], ((int*)args)[2]);

		if (ret < 0)
		{
			*ERRN = -ret;
			return -1;
		}

		return ret;
	}

	// Invalid descriptor
	*ERRN = EBADF;
    return -1;
}

int svc_ioctl_fcntl (struct ThreadControl *ctc, void *args)
{
	// File descriptor index from the caller
	unsigned int pfd = ((unsigned int*)args)[0];

	// Index to file descriptor in the kernel
	int kfd;

	// Check for validity and get kfd
	if (pfd < MAX_NODES && (kfd = ctc->pctrl->fnodes[pfd]) != 255)
	{
		// Check if we have a dev pointer
		if (!nodetable[kfd].dev)
		{
			*ERRN = EINVAL;
			return -1;
		}

		// Call the driver ioctl
		int ret = nodetable[kfd].dev->ioctl (&nodetable[kfd], (unsigned int)((int*)args)[1], (void*)((int*)args)[2]);

		if (ret < 0)
		{
			*ERRN = -ret;
			return -1;
		}

		return ret;
	}

	// Invalid descriptor
	*ERRN = EBADF;
    return -1;
}
