/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "posix/sys/types.h"
#include "posix/spawn.h"
#include "ioctl/ioctl.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Local functions
///////////////////////////////////////////////////////////////////////////////
__attribute__((naked))
int asm_posix_spawn (pid_t *pid, const char *name, int *arg, int *errno)
{
	asm volatile("svc 31\n"
				"bx lr\n"
				"nop");
}

__attribute__((naked))
int asm_open (const char *name, int mode, int asdf)
{
	asm volatile ("svc 0\n"
		"bx lr\n");
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int open (const char *name, int mode, ...)
{
	return asm_open (name, mode, 0);
}

__attribute__((naked))
int close (int fd)
{
	asm volatile ("svc 1\n"
		"bx lr\n");
}

__attribute__((naked))
int read (int fd)
{
	asm volatile ("svc 2\n"
		"bx lr\n");
}

__attribute__((naked))
int syscall_write (int fd, const void *data, int nbytes, int *error)
{
	asm volatile("svc 3\n"
				"bx lr\n"
				"nop");
}

__attribute__((naked))
int ioctl (int fd, int code, void *data)
{
	asm volatile ("svc 4\n"
		"bx lr\n");
}

__attribute__((naked))
off_t lseek (int fd, off_t offset, int whence)
{
	asm volatile ("svc 5\n"
		"bx lr\n");
}

__attribute__((naked))
int waitpid (int pid, int *status, int flags)
{
	asm volatile("svc 32\n"
				"bx lr\n"
				"nop");
}

int posix_spawn (pid_t *pid,
	const char *name,
	const posix_spawn_file_actions_t *factions,
	const posix_spawnattr_t *attr,
	char **argv,
	char **env)
{
	// Construct extra arguments
	int arg[4] = {(int)factions, (int)attr, (int)argv, (int)env};

	// Fake errno
	int errno = 0;

	// Make the assembly call
	return asm_posix_spawn (pid, name, arg, &errno);
}

int tcsetpgrp (int pfd, int pgid)
{
	// TODO: Ioctl
	return ioctl (pfd, TCSETPGRP, (void*)pgid);
}

ssize_t write (int a, const void *b, ssize_t c)
{
	int err = 0;
	return syscall_write (a, b, c, &err);
}
