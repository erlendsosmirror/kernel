/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "process/processcontrol.h"
#include "fs/vfs.h"
#include "posix/errno.h"
#include "posix/time.h"
#include "posix/sys/time.h"
#include "posix/sys/stat.h"
#include "sched/scheduler.h"
#include "platform/platform.h"
#include "print/print.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// For manipulating stack arguments
#define ERRN ((int**)args)[3]

///////////////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////////////
int svc_nanosleep_callback (struct ThreadControl *ctc, unsigned int args)
{
	// The sleep was either interrupted or elapsed. Since we do not care about
	// the remainder time, just return success for now.
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int svc_times (struct ThreadControl *ctc, void *args)
{
	Print ("TODO: times\n");
	return -ENOSYS;
}

int svc_nanosleep (struct ThreadControl *ctc, void *args)
{
	// Get arguments
	struct timespec *req = (struct timespec*)((int*)args)[0];
	//struct timespec *rem = (struct timespec*)((int*)args)[1];

	// Do not continue if invalid arguments
	if (!req)
		return -EINVAL;

	// Calculate the time in milliseconds
	unsigned int time = (req->tv_sec * 1000) + (req->tv_nsec / 1000000);

	// In a critical section, check that we may indeed sleep (no signals pending)
	SchedEnterCritical ();

	if (ctc->sigpending)
	{
		// There is a signal pending, abort sleep
		SchedExitCritical ();
		return 0;
	}

	// Set state to sleeping while in critical, then exit critical
	SchedState (ctc, SCHED_STATUS_SLEEPING, &svc_nanosleep_callback, ((int*)args)[1], time);
	SchedExitCritical ();

	// If a signal is asserted here, the thread MUST BE WOKEN or it will
	// trigger a kernel panic in scheduler-algo.c
	return 0;
}

int svc_gettimeofday (struct ThreadControl *ctc, void *args)
{
	struct timeval *tv = (struct timeval*)((int*)args)[0];
	//struct timezone *tz = (struct timezone*)((int*)args)[1];

	if (!tv)
		return -EINVAL;

	unsigned int t = PlatformGetTick ();
	tv->tv_sec = t / 1000;
	tv->tv_usec = (t % 1000) * 1000;
	return 0;
}
