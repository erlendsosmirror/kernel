/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "syscall/svc-basicfileops.h"
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "print/print.h"
#include "process/process.h"
#include "process/process-allocate.h"
#include "process/processcontrol.h"
#include "sched/scheduler.h"
#include "panic/panic.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// For manipulating stack arguments
#define RET ((int*)args)[0]
#define ERRN ((int**)args)[3]

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int svc_exit (struct ThreadControl *ctc, void *args)
{
	// Delete from all lists. Yield and zombie state is done here
	SchedRemoveProcess (ctc);

	// Store its return value
	ctc->pctrl->returncode = ((int*)args)[0] << 8;

	// Close all open file descriptors
	ProcessFileCleanup (ctc->pctrl);

	// Wake owner from waitpid or notify with signal
	ProcessNotifyOnStatusChange (ctc, 1);
	return 0;
}

int svc_getpid (struct ThreadControl *ctc, void *args)
{
	return ctc->pctrl->pid;
}

int svc_signal (struct ThreadControl *ctc, void *args)
{
	int signal = 1 << (((int*)args)[0] - 1);
	void(*sigh)(int) = (void(*)(int)) ((int*)args)[1];
	void(*sigt)(int) = (void(*)(int)) ((int*)args)[2];

	if (sigh)
	{
		ctc->sigmask |= signal;
		ctc->sigh = sigh;
		ctc->sigt = sigt;
	}
	else
		ctc->sigmask &= ~signal;

	return 0;

}

int svc_trampoline (struct ThreadControl *ctc, void *args)
{
	SchedLeaveCurrentProcessSignal ();
	return 0;
}

int svc_getcwd (struct ThreadControl *ctc, void *args)
{
	// Get destination and destination max length
	char *dst = (char*)((int*)args)[0];
	int size = ((int*)args)[1];

	// Get the current directory
	struct ProcessControl *p = ctc->pctrl;

	if (!p)
		Panic ("svc_getcwd no pctrl");

	// Index and source
	int i = 0;
	char *src = p->cwd;

	// Copy up to size bytes
	for (; i < size && src[i]; i++)
		dst[i] = src[i];

	// Set the null terminator correctly
	if (i == size)
		dst[i-1] = 0;
	else
		dst[i] = 0;

	// Return a pointer to the destination
	return (int)dst;
}

int svc_chdir (struct ThreadControl *ctc, void *args)
{
	return vfs_chdir (ctc->pctrl->cwd, (char*)((int*)args)[0]);
}

int svc_dup2 (struct ThreadControl *ctc, void *args)
{
	// Arguments
	int oldfd = (int)((int*)args)[0];
	int newfd = (int)((int*)args)[1];

	// Check that oldfd is valid and that newfd is not too large
	// If newfd is < 0, it is a wildcard
	if (oldfd < 0 || oldfd >= MAX_NODES || newfd >= MAX_NODES)
	{
		*ERRN = EINVAL;
		return -1;
	}

	// Check that the old fd is valid
	if (ctc->pctrl->fnodes[oldfd] == 255)
	{
		*ERRN = EBADF;
		return -1;
	}

	// New file descriptor
	int pfd = newfd;

	// Check if newfd is not specified
	if (newfd < 0)
	{
		// Not specified, so we need to find one
		for (int i = 0; i < MAX_NODES; i++)
			if (ctc->pctrl->fnodes[i] == 255)
				{pfd = i; break;}
	}
	else
	{
		// Newfd is specified, we have to make sure it is available
		// Call close on the file descriptor
		int errcode = 0;
		int closeargs[4] = {newfd, 0, 0, (int) &errcode};
		int ret = svc_close (ctc, closeargs);

		// If it failed, it is probably because the fd did not exist
		if (ret < 0 && errcode != EBADF)
		{
			// In that case, fail with the errno
			*ERRN = errcode;
			return -1;
		}
	}

	// Check if valid
	if (pfd == 255)
	{
		*ERRN = EBADF;
		return -1;
	}

	// Duplicate file descriptor
	int kfd = ctc->pctrl->fnodes[oldfd];
	ctc->pctrl->fnodes[pfd] = kfd;
	nodetable[kfd].usecounter++;

	// Done
	return pfd;
}

int svc_yield (struct ThreadControl *ctc, void *args)
{
	SchedYield ();
	return 0;
}

int svc_getpgrp (struct ThreadControl *ctc, void *args)
{
	return ctc->pctrl->gid;
}

int svc_setpgrp (struct ThreadControl *ctc, void *args)
{
	int pid = (int)((int*)args)[0];
	int pgid = (int)((int*)args)[1];

	if (!pid)
		pid = ctc->pctrl->pid;
	else if (pid < 0 || pgid < 0)
	{
		*ERRN = EINVAL;
		return -1;
	}

	struct ProcessControl *prd = ProcessFindByPid (pid);

	if (prd)
	{
		if (pid != ctc->pctrl->pid)
		{
			*ERRN = EACCES;
			return -1;
		}

		if (!pgid)
			prd->gid = prd->pid;
		else
			prd->gid = pgid;

		return 0;
	}

	*ERRN = ESRCH;
	return -1;
}
