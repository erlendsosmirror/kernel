/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <sched/threadcontrol.h>

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

/*
 * Syscalls take a pointer to the current control block, and a pointer to
 * the arguments. This typedef is for convenience when defining the jump table.
 */
typedef int(*svc_t)(struct ThreadControl*,void*);

///////////////////////////////////////////////////////////////////////////////
// External functions
///////////////////////////////////////////////////////////////////////////////

// Basic file operations, aligns with the DeviceDriver structure
extern int svc_open (struct ThreadControl*, void*);
extern int svc_close (struct ThreadControl*, void*);
extern int svc_read (struct ThreadControl*, void*);
extern int svc_write (struct ThreadControl*, void*);
extern int svc_ioctl_fcntl (struct ThreadControl*, void*);

// Extended file operations
extern int svc_lseek (struct ThreadControl*, void*);
extern int svc_fstat (struct ThreadControl*, void*);

// File system operations
extern int svc_stat (struct ThreadControl*, void*);
extern int svc_link (struct ThreadControl*, void*);
extern int svc_unlink (struct ThreadControl*, void*);
extern int svc_rename (struct ThreadControl*, void*);
extern int svc_mkdir (struct ThreadControl*, void*);
extern int svc_opendir (struct ThreadControl*, void*);
extern int svc_closedir (struct ThreadControl*, void*);
extern int svc_readdir (struct ThreadControl*, void*);
extern int svc_statvfs (struct ThreadControl*, void*);

// Mount and unmount functions
extern int svc_mount (struct ThreadControl*, void*);
extern int svc_unmount (struct ThreadControl*, void*);

// Process operations for the current process
extern int svc_exit (struct ThreadControl*, void*);
extern int svc_getpid (struct ThreadControl*, void*);
extern int svc_signal (struct ThreadControl*, void*);
extern int svc_trampoline (struct ThreadControl*, void*);
extern int svc_getcwd (struct ThreadControl*, void*);
extern int svc_chdir (struct ThreadControl*, void*);
extern int svc_dup2 (struct ThreadControl*, void*);
extern int svc_yield (struct ThreadControl*, void*);
extern int svc_getpgrp (struct ThreadControl*, void*);
extern int svc_setpgrp (struct ThreadControl*, void*);

// Time and timing operations
extern int svc_times (struct ThreadControl*, void*);
extern int svc_nanosleep (struct ThreadControl*, void*);
extern int svc_gettimeofday (struct ThreadControl*, void*);

// Process operations on other processes
extern int svc_spawn (struct ThreadControl*, void*);
extern int svc_waitpid (struct ThreadControl*, void*);
extern int svc_kill (struct ThreadControl*, void*);

// Scheduler
extern int SchedStartSvc (struct ThreadControl*, void*);

// Socket
extern int svc_bind (struct ThreadControl*, void*);
extern int svc_recvfrom (struct ThreadControl*, void*);
extern int svc_sendto (struct ThreadControl*, void*);
extern int svc_socket (struct ThreadControl*, void*);

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int svc_invalid (struct ThreadControl *ctc, void *args)
{
	(void)ctc;
	(void)args;
	return -1;
}

///////////////////////////////////////////////////////////////////////////////
// Array of SVC handlers
///////////////////////////////////////////////////////////////////////////////
__attribute((__section__(".text.JumpTable"))) const svc_t svcJumpTable[64] =
{
	// Basic file operations
	svc_open,							// SVC 0
	svc_close,							// SVC 1
	svc_read,							// SVC 2
	svc_write,							// SVC 3
	svc_ioctl_fcntl,					// SVC 4

	// Extended file operations
	svc_lseek,							// SVC 5
	svc_fstat,							// SVC 6

	// File system operations
	svc_stat,							// SVC 7
	svc_link,							// SVC 8
	svc_unlink,							// SVC 9
	svc_rename,							// SVC 10
	svc_mkdir,							// SVC 11
	svc_opendir,						// SVC 12
	svc_closedir,						// SVC 13
	svc_readdir,						// SVC 14
	svc_statvfs,						// SVC 15

	// Mount and unmount functions
	svc_mount,							// SVC 16
	svc_unmount,						// SVC 17

	// Process operations for the current process
	svc_exit,							// SVC 18
	svc_getpid,							// SVC 19
	svc_signal,							// SVC 20
	svc_trampoline,						// SVC 21
	svc_getcwd,							// SVC 22
	svc_chdir,							// SVC 23
	svc_dup2,							// SVC 24
	svc_yield,							// SVC 25
	svc_getpgrp,						// SVC 26
	svc_setpgrp,						// SVC 27

	// Time and timing operations
	svc_times,							// SVC 28
	svc_nanosleep,						// SVC 29
	svc_gettimeofday,					// SVC 30

	// Process operations on other processes
	svc_spawn,							// SVC 31
	svc_waitpid,						// SVC 32
	svc_kill,							// SVC 33

	// Socket
	svc_bind,							// SVC 34
	svc_recvfrom,						// SVC 35
	svc_sendto,							// SVC 36
	svc_socket,							// SVC 37

	// Unused slots
	svc_invalid,						// SVC 38
	svc_invalid,						// SVC 39
	svc_invalid,						// SVC 40
	svc_invalid,						// SVC 41
	svc_invalid,						// SVC 42
	svc_invalid,						// SVC 43
	svc_invalid,						// SVC 44
	svc_invalid,						// SVC 45
	svc_invalid,						// SVC 46
	svc_invalid,						// SVC 47
	svc_invalid,						// SVC 48
	svc_invalid,						// SVC 49
	svc_invalid,						// SVC 50
	svc_invalid,						// SVC 51
	svc_invalid,						// SVC 52
	svc_invalid,						// SVC 53
	svc_invalid,						// SVC 54
	svc_invalid,						// SVC 55
	svc_invalid,						// SVC 56
	svc_invalid,						// SVC 57
	svc_invalid,						// SVC 58
	svc_invalid,						// SVC 59
	svc_invalid,						// SVC 60
	svc_invalid,						// SVC 61
	svc_invalid,						// SVC 62

	// Scheduler
	SchedStartSvc,						// SVC 63
};
