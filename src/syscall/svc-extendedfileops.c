/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "process/processcontrol.h"
#include "fs/vfs.h"
#include "posix/errno.h"
#include "posix/sys/stat.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// For manipulating stack arguments
#define RET ((int*)args)[0]
#define ERRN ((int**)args)[3]

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int svc_lseek (struct ThreadControl *ctc, void *args)
{
	// File descriptor index from the caller
	unsigned int pfd = ((unsigned int*)args)[0];

	// Index to file descriptor in the kernel
	int kfd;

	// Check for validity and get kfd
	if (pfd < MAX_NODES && (kfd = ctc->pctrl->fnodes[pfd]) != 255)
	{
		int ret = nodetable[kfd].fs->lseek (&nodetable[kfd], ((int*)args)[1], ((int*)args)[2]);

		if (ret < 0)
		{
			*ERRN = -ret;
			return -1;
		}

		return ret;
	}

	// Invalid descriptor
	*ERRN = EBADF;
	return -1;
}

int svc_fstat (struct ThreadControl *ctc, void *args)
{
	// File descriptor index from the caller
	unsigned int pfd = ((unsigned int*)args)[0];

	// Index to file descriptor in the kernel
	int kfd;

	// Check for validity and get kfd
	if (pfd < MAX_NODES && (kfd = ctc->pctrl->fnodes[pfd]) != 255)
	{
		int ret = nodetable[kfd].fs->fstat (&nodetable[kfd], (struct stat *)((int*)args)[1]);

		if (ret < 0)
		{
			*ERRN = -ret;
			return -1;
		}

		return ret;
	}

	// Invalid descriptor
	*ERRN = EBADF;
	return -1;
}
