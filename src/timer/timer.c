/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sched/threadcontrol.h"
#include "sched/scheduler.h"
#include "sched/list.h"
#include "platform/platform.h"
#include "timer/timer.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
List timerlist;
unsigned int timer_now;

///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
void TimerUpdate (void)
{
	// Get current time
	unsigned int now = PlatformGetTick ();

	// Loop until we catch up
	while (timer_now != now)
	{
		// Increment our timer "position"
		timer_now++;

		// Briefly disable interrupt when we check
		// TODO: Check if we can spend less time in critical
		SchedEnterCritical ();

		// Check for timers, there may be multiple elapsing now
		while (timerlist.nitems)
		{
			ListItem *it = timerlist.current;

			// This holds because we increment it syncronously
			if (timer_now == it->sortvalue)
			{
				TimerHandle *handle = (TimerHandle*) it;

				ListRemove (it);
				it->container = 0;

				if (handle->flags & TIMER_FLAG_REPEATING)
					ListAddSorted (&timerlist, it, timer_now + handle->interval);

				// TODO: Do not call while in critical
				handle->Callback (handle);
			}
			else
				break;
		}

		// Re-enable interrupts
		SchedExitCritical ();
	}
}

void TimerThread (void)
{
	while (1)
	{
		TimerUpdate ();
		SchedState (currentthc, SCHED_STATUS_SLEEPING, 0, 0, 1);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void TimerInitialize (void)
{
	static struct ThreadControl timerthread;
	static unsigned int timerstack[64];

	timer_now = PlatformGetTick ();
	ListInit (&timerlist);

	timerthread.privileged = 0;
	SchedAddThread (&timerthread, &TimerThread, timerstack, 64, SCHED_PRIORITY_MEDIUM);
}

void TimerRegister (TimerHandle *handle)
{
	ListItem *it = (ListItem*) handle;

	if (it->container)
		ListRemove (it);

	ListAddSorted (&timerlist, it, timer_now + handle->interval);
}

void TimerUnRegister (TimerHandle *handle)
{
	ListItem *it = (ListItem*) handle;

	if (it->container)
	{
		ListRemove (it);
		it->container = 0;
	}
}

int TimerIsRegistered (TimerHandle *handle)
{
	ListItem *it = (ListItem*) handle;

	return it->container ? 1 : 0;
}
