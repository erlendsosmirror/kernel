/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "ioctl/ioctl.h"
#include "kmod/kmod.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define VOLUME_HEADROOM 6

///////////////////////////////////////////////////////////////////////////////
// External functions
///////////////////////////////////////////////////////////////////////////////
int dev_i2c_open (struct FileNode *node, const char *filename);
int dev_i2c_read (struct FileNode *node, void *data, unsigned int nbytes);
int dev_i2c_write (struct FileNode *node, void *data, unsigned int nbytes);

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
int lch_cur;
int rch_cur;

///////////////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////////////
int PT2257_SaveVol (struct FileNode *node, int lch, int rch)
{
	unsigned char data[] = {0x52, 0, lch >> 24, lch >> 16, lch >> 8, lch,
		rch >> 24, rch >> 16, rch >> 8, rch};

	int ret = dev_i2c_write (node, data, sizeof(data));

	if (ret < 0)
		return ret;

	return 0;
}

int PT2257_LoadVol (struct FileNode *node, int *lch, int *rch)
{
	unsigned char data[9] = {0x52, 0};
	int ret;

	if ((ret = dev_i2c_write (node, data, 2)) != 2)
		return ret;

	if ((ret = dev_i2c_read (node, data, 9)) != 9)
		return ret;

	*lch = (data[1] << 24) | (data[2] << 16) | (data[3] << 8) | data[4];
	*rch = (data[5] << 24) | (data[6] << 16) | (data[7] << 8) | data[8];

	return 0;
}

int PT2257_SetVol (struct FileNode *node, int lch, int rch)
{
	lch -= VOLUME_HEADROOM;
	rch -= VOLUME_HEADROOM;

	if (lch > 0) lch = 0;
	if (lch < -79) lch = -79;
	if (rch > 0) rch = 0;
	if (rch < -79) rch = -79;

	lch_cur = lch + VOLUME_HEADROOM;
	rch_cur = rch + VOLUME_HEADROOM;

	lch = -lch;
	rch = -rch;

	unsigned char data[9];

	data[0] = 0x44;
	data[1] = 0xB0 | (lch / 10);
	data[2] = 0xB0 | (lch / 10);
	data[3] = 0xA0 | (lch % 10);
	data[4] = 0xA0 | (lch % 10);
	data[5] = 0x30 | (rch / 10);
	data[6] = 0x30 | (rch / 10);
	data[7] = 0x20 | (rch % 10);
	data[8] = 0x20 | (rch % 10);

	int ret = dev_i2c_write (node, data, 9);

	if (ret < 0)
		return ret;

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_pt2257_open (struct FileNode *node, const char *filename)
{
	static struct FileNode pt2257_node;
	static char pt2257_initialized;

	node->priv = &pt2257_node;

	if (!pt2257_initialized)
	{
		int ret;

		if ((ret = dev_i2c_open (&pt2257_node, "/dev/i2c")))
			return ret;

		pt2257_initialized = 1;

		if ((ret = PT2257_LoadVol (&pt2257_node, &lch_cur, &rch_cur)))
			return ret;

		if ((ret = PT2257_SetVol (&pt2257_node, lch_cur, rch_cur)))
			return ret;
	}

	return 0;
}

int dev_pt2257_close (struct FileNode *node)
{
	return 0;
}

int dev_pt2257_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	if (nbytes != 8)
		return -EINVAL;

	((int*)data)[0] = lch_cur;
	((int*)data)[1] = rch_cur;

	return nbytes;
}

int dev_pt2257_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	if (nbytes != 8)
		return -EINVAL;

	int ret;

	if ((ret = PT2257_SetVol ((struct FileNode*)node->priv, ((int*)data)[0], ((int*)data)[1])))
		return ret;

	if ((ret = PT2257_SaveVol (node, lch_cur, rch_cur)))
		return ret;

	return nbytes;
}

int dev_pt2257_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	// Unknown ioctl: Return "Inappropriate ioctl for device"
	return -ENOTTY;
}

void dev_pt2257_init (void)
{
	struct FileNode node;

	dev_pt2257_open (&node, 0);
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (pt2257,
	dev_pt2257_open,
	dev_pt2257_close,
	dev_pt2257_read,
	dev_pt2257_write,
	dev_pt2257_ioctl,
	"pt2257");

KMOD_DESCRIPTOR (pt2257, dev_pt2257_init, "pt2257_init");
