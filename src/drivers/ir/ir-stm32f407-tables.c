/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "ioctl/ioctl.h"

///////////////////////////////////////////////////////////////////////////////
// Data
///////////////////////////////////////////////////////////////////////////////
const unsigned char key2ascii[128] =
{
	0,    0,   'b', '\t', 142, ' ', 0,   0,		// 0 - 7	141=right
	'+',  0,   0,   '\r', 0,   143, 0,   0,		// 8 - F	140=left
	0,    0,   'a', 's',  'd', 'f', 'j', 0,		// 10 - 17
	'k',  'l', 0,   0,    0,   0,   '<', 0,		// 18 - 1F
	0,    0,   'q', 'w',  'e', 'r', 'u', 136,	// 20 - 27	136=rec
	'i',  'o', 0,   'p',  '"', 0,   0,   0,		// 28 - 2F
	0,    0,   'z', 'x',  'c', 'v', 'm', 0,		// 30 - 37
	',',  '.', 0,   '-',  0,   'n', 0,   0,		// 38 - 3F
	0,    0,   139, 0,    0x18,'5', '6', 0,		// 40 - 47	18=^X(Green,"Quit"), 139=mute
	0x1A, 133, 0,   '\\', 8,   0,   0,   0,		// 48 - 4F	1A=^Z(Yellow), 133=stop
	0,    0,   138, 137,  3,   'g', 'h', 0,		// 50 - 57	3=^C(Red), 137=vol-, 138=vol+
	0x0F, 130, 134, 0,    140, '\r',0,   '\'',	// 58 - 5F	F=^O(Blue,"Save"), 130=play/pause, 134=prev, 142=up, OK
	0,    0,   '1', '2',  '3', '4', '7', 0,		// 60 - 67
	'8',  '9', 0,   '0',  141, 0,   0,   0,		// 68 - 6F	143=down
	0,    0,   0,    0,   0,   't', 'y', 0,		// 70 - 77
	132,  131, 135,  0,   0,   0,   0,   0 		// 78 - 7F	131=rewind, 132=fastforward, 135=next
};

const unsigned char key2asciishifted[128] =
{
	0,    0,   'B', 0,    0,   ' ', 0,   0,		// 0 - 7	missing back tab
	'?',  0,   0,   '\r', 0,   0,   0,   0,		// 8 - F
	0,    0,   'A', 'S',  'D', 'F', 'J', 0,		// 10 - 17
	'K',  'L', 0,   0,    0,   0,   '>', 0,		// 18 - 1F
	0,    0,   'Q', 'W',  'E', 'R', 'U', 0,		// 20 - 27
	'I',  'O', 0,   'P',  '^', 0,   0,   0,		// 28 - 2F
	0,    0,   'Z', 'X',  'C', 'V', 'M', 0,		// 30 - 37
	';',  ':', 0,   '_',  0,   'N', 0,   0,		// 38 - 3F
	0,    0,   0,   0,    0x18,'%', '&', 0,		// 40 - 47	18=^X(Green,"Quit")
	0x1A, 0,   0,   '`',  8,   0,   0,   0,		// 48 - 4F	1A=^Z(Yellow)
	0,    0,   0,   0,    3,   'G', 'H', 0,		// 50 - 57	3=^C(Red)
	0x0F, 0,   0,   0,    0,   0,   0,   '*',	// 58 - 5F	F=^O(Blue,"Save")
	0,    0,   '!', '"',  '#', ' ', '/', 0,		// 60 - 67	missing ¤ (multi-byte)
	'(',  ')', 0,   '=',  0,   0,   0,   0,		// 68 - 6F
	0,    0,   0,    0,   0,   'T', 'Y', 0,		// 70 - 77
	0,    0,   0,    0,   0,   0,   0,   0 		// 78 - 7F
};

const unsigned char key2asciialtgr[128] =
{
	0,    0,   0,   0,    0,   ' ', 0,   0,		// 0 - 7
	0,    0,   0,   '\r', 0,   0,   0,   0,		// 8 - F
	0,    0,   0,   0,    0,   0,   0,   0,		// 10 - 17
	0,    0,   0,   0,    0,   0,   0,   0,		// 18 - 1F
	0,    0,   0,   0,    0,   0,   0,   0,		// 20 - 27
	0,    0,   0,   0,    '~', 0,   0,   0,		// 28 - 2F
	0,    0,   0,   0,    0,   0,   0,   0,		// 30 - 37
	0,    0,   0,   0,    0,   0,   0,   0,		// 38 - 3F
	0,    0,   0,   0,    0x18,0,   0,   0,		// 40 - 47	18=^X(Green,"Quit")
	0x1A, 0,   0,   '\'', 8,   0,   0,   0,		// 48 - 4F	1A=^Z(Yellow)
	0,    0,   0,   0,    3,   0,   0,   0,		// 50 - 57	3=^C(Red)
	0x0F, 0,   0,   0,    0,   0,   0,   0,		// 58 - 5F	F=^O(Blue,"Save")
	0,    0,   0,   '@',  ' ', '$', '{', 0,		// 60 - 67	missing £ (multi-byte)
	'[',  ']', 0,   '}',  0,   0,   0,   0,		// 68 - 6F
	0,    0,   0,    0,   0,   0,   0,   0,		// 70 - 77
	0,    0,   0,    0,   0,   0,   0,   0 		// 78 - 7F
};
