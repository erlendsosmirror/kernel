/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "posix/sys/signal.h"
#include "ioctl/ioctl.h"
#include "process/process.h"
#include "platform/platform.h"
#include "arch/stm32f407/rcc.h"
#include "drivers/ir/ir-stm32f407.h"
#include "drivers/gpio/gpio-stm32f407.h"
#include "../config/live/drivers/ir.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
ir *ir_irq_instances[1];

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void IRPostByte (ir *ir, char c)
{
	// Check for special function
	if (c == 0x03)
		ProcessSendSignal (-ir->ir_group, SIGINT);
	else if (c == 0x1A)
		ProcessSendSignal (-ir->ir_group, SIGTSTP);
	else if (c >= 140)
	{
		ir->queue_keys[ir->queue_wrpos] = 27;
		ir->queue_wrpos = (ir->queue_wrpos + 1) & 0x0F;
		ir->queue_keys[ir->queue_wrpos] = 91;
		ir->queue_wrpos = (ir->queue_wrpos + 1) & 0x0F;
		ir->queue_keys[ir->queue_wrpos] = c - 140 + 65;
		ir->queue_wrpos = (ir->queue_wrpos + 1) & 0x0F;
	}
	else if (c)
	{
		ir->queue_keys[ir->queue_wrpos] = c;
		ir->queue_wrpos = (ir->queue_wrpos + 1) & 0x0F;
	}
}

void IRHandleKey (ir *ir, unsigned int key)
{
	// Extract keycode and flags
	int keycode = (int) (key >> 8) & 0x007F;
	int released = key & (1 << 23);
	int shiftheld = key & (1 << 21);
	int altgrheld = key & (1 << 18);

	// Caps Lock, shift key, alt-gr
	if (keycode == 0x73)
	{
		ir->capslock = !ir->capslock;
		return;
	}
	else if (keycode == 0x07 || keycode == 0x21)
		return;

	// Variables
	char character;

	// Get from the correct table
	if (!shiftheld != !ir->capslock && !altgrheld)
		character = key2asciishifted[keycode];
	else if (!shiftheld && !ir->capslock && altgrheld)
		character = key2asciialtgr[keycode];
	else
		character = key2ascii[keycode];

	// Repetition
	if (released)
	{
		ir->repetition = 0;
		ir->rep_char = 0;
	}
	else if (!ir->repetition)
	{
		ir->rep_char = character;
		ir->repetition++;
		IRPostByte (ir, character);
	}
	else
	{
		ir->repetition++;

		if (ir->repetition > 5)
			IRPostByte (ir, character);
	}
}

void IROut (ir *ir, unsigned int bit)
{
	if (ir->keystate < 32 && bit)
		ir->key |= (1 << (30 - ir->keystate));

	ir->keystate++;
}

void IRCalculate (ir *ir)
{
	// Previous change in the middle of bit?
	unsigned int prevmiddle = 1;
	unsigned int changedto = 0;
	ir->keystate = 0;

	for (int i = 4; i < ir->state; i++)
	{
		if (ir->values[i] > 12)
			continue;

		if (ir->values[i] > 5)
		{
			prevmiddle = 1;
			changedto = !changedto;
			IROut (ir, changedto);
		}
		else
		{
			if (!prevmiddle)
				IROut (ir, changedto);

			prevmiddle = !prevmiddle;
		}
	}

	IRHandleKey (ir, ir->key);
}

void IR_IRQHANDLER (void)
{
	// Get ir pointer
	ir *ir = ir_irq_instances[0];

	// Get register
	unsigned int reg = ir->instance->SR;
	ir->instance->SR = 0;

	if (reg & 0x0010)
	{
		// Capture occured
		if (ir->state == 0)
		{
			// 30ms total timeout, update, start counter, reset oldtim

			ir->instance->CR1 = 1 << 2;		// Update Request Source: Only overflow
			ir->instance->ARR = 300;		// Set timeout to 30 ms
			ir->instance->EGR = 0x0001;		// Generate update event: Clears counter
			ir->instance->CR1 = (1<<2) | 1;	// Start the counter

			ir->oldtim = 0;			// Reset internal old value
			ir->state = 1;			// Set internal state
		}
		else
		{
			// Get value, calculate difference and store old value
			unsigned int val = ir->instance->CCR4;
			unsigned int diff = val - ir->oldtim;
			ir->oldtim = val;

			// Store up to 75 values
			if (ir->state < 75)
			{
				ir->values[ir->state] = diff;
				ir->state++;
			}
		}
	}
	else if (reg & 0x0001)
	{
		// Update event occured. Should only get these after a timeout,
		// so do not bother checking state.

		// Output
		IRCalculate (ir);

		// Reset internal state
		ir->state = 0;
		ir->key = 0;

		// Disable counter
		ir->instance->CR1 = 0;
	}
}


///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_ir_open (struct FileNode *node, const char *filename)
{
	// Static init
	static ir irinstance;
	ir *t = &irinstance;
	node->priv = t;
	ir_irq_instances[0] = t;

	// Enable clocks
	RCC->APB1ENR |= (1 << 0);
	RCC->AHB1ENR |= (1 << 2);

	// Pin init
	GPIOInit (IR_PORT, IR_PIN, GPIO_ALTERNATE(1) | GPIO_AF | GPIO_SPEED_VERYHIGH | GPIO_PULLUP);

	// Set instance
	t->instance = IR_INSTANCE;

	// Initialize
	// 84 MHz APB1 Clock
	// Prescaler = 8400 (84 MHz / 84 / 100 = 10 KHz)
	t->instance->PSC = 8400;

	// Max auto-reload
	t->instance->ARR = 0x0000FFFF;

	// Normal CR2 configuration, no master mode stuff
	t->instance->CR2 = 0;

	// No filter, no prescaler, CC4 is configured input for TI4
	t->instance->CCMR2 = 0x0100;

	// Inverted capture (falling edge), enable ch 4
	t->instance->CCER = 0x3000 | 0x8000;

	// Enable Capture/Compare 4 interrupt and update interrupt
	t->instance->DIER = 0x0011;

	// Re-initialize counter and update registers
	t->instance->EGR = 0x0001;

	// Set DMA channel IRQ priorities
	PlatformSetInterruptPriority (IR_IRQN, 5);
	PlatformEnableInterrupt (IR_IRQN);

	// Done
	return 0;
}

int dev_ir_close (struct FileNode *node)
{
	return 0;
}

int dev_ir_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	ir *t = (ir*) node->priv;

	for (int i = 0; i < nbytes; i++)
	{
		if (t->queue_rdpos == t->queue_wrpos)
			return i;

		*((char*)data) = t->queue_keys[t->queue_rdpos];
		t->queue_rdpos = (t->queue_rdpos + 1) & 0x0F;
	}

	return nbytes;
}

int dev_ir_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	return nbytes;
}

int dev_ir_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	ir *t = (ir*) node->priv;

	switch (code)
	{
	case TCGETPGRP:
		return t->ir_group;
	case TCSETPGRP:
		t->ir_group = (short) ((int)data);
		return 0;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (ir,
	dev_ir_open,
	dev_ir_close,
	dev_ir_read,
	dev_ir_write,
	dev_ir_ioctl,
	"ir");
