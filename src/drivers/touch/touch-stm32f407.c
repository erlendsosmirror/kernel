/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * This driver is for the FT6236
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "posix/sys/signal.h"
#include "ioctl/ioctl.h"
#include "platform/platform.h"
#include "arch/stm32f407/rcc.h"
#include "sched/scheduler.h"
#include "sched/scheduler-signal.h"
#include "process/process-allocate.h"
#include "drivers/gpio/gpio-stm32f407.h"
#include "drivers/lcd/lcd.h"
#include "../config/live/drivers/touch.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define TOUCHEVENT_TYPE_PRESSDOWN	0
#define TOUCHEVENT_TYPE_LIFTUP		1
#define TOUCHEVENT_TYPE_CONTACT		2

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct TouchEvent
{
	short x;
	short y;
	short event;
} TouchEvent;

///////////////////////////////////////////////////////////////////////////////
// External functions
///////////////////////////////////////////////////////////////////////////////
int dev_i2c_open (struct FileNode *node, const char *filename);
int dev_i2c_close (struct FileNode *node);
int dev_i2c_read (struct FileNode *node, void *data, unsigned int nbytes);
int dev_i2c_write (struct FileNode *node, void *data, unsigned int nbytes);

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
int touch_pidtonotify;
int touch_lasteventtype;
TouchEvent touch_ev[4];

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int TouchReadPacket (unsigned char buf[20])
{
	struct FileNode node;
	int ret;

	if ((ret = dev_i2c_open (&node, "i2c")))
		return ret;

	buf[0] = 0x38;
	buf[1] = 0;

	if ((ret = dev_i2c_write (&node, buf, 2)) != 2)
		return ret;

	if ((ret = dev_i2c_read (&node, buf, 17)) != 17)
		return ret;

	return dev_i2c_close (&node);
}

void TOUCH_IRQHANDLER (void)
{
	EXTI->PR = (1 << TOUCH_IRQ_PIN);

	unsigned char uc[20];

	TouchReadPacket (uc);

	int eventno = (uc[4] >> 6) & 0x03;

	touch_ev[eventno].event = eventno;
	touch_ev[eventno].x = uc[5] | ((uc[4] & 0x0F) << 8);
	touch_ev[eventno].y = uc[7] | ((uc[6] & 0x0F) << 8);

	if (LCDGetOrientation ())
	{
		unsigned int w, h;

		LCDGetMetrics (&w, &h);

		short temp = touch_ev[eventno].x;
		touch_ev[eventno].x = w - touch_ev[eventno].y;
		touch_ev[eventno].y = temp;
	}

	if (uc[3] == 1 || eventno == 0x01)
	{
		if (touch_pidtonotify)
		{
			struct ProcessControl *prd = ProcessFindByPid (touch_pidtonotify);

			if (prd)
				SchedPostSignal (&prd->trc, SIGURG);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_touch_open (struct FileNode *node, const char *filename)
{
	touch_pidtonotify = 0;
	touch_lasteventtype = 0;
	touch_ev[0].event = 3;

	RCC->APB2ENR |= 1 << 14;

	GPIOInit (TOUCH_PORT, PIN(TOUCH_IRQ_PIN), GPIO_INPUT);

	EXTI->IMR |= (1 << TOUCH_IRQ_PIN);
	EXTI->FTSR |= (1 << TOUCH_IRQ_PIN);
	SYSCFG->EXTICR1 |= (1 << 4);

	PlatformSetInterruptPriority (TOUCH_IRQN, 5);
	PlatformEnableInterrupt (TOUCH_IRQN);

	return 0;
}

int dev_touch_close (struct FileNode *node)
{
	touch_pidtonotify = 0;
	return 0;
}

int dev_touch_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	// Check alignment
	if ((int)data & 0x00000003)
		return -EINVAL;

	// Check size
	if (nbytes != 6)
		return -EINVAL;

	// Cast data pointer
	TouchEvent *ev = (TouchEvent*) data;

	// Get most relevant event
	TouchEvent *e = &touch_ev[0];

	for (int i = 0; i < 4; i++)
	{
		int ii = (touch_lasteventtype + i) % 4;

		if (touch_ev[ii].event != 3)
		{
			e = &touch_ev[ii];
			touch_lasteventtype = ii;
			break;
		}
	}

	// Copy in a critical section
	SchedEnterCritical ();
	ev->event = e->event;
	ev->x = e->x;
	ev->y = e->y;
	e->event = 3;
	SchedExitCritical ();

	// Done
	return 6;
}

int dev_touch_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	return nbytes;
}

int dev_touch_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	switch (code)
	{
	case F_SETOWN:
		touch_pidtonotify = (int)data;
		break;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	// Default return value
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (touch,
	dev_touch_open,
	dev_touch_close,
	dev_touch_read,
	dev_touch_write,
	dev_touch_ioctl,
	"touch");
