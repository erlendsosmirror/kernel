/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "mem/string.h"
#include "print/print.h"
#include "ioctl/ioctl.h"
#include "drivers/spi/spi-stm32f407.h"
#include "drivers/gpio/gpio-stm32f407.h"
#include "arch/stm32f407/rcc.h"
#include "platform/platform.h"
#include "../config/live/drivers/spi.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
spi *spi_irq_instances[SPI_N_MODULES] = {0};

///////////////////////////////////////////////////////////////////////////////
// Forward declarations
///////////////////////////////////////////////////////////////////////////////
int dev_spi_open (struct FileNode *node, const char *filename);
int dev_spi_close (struct FileNode *node);
int dev_spi_read (struct FileNode *node, void *data, unsigned int nbytes);
int dev_spi_write (struct FileNode *node, void *data, unsigned int nbytes);
int dev_spi_ioctl (struct FileNode *node, unsigned int code, void *data);

///////////////////////////////////////////////////////////////////////////////
// IRQ functions
///////////////////////////////////////////////////////////////////////////////
#if SPI_ENABLE_AVAIL_0
void DMA2CH0_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[0]->dmarx);
}

void DMA2CH5_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[0]->dmatx);
}
#endif

#if SPI_ENABLE_AVAIL_1
void DMA1CH3_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[1]->dmarx);
}

void DMA1CH4_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[1]->dmatx);
}
#endif

#if SPI_ENABLE_AVAIL_2
void DMA1CH0_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[2]->dmarx);
}

void DMA1CH7_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[2]->dmatx);
}
#endif

#if SPI_ENABLE_AVAIL_3
void DMA2CH0_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[3]->dmarx);
}

void DMA2CH1_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[3]->dmatx);
}
#endif

#if SPI_ENABLE_AVAIL_4
void DMA2CH3_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[4]->dmarx);
}

void DMA2CH4_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[4]->dmatx);
}
#endif

#if SPI_ENABLE_AVAIL_5
void DMA2CH6_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[5]->dmarx);
}

void DMA2CH5_IrqHandler (void)
{
	DMAIRQHandler (&spi_irq_instances[5]->dmatx);
}
#endif

///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
void dev_spi_init (spi *t)
{
	// Lookup tables
	static const _SPI *instances[] = {SPI1, SPI2, SPI3, SPI4, SPI5, SPI6};
	static const DMAChannel *dmarx[] = {DMAChannel_2_0, DMAChannel_1_3,
		DMAChannel_1_0, DMAChannel_2_0,
		DMAChannel_2_3, DMAChannel_2_6};
	static const DMAChannel *dmatx[] = {DMAChannel_2_5, DMAChannel_1_4,
		DMAChannel_1_7, DMAChannel_2_1,
		DMAChannel_2_4, DMAChannel_2_5};

	static const volatile unsigned int *rccreg[] =
		{&RCC->APB2ENR, &RCC->APB1ENR, &RCC->APB1ENR, &RCC->APB2ENR, &RCC->APB2ENR, &RCC->APB2ENR};

	static const unsigned int rccval[] =
		{1 << 12, 1 << 14, 1 << 15, 1 << 13, 1 << 20, 1 << 21};

	/*static const unsigned char dmarxirqn[] =
		{56, 14, 11, 56, 59, 69};

	static const unsigned char dmatxirqn[] =
		{68, 15, 47, 57, 60, 68};

	static const unsigned char spiirqn[] =
		{35, 36, 51, 84, 85, 86};*/

	static const GPIO *ports[] =
		{GPIOB, GPIOB, GPIOC, GPIOE, GPIOF, GPIOG};

	static const int pins[] =
		{PIN(3) | PIN(4) | PIN(5),
		PIN(10) | PIN(14) | PIN(15),
		PIN(10) | PIN(11) | PIN(12),
		PIN(2) | PIN(5) | PIN(6),
		PIN(7) | PIN(8) | PIN(9),
		PIN(12) | PIN(13) | PIN(14)};

	static const int modes[] =
		{GPIO_ALTERNATE(5) | GPIO_AF | GPIO_SPEED_VERYHIGH};

	static const int chsels[] =
		{DMA_CR_CHSEL3, DMA_CR_CHSEL0, DMA_CR_CHSEL0,
		DMA_CR_CHSEL4, DMA_CR_CHSEL2, DMA_CR_CHSEL1};

	// Enable the clock
	volatile unsigned int *clockreg = (volatile unsigned int*) rccreg[t->spinum];
	*clockreg |= rccval[t->spinum];

	// Enable GPIO clocks
	RCC->AHB1ENR |= 0x7FF;

	// Set instances
	t->instance = (_SPI*) instances[t->spinum];
	t->dmarx.chinst = (DMAChannel*) dmarx[t->spinum];
	t->dmatx.chinst = (DMAChannel*) dmatx[t->spinum];

	// Store initial DMA config, rx medium priority
	t->dmarx.config = chsels[t->spinum] | DMA_CR_DIR_P2M | 0x00010000;
	t->dmatx.config = chsels[t->spinum] | DMA_CR_DIR_M2P | DMA_CR_MINC;

	// Make sure DMA channels and SPI is disabled
	t->dmatx.chinst->CR = 0;
	t->dmarx.chinst->CR = 0;
	t->instance->CR1 = 0;

	// Set output pins
	GPIOInit ((GPIO*)ports[t->spinum], pins[t->spinum], modes[t->spinum]);

	// Set chip select pins
	GPIOInit (SPI_PORT_CS, SPI_PIN_CS0, GPIO_OUTPUT);

	// Set DMA channel and IRQ priorities
	//PlatformSetInterruptPriority (dmarxirqn[t->spinum], 5);
	//PlatformSetInterruptPriority (dmatxirqn[t->spinum], 5);
	//PlatformSetInterruptPriority (spiirqn[t->spinum], 5);
	//PlatformEnableInterrupt (dmarxirqn[t->spinum]);
	//PlatformEnableInterrupt (dmatxirqn[t->spinum]);
	//PlatformEnableInterrupt (spiirqn[t->spinum]);

	// Set config
	t->instance->CR1 = CR1_MSTR | CR1_SSI | CR1_SSM | CR1_BRDIV2;
	t->instance->CR2 = 0;

	// Enable the SPI
	t->instance->CR1 |= CR1_SPE;

	// Configure DMA
	DMAInit (&t->dmarx);
	DMAInit (&t->dmatx);
}

void dev_spi_printflags (unsigned int flags, spi *t, const char *fmt)
{
	if (flags & 0x08)
	{
		Print (fmt, (int)t);

		if (flags & 0x01) Print ("DMA 'A FIFO Error event occurred'\n");
		if (flags & 0x04) Print ("DMA 'A Direct Mode Error occurred'\n");
		if (flags & 0x08) Print ("DMA 'A transfer error occurred'\n");
		if (flags & 0x10) Print ("DMA 'A half transfer event occurred'\n");
		if (flags & 0x20) Print ("DMA 'A transfer complete event occurred'\n");
	}
}

int dev_spi_txtx (spi *t, void *txbuf, void *rxbuf, unsigned int size, unsigned int timeout)
{
	// Check for buffer in ccm
	if (((unsigned int) txbuf & 0xF0000000) == 0x10000000)
		Print ("Error: txbuf pointing to CCM\n");
	if (((unsigned int) rxbuf & 0xF0000000) == 0x10000000)
		Print ("Error: rxbuf pointing to CCM\n");

	// Disable the channels, make sure they are disabled and clear irqs
	t->instance->CR2 = 0;
	t->dmatx.chinst->CR = 0;
	t->dmarx.chinst->CR = 0;
	while (t->dmatx.chinst->CR & 0x01);
	while (t->dmarx.chinst->CR & 0x01);
	t->dmatx.dma->IFCR = 0x0000003D << t->dmatx.shift;
	t->dmarx.dma->IFCR = 0x0000003D << t->dmarx.shift;

	// Set the peripheral port address registers
	t->dmatx.chinst->PAR = (unsigned int) &t->instance->DR;
	t->dmarx.chinst->PAR = (unsigned int) &t->instance->DR;

	// Set the memory address registers
	volatile unsigned int dummy;

	t->dmatx.chinst->M0AR = (unsigned int) txbuf;
	t->dmarx.chinst->M0AR = (!rxbuf) ? (unsigned int)&dummy : (unsigned int)rxbuf;

	// Set the transfer count registers
	t->dmatx.chinst->NDTR = size;
	t->dmarx.chinst->NDTR = size;

	// Configure FIFOs
	t->dmatx.chinst->FCR = 7;
	t->dmarx.chinst->FCR = 0;

	// Set configuration of channels and activate streams
	t->dmatx.chinst->CR = t->dmatx.config | 1;
	t->dmarx.chinst->CR = t->dmarx.config | ((!rxbuf) ? 1 : (1 | DMA_CR_MINC));

	// Activate SPI streams with DMA
	t->instance->CR2 = 0x03;

	// Wait for completion, checking TCIF, HTIF, TEIF
	unsigned int tickstart = PlatformGetTick ();

	while (!(t->dmarx.dma->ISR & (0x00000028 << t->dmarx.shift)))
	{
		if ((PlatformGetTick() - tickstart) > timeout)
		{
			Print ("DMA Transfer timeout, ISR %x\n", t->dmarx.dma->ISR);
			while (1);
			return -1;
		}
	}

	// Disable the peripheral and DMA
	t->dmatx.chinst->CR = 0;
	t->dmarx.chinst->CR = 0;
	//t->instance->CR2 = 0;

	// Check flags
	dev_spi_printflags (t->dmatx.dma->ISR >> t->dmatx.shift, t, "tx t %x:\n");
	dev_spi_printflags (t->dmarx.dma->ISR >> t->dmarx.shift, t, "rx t %x:\n");
	return size;
}

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_spi_open (struct FileNode *node, const char *filename)
{
	unsigned int spinum = filename[3] - '0';

	if (spinum > (SPI_N_MODULES-1))
		return -ENXIO;

	unsigned int legal = 0 |

#if SPI_ENABLE_AVAIL_0
		(1 << 0) |
#endif
#if SPI_ENABLE_AVAIL_1
		(1 << 1) |
#endif
#if SPI_ENABLE_AVAIL_2
		(1 << 2) |
#endif
#if SPI_ENABLE_AVAIL_3
		(1 << 3) |
#endif
#if SPI_ENABLE_AVAIL_4
		(1 << 4) |
#endif
#if SPI_ENABLE_AVAIL_5
		(1 << 5) |
#endif
		0;

	if (!((1 << spinum) & legal))
		return -ENXIO;

	if (spi_irq_instances[spinum])
		return -EBUSY;

	static spi ty[SPI_N_MODULES];
	spi *t = &ty[spinum];

	MemSet (t, 0, sizeof(spi));

	t->spinum = spinum;
	spi_irq_instances[spinum] = t;
	node->priv = (void*) t;

	dev_spi_init (t);
	dev_spi_ioctl (node, SPICS, (void*)0);
	return 0;
}

int dev_spi_close (struct FileNode *node)
{
	spi *t = (spi*) node->priv;

	t->dmatx.chinst->CR = 0;
	t->dmarx.chinst->CR = 0;
	t->instance->CR1 = 0;

	spi_irq_instances[t->spinum] = 0;
	return 0;
}

int dev_spi_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	spi *t = (spi*) node->priv;
	return dev_spi_txtx (t, data, data, nbytes, 1000);
}

int dev_spi_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	spi *t = (spi*) node->priv;
	return dev_spi_txtx (t, data, 0, nbytes, 1000);
}

int dev_spi_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	(void)node;

	switch (code)
	{
	case SPIBUSY:
		return 0;
	case SPICS:
		if ((int)data)
			SPI_PORT_CS->BSRR = SPI_PIN_CS0 << 16;	// Assert by clearing
		else
			SPI_PORT_CS->BSRR = SPI_PIN_CS0 << 0;	// Deassert by setting
		return 0;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
#if SPI_ENABLE_AVAIL_0
MODULE_DESCRIPTOR (spi0,
	dev_spi_open,
	dev_spi_close,
	dev_spi_read,
	dev_spi_write,
	dev_spi_ioctl,
	"spi0");
#endif

#if SPI_ENABLE_AVAIL_1
MODULE_DESCRIPTOR (spi1,
	dev_spi_open,
	dev_spi_close,
	dev_spi_read,
	dev_spi_write,
	dev_spi_ioctl,
	"spi1");
#endif

#if SPI_ENABLE_AVAIL_2
MODULE_DESCRIPTOR (spi2,
	dev_spi_open,
	dev_spi_close,
	dev_spi_read,
	dev_spi_write,
	dev_spi_ioctl,
	"spi2");
#endif

#if SPI_ENABLE_AVAIL_3
MODULE_DESCRIPTOR (spi3,
	dev_spi_open,
	dev_spi_close,
	dev_spi_read,
	dev_spi_write,
	dev_spi_ioctl,
	"spi3");
#endif

#if SPI_ENABLE_AVAIL_4
MODULE_DESCRIPTOR (spi4,
	dev_spi_open,
	dev_spi_close,
	dev_spi_read,
	dev_spi_write,
	dev_spi_ioctl,
	"spi4");
#endif

#if SPI_ENABLE_AVAIL_5
MODULE_DESCRIPTOR (spi5,
	dev_spi_open,
	dev_spi_close,
	dev_spi_read,
	dev_spi_write,
	dev_spi_ioctl,
	"spi5");
#endif
