/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/string.h"
#include "posix/errno.h"
#include "fs/dev/dev.h"
#include "ioctl/ioctl.h"
#include "drivers/spi/spi-stm32f407.h"
#include "drivers/flash/spi-nor.h"
#include "../config/live/drivers/spi-nor.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define N_SPIFLASH_MODULES 1

#define FLASH_CMD_RDSTATUS1		0x05
#define FLASH_CMD_RDSTATUS2		0x35
#define FLASH_CMD_PAGEPROGRAM	0x02
#define FLASH_CMD_SECTORERASE	0x20
#define FLASH_CMD_JEDECID		0x9F
#define FLASH_CMD_READ			0x03
#define FLASH_CMD_FASTREAD		0x0B
#define FLASH_CMD_WRITEENABLE	0x06

#define FLASH_BLOCK_SIZE		4096

///////////////////////////////////////////////////////////////////////////////
// Forward declarations of SPI things
///////////////////////////////////////////////////////////////////////////////
int dev_spi_open (struct FileNode *node, const char *filename);
int dev_spi_close (struct FileNode *node);
int dev_spi_read (struct FileNode *node, void *data, unsigned int nbytes);
int dev_spi_write (struct FileNode *node, void *data, unsigned int nbytes);
int dev_spi_ioctl (struct FileNode *node, unsigned int code, void *data);

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////

// These are used by the IRQ handlers to get back to the data
struct spiflash *spiflash_irq_instances[N_SPIFLASH_MODULES] = {0};

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int JedecID (struct FileNode *spi)
{
	// Wait timer
	int timer = 1000000;

	// Wait for the SPI to become ready
	while (timer && dev_spi_ioctl (spi, SPIBUSY, 0))
		timer--;

	// Select chip
	dev_spi_ioctl (spi, SPICS, (void*)1);

	// Write command
	unsigned char cmd[] = {FLASH_CMD_JEDECID};
	int jedec = 0;

	while (timer && dev_spi_write (spi, cmd, 1) == -EBUSY)
		timer--;

	// Read data into buffer
	while (timer && dev_spi_read (spi, &jedec, 4) == -EBUSY)
		timer--;

	// Wait for completion and deassert chip select
	while (timer && dev_spi_ioctl (spi, SPIBUSY, 0))
		timer--;

	dev_spi_ioctl (spi, SPICS, (void*)0);

	// Check timeout
	if (!timer)
		return -1;

	// Done
	return jedec;
}

int WriteEnable (struct spiflash *t)
{
	// Wait timer
	int timer = 1000000;

	// Wait for the SPI to become ready
	while (timer && dev_spi_ioctl (&t->spi, SPIBUSY, 0))
		timer--;

	// Select chip
	dev_spi_ioctl (&t->spi, SPICS, (void*)1);

	// Write command
	unsigned char cmd[] = {FLASH_CMD_WRITEENABLE};

	while (timer && dev_spi_write (&t->spi, cmd, 1) == -EBUSY)
		timer--;

	// Wait for completion and deassert chip select
	while (timer && dev_spi_ioctl (&t->spi, SPIBUSY, 0))
		timer--;

	dev_spi_ioctl (&t->spi, SPICS, (void*)0);

	// Check timer
	if (!timer)
		return -1;

	// Done
	return 0;
}

int EraseSector (struct spiflash *t)
{
	// Wait timer
	int timer = 160000000;

	// Wait for ready
	while (timer && dev_spi_ioctl (&t->spi, SPIBUSY, 0))
		timer--;

	// Select chip
	dev_spi_ioctl (&t->spi, SPICS, (void*)1);

	// Address and command
	unsigned int address = t->sector * FLASH_BLOCK_SIZE;
	unsigned char cmd[] = {FLASH_CMD_SECTORERASE, address >> 16, address >> 8, address};

	// Write command
	int ret = 0;

	while (timer && (ret = dev_spi_write (&t->spi, cmd, 4)) == -EBUSY)
		timer--;

	if (ret != 4)
		return ret;

	// Wait for completion and deassert chip select
	while (timer && dev_spi_ioctl (&t->spi, SPIBUSY, 0))
		timer--;

	dev_spi_ioctl (&t->spi, SPICS, (void*)0);

	// Check timer
	if (!timer)
		return -1;

	// Done
	return 0;
}

int WaitForReadyFlag (struct spiflash *t)
{
	// Wait timer
	int timer = 1000000;

	// Wait for ready
	while (timer && dev_spi_ioctl (&t->spi, SPIBUSY, 0))
		timer--;

	// Select chip
	dev_spi_ioctl (&t->spi, SPICS, (void*)1);

	// Write command
	unsigned char cmd[] = {FLASH_CMD_RDSTATUS1};
	int ret = 0;

	while (timer && (ret = dev_spi_write (&t->spi, cmd, 1)) == -EBUSY)
		timer--;

	if (ret != 1)
		return ret;

	// Read status until ready
	while (1)
	{
		// Read status byte
		unsigned char response = 0;

		while (timer > 0 && dev_spi_read (&t->spi, &response, 1) == -EBUSY)
			timer--;

		// Wait for completion
		while (timer > 0 && dev_spi_ioctl (&t->spi, SPIBUSY, 0))
			timer--;

		// Check timer
		if (timer <= 0)
			return -1;

		// Check received byte
		if (!(response & 0x01))
			break;

		// Prevent looping forever if none of the calls fail, but the response is bad
		timer--;
	}

	// Wait for completion and deassert chip select
	dev_spi_ioctl (&t->spi, SPICS, (void*)0);

	// Done
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
int dev_spiflash_open (struct FileNode *node, const char *filename)
{
	// Get number
	unsigned int num = filename[6] - '0';

	// Check it
	if (num >= N_SPIFLASH_MODULES)
		return -ENXIO;

	// Already initialized?
	if (spiflash_irq_instances[num])
		return -EBUSY;

	// Allocate (static for now)
	static struct spiflash sp[N_SPIFLASH_MODULES];
	struct spiflash *t = &sp[num];

	// Clear the structure
	MemSet (t, 0, sizeof (struct spiflash));

	// Try to open spi driver
	if (dev_spi_open (&t->spi, SPINOR_BUS_NAME))
		return -EBUSY;

	// Set the irq-handlers
	spiflash_irq_instances[num] = t;

	// Set the file node
	node->priv = (void*) t;

	// Set the number
	t->num = num;

	// Done
	return 0;
}

int dev_spiflash_close (struct FileNode *node)
{
	// Extract pointer
	struct spiflash *t = (struct spiflash*) node->priv;

	// Close spi driver
	dev_spi_close (&t->spi);

	// Clear allocation
	spiflash_irq_instances[t->num] = 0;

	// Done
	return 0;
}

int dev_spiflash_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	// Only allow reading multiples of 4096
	if (nbytes % 4096)
		return -EINVAL;

	// Extract pointer
	struct spiflash *t = (struct spiflash*) node->priv;

	// Wait timer
	int timer = 1000000;

	// Wait for ready
	while (timer && dev_spi_ioctl (&t->spi, SPIBUSY, 0))
		timer--;

	// Select chip
	dev_spi_ioctl (&t->spi, SPICS, (void*)1);

	// Address and command
	unsigned int address = t->sector * FLASH_BLOCK_SIZE;
	unsigned char cmd[] = {FLASH_CMD_FASTREAD, address >> 16, address >> 8, address, 0x00};

	// Write command
	int ret = 0;

	while (timer && (ret = dev_spi_write (&t->spi, cmd, 5)) == -EBUSY)
		timer--;

	if (ret != 5)
		return ret;

	// Read data into buffer
	while (timer && (ret = dev_spi_read (&t->spi, data, (nbytes/4096) * FLASH_BLOCK_SIZE)) == -EBUSY)
		timer--;

	// Wait for completion and deassert chip select
	while (timer && dev_spi_ioctl (&t->spi, SPIBUSY, 0))
		timer--;

	dev_spi_ioctl (&t->spi, SPICS, (void*)0);

	// Check timer
	if (!timer)
		return -EBUSY;

	// Done
	return nbytes;
}

int dev_spiflash_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	// Only allow writing multiples of 4096
	if (nbytes % 4096)
		return -EINVAL;

	// Extract pointer
	struct spiflash *t = (struct spiflash*) node->priv;

	// Enable writing
	if (WriteEnable (t))
		return -EBUSY;

	// Erase sector
	if (EraseSector (t))
		return -EBUSY;

	// Wait for erase to complete
	if (WaitForReadyFlag (t))
		return -EBUSY;

	// Write position
	unsigned int pos = 0;

	// Write sector parts
	for (int i = 0; i < 16; i++)
	{
		// Enable writing
		if (WriteEnable (t))
			return -EBUSY;

		// Wait timer
		int timer = 1000000;

		// Wait for ready
		while (timer && dev_spi_ioctl (&t->spi, SPIBUSY, 0))
			timer--;

		// Select chip
		dev_spi_ioctl (&t->spi, SPICS, (void*)1);

		// Address and command
		unsigned int address = (t->sector * FLASH_BLOCK_SIZE) + (i * 256);
		unsigned char cmd[] = {FLASH_CMD_PAGEPROGRAM, address >> 16, address >> 8, address};

		// Write command
		int ret = 0;

		while (timer && (ret = dev_spi_write (&t->spi, cmd, 4)) == -EBUSY)
			timer--;

		if (ret != 4)
			return ret;

		// Write data
		while (timer && (ret = dev_spi_write (&t->spi, data + pos, 256)) == -EBUSY)
			timer--;

		// Wait for completion and deassert chip select
		while (timer && dev_spi_ioctl (&t->spi, SPIBUSY, 0))
			timer--;

		dev_spi_ioctl (&t->spi, SPICS, (void*)0);

		// Check timer
		if (!timer)
			return -EBUSY;

		// Increment byte position
		pos += 256;

		// Wait for write flag
		if (WaitForReadyFlag (t))
			return -EBUSY;
	}

	// Done
	return nbytes;
}

int dev_spiflash_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	// Extract pointer
	struct spiflash *t = (struct spiflash*) node->priv;

	// Switch on the ioctl control code
	switch (code)
	{
	case SPIFSETSECT:
		t->sector = (unsigned int)data;
		return 0;
	case SPIFGETSECT:
		return t->sector;
	case SPIFJEDEC:
		return JedecID (&t->spi);
	case SPIFGETSECTSIZE:
		*(unsigned short*)data = 4096;
		return 0;
	case SPIFGETSECTCNT:
		*(unsigned int*)data = (1 << (JedecID (&t->spi) >> 16)) / 4096;
		return 0;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	// Default return value
	return 0;
}

MODULE_DESCRIPTOR (spiflash,
	dev_spiflash_open,
	dev_spiflash_close,
	dev_spiflash_read,
	dev_spiflash_write,
	dev_spiflash_ioctl,
	"spinor0");

