/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "mem/string.h"
#include "print/print.h"
#include "ioctl/ioctl.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
extern char loader_printrel[16];

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_sys_open (struct FileNode *node, const char *filename)
{
	return 0;
}

int dev_sys_close (struct FileNode *node)
{
	return 0;
}

int dev_sys_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	return 0;
}

int dev_sys_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	return nbytes;
}

int dev_sys_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	switch (code)
	{
	case SYSSHOWREL:
		if (!data)
			loader_printrel[0] = 0;
		else
		{
			char *str = (char*) data;

			for (int i = 0; i < sizeof (loader_printrel); i++)
				if (!(loader_printrel[i] = str[i]))
					break;

			loader_printrel[sizeof (loader_printrel) - 1] = 0;
		}

		break;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	// Default return value
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (sys,
	dev_sys_open,
	dev_sys_close,
	dev_sys_read,
	dev_sys_write,
	dev_sys_ioctl,
	"sys");
