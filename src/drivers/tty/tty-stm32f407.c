/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "mem/string.h"
#include "platform/platform.h"
#include "drivers/tty/tty-stm32.h"
#include "drivers/gpio/gpio-stm32f407.h"
#include "arch/stm32f407/rcc.h"

///////////////////////////////////////////////////////////////////////////////
// Forward declarations
///////////////////////////////////////////////////////////////////////////////
int dev_tty_read_circular (tty *t, void *data, unsigned int nbytes);
int dev_tty_write_circular (tty *t, void *data, unsigned int nbytes);
void dev_tty_irq_circular (tty *t);
int dev_tty_read_direct (tty *t, void *data, unsigned int nbytes);
int dev_tty_write_direct (tty *t, void *data, unsigned int nbytes);
void dev_tty_irq_direct (tty *t);

///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
void dev_tty_uart_defconfig (tty *t, int ttynum)
{
	// Lookup tables
	static const _UART *instances[] = {UART1, UART2, UART3, UART4, UART5, UART6};
	static const DMAChannel *dmarx[] = {DMAChannel_2_2, DMAChannel_1_5, DMAChannel_1_1,
		DMAChannel_1_2, DMAChannel_1_0, DMAChannel_2_1};
	static const DMAChannel *dmatx[] = {DMAChannel_2_7, DMAChannel_1_6, DMAChannel_1_3,
		DMAChannel_1_4, DMAChannel_1_7, DMAChannel_2_6};
	static const unsigned char chsel[] = {4, 4, 4, 4, 4, 5};

	// Clear tty structure
	MemSet (t, 0, sizeof(tty));

	// Set instances
	t->ttynum = ttynum;
	t->instance = (_UART*) instances[ttynum];
	t->dmarx.chinst = (DMAChannel*) dmarx[ttynum];
	t->dmatx.chinst = (DMAChannel*) dmatx[ttynum];

	// Store initial UART configuration (standard curcular receive at 115200)
	t->stopbits = 0;
	t->word_parity_mode_sampling = MODE_TXRX | OVERSAMPLING_16 | CR1_RXNEIE;
	t->flowcontrol = 0;
	t->baudrate = 115200;

	// Store initial DMA config
	t->dmarx.config = DMA_CR_CHSEL(chsel[t->ttynum]) | DMA_CR_DIR_P2M | DMA_CR_MINC | DMA_CR_CIRC;
	t->dmatx.config = DMA_CR_CHSEL(chsel[t->ttynum]) | DMA_CR_DIR_M2P | DMA_CR_MINC;

	// Set rxbuf
	t->rxbuf = t->defrxbuf;
	t->rxbufsize = TTY_BUF_SIZE;

	// Set default handlers
	t->read = dev_tty_read_circular;
	t->write = dev_tty_write_circular;
	t->irq = dev_tty_irq_circular;
}

void dev_tty_uart_altconfig (tty *t)
{
	// In direct mode we do not want reception interrupts
	t->word_parity_mode_sampling &= ~CR1_RXNEIE;

	// Error interrupt enable
	t->flowcontrol = 1;

	// Set reception DMA config, change to "normal" and high priority
	t->dmarx.config = DMA_CR_CHSEL4 | DMA_CR_DIR_P2M | DMA_CR_MINC;
	t->dmarx.fconfig = 0;

	// Set handlers
	t->read = dev_tty_read_direct;
	t->write = dev_tty_write_direct;
	t->irq = dev_tty_irq_direct;
}

void dev_tty_uart_setbaud (tty *t)
{
	// Get correct peripheral clock
	unsigned int clk;

	if (t->instance == UART1 || t->instance == UART6)
		clk = PlatformPCLK2 ();
	else
		clk = PlatformPCLK1 ();

	// Set correct baud based on oversampling
	if (t->word_parity_mode_sampling & OVERSAMPLING_8)
		t->instance->BRR = (clk * 2) / t->baudrate;
	else
		t->instance->BRR = clk / t->baudrate;
}

void dev_tty_uart_init (tty *t)
{
	// Lookup tables
	static const volatile unsigned int *rccreg[] =
		{&RCC->APB2ENR, &RCC->APB1ENR, &RCC->APB1ENR,
		&RCC->APB1ENR, &RCC->APB1ENR, &RCC->APB2ENR};

	static const unsigned int rccval[] =
		{(1 << 4), (1 << 17), (1 << 18),
		(1 << 19), (1 << 20), (1 << 5)};

	static const unsigned char dmarxirqn[] =
		{58, 16, 12, 13, 11, 57};

	static const unsigned char dmatxirqn[] =
		{70, 17, 14, 15, 47, 69};

	static const unsigned char uartirqn[] =
		{37, 38, 39, 52, 53, 71};

	static const GPIO *ports[] =
		{GPIOA, GPIOA, GPIOB, GPIOA, GPIOD, GPIOC};

	static const int pins[] =
		{PIN(9) | PIN(10), PIN(2) | PIN(3), PIN(10) | PIN(11),
		PIN(0) | PIN(1), PIN(2), PIN(6) | PIN(7)};

	static const unsigned char config[] =
		{7, 7, 7, 8, 8, 8};

	// Make sure DMA channels and UART is disabled
	t->dmatx.chinst->CR = 0;
	t->dmarx.chinst->CR = 0;
	t->instance->CR1 = 0;

	// Enable the clock
	volatile unsigned int *clockreg = (volatile unsigned int*) rccreg[t->ttynum];
	*clockreg |= rccval[t->ttynum];

	// Enable GPIO clocks
	RCC->AHB1ENR |= 0x0F;

	// Set output pins
	GPIOInit ((GPIO*)ports[t->ttynum], pins[t->ttynum], GPIO_ALTERNATE(config[t->ttynum]) | GPIO_AF);

	// UART5 is split on two ports
	if (t->ttynum == 4)
		GPIOInit (GPIOC, PIN(12), GPIO_ALTERNATE(config[t->ttynum]) | GPIO_AF);

	// Set DMA channel and UART IRQ priorities
	PlatformSetInterruptPriority (dmarxirqn[t->ttynum], 4);
	PlatformSetInterruptPriority (dmatxirqn[t->ttynum], 4);
	PlatformSetInterruptPriority (uartirqn[t->ttynum], 5);
	PlatformEnableInterrupt (dmarxirqn[t->ttynum]);
	PlatformEnableInterrupt (dmatxirqn[t->ttynum]);
	PlatformEnableInterrupt (uartirqn[t->ttynum]);

	// Reset RXNE (Read data register not empty)
	t->instance->SR = 0;

	// Set config
	t->instance->CR1 = t->word_parity_mode_sampling;
	t->instance->CR2 = t->stopbits;
	t->instance->CR3 = t->flowcontrol;

	dev_tty_uart_setbaud (t);

	// Enable the UART
	t->instance->CR1 |= CR1_UE;

	// Configure DMA
	DMAInit (&t->dmarx);
	DMAInit (&t->dmatx);

	// Start DMA reception if circular
	if (t->dmarx.config & DMA_CR_CIRC)
	{
		DMAStart (&t->dmarx, (void*) &t->instance->DR, t->rxbuf, t->rxbufsize);
		t->instance->CR3 |= CR3_DMAR;
	}
}
