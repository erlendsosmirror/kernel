/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/dev/dev.h"
#include "posix/sys/signal.h"
#include "process/process.h"
#include "drivers/tty/tty-stm32.h"
#include "drivers/dma/dma-stm32f407.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int dev_tty_read_circular (tty *t, void *data, unsigned int nbytes)
{
	// Since the register contains remaining bytes, we need to "invert" to get position
	int pos_now = t->rxbufsize - t->dmarx.chinst->NDTR;

	// Fetch old position, calculate number of bytes, check wrap-around
	int pos_old = t->pos_old;
	int n = pos_now - pos_old;

	if (pos_now < pos_old)
		n += t->rxbufsize;

	// Make sure we never read more than nbytes
	if (nbytes < n)
		n = nbytes;

	// Number of bytes read
	int nread = 0;

	// Check mode for signal processing, then process data
	if (t->mode == 2)
	{
		for (int i = 0; i < n; i++)
		{
			char d = t->rxbuf[(pos_old + i) & TTY_BUF_MASK];
			((char*)data)[nread++] = d;
		}
	}
	else
	{
		for (int i = 0; i < n; i++)
		{
			char d = t->rxbuf[(pos_old + i) & TTY_BUF_MASK];

			// Do not copy SIGINT and SIGTSTP
			if (d != 0x03 && d != 0x1A)
				((char*)data)[nread++] = d;
		}
	}

	// Set old position and return number of bytes read
	t->pos_old = (pos_old + n) & TTY_BUF_MASK;
	return nread;
}

int dev_tty_write_circular (tty *t, void *data, unsigned int nbytes)
{
	// Make sure we do not accept more than TTY_BUF_SIZE bytes
	unsigned int n = (nbytes < TTY_BUF_SIZE) ? nbytes : TTY_BUF_SIZE;

	// Copy data to transmit buffer
	for (int i = 0; i < n; i++)
		t->deftxbuf[i] = ((char*)data)[i];

	// Start the DMA, start transmitting and return nbytes
	DMAStart (&t->dmatx, t->deftxbuf, (void*) &t->instance->DR, n);
	t->instance->CR3 |= CR3_DMAT;
	return n;
}

void dev_tty_irq_circular (tty *t)
{
	// Calculate the proper current index, then
	// fetch old position, calculate number of bytes, check wrap-around
	int pos_now = t->rxbufsize - t->dmarx.chinst->NDTR;
	int pos_old = t->irq_pos_old;
	int n = pos_now - pos_old;

	if (pos_now < pos_old)
		n += t->rxbufsize;

	// Loop through characters
	for (int i = 0; i < n; i++)
	{
		// Fetch the character
		char b = t->rxbuf[(pos_old + i) & TTY_BUF_MASK];

		// Check for SIGINT and SIGTSTP, send them to the process group
		if (b == 0x03)
			ProcessSendSignal (-t->pgid, SIGINT);
		else if (b == 0x1A)
			ProcessSendSignal (-t->pgid, SIGTSTP);
	}

	// Save the position
	t->irq_pos_old = (pos_old + n) & TTY_BUF_MASK;
}

int dev_tty_read_direct (tty *t, void *data, unsigned int nbytes)
{
	// Fail if we are in the receiving state already
	if (t->dmarx.busystate)
		return -1;

	// Disable DMA mode for reception
	t->instance->CR3 &= ~CR3_DMAR;

	// Clear error flag and set DMA as busy
	t->errflag = 0;
	t->dmarx.busystate = 1;

	// Set receive buffer and number of bytes
	t->rxbuf = (unsigned char*) data;
	t->rxbufsize = nbytes;

	// Start the DMA, start reception and return nbytes
	DMAStart (&t->dmarx, (void*) &t->instance->DR, data, nbytes);
	t->instance->CR3 |= CR3_DMAR;
	return nbytes;
}

int dev_tty_write_direct (tty *t, void *data, unsigned int nbytes)
{
	// Fetch DMA handle
	DMAHandle *hdma = &t->dmatx;

	// Disable DMA mode for transfer
	t->instance->CR3 &= ~CR3_DMAT;

	// Disable transfer DMA channel and wait for it to clear
	hdma->chinst->CR = 0;
	while (hdma->chinst->CR);

	// Clear all interrupts for the DMA channel
	hdma->dma->IFCR = (DMA_TEIF | DMA_DMEIF | DMA_HTIF | DMA_TCIF | DMA_FEIF) << hdma->shift;

	// Configure the DMA channel for a new channel
	hdma->chinst->NDTR = nbytes;
	hdma->chinst->PAR = (unsigned int) &t->instance->DR;
	hdma->chinst->M0AR = (unsigned int) data;
	hdma->chinst->CR = hdma->config | DMA_CR_TCIE | DMA_CR_HTIE | DMA_CR_TEIE | DMA_CR_DMEIE;
#if DMA_HAVE_FCR_REG == 1
	hdma->chinst->FCR = 0;
#endif
	t->instance->SR &= ~(1 << 6); // Clear TC

	// Enable DMA transfer, enable UART transmitter and return number of bytes sent
	DMAEnable (hdma);
	t->instance->CR3 |= CR3_DMAT;
	return nbytes;
}

void dev_tty_irq_direct (tty *t)
{
	// In direct mode, we only check errors
	// Read the status register
	unsigned int reg = t->instance->SR;

	// In case of errors: ORE, NF, FE, PE
	if (reg & 0x000F)
	{
		// Set the error flag and read the data register to clear them
		t->errflag = 1;
		reg = t->instance->DR;
	}

	// Clear busy state
	t->dmarx.busystate = 0;
}
