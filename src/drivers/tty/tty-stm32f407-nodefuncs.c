/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "ioctl/ioctl.h"
#include "drivers/tty/tty-stm32.h"

///////////////////////////////////////////////////////////////////////////////
// Forward declarations
///////////////////////////////////////////////////////////////////////////////
void dev_tty_irq_direct (tty *t);

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
tty *irq_instances[TTY_N_MODULES] = {0};

///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
void dev_tty_uart_defconfig (tty *t, int ttynum);
void dev_tty_uart_altconfig (tty *t);
void dev_tty_uart_setbaud (tty *t);
void dev_tty_uart_init (tty *t);

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_tty_open (struct FileNode *node, const char *filename)
{
	// Get tty number
	unsigned int ttynum = filename[3] - '0';

	// Check it
	if (ttynum > (TTY_N_MODULES-1))
		return -ENXIO;

	unsigned int legal = 0 |

#if TTY_ENABLE_AVAIL_0
		(1 << 0) |
#endif
#if TTY_ENABLE_AVAIL_1
		(1 << 1) |
#endif
#if TTY_ENABLE_AVAIL_2
		(1 << 2) |
#endif
#if TTY_ENABLE_AVAIL_3
		(1 << 3) |
#endif
#if TTY_ENABLE_AVAIL_4
		(1 << 4) |
#endif
#if TTY_ENABLE_AVAIL_5
		(1 << 5) |
#endif
		0;

	if (!((1 << ttynum) & legal))
		return -ENXIO;

	// Allocate (static for now)
	static tty ty[TTY_N_MODULES];
	tty *t = &ty[ttynum];

	// Set the irq-handlers
	irq_instances[ttynum] = t;

	// Set the file node
	node->priv = (void*) t;

	// Set configuration
	dev_tty_uart_defconfig (t, ttynum);

	// Initialize uart
	dev_tty_uart_init (t);

	// Done
	return 0;
}

int dev_tty_close (struct FileNode *node)
{
	// Extract tty pointer
	tty *t = (tty*) node->priv;

	// Make sure DMA channels and UART is disabled
	t->dmatx.chinst->CR = 0;
	t->dmarx.chinst->CR = 0;
	t->instance->CR1 = 0;

	// Clear irqhandler (also "clears" the static allocation)
	irq_instances[t->ttynum] = 0;

	// Done
	return 0;
}

int dev_tty_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	// Extract tty pointer and call read
	tty *t = (tty*) node->priv;
	return t->read (t, data, nbytes);
}

int dev_tty_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	// Extract tty pointer
	tty *t = (tty*) node->priv;

	// Write nothing if transfer is busy
	if (t->dmatx.busystate || !nbytes)
	{
		// The OS may have crashed and is running in a state with no
		// interrupts, so do an extra check
		if (!(t->dmatx.dma->ISR & (DMA_TCIF << t->dmatx.shift)))
			return 0;
	}

	// Set transfer state as busy
	t->dmatx.busystate = 1;

	// Call the correct write function
	return t->write (t, data, nbytes);
}

int dev_tty_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	// Extract tty pointer
	tty *t = (tty*) node->priv;

	// Switch on the ioctl control code
	switch (code)
	{
	case TCGETS:
		{
			struct termios *tios = (struct termios*) data;
			tios->baudrate = t->baudrate;
		}
		return 0;
	case TCSETS:
		{
			struct termios *tios = (struct termios*) data;
			t->baudrate = tios->baudrate;
			dev_tty_uart_setbaud (t);
		}
		return 0;
	case TISATTY:
		// Always return 1 since this is a terminal
		return 1;
	case TCGETPGRP:
		return t->pgid;
	case TCSETPGRP:
		t->pgid = (short) ((int)data);
		return 0;
	case TTYSETMODE:
		if ((int)data == 2)
		{
			// Direct mode with buffered recv
			dev_tty_uart_defconfig (t, t->ttynum);
			t->flowcontrol |= 1; // Set EIE (Framing error, overrun error, noise flag)
			t->irq = dev_tty_irq_direct;
			t->mode = 2;
			dev_tty_uart_init (t);
		}
		else if ((int)data == 1)
		{
			// Direct mode
			dev_tty_uart_altconfig (t);
			t->mode = 1;
			dev_tty_uart_init (t);
		}
		else
		{
			// Normal mode
			dev_tty_uart_defconfig (t, t->ttynum);
			t->mode = 0;
			dev_tty_uart_init (t);
		}
		break;
	case TTYGETRXBUSY:
		// Return 2 instead if an error occured
		if (t->errflag)
		{
			t->errflag = 0;
			return 2;
		}

		// Otherwise return the busystate, either 0 or 1
		return t->dmarx.busystate;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	// Default return value
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
#if TTY_ENABLE_AVAIL_0
MODULE_DESCRIPTOR (tty0,
	dev_tty_open,
	dev_tty_close,
	dev_tty_read,
	dev_tty_write,
	dev_tty_ioctl,
	"tty0");
#endif

#if TTY_ENABLE_AVAIL_1
MODULE_DESCRIPTOR (tty1,
	dev_tty_open,
	dev_tty_close,
	dev_tty_read,
	dev_tty_write,
	dev_tty_ioctl,
	"tty1");
#endif

#if TTY_ENABLE_AVAIL_2
MODULE_DESCRIPTOR (tty2,
	dev_tty_open,
	dev_tty_close,
	dev_tty_read,
	dev_tty_write,
	dev_tty_ioctl,
	"tty2");
#endif

#if TTY_ENABLE_AVAIL_3
MODULE_DESCRIPTOR (tty3,
	dev_tty_open,
	dev_tty_close,
	dev_tty_read,
	dev_tty_write,
	dev_tty_ioctl,
	"tty3");
#endif

#if TTY_ENABLE_AVAIL_4
MODULE_DESCRIPTOR (tty4,
	dev_tty_open,
	dev_tty_close,
	dev_tty_read,
	dev_tty_write,
	dev_tty_ioctl,
	"tty4");
#endif

#if TTY_ENABLE_AVAIL_5
MODULE_DESCRIPTOR (tty5,
	dev_tty_open,
	dev_tty_close,
	dev_tty_read,
	dev_tty_write,
	dev_tty_ioctl,
	"tty5");
#endif
