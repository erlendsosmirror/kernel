/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "drivers/tty/tty-stm32.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
extern tty *irq_instances[TTY_N_MODULES];

///////////////////////////////////////////////////////////////////////////////
// IRQ functions
///////////////////////////////////////////////////////////////////////////////
#if TTY_ENABLE_AVAIL_0
void DMA2CH2_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[0]->dmarx);
}

void DMA2CH7_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[0]->dmatx);
}

void UART1_IrqHandler (void)
{
	irq_instances[0]->irq (irq_instances[0]);
}
#endif

#if TTY_ENABLE_AVAIL_1
void DMA1CH5_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[1]->dmarx);
}

void DMA1CH6_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[1]->dmatx);
}

void UART2_IrqHandler (void)
{
	irq_instances[1]->irq (irq_instances[1]);
}
#endif

#if TTY_ENABLE_AVAIL_2
void DMA1CH1_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[2]->dmarx);
}

void DMA1CH3_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[2]->dmatx);
}

void UART3_IrqHandler (void)
{
	irq_instances[2]->irq (irq_instances[2]);
}
#endif

#if TTY_ENABLE_AVAIL_3
void DMA1CH2_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[3]->dmarx);
}

void DMA1CH4_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[3]->dmatx);
}

void UART4_IrqHandler (void)
{
	irq_instances[3]->irq (irq_instances[3]);
}
#endif

#if TTY_ENABLE_AVAIL_4
void DMA1CH0_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[4]->dmarx);
}

void DMA1CH7_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[4]->dmatx);
}

void UART5_IrqHandler (void)
{
	irq_instances[4]->irq (irq_instances[4]);
}
#endif

#if TTY_ENABLE_AVAIL_5
void DMA2CH1_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[5]->dmarx);
}

void DMA2CH6_IrqHandler (void)
{
	DMAIRQHandler (&irq_instances[5]->dmatx);
}

void UART6_IrqHandler (void)
{
	irq_instances[5]->irq (irq_instances[5]);
}
#endif
