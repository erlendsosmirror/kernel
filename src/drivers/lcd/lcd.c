/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "mem/string.h"
#include "print/print.h"
#include "ioctl/ioctl.h"
#include "drivers/lcd/lcd.h"

///////////////////////////////////////////////////////////////////////////////
// External functions
///////////////////////////////////////////////////////////////////////////////
void LCDWaitMs (int t);

///////////////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////////////
void dev_lcd_test (void)
{
	LCDFill (TFT_BLUE);

	for (int i = 0; i < 240; i++)
	{
		LCDHLine (0, 80, i, LCDColor (i, 0, 0));
		LCDHLine (80, 160, i, LCDColor (0, i, 0));
		LCDHLine (160, 239, i, LCDColor (0, 0, i));
	}

	LCDBox (10, 10, 100, 100, TFT_RED);
	LCDLine (120, 120, 130, 150, TFT_GREEN);
	LCDRectangle (120, 10, 180, 100, TFT_GREEN);
	LCDText ((char*)"Hello", 10, 110, 16, 0xFFFF, TFT_BLUE);
	LCDText ((char*)"This is some smaller text", 10, 150, 8, 0xFFFF, TFT_BLUE);
	LCDText ((char*)" !\"#$%&'()*+,-./0123456789", 10, 170, 8, 0xFFFF, TFT_BLUE);

	LCDLine (10, 10, 100, 1, TFT_WHITE);
	LCDLine (10, 10, 100, 10, TFT_WHITE);
	LCDLine (10, 10, 100, 20, TFT_WHITE);
	LCDLine (10, 10, 100, 30, TFT_WHITE);

	LCDLine (20, 20, 10, 100, TFT_WHITE);
	LCDLine (20, 20, 20, 100, TFT_WHITE);
	LCDLine (20, 20, 30, 100, TFT_WHITE);
	LCDLine (20, 20, 40, 100, TFT_WHITE);

	LCDLine (200, 200, 120, 190, TFT_WHITE);
	LCDLine (200, 200, 120, 200, TFT_WHITE);
	LCDLine (200, 200, 120, 210, TFT_WHITE);
	LCDLine (200, 200, 120, 220, TFT_WHITE);

	LCDLine (200, 200, 190, 120, TFT_WHITE);
	LCDLine (200, 200, 200, 120, TFT_WHITE);
	LCDLine (200, 200, 210, 120, TFT_WHITE);
	LCDLine (200, 200, 220, 120, TFT_WHITE);

	LCDLine (30, 30, 90, 90, TFT_WHITE);

	LCDWaitMs (3000);
}

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_lcd_open (struct FileNode *node, const char *filename)
{
	LCDInit ();
	//dev_lcd_test ();
	return 0;
}

int dev_lcd_close (struct FileNode *node)
{
	return 0;
}

int dev_lcd_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	return 0;
}

int dev_lcd_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	LCDData ((unsigned short*)data, nbytes >> 1);
	return 0;
}

int dev_lcd_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	// Check alignment
	if ((int)data & 0x00000003)
		return -EINVAL;

	// Cast
	unsigned int *d = (unsigned int*) data;

	// Switch on code
	switch (code)
	{
	case 0:
		LCDText ((char*)d[0], d[1], d[2], d[3], d[4], d[5]);
		break;
	case 1:
		LCDChar ((char)d[0], d[1], d[2], d[3], d[4], d[5]);
		break;
	case 2:
		LCDRectangle (d[0], d[1], d[2], d[3], d[4]);
		break;
	case 3:
		LCDVLine (d[0], d[1], d[2], d[3]);
		break;
	case 4:
		LCDHLine (d[0], d[1], d[2], d[3]);
		break;
	case 5:
		LCDLine (d[0], d[1], d[2], d[3], d[4]);
		break;
	case 6:
		LCDDot (d[0], d[1], d[2]);
		break;
	case 7:
		LCDBox (d[0], d[1], d[2], d[3], d[4]);
		break;
	case 8:
		LCDFill (d[0]);
		break;
	case 9:
		LCDRotate (d[0]);
		break;
	case 10:
		LCDSetAddress (d[0], d[1], d[2], d[3]);
		break;
	case 11:
		LCDData ((unsigned short*)d[0], d[1]);
		break;
	case 12:
		LCDGetMetrics ((unsigned int*)d[0], (unsigned int*)d[1]);
		break;
	case 13:
		*((unsigned int*)d[0]) = LCDGetOrientation ();
		break;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	// Default return value
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (lcd,
	dev_lcd_open,
	dev_lcd_close,
	dev_lcd_read,
	dev_lcd_write,
	dev_lcd_ioctl,
	"lcd");
