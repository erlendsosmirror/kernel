/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "mem/string.h"
#include "print/print.h"
#include "ioctl/ioctl.h"
#include "platform/platform.h"
#include "drivers/lcd/lcd.h"
#include "../config/live/drivers/lcd.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
extern int lcdWidth;
extern int lcdHeight;
char lcdOrientation;
char lcdInitialized;

///////////////////////////////////////////////////////////////////////////////
// Macros
///////////////////////////////////////////////////////////////////////////////
#define LCDWriteReg(command,data) \
	LCD_COMMAND (command); \
	LCD_DATA_16 (data);

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void LCDInit (void)
{
	// SSD1289

	// MCD 0x0001: Driver output control
	// bit 14: RL
	// bit 10: SM
	// bit  9: TB

	if (lcdInitialized)
		return;

	lcdInitialized = 1;

	LCDWriteReg (0x0000,0x0001); // Oscillation start

	unsigned int start = PlatformGetTick ();
	while ((PlatformGetTick () - start) < 30);

	LCDWriteReg (0x000C,0x0000); // Power control
	LCDWriteReg (0x000D,0x800C); // Power control
	LCDWriteReg (0x000E,0x2B00); // Power control
	LCDWriteReg (0x001E,0x00B7); // Power control
	LCDWriteReg (0x0001,0x2B3F); // Driver output control, RL=0, SM=0, TB=1, MUX=319d
	LCDWriteReg (0x0010,0x0000); // Sleep mode
	LCDWriteReg (0x0011,0x6070); // Entry mode 6050=horizontal hack mode
	LCDWriteReg (0x0016,0xEF1C); // Horizontal porch
	LCDWriteReg (0x0017,0x0003); // Vertical porch
	LCDWriteReg (0x000B,0x0000); // Frame cycle control
	LCDWriteReg (0x000F,0x0000); // Gate scan start position
	LCDWriteReg (0x0041,0x0000); // Vertical scroll control
	LCDWriteReg (0x0042,0x0000); // Vertical scroll control
	LCDWriteReg (0x0044,0xEF95); // Horizontal RAM address position
	LCDWriteReg (0x0045,0x0000); // Vertical RAM address start position
	LCDWriteReg (0x0030,0x0707); // y control
	LCDWriteReg (0x0031,0x0204); // y control
	LCDWriteReg (0x0032,0x0204); // y control
	LCDWriteReg (0x0033,0x0502); // y control
	LCDWriteReg (0x0034,0x0507); // y control
	LCDWriteReg (0x0035,0x0204); // y control
	LCDWriteReg (0x0036,0x0204); // y control
	LCDWriteReg (0x0037,0x0502); // y control
	LCDWriteReg (0x003A,0x0302); // y control
	LCDWriteReg (0x003B,0x0302); // y control
	LCDWriteReg (0x0023,0x0000); // RAM write data mask
	LCDWriteReg (0x0024,0x0000); // RAM write data mask
	LCDWriteReg (0x0025,0x8000); // Frame frequency
	LCDWriteReg (0x0046,0x013F); // Vertical RAM address end position
	LCDWriteReg (0x0048,0x0000); // First window start
	LCDWriteReg (0x0049,0x013F); // First window end
	LCDWriteReg (0x004A,0x0000); // Second window start
	LCDWriteReg (0x004B,0x0000); // Second window end
	LCDWriteReg (0x004F,0x0000); // Set GDDRAM Y address counter
	LCDWriteReg (0x004E,0x0000); // Set GDDRAM X address counter
	LCDWriteReg (0x0002,0x0600); // LCD drive ac control
	LCDWriteReg (0x0003,0xA8A4); // Power control
	LCDWriteReg (0x0005,0x0000); // Compare register
	LCDWriteReg (0x0006,0x0000); // Compare register
	LCDWriteReg (0x0007,0x0233); // Display control
	LCDRotate (LCD_DEFAULT_ORIENTATION);

	LCD_COMMAND (0x0022); // RAM data write
}


void LCDRotate (int direction)
{
	if (direction == LCD_VERTICAL)
	{
		// Vertical
		// Entry mode
		// bit  5: ID1
		// bit  4: ID0
		// bit  3: AM
		LCDWriteReg (0x0011,0x6070); // ID=11, AM=0
		lcdWidth = LCD_HEIGHT;
		lcdHeight = LCD_WIDTH;
	}
	else
	{
		// Horizontal
		// Entry mode
		// bit  5: ID1
		// bit  4: ID0
		// bit  3: AM
		LCDWriteReg (0x0011,0x6058); // ID=01, AM=1
		lcdWidth = LCD_WIDTH;
		lcdHeight = LCD_HEIGHT;
	}

	lcdOrientation = direction;
}

void LCDSetAddress (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2)
{
	if (lcdOrientation == LCD_VERTICAL)
	{
		LCD_COMMAND (0x44);		// x address start and end
		LCD_DATA ((x2 << 8) + x1);
		LCD_COMMAND (0x45);		// y address start
		LCD_DATA (y1);
		LCD_COMMAND (0x46);		// y address end
		LCD_DATA (y2);
		LCD_COMMAND (0x4E);		// x address start set
		LCD_DATA (x1);
		LCD_COMMAND (0x4F);		// y address start set
		LCD_DATA (y1);
		LCD_COMMAND (0x22);
	}
	else
	{
		LCD_COMMAND (0x44);		// x address start and end
		LCD_DATA ((y2 << 8) + y2);
		LCD_COMMAND (0x45);		// y address start
		LCD_DATA (319 - x2);
		LCD_COMMAND (0x46);		// y address end
		LCD_DATA (319 - x1);
		LCD_COMMAND (0x4E);		// x address start set
		LCD_DATA (y1);
		LCD_COMMAND (0x4F);		// y address start set
		LCD_DATA (319 - x1);
		LCD_COMMAND (0x22);
	}
}
