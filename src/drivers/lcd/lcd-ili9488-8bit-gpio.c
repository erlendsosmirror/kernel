/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "mem/string.h"
#include "print/print.h"
#include "ioctl/ioctl.h"
#include "platform/platform.h"
#include "drivers/lcd/lcd.h"
#include "../config/live/drivers/lcd.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
extern int lcdWidth;
extern int lcdHeight;
char lcdOrientation;
char lcdInitialized;

///////////////////////////////////////////////////////////////////////////////
// Helper functions
///////////////////////////////////////////////////////////////////////////////
void LCDWaitMs (int t)
{
	unsigned int start = PlatformGetTick ();
	while ((PlatformGetTick () - start) < t);
}

inline void LCDWait (void)
{
	asm volatile ("nop\n");
	asm volatile ("nop\n");
}

void LCD_COMMAND (int cmd)
{
	LCD_CTRL_PORT->BSRR = LCD_RS_PIN << 16;
	GPIOB->ODR = cmd;
	LCD_CTRL_PORT->BSRR = LCD_WR_PIN << 16;
	LCDWait ();
	LCD_CTRL_PORT->BSRR = LCD_WR_PIN;
	LCDWait ();
	LCD_CTRL_PORT->BSRR = LCD_RS_PIN;
}

void LCD_DATA (int data)
{
	GPIOB->ODR = data;
	LCD_CTRL_PORT->BSRR = LCD_WR_PIN << 16;
	LCDWait ();
	LCD_CTRL_PORT->BSRR = LCD_WR_PIN;
	LCDWait ();
}

void LCD_DATA_16 (int data)
{
	int r = data & 0xF800;
	int g = data & 0x07E0;
	int b = data & 0x001F;

	int data1 = (r >> 7) | (g >> 8);
	int data2 = (g << 1) | (b << 1);

	GPIOB->ODR = data1;
	LCD_CTRL_PORT->BSRR = LCD_WR_PIN << 16;
	LCDWait ();
	LCD_CTRL_PORT->BSRR = LCD_WR_PIN;
	LCDWait ();
	GPIOB->ODR = data2;
	LCD_CTRL_PORT->BSRR = LCD_WR_PIN << 16;
	LCDWait ();
	LCD_CTRL_PORT->BSRR = LCD_WR_PIN;
	LCDWait ();
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void LCDInit (void)
{
	// ILI9488

	if (lcdInitialized)
		return;

	lcdInitialized = 1;

	GPIOInit (LCD_DATA_PORT,
		PIN(0) | PIN(1) | PIN(2) | PIN(3) | PIN(4) | PIN(5) | PIN(6) | PIN(7) | PIN(8),
		GPIO_OUTPUT | GPIO_SPEED_VERYHIGH);

	GPIOInit (LCD_CTRL_PORT, LCD_RD_PIN | LCD_WR_PIN | LCD_RS_PIN,
		GPIO_OUTPUT | GPIO_SPEED_VERYHIGH);

	LCD_CTRL_PORT->BSRR = LCD_RD_PIN | LCD_WR_PIN | LCD_RS_PIN;

	LCDWaitMs (10);

	LCD_COMMAND (0x11);

	LCDWaitMs (120);

	LCD_COMMAND (0xE0);
	LCD_DATA (0x00);
	LCD_DATA (0x03);
	LCD_DATA (0x09);
	LCD_DATA (0x08);
	LCD_DATA (0x16);
	LCD_DATA (0x0A);
	LCD_DATA (0x3F);
	LCD_DATA (0x78);
	LCD_DATA (0x4C);
	LCD_DATA (0x09);
	LCD_DATA (0x0A);
	LCD_DATA (0x08);
	LCD_DATA (0x16);
	LCD_DATA (0x1A);
	LCD_DATA (0x0F);

	LCD_COMMAND (0xE1);
	LCD_DATA (0x00);
	LCD_DATA (0x16);
	LCD_DATA (0x19);
	LCD_DATA (0x03);
	LCD_DATA (0x0F);
	LCD_DATA (0x05);
	LCD_DATA (0x32);
	LCD_DATA (0x45);
	LCD_DATA (0x46);
	LCD_DATA (0x04);
	LCD_DATA (0x0E);
	LCD_DATA (0x0D);
	LCD_DATA (0x35);
	LCD_DATA (0x37);
	LCD_DATA (0x0F);

	LCD_COMMAND (0xC0); // Power control 1
	LCD_DATA (0x17);
	LCD_DATA (0x15);

	LCD_COMMAND (0xC1); // Power control 2
	LCD_DATA (0x41);

	LCD_COMMAND (0xC5); // VCOM Control
	LCD_DATA (0x00);
	LCD_DATA (0x2C);
	LCD_DATA (0x80);

	LCD_COMMAND (0x36); // Memory access control
	LCD_DATA (0x48);

	LCD_COMMAND (0x3A); // Interface pixel format
	LCD_DATA (0x56);

	LCD_COMMAND (0xB0); // Interface mode control
	LCD_DATA (0x00);

	LCD_COMMAND (0xB1); // Frame rate
	LCD_DATA (0xB0);

	LCD_COMMAND (0xB4); // Display inversion control
	LCD_DATA (0x02);

	LCD_COMMAND (0xB6); // Display function control
	LCD_DATA (0x02);
	LCD_DATA (0x02);

	LCD_COMMAND (0xE9); // Set image function
	LCD_DATA (0x00);

	LCD_COMMAND (0xF7); // Adjust control 3
	LCD_DATA (0xA9);
	LCD_DATA (0x51);
	LCD_DATA (0x2C);
	LCD_DATA (0x82);

	LCD_COMMAND (0x11); // Sleep out
	LCDWaitMs (120);

	LCDRotate (LCD_DEFAULT_ORIENTATION);

	// Display on
	LCD_COMMAND (0x29);
}


void LCDRotate (int direction)
{
	if (direction == LCD_VERTICAL)
	{
		// Vertical
		LCD_COMMAND (0x36);
		LCD_DATA (0x48);
	}
	else
	{
		// Horizontal
		LCD_COMMAND (0x36);
		LCD_DATA (0x80 | 0x40 | 0x20 | 0x08);
	}

	lcdOrientation = direction;
}

void LCDSetAddress (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2)
{
	LCD_COMMAND (0x2A);
	LCD_DATA (x1 >> 8);
	LCD_DATA (x1);
	LCD_DATA (x2 >> 8);
	LCD_DATA (x2);
	LCD_COMMAND (0x2B);
	LCD_DATA (y1 >> 8);
	LCD_DATA (y1);
	LCD_DATA (y2 >> 8);
	LCD_DATA (y2);
	LCD_COMMAND (0x2C);
}
