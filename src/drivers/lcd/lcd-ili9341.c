/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "mem/string.h"
#include "print/print.h"
#include "ioctl/ioctl.h"
#include "platform/platform.h"
#include "drivers/lcd/lcd.h"
#include "../config/live/drivers/lcd.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
extern int lcdWidth;
extern int lcdHeight;
char lcdOrientation;
char lcdInitialized;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void LCDInit (void)
{
	// ILI9341

	if (lcdInitialized)
		return;

	lcdInitialized = 1;

	LCD_COMMAND = 0x11;

	unsigned int start = PlatformGetTick ();
	while ((PlatformGetTick () - start) < 120);

	LCD_COMMAND = 0xCF; // Power control B
	LCD_DATA = 0x00;
	LCD_DATA = 0xC3;
	LCD_DATA = 0X30;

	LCD_COMMAND = 0xED; // Power on sequence control
	LCD_DATA = 0x64;
	LCD_DATA = 0x03;
	LCD_DATA = 0x12;
	LCD_DATA = 0x81;

	LCD_COMMAND = 0xE8; // Driver timing control A
	LCD_DATA = 0x85;
	LCD_DATA = 0x10;
	LCD_DATA = 0x79;

	LCD_COMMAND = 0xCB; // Power control A
	LCD_DATA = 0x39;
	LCD_DATA = 0x2C;
	LCD_DATA = 0x00;
	LCD_DATA = 0x34;
	LCD_DATA = 0x02;

	LCD_COMMAND = 0xF7; // Pump ratio control
	LCD_DATA = 0x20;

	LCD_COMMAND = 0xEA; // Driver timing control B
	LCD_DATA = 0x00;
	LCD_DATA = 0x00;

	LCD_COMMAND = 0xC0; // Power control 1
	LCD_DATA = 0x22;

	LCD_COMMAND = 0xC1; // Power control 2
	LCD_DATA = 0x11;

	LCD_COMMAND = 0xC5; // VCOM control 1
	LCD_DATA = 0x3D;
	LCD_DATA = 0x20;

	LCD_COMMAND = 0xC7; // VCOM control 2
	LCD_DATA = 0xAA;

	LCD_COMMAND = 0x36; // Memory Access Control
	LCD_DATA = 0x08;

	LCD_COMMAND = 0x3A; // Pixel format set
	LCD_DATA = 0x55;

	LCD_COMMAND = 0xB1; // Frame rate control in normal mode / full colors
	LCD_DATA = 0x00;
	LCD_DATA = 0x13;

	LCD_COMMAND = 0xB6; // Display Function Control
	LCD_DATA = 0x0A;
	LCD_DATA = 0xA2;

	LCD_COMMAND = 0xF6; // Interface control
	LCD_DATA = 0x01;
	LCD_DATA = 0x30;

	LCD_COMMAND = 0xF2; // Enable 3 gamma
	LCD_DATA = 0x00;

	LCD_COMMAND = 0x26; // Gamma set
	LCD_DATA = 0x01;

	LCD_COMMAND = 0xE0; // Positive gamma correction
	LCD_DATA = 0x0F;
	LCD_DATA = 0x3F;
	LCD_DATA = 0x2F;
	LCD_DATA = 0x0C;
	LCD_DATA = 0x10;
	LCD_DATA = 0x0A;
	LCD_DATA = 0x53;
	LCD_DATA = 0xD5;
	LCD_DATA = 0x40;
	LCD_DATA = 0x0A;
	LCD_DATA = 0x13;
	LCD_DATA = 0x03;
	LCD_DATA = 0x08;
	LCD_DATA = 0x03;
	LCD_DATA = 0x00;

	LCD_COMMAND = 0xE1; // Negative gamma correction
	LCD_DATA = 0x00;
	LCD_DATA = 0x00;
	LCD_DATA = 0x10;
	LCD_DATA = 0x03;
	LCD_DATA = 0x0F;
	LCD_DATA = 0x05;
	LCD_DATA = 0x2C;
	LCD_DATA = 0xA2;
	LCD_DATA = 0x3F;
	LCD_DATA = 0x05;
	LCD_DATA = 0x0E;
	LCD_DATA = 0x0C;
	LCD_DATA = 0x37;
	LCD_DATA = 0x3C;
	LCD_DATA = 0x0F;

	LCD_COMMAND = 0x11;

	start = PlatformGetTick ();
	while ((PlatformGetTick () - start) < 120);

	LCDRotate (LCD_DEFAULT_ORIENTATION);

	LCD_COMMAND = 0x29;
}


void LCDRotate (int direction)
{
	if (direction == LCD_VERTICAL)
	{
		// Vertical
		LCD_COMMAND = 0x36;
		LCD_DATA = 0x48;
	}
	else
	{
		// Horizontal
		LCD_COMMAND = 0x36;
		LCD_DATA = 0x80 | 0x40 | 0x20 | 0x08;
	}

	lcdOrientation = direction;
}

void LCDSetAddress (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2)
{
	LCD_COMMAND = 0x2A;
	LCD_DATA = x1 >> 8;
	LCD_DATA = x1 & 0x00FF;
	LCD_DATA = x2 >> 8;
	LCD_DATA = x2 & 0x00FF;
	LCD_COMMAND = 0x2B;
	LCD_DATA = y1 >> 8;
	LCD_DATA = y1 & 0x00FF;
	LCD_DATA = y2 >> 8;
	LCD_DATA = y2 & 0x00FF;
	LCD_COMMAND = 0x2C;
}
