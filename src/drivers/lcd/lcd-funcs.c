/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "mem/string.h"
#include "print/print.h"
#include "ioctl/ioctl.h"
#include "drivers/lcd/lcd.h"
#include "../config/live/drivers/lcd.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
extern const unsigned char Font8[760];
extern const unsigned char *Font16;
extern char lcdOrientation;
int lcdWidth;
int lcdHeight;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
unsigned int LCDColor (unsigned int r, unsigned int g, unsigned int b)
{
	// rrrrrggggggbbbbb
	unsigned int temp = 0;
	temp = ((r & 0xF8) << 8) | ((g & 0xFC) << 3) | ((b & 0xF8) >> 3);
	return temp;
}

void LCDText (char *str, unsigned int x, unsigned int y, int fontsize, unsigned int fcolor, unsigned int bcolor)
{
	while (*str)
	{
		LCDChar (*str, x, y, fontsize, fcolor, bcolor);
		x += fontsize-2;
		str++;
	}
}

void LCDChar (char c, unsigned int x, unsigned int y, int fontsize, unsigned int fcolor, unsigned int bcolor)
{
	const unsigned char *font;
	int fontincx;
	int fontincy;

	if (fontsize == 8)
	{
		font = Font8 + (((unsigned int)c - ' ') << 3);
		fontincx = x + fontsize - 3;
		fontincy = y + fontsize - 1;
	}
	else
	{
		return;
		//font = Font16 + (((unsigned int)c - ' ') << 5);
	}

	LCDSetAddress (x, y, fontincx, fontincy);

	for (; y <= fontincy; y++)
	{
		unsigned char data = *font++;

		for (int ix = x; ix <= fontincx; ix++)
		{
			unsigned int d = (data & 0x80) ? fcolor : bcolor;
			LCD_DATA_16 (d);
			data = data << 1;
		}
	}
}

void LCDRectangle (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int color)
{
	LCDHLine (x1, x2, y1, color);
	LCDHLine (x1, x2, y2, color);
	LCDVLine (y1, y2, x1, color);
	LCDVLine (y1, y2, x2, color);
}

void LCDVLine (unsigned int y1, unsigned int y2, unsigned int x, unsigned int color)
{
	LCDSetAddress (x, y1, x, y2);

	unsigned int n = y2 - y1 + 1;

	for (int i = 0; i < n; i++)
		LCD_DATA_16 (color);
}

void LCDHLine (unsigned int x1, unsigned int x2, unsigned int y, unsigned int color)
{
	LCDSetAddress (x1, y, x2, y);

	unsigned int n = x2 - x1 + 1;

	for (int i = 0; i < n; i++)
		LCD_DATA_16 (color);
}

void PlotLineLow (int x1, int y1, int x2, int y2, int color)
{
	int dx = x2 - x1;
	int dy = y2 - y1;

	int yi = 1;

	if (dy < 0)
	{
		yi = -1;
		dy = -dy;
	}

	dy *= 2;

	for (int d = dy - dx; x1 <= x2; x1++)
	{
		LCDDot (x1, y1, color);

		if (d > 0)
		{
			y1 += yi;
			d -= dx << 1;
		}

		d += dy;
	}
}

void PlotLineHigh (int x1, int y1, int x2, int y2, int color)
{
	int dx = x2 - x1;
	int dy = y2 - y1;

	int xi = 1;

	if (dx < 0)
	{
		xi = -1;
		dx = -dx;
	}

	dx *= 2;

	for (int d = dx - dy; y1 <= y2; y1++)
	{
		LCDDot (x1, y1, color);

		if (d > 0)
		{
			x1 += xi;
			d -= dy << 1;
		}

		d += dx;
	}
}

void LCDLine (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int color)
{
#define ABS(x) x < 0 ? -x : x;

	int dx = x2 - x1;
	int dy = y2 - y1;

	dx = ABS(dx);
	dy = ABS(dy);

	if (dy < dx)
	{
		if (x1 > x2)
			PlotLineLow (x2, y2, x1, y1, color);
		else
			PlotLineLow (x1, y1, x2, y2, color);
	}
	else
	{
		if (y1 > y2)
			PlotLineHigh (x2, y2, x1, y1, color);
		else
			PlotLineHigh (x1, y1, x2, y2, color);
	}
}

void LCDDot (unsigned int x, unsigned int y, unsigned int color)
{
	LCDSetAddress (x, y, x, y);
	LCD_DATA_16 (color);
}

void LCDBox (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int color)
{
	LCDSetAddress (x1, y1, x2, y2);

	unsigned int n = (y2 - y1 + 1) * (x2 - x1 + 1);

	for (int i = 0; i < n; i++)
		LCD_DATA_16 (color);
}

void LCDFill (unsigned int color)
{
	LCDSetAddress (0, 0, lcdWidth-1, lcdHeight-1);

	unsigned int n = LCD_WIDTH * LCD_HEIGHT;

	for (int i = 0; i < n; i++)
		LCD_DATA_16 (color);
}

void LCDData (unsigned short *data, int n)
{
	for (int i = 0; i < n; i++)
		LCD_DATA_16 (data[i]);
}

void LCDGetMetrics (unsigned int *w, unsigned int *h)
{
	if (lcdOrientation == LCD_VERTICAL)
	{
		*w = LCD_HEIGHT;
		*h = LCD_WIDTH;
	}
	else
	{
		*w = LCD_WIDTH;
		*h = LCD_HEIGHT;
	}
}

int LCDGetOrientation (void)
{
	return lcdOrientation;
}
