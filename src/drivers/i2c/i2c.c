/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "ioctl/ioctl.h"
#include "drivers/i2c/i2c-stm32f407.h"

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_i2c_open (struct FileNode *node, const char *filename)
{
	I2CInit ();
	return 0;
}

int dev_i2c_close (struct FileNode *node)
{
	return 0;
}

int dev_i2c_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	unsigned char addr = ((unsigned char*)data)[0];

	if (I2CReadPacket (addr, data+1, nbytes-1))
		return -EIO;
	else
		return nbytes;
}

int dev_i2c_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	unsigned char addr = ((unsigned char*)data)[0];

	if (I2CWritePacket (addr, data+1, nbytes-1))
		return -EIO;
	else
		return nbytes;
}

int dev_i2c_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	switch (code)
	{
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	// Default return value
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (i2c,
	dev_i2c_open,
	dev_i2c_close,
	dev_i2c_read,
	dev_i2c_write,
	dev_i2c_ioctl,
	"i2c");
