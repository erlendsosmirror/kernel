/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "ioctl/ioctl.h"
#include "drivers/gpio/gpio-stm32f407.h"
#include "../config/live/drivers/i2c.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define DEMCR		(*((volatile unsigned int *)0xE000EDFC))
#define DWT_CTRL	(*(volatile unsigned int *)0xe0001000)
#define CPU_CYCLES	*((volatile unsigned int *)0xE0001004)

///////////////////////////////////////////////////////////////////////////////
// Helper functions
///////////////////////////////////////////////////////////////////////////////
void Delay_Init ()
{
	// Enable DWT
	DEMCR |= 0x01000000;

	// Reset counter
	CPU_CYCLES = 0;

	// Enable CPU cycle counter
	DWT_CTRL |= 1;
}

void Delay_us (unsigned int us)
{
	CPU_CYCLES = 0;
	unsigned int cyc_per_us = 168;
	unsigned int time_to_wait = cyc_per_us * us;

	while (1)
	{
		if (CPU_CYCLES >= time_to_wait)
			return;
	}
}

int I2C1_WaitSCL (void)
{
	CPU_CYCLES = 0;
	unsigned int cyc_per_us = 168;
	unsigned int time_to_wait = cyc_per_us * 1000;

	while (!(I2C1_PORT->IDR & PIN(I2C1_SCL)))
	{
		if (CPU_CYCLES >= time_to_wait)
			return 1;
	}

	return 0;
}

int I2C1_Start ()
{
	// Always make sure the bus is consistent
	// SCL xx-
	// SDA x--
	I2C1_PORT->BSRR = PIN(I2C1_SDA);
	Delay_us (I2CDELAY);
	I2C1_PORT->BSRR = PIN(I2C1_SCL);
	Delay_us (I2CDELAY);

	// Wait for SCL
	int ret = I2C1_WaitSCL ();

	// Start condition
	// SCL --_
	// SDA -__
	I2C1_PORT->BSRR = PIN(I2C1_SDA) << 16;
	Delay_us (I2CDELAY);
	I2C1_PORT->BSRR = PIN(I2C1_SCL) << 16;
	Delay_us (I2CDELAY);
	return ret;
}

int I2C1_Stop ()
{
	// SCL xx-
	// SDA x__
	I2C1_PORT->BSRR = PIN(I2C1_SDA) << 16;
	Delay_us (I2CDELAY);
	I2C1_PORT->BSRR = PIN(I2C1_SCL);
	Delay_us (I2CDELAY);

	// Wait for SCL
	int ret = I2C1_WaitSCL ();

	// SCL --
	// SDA _-
	I2C1_PORT->BSRR = PIN(I2C1_SDA);
	Delay_us (I2CDELAY);
	return ret;
}

int I2C1_Wr (unsigned char d)
{
	// Assume
	// SCL _
	// SDA _
	int ret = 0;

	for (int i = 0x80; i; i = i >> 1)
	{
		// SCL __
		// SDA _x
		I2C1_PORT->BSRR = (d & i) ? PIN(I2C1_SDA) : PIN(I2C1_SDA) << 16;
		Delay_us (I2CDELAY);

		// SCL __-
		// SDA _xx
		I2C1_PORT->BSRR = PIN(I2C1_SCL);
		Delay_us (I2CDELAY);

		// Wait for SCL
		ret |= I2C1_WaitSCL ();

		// SCL __-_
		// SDA _xxx
		I2C1_PORT->BSRR = PIN(I2C1_SCL) << 16;
		Delay_us (I2CDELAY);
	}

	// SCL __-__-
	// SDA _xxxaa
	I2C1_PORT->BSRR = PIN(I2C1_SDA);
	Delay_us (I2CDELAY);
	I2C1_PORT->BSRR = PIN(I2C1_SCL);
	Delay_us (I2CDELAY);

	// Wait for SCL
	ret |= I2C1_WaitSCL ();

	// Read SDA
	ret |= I2C1_PORT->IDR & PIN(I2C1_SDA) ? 1 : 0;

	// SCL __-__--__
	// SDA _xxxaaaax
	I2C1_PORT->BSRR = PIN(I2C1_SCL) << 16;
	Delay_us (I2CDELAY);
	return ret;
}

unsigned char I2C1_Rd (int doack, int *errstate)
{
	// Assume
	// SCL _
	// SDA -
	unsigned char ret = 0;

	for (int i = 0x80; i; i = i >> 1)
	{
		// SCL __
		// SDA -x
		I2C1_PORT->BSRR = PIN(I2C1_SDA);
		Delay_us (I2CDELAY);

		// SCL __-
		// SDA -xx
		I2C1_PORT->BSRR = PIN(I2C1_SCL);
		Delay_us (I2CDELAY);

		// Wait for SCL
		*errstate |= I2C1_WaitSCL ();

		// Read
		if (I2C1_PORT->IDR & PIN(I2C1_SDA))
			ret |= i;

		// SCL __-_
		// SDA -xxx
		I2C1_PORT->BSRR = PIN(I2C1_SCL) << 16;
		Delay_us (I2CDELAY);
	}

	// SCL __-__
	// SDA -xxxa
	I2C1_PORT->BSRR = doack ? PIN(I2C1_SDA) << 16 : PIN(I2C1_SDA);
	Delay_us (I2CDELAY);

	// SCL __-__-
	// SDA -xxxaa
	I2C1_PORT->BSRR = PIN(I2C1_SCL);
	Delay_us (I2CDELAY);

	// Wait for SCL
	*errstate |= I2C1_WaitSCL ();

	// SCL __-__-_
	// SDA -xxxaaa
	I2C1_PORT->BSRR = PIN(I2C1_SCL) << 16;
	Delay_us (I2CDELAY);

	I2C1_PORT->BSRR = PIN(I2C1_SDA);
	Delay_us (I2CDELAY);
	return ret;
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void I2CInit (void)
{
	static char i2cinit;

	if (!i2cinit)
	{
		I2C1_PORT->BSRR = PIN(I2C1_SDA);
		I2C1_PORT->BSRR = PIN(I2C1_SCL);

		GPIOInit (I2C1_PORT, PIN(I2C1_SCL) | PIN(I2C1_SDA), GPIO_OUTPUT | GPIO_OPENDRAIN);

		Delay_Init ();

		i2cinit = 1;
	}
}

int I2CReadPacket (unsigned char addr, unsigned char *buf, int size)
{
	int ret = 0;

	ret |= I2C1_Start ();
	ret |= I2C1_Wr ((addr << 1) | 0x01);

	for (int i = 0, j = size-1; i < size; i++, j--)
		buf[i] = I2C1_Rd (j, &ret);

	ret |= I2C1_Stop ();
	return ret;
}

int I2CWritePacket (unsigned char addr, unsigned char *buf, int size)
{
	int ret = 0;

	ret |= I2C1_Start ();

	ret |= I2C1_Wr (addr << 1);

	for (int i = 0; i < size; i++)
		ret |= I2C1_Wr (buf[i]);

	ret |= I2C1_Stop ();
	return ret;
}
