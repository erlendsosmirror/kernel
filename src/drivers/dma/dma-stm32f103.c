/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "drivers/dma/dma-stm32f103.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void DMAInit (DMAHandle *hdma)
{
	static const char bitshift[] = {0, 4, 8, 12, 16, 20, 24};

	unsigned int ch = ((unsigned int)hdma->chinst) & 0x0FF;
	ch = (ch - 0x0008) / 20;

	hdma->shift = bitshift[ch];
	hdma->dma = (DMA*) (((unsigned int)hdma->chinst) & 0xFFFFFF00);
}

void DMAStart (DMAHandle *hdma, void *src, void *dst, unsigned int nbytes)
{
	// Make sure the channel is disabled
	while (hdma->chinst->CR) hdma->chinst->CR = 0;

	// Set configuration
	hdma->chinst->CR = hdma->config;
	hdma->chinst->NDTR = nbytes;

	// Set destination and source
	if ((hdma->config & DMA_CR_DIR) == DMA_CR_DIR_M2P)
	{
		hdma->chinst->PAR = (unsigned int) dst;
		hdma->chinst->M0AR = (unsigned int) src;
	}
	else
	{
		hdma->chinst->PAR = (unsigned int) src;
		hdma->chinst->M0AR = (unsigned int) dst;
	}

	// Clear all pending interrupts
	hdma->dma->IFCR = (DMA_TEIF | DMA_HTIF | DMA_TCIF) << hdma->shift;

	// Enable interrupts
	hdma->chinst->CR |= DMA_CR_TCIE | DMA_CR_HTIE | DMA_CR_TEIE;

	// Enable DMA channel
	hdma->chinst->CR |= DMA_CR_EN;
}

void DMAIRQHandler (DMAHandle *hdma)
{
	// Transfer error
	if (hdma->dma->ISR & (DMA_TEIF << hdma->shift))
	{
		hdma->chinst->CR &= ~DMA_CR_TEIE;
		hdma->dma->IFCR = DMA_TEIF << hdma->shift;
	}

	// Half transfer complete
	if (hdma->dma->ISR & (DMA_HTIF << hdma->shift))
	{
		if (!(hdma->chinst->CR & DMA_CR_CIRC))
			hdma->chinst->CR &= ~DMA_CR_HTIE;

		hdma->dma->IFCR = DMA_HTIF << hdma->shift;
	}

	// Transfer complete
	if (hdma->dma->ISR & (DMA_TCIF << hdma->shift))
	{
		if (!(hdma->chinst->CR & DMA_CR_CIRC))
		{
			hdma->chinst->CR &= ~DMA_CR_TCIE;
			hdma->busystate = 0;
		}

		hdma->dma->IFCR = DMA_TCIF << hdma->shift;
	}
}
