/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * How to use:
 *  - Open file
 *  - Set up signal handler for SIGURG
 *  - Call ioctl with F_SETOWN to enable reception of SIGURG
 *  - Call ioctl with F_GETFL, or with FASYNC, and call ioctl with F_SETFL
 *  - Write 4096 bytes of audio to fill the buffer
 *  - Call ioctl with I2SSTART and sample rate to start transfer
 *  - Use signal handler to write 2048 bytes for each SIGURG
 *  - Call ioctl with I2SSTOP after last block has been written to stop
 *
 * With 48 kHz sampling rate and 4096 byte buffer (1024 stereo samples),
 * one buffer is transferred every 21.3 ms (46.87 Hz). SIGURG happens twice
 * as often due to half-buffer transfer.
 */

/*
 * Blue: BCK
 * Purple: DIN
 * Green: LCK
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "ioctl/ioctl.h"
#include "mem/string.h"
#include "platform/platform.h"
#include "process/process-allocate.h"
#include "sched/scheduler-signal.h"
#include "posix/sys/signal.h"
#include "arch/stm32f407/rcc.h"
#include "drivers/gpio/gpio-stm32f407.h"
#include "drivers/i2s/i2s-stm32f407.h"
#include "../config/live/drivers/i2s.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
i2s *i2s_irq_instances[1];

///////////////////////////////////////////////////////////////////////////////
// Interrupt functions
///////////////////////////////////////////////////////////////////////////////
void I2SDMAInterrupt (void)
{
	// Get pointers
	i2s *p = i2s_irq_instances[0];
	DMAHandle *hdma = &p->dmatx;

	// Half transfer complete
	if (hdma->dma->ISR & (DMA_HTIF << hdma->shift))
	{
		if (p->prctonotify)
			SchedPostSignal (&p->prctonotify->trc, SIGURG);

		p->dmaflag = 1;
	}

	// Transfer complete
	if (hdma->dma->ISR & (DMA_TCIF << hdma->shift))
	{
		if (p->prctonotify)
			SchedPostSignal (&p->prctonotify->trc, SIGURG);

		p->dmaflag = 2;
	}

	// Handle the rest with the default handler
	DMAIRQHandler (hdma);
}

///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
__attribute__((naked))
int copysamples (void *dst, void *src, unsigned int nbytes)
{
	/*
	 * This function copies nbytes of sample memory from src to dst,
	 * while using SIMD instructions to find the peaks for the left
	 * and right channel. The peaks are in the return value.
	 */
	__asm("		stmdb sp!, {r3,r4,r5,r6}");
	__asm("		mov r3, #0");			// max = 0
	__asm("		mov r4, #0");			// i = 0
	__asm("		b loopcheck");
	__asm("	loopbody:");
	__asm("		ldr r5, [r1, r4]");		// data = src[i]
	__asm("		str r5, [r0, r4]");		// dst[i] = data
	__asm("		add r4, r4, #4");		// i++
	__asm("		ssub16 r6, r3, r5");	// (void) max - data (set GE bits)
	__asm("		sel r3, r3, r5");		// max = max > data ? max : data
	__asm("	loopcheck:");
	__asm("		cmp r2, r4");
	__asm("		bgt loopbody");
	__asm("		mov r0, r3");			// Store audiopeaks
	__asm("		ldmia sp!, {r3,r4,r5,r6}");
	__asm("		bx lr");
}

int I2SClockValid (int samplerate)
{
	static const int rates[] = {
		0,
		8000, 16000, 22050, 32000,
		44100, 48000, 96000, 192000};

	for (int i = 0; i < sizeof(rates)/4; i++)
		if (rates[i] == samplerate)
			return i;

	return 0;
}

void I2SClockCalc (i2s *p)
{
	// Static table of precalculated values for the I2S PLL and
	// clock divider. Indexed with I2SClockValid (above).
	static const int configs[] = {
		(5 << 28) | (192 << 16) | (1 << 8) | 12,	// 48000 (invalid)
		(2 << 28) | (192 << 16) | (1 << 8) | 187,	// 8000
		(3 << 28) | (192 << 16) | (1 << 8) | 62,	// 16000
		(3 << 28) | (290 << 16) | (1 << 8) | 68,	// 22050
		(2 << 28) | (256 << 16) | (1 << 8) | 62,	// 32000
		(2 << 28) | (302 << 16) | (1 << 8) | 53,	// 44100
		(5 << 28) | (192 << 16) | (1 << 8) | 12,	// 48000
		(5 << 28) | (384 << 16) | (1 << 8) | 12,	// 96000
		(3 << 28) | (424 << 16) | (1 << 8) | 11};	// 192000

	// Get the index and extract PLL config
	int index = I2SClockValid (p->samplerate);
	int plldiv = configs[index] & (0x0F << 28);
	int pllmul = (configs[index] >> 16) & 0x0FFF;

	// Configure the I2S PLL
	RCC->CR &= ~0x04000000;
	while (RCC->CR & 0x08000000);
	RCC->PLLI2SCFGR = (pllmul << 6) | plldiv;
	RCC->CR |= 0x04000000;
	while (!(RCC->CR & 0x08000000));

	// Configure the I2S divider
	p->instance->I2SPR = configs[index] & 0x1FF;
}

int I2SInit (i2s *p)
{
	// Enable peripheral clock
	RCC->APB1ENR |= (1 << 14);
	RCC->AHB1ENR |= (1 << 1) | (1 << 2);

	// GPIO Init
	GPIOInit (I2S_PORT_SD, I2S_PIN_SD, GPIO_ALTERNATE(5) | GPIO_AF | GPIO_SPEED_VERYHIGH);
	GPIOInit (I2S_PORT_WS, I2S_PIN_WS, GPIO_ALTERNATE(5) | GPIO_AF | GPIO_SPEED_VERYHIGH);
	GPIOInit (I2S_PORT_CK, I2S_PIN_CK, GPIO_ALTERNATE(5) | GPIO_AF | GPIO_SPEED_VERYHIGH);

	// DMA Init
	p->dmatx.chinst = I2S_DMACHAN;
	p->dmatx.config = I2S_DMACHSEL | DMA_CR_DIR_M2P | DMA_CR_MINC | (1 << 11) | (1 << 13) | DMA_CR_CIRC;
	DMAInit (&p->dmatx);

	// Clear register config
	p->instance->I2SCFGR = 0;

	// Set clock
	I2SClockCalc (p);

	// Set config
	p->instance->I2SCFGR = (1 << 11) | (2 << 8);

	// Enable interrupts
	PlatformSetInterruptPriority (I2S_DMAIRQN, 5);
	PlatformEnableInterrupt (I2S_DMAIRQN);
	return 0;
}

void I2SStop (i2s *p)
{
	// Disable DMA requests
	p->instance->CR2 &= ~3;

	// Disable DMA channel
	p->dmatx.chinst->CR &= ~1;

	// Disable I2S
	p->instance->I2SCFGR = 0;
}

int I2SStart (i2s *p)
{
	// Setup and enable the DMA channel, enable the I2S, enable DMA TX requests
	DMAStart (&p->dmatx, p->audiodata, (void*) &p->instance->DR, AUDIO_BUFFER_WORDSIZE);
	p->instance->I2SCFGR |= 1 << 10;
	p->instance->CR2 |= 2;
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_i2s_open (struct FileNode *node, const char *filename)
{
	static i2s i2sp;

	if (i2sp.samplerate)
		return -EBUSY;

	MemSet (&i2sp, 0, sizeof (i2s));

	i2sp.instance = I2S_INSTANCE;
	i2sp.samplerate = 44100;

	node->priv = &i2sp;
	i2s_irq_instances[0] = &i2sp;

	return I2SInit (&i2sp);
}

int dev_i2s_close (struct FileNode *node)
{
	i2s *p = (i2s*)node->priv;
	I2SStop (p);
	p->samplerate = 0;
	return 0;
}

int dev_i2s_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	return 0;
}

int dev_i2s_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	i2s *p = (i2s*)node->priv;

	if (nbytes == AUDIO_BUFFER_WORDSIZE)
	{
		if (p->dmaflag == 1)
			p->audiopeaks = copysamples (p->audiodata, data, nbytes);
		else
			p->audiopeaks = copysamples (p->audiodata + AUDIO_BUFFER_HALFSIZE, data, nbytes);

		p->dmaflag = 0;
		return nbytes;
	}
	else if (nbytes == AUDIO_BUFFER_BYTESIZE)
	{
		p->audiopeaks = copysamples (p->audiodata, data, nbytes);
		p->dmaflag = 0;
		return nbytes;
	}
	else
		return -EINVAL;
}

int dev_i2s_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	i2s *p = (i2s*)node->priv;

	switch (code)
	{
	case I2SSTART:
		if (I2SClockValid ((int)data))
		{
			p->samplerate = (int)data;
			I2SInit (p);
			return I2SStart (p);
		}

		return -EINVAL;
	case I2SSTOP:
		I2SStop (p);
		break;
	case I2SGETPEAKS:
		*((unsigned int*)data) = p->audiopeaks;
		break;
	case F_SETOWN:
		p->pidtonotify = (int)data;
		p->prctonotify = ProcessFindByPid (p->pidtonotify);
		break;
	case F_GETFL:
	case F_SETFL:
		break;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	// Default return value
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (i2s,
	dev_i2s_open,
	dev_i2s_close,
	dev_i2s_read,
	dev_i2s_write,
	dev_i2s_ioctl,
	"i2s");
