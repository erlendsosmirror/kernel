/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "drivers/gpio/gpio-stm32f407.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void GPIOInit (GPIO *gpio, int pins, int mode)
{
	for (int pin = 1, pinnum = 0; pinnum < 16; pin = pin << 1, pinnum++)
	{
		// Skip unspecified pin
		if (!(pins & pin))
			continue;

		// Disable while config
		gpio->MODER &= ~(0x3 << (pinnum << 1));

		// Set AF
		unsigned int af = (mode >> 16) & 0x0F;

		if (pinnum >= 8)
		{
			unsigned int shifter = ((pinnum - 8) << 2);
			gpio->AFRH &= ~(0x0F << shifter);
			gpio->AFRH |= af << shifter;
		}
		else
		{
			unsigned int shifter = pinnum << 2;
			gpio->AFRL &= ~(0x0F << shifter);
			gpio->AFRL |= af << shifter;
		}

		// Set pullup/down
		gpio->PUPDR &= ~(0x3 << (pinnum << 1));
		gpio->PUPDR |= ((mode >> 12) & 0x3) << (pinnum << 1);

		// Set speed
		gpio->OSPEEDR &= ~(0x3 << (pinnum << 1));
		gpio->OSPEEDR |= ((mode >> 8) & 0x3) << (pinnum << 1);

		// Set push-pull/open-drain
		gpio->OTYPER &= ~(1 << pinnum);
		gpio->OTYPER |= ((mode >> 4) & 1) << pinnum;

		// Set mode
		gpio->MODER |= (mode & 3) << (pinnum << 1);
	}
}
