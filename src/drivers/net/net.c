/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Crappy devnode for getting stats about network stuffs.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "ioctl/ioctl.h"
#include "mem/string.h"

#include "../config/live/network-config.h"
#include "net/enet/network.h"
#include "net/enet/layer4/udp.h"
#include "net/enet/layer2/linkspeed.h"

///////////////////////////////////////////////////////////////////////////////
// Vars
///////////////////////////////////////////////////////////////////////////////
extern struct ENetGlobals eglobals;

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_net_open (struct FileNode *node, const char *filename)
{
	return 0;
}

int dev_net_close (struct FileNode *node)
{
	return 0;
}

int dev_net_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	// Find the interface
	Layer2 *interface = eglobals.iflist;
	int n = (int) node->priv;

	for (int i = 0; i < n && interface; i++)
		interface = interface->next;

	// Copy if enough space
	if (interface && nbytes >= sizeof (struct Layer2Statistics))
	{
		MemCpy (data, &interface->stats, sizeof (struct Layer2Statistics));
		return sizeof (struct Layer2Statistics);
	}

	return -1;
}

int dev_net_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	// Set index
	if (nbytes > 0)
		node->priv = (void*) ((unsigned int) ((unsigned char*)data)[0]);

	// Find the interface
	Layer2 *interface = eglobals.iflist;
	int n = (int) node->priv;

	for (int i = 0; i < n && interface; i++)
		interface = interface->next;

	// Get connected state
	if (interface)
		return Layer2LinkUp (interface);

	return -1;
}

int dev_net_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	switch (code)
	{
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	// Default return value
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (net,
	dev_net_open,
	dev_net_close,
	dev_net_read,
	dev_net_write,
	dev_net_ioctl,
	"net");
