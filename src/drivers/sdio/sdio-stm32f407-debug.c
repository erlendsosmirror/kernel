/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "mem/string.h"
#include "drivers/gpio/gpio-stm32f407.h"
#include "drivers/dma/dma-stm32f407.h"
#include "drivers/sdio/sdio-stm32f407.h"
#include "arch/stm32f407/rcc.h"
#include "platform/platform.h"
#include "ioctl/ioctl.h"
#include "print/print.h"

///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
void dev_sdio_dumpresponse (sdio *t)
{
	Print ("SDIO RESP1 0x%x\n", t->instance->RESP1);
	Print ("SDIO RESP2 0x%x\n", t->instance->RESP2);
	Print ("SDIO RESP3 0x%x\n", t->instance->RESP3);
	Print ("SDIO RESP4 0x%x\n", t->instance->RESP4);
}

void dev_sdio_printcid (sdio *t)
{
	int mid = (t->cid[0] >> 24) & 0xFF;
	int oid = (t->cid[0] >> 8) & 0xFFFF;
	int pnm0 = t->cid[0] & 0xFF;
	int pnm1 = t->cid[1];
	int prv = (t->cid[2] >> 24) & 0xFF;
	int psn = ((t->cid[2] & 0x00FFFFFF) << 8) | (t->cid[3] >> 24);
	int mdt = (t->cid[3] >> 8) & 0x0FFF;

	Print ("Manufacturer ID (MID):    0x%x (%i)\n", mid, mid);
	Print ("OEM/Application ID (OID): 0x%x (%i)\n", oid, oid);
	Print ("Product name (PNM):       0x%x (%i)\n", pnm0, pnm0);
	Print ("Product name (PNM):       0x%x (%i)\n", pnm1, pnm1);
	Print ("Product revision (PRV):   0x%x (%i)\n", prv, prv);
	Print ("Product serial no (PSN):  0x%x (%i)\n", psn, psn);
	Print ("Manufacturing date (MDT): 0x%x (%i)\n", mdt, mdt);
}

void dev_sdio_printcsd (sdio *t)
{
	int csd_structure = t->csd[0] >> 30;
	int c_size = ((t->csd[1] & 0x3F) << 16) | ((t->csd[2] >> 16) & 0x0000FFFF);

	Print ("CSD Structure 0x%x (%i)\n", csd_structure, csd_structure);
	Print ("Device Size 0x%x (%i)\n", c_size, c_size);
}
