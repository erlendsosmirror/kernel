/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * WARNING: This driver only works with high-capacity cards (4 GB to 32 GB)
 *
 * Resources:
 *  http://users.ece.utexas.edu/~valvano/EE345M/SD_Physical_Layer_Spec.pdf
 *  https://yannik520.github.io/sdio.html
 *  https://blog.frankvh.com/2011/09/04/stm32f2xx-sdio-sd-card-interface/
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "mem/string.h"
#include "drivers/gpio/gpio-stm32f407.h"
#include "drivers/dma/dma-stm32f407.h"
#include "drivers/sdio/sdio-stm32f407.h"
#include "arch/stm32f407/rcc.h"
#include "platform/platform.h"
#include "ioctl/ioctl.h"
#include "print/print.h"

///////////////////////////////////////////////////////////////////////////////
// Forward declarations
///////////////////////////////////////////////////////////////////////////////

// Debug
void dev_sdio_dumpresponse (sdio *t);
void dev_sdio_printcid (sdio *t);
void dev_sdio_printcsd (sdio *t);

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
sdio *sdio_irq_instance;
int isinited = 0;

///////////////////////////////////////////////////////////////////////////////
// IRQ functions
///////////////////////////////////////////////////////////////////////////////
void DMA2CH3_IrqHandler (void)
{
	DMAIRQHandler (&sdio_irq_instance->dmarx);
}

void DMA2CH6_IrqHandler (void)
{
	DMAIRQHandler (&sdio_irq_instance->dmatx);
}

void SDIO_IrqHandler (void)
{
	sdio *t = sdio_irq_instance;
	t->transferstate = t->instance->STA;

	t->instance->ICR = SDIO_STA_DCRCFAIL | SDIO_STA_DTIMEOUT |
		SDIO_STA_TXUNDERR | SDIO_STA_RXOVERR | SDIO_STA_DATAEND |
		SDIO_STA_STBITERR;
	t->instance->MASK = 0;
}

///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
void dev_sdio_init_instance (sdio *t)
{
	// Clear sdio structure
	MemSet (t, 0, sizeof(sdio));

	// Set instances
	t->instance = SDIO;
	t->dmarx.chinst = DMAChannel_2_3;
	t->dmatx.chinst = DMAChannel_2_6;

	// Store initial DMA config
	t->dmarx.config = DMA_CR_CHSEL(4) | DMA_CR_DIR_P2M | DMA_CR_MINC | DMA_CR_PBURST4 | DMA_CR_MBURST4
		| DMA_CR_PSIZE_32 | DMA_CR_MSIZE_32 | DMA_CR_PFCTRL;
	t->dmarx.fconfig = DMA_FCR_FEIE | DMA_FCR_DMDIS | DMA_FCR_FTH_FULL;

	t->dmatx.config = DMA_CR_CHSEL(4) | DMA_CR_DIR_M2P | DMA_CR_MINC | DMA_CR_PBURST4 | DMA_CR_MBURST4
		| DMA_CR_PSIZE_32 | DMA_CR_MSIZE_32 | DMA_CR_PFCTRL;
	t->dmatx.fconfig = DMA_FCR_FEIE | DMA_FCR_DMDIS | DMA_FCR_FTH_FULL;

	// Enable the clock
	RCC->APB2ENR |= 1 << 11;

	// Enable GPIO clocks
	RCC->AHB1ENR |= 0x0C;

	// Set output pins
	GPIOInit (GPIOC, PIN(8) | PIN(9) | PIN(10) | PIN(11) | PIN(12), GPIO_ALTERNATE(12) | GPIO_AF | GPIO_SPEED_VERYHIGH);
	GPIOInit (GPIOD, PIN(2), GPIO_ALTERNATE(12) | GPIO_AF | GPIO_SPEED_VERYHIGH);

	// Set DMA channel and UART IRQ priorities
	PlatformSetInterruptPriority (59, 5);
	PlatformSetInterruptPriority (69, 5);
	PlatformSetInterruptPriority (49, 5);
	PlatformEnableInterrupt (59);
	PlatformEnableInterrupt (69);
	PlatformEnableInterrupt (49);

	// Configure DMA
	DMAInit (&t->dmarx);
	DMAInit (&t->dmatx);
}

int dev_sdio_waitflags (sdio *t, unsigned int flags)
{
	unsigned int start = PlatformGetTick ();
	unsigned int reg = 0;

	while (!reg && (PlatformGetTick () - start) < 1000)
		reg = t->instance->STA & flags;

	t->instance->ICR = 0x00C007FF;

	return reg;
}

void dev_sdio_powerup (sdio *t)
{
	// Disable clock, enable power and wait
	t->instance->CLKCR = 0;
	t->instance->POWER = 3;
	unsigned int start = PlatformGetTick ();
	while ((PlatformGetTick () - start) < 1);

	// Set clock rate < 400 KHz and enable it
	// Solve for "400000 = 48000000 / (x + 2)" is 118, setting 150
	t->instance->CLKCR = 150 | (1 << 8);

	// Reset with CMD0, wait for cmdsent
	t->instance->ARG = 0;
	t->instance->CMD = 0 | SDIO_CMD_CPSMEN;
	dev_sdio_waitflags (t, 1 << 7);

	// Send CMD8 for voltage select, check flags ccrcfail, cmdrend, ctimeout
	t->instance->ARG = 0x1AA;
	t->instance->CMD = 8 | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_SHORT;
	int ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

	if (!(ret & SDIO_STA_CMDREND))
	{
		Print ("No high-capacity card?\n");
	}
	else if (t->instance->RESP1 != 0x1AA)
	{
		Print ("Unexpected response\n");
		dev_sdio_dumpresponse (t);
	}

	// Voltage selection loop
	int initialized = 0;
	int tries = 10;

	while (!initialized && tries)
	{
		// Send CMD55 app-cmd, required before CMD41, check flags ccrcfail, cmdrend, ctimeout
		t->instance->ARG = 0;
		t->instance->CMD = 55 | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_SHORT;
		ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

		if (!(ret & SDIO_STA_CMDREND))
		{
			Print ("Unexpected response 0x%x\n", ret);
			dev_sdio_dumpresponse (t);
		}

		// Send CMD41 to select voltage range, 3.2 - 3.3 V
		t->instance->ARG = (1 << 20) | (1 << 30);
		t->instance->CMD = 41 | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_SHORT;
		ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

		// For some reason getting crc error here, but data appears valid
		if (!(ret & (SDIO_STA_CMDREND | SDIO_STA_CCRCFAIL)))
		{
			Print ("Unexpected response 0x%x\n", ret);
			dev_sdio_dumpresponse (t);
		}

		initialized = t->instance->RESP1 >> 31;
		tries--;
	}

	if (!tries)
		Print ("Timeout\n");
}

void dev_sdio_initcard (sdio *t)
{
	// Send CMD2
	t->instance->ARG = 0;
	t->instance->CMD = 2 | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_LONG;
	int ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

	if (!(ret & SDIO_STA_CMDREND))
	{
		Print ("Unexpected response 0x%x\n", ret);
		dev_sdio_dumpresponse (t);
	}

	t->cid[0] = t->instance->RESP1;
	t->cid[1] = t->instance->RESP2;
	t->cid[2] = t->instance->RESP3;
	t->cid[3] = t->instance->RESP4;

	// Send CMD3 set relative address
	t->instance->ARG = 0;
	t->instance->CMD = 3 | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_SHORT;
	ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

	if (!(ret & SDIO_STA_CMDREND))
	{
		Print ("Unexpected response 0x%x\n", ret);
		dev_sdio_dumpresponse (t);
	}

	t->rca = t->instance->RESP1 >> 16;

	// Get the cards CSD with CMD9
	t->instance->ARG = t->rca << 16;
	t->instance->CMD = 9 | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_LONG;
	ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

	if (!(ret & SDIO_STA_CMDREND))
	{
		Print ("Unexpected response 0x%x\n", ret);
		dev_sdio_dumpresponse (t);
	}

	t->csd[0] = t->instance->RESP1;
	t->csd[1] = t->instance->RESP2;
	t->csd[2] = t->instance->RESP3;
	t->csd[3] = t->instance->RESP4;

	// Set to transfer state with CMD7 (select/deselect card)
	t->instance->ARG = t->rca << 16;
	t->instance->CMD = 7 | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_SHORT;
	ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

	if (!(ret & SDIO_STA_CMDREND))
	{
		Print ("Unexpected response 0x%x\n", ret);
		dev_sdio_dumpresponse (t);
	}
}

void dev_sdio_setspeed (sdio *t, int mode)
{
	// CMD55
	t->instance->ARG = t->rca << 16;
	t->instance->CMD = 55 | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_SHORT;
	int ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

	if (!(ret & SDIO_STA_CMDREND))
	{
		Print ("Unexpected response 0x%x\n", ret);
		dev_sdio_dumpresponse (t);
	}

	// CMD6
	t->instance->ARG = mode ? 2 : 0;
	t->instance->CMD = 6 | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_SHORT;
	ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

	if (!(ret & SDIO_STA_CMDREND))
	{
		Print ("Unexpected response 0x%x\n", ret);
		dev_sdio_dumpresponse (t);
	}

	// Set clock rate
	unsigned int reg = t->instance->CLKCR & ~((3 << 11) | 0x0FF);

	if (mode)
		reg |= 1 << 11;

	t->instance->CLKCR = reg | 5;
}

void dev_sdio_preparetransfer (sdio *t, unsigned int nbytes)
{
	// Stop all transfer and prepare
	t->instance->DCTRL = 0;
	t->transferstate = 0;

	// Enable transfer interrupts
	t->instance->MASK |= SDIO_STA_DCRCFAIL | SDIO_STA_DTIMEOUT |
		SDIO_STA_TXUNDERR | SDIO_STA_RXOVERR | SDIO_STA_DATAEND |
		SDIO_STA_STBITERR;

	// Configure data transfer
	t->instance->DTIMER = 100000000;
	t->instance->DLEN = nbytes;
	t->instance->ARG = t->address;
}

int dev_sdio_waittransfer (sdio *t, unsigned int nbytes, DMAHandle *dma, int flag)
{
	int time = 100000000;

	while (time && !t->transferstate && dma->busystate)
		time--;

	if (!time)
		return -ETIME;

	time = 100000000;

	while (time && (t->instance->STA & flag))
		time--;

	if (!time)
		return -ETIME;

	if (nbytes > 512)
	{
		t->instance->ARG = 0;
		t->instance->CMD = 12 | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_SHORT;
		int ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

		if (!(ret & SDIO_STA_CMDREND))
		{
			Print ("Unexpected response 0x%x\n", ret);
			dev_sdio_dumpresponse (t);
		}
	}

	t->address += nbytes / 512;
	return nbytes;
}

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_sdio_open (struct FileNode *node, const char *filename)
{
	// Allocate (static for now)
	static sdio ty;
	sdio *t = &ty;

	// Set the irq-handlers
	sdio_irq_instance = t;

	// Set the file node
	node->priv = (void*) t;

	if (isinited)
		return 0;
	isinited = 1;

	// Initialize
	dev_sdio_init_instance (t);
	dev_sdio_powerup (t);
	dev_sdio_initcard (t);
	dev_sdio_setspeed (t, 1);

	// Print details
	//dev_sdio_printcid (t);
	//dev_sdio_printcsd (t);

	// Set block size
	t->instance->ARG = 512;
	t->instance->CMD = 16 | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_SHORT;
	int ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

	if (!(ret & SDIO_STA_CMDREND))
	{
		Print ("Unexpected response 0x%x\n", ret);
		dev_sdio_dumpresponse (t);
	}

	// Configure data transfer
	t->instance->DTIMER = 100000000;
	t->instance->DLEN = 512;

	// Done
	return 0;
}

int dev_sdio_close (struct FileNode *node)
{
	// Make sure DMA channels and SDIO is disabled
	sdio *t = (sdio*) node->priv;
	t->dmatx.chinst->CR = 0;
	t->dmarx.chinst->CR = 0;

	// Clear irqhandler (also "clears" the static allocation)
	//sdio_irq_instance = 0;
	return 0;
}

int dev_sdio_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	// Check for complete sector read
	if (nbytes % 512 || nbytes < 512)
		return -EINVAL;

	// Extract sdio pointer
	sdio *t = (sdio*) node->priv;

	// Prepare transfer
	dev_sdio_preparetransfer (t, nbytes);

	// Enable DMA, nbytes is ignored when SDIO is flow controller
	t->dmarx.busystate = 1;
	DMAStart (&t->dmarx, (void*) &t->instance->FIFO, data, 0);

	// Start data transfer
	t->instance->DCTRL |= (9 << 4) | (1 << 3) | (1 << 1) | (1 << 0);

	// CMD18 read multiple or CMD17 read single
	int cmd = nbytes > 512 ? 18 : 17;
	t->instance->CMD = cmd | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_SHORT;
	int ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

	if (!(ret & SDIO_STA_CMDREND))
	{
		Print ("Unexpected response 0x%x\n", ret);
		dev_sdio_dumpresponse (t);
	}

	// Wait for completion
	return dev_sdio_waittransfer (t, nbytes, &t->dmarx, SDIO_STA_RXACT);
}

int dev_sdio_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	// Check for complete sector read
	if (nbytes % 512 || nbytes < 512)
		return -EINVAL;

	// Extract sdio pointer
	sdio *t = (sdio*) node->priv;

	// Prepare transfer
	dev_sdio_preparetransfer (t, nbytes);

	// Enable DMA, nbytes is ignored when SDIO is flow controller
	t->dmatx.busystate = 1;
	DMAStart (&t->dmatx, data, (void*) &t->instance->FIFO, 0);

	// CMD25 write multiple or CMD24 write single
	int cmd = nbytes > 512 ? 25 : 24;
	t->instance->CMD = cmd | SDIO_CMD_CPSMEN | SDIO_CMD_WAITRESP_SHORT;
	int ret = dev_sdio_waitflags (t, SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND | SDIO_STA_CTIMEOUT);

	if (!(ret & SDIO_STA_CMDREND))
	{
		Print ("Unexpected response 0x%x\n", ret);
		dev_sdio_dumpresponse (t);
	}

	// Start data transfer
	t->instance->DCTRL |= (9 << 4) | (1 << 3) | (1 << 0);

	// Wait for completion
	return dev_sdio_waittransfer (t, nbytes, &t->dmatx, SDIO_STA_TXACT);
}

int dev_sdio_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	sdio *t = (sdio*) node->priv;

	switch (code)
	{
	case SPIFSETSECT:
		t->address = (unsigned int)data;
		return 0;
	case SPIFGETSECT:
		return t->address;
	case SPIFGETSECTSIZE:
		*(unsigned short*)data = 512;
		return 0;
	case SPIFGETSECTCNT:
		{
			int c_size = ((t->csd[1] & 0x3F) << 16) | ((t->csd[2] >> 16) & 0x0000FFFF);
			*(unsigned int*)data = c_size;
		}
		return 0;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (sdio0,
	dev_sdio_open,
	dev_sdio_close,
	dev_sdio_read,
	dev_sdio_write,
	dev_sdio_ioctl,
	"sdio");
