/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "ioctl/ioctl.h"
#include "sched/scheduler.h"
#include "drivers/gpio/gpio-stm32f103.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define DEMCR		(*((volatile unsigned int *)0xE000EDFC))
#define DWT_CTRL	(*(volatile unsigned int *)0xe0001000)
#define CPU_CYCLES	*((volatile unsigned int *)0xE0001004)

#define ws2811Port1 GPIOB
#define ws2811Pin1  PIN(9)
#define ws2811Port2 GPIOB
#define ws2811Pin2  PIN(8)

///////////////////////////////////////////////////////////////////////////////
// Helper functions
///////////////////////////////////////////////////////////////////////////////
void Delay_Init ()
{
	// Enable DWT
	DEMCR |= 0x01000000;

	// Reset counter
	CPU_CYCLES = 0;

	// Enable CPU cycle counter
	DWT_CTRL |= 1;
}

void Delay_us (unsigned int us)
{
	CPU_CYCLES = 0;
	unsigned int cyc_per_us = 72;
	unsigned int time_to_wait = cyc_per_us * us;

	while (1)
	{
		if (CPU_CYCLES >= time_to_wait)
			return;
	}
}

void Delay_ns (unsigned int ns)
{
	CPU_CYCLES = 0;
	unsigned int cyc_per_us = 72000000 / 1000000;
	unsigned int time_to_wait = (cyc_per_us * ns) / 1000;

	while (1)
	{
		if (CPU_CYCLES >= time_to_wait)
			return;
	}
}

///////////////////////////////////////////////////////////////////////////////
// WS2811
///////////////////////////////////////////////////////////////////////////////
void WS2811OutAsm (unsigned char *bytes, int size, volatile unsigned int *BSRR, int pin);

void WS2811_Out (unsigned char *bytes, int size, GPIO* ws2811Port, int ws2811Pin)
{
	// Disable IRQ
	SchedEnterCritical ();

	// 800 kHz
	WS2811OutAsm (bytes, size, &ws2811Port->BSRR, ws2811Pin);

	// Enable IRQ and reset
	SchedExitCritical ();
	Delay_us (70);
}

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_ws2811_open (struct FileNode *node, const char *filename)
{
	Delay_Init ();
	GPIOInit (ws2811Port1, ws2811Pin1, GPIO_OUTPUT);
	GPIOInit (ws2811Port2, ws2811Pin2, GPIO_OUTPUT);
	return 0;
}

int dev_ws2811_close (struct FileNode *node)
{
	return 0;
}

int dev_ws2811_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	return 0;
}

int dev_ws2811_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	// Test loop
	//unsigned char bytes[3] = {0x55, 0x55, 0x55};
	//while (1) WS2811_Out ((unsigned char*) bytes, 3, ws2811Port2, ws2811Pin2);

	// Normal
	if (node->priv)
		WS2811_Out ((unsigned char*) data, nbytes, ws2811Port2, ws2811Pin2);
	else
		WS2811_Out ((unsigned char*) data, nbytes, ws2811Port1, ws2811Pin1);

	return nbytes;
}

int dev_ws2811_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	switch (code)
	{
	case 0:
		node->priv = data;
		break;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	// Default return value
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (ws2811,
	dev_ws2811_open,
	dev_ws2811_close,
	dev_ws2811_read,
	dev_ws2811_write,
	dev_ws2811_ioctl,
	"ws2811");
