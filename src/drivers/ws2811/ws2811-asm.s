/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

.syntax unified
.cpu cortex-m3
.fpu softvfp
.thumb

/*
 * r0 = data
 * r1 = nbytes
 * r2 = bsrr
 * r3 = pin
 * r4 = brr
 * r5 = inner and variable 0x80
 * r6 = data byte
 */

.section .text
.global WS2811OutLow
.type WS2811OutLow, %function
WS2811OutLow:
	// 0.25 us / 1.25 divs
	strh r3, [r2]
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	// 1 us / 5 divs (total 6.25 divs)
	strh r3, [r4]
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	bx lr
.size WS2811OutLow, .-WS2811OutLow

.section .text
.global WS2811OutHigh
.type WS2811OutHigh, %function
WS2811OutHigh:
	// 0.6 us / 3 divs
	strh r3, [r2]
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	// 0.65 / 3.25 divs
	strh r3, [r4]
	nop
	nop
	nop
	nop
	nop
	nop
	bx lr
.size WS2811OutHigh, .-WS2811OutHigh

.section .text
.global WS2811OutAsm
.type WS2811OutAsm, %function
WS2811OutAsm:
	// Stack regs
	push {r4, r5, r6, r7, lr}

	// Prepare BRR pointer
	add r4, r2, #4

	// Outer loop: for (; data != end; data++)
	add r1, r0, r1
outer:

	// Inner: for (j = 0x80, byte = data[0]; j; j = j >> 1)
	ldr r5, =0x80
	ldrb r6, [r0]
inner:
	and r7, r6, r5
	cmp r7, #0
	bne highbit
	// Low bit
		bl WS2811OutLow
		b donebit
	// High bit
	highbit:
		bl WS2811OutHigh
	donebit:

	// Inner control
	lsrs r5, r5, #1
	cmp r5, #0
	bne inner

	// Outer control: data++
	add r0, #1
	cmp r0, r1
	bne outer

	// Return
	pop {r4, r5, r6, r7, lr}
	bx lr
.size WS2811OutAsm, .-WS2811OutAsm
