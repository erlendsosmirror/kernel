/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "ioctl/ioctl.h"
#include "print/print.h"
#include "platform/platform.h"
#include "arch/stm32f407/rcc.h"

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
struct rtc_time {
	int tm_sec;
	int tm_min;
	int tm_hour;
	int tm_mday;
	int tm_mon;
	int tm_year;
	int tm_wday;	// Day of the week (0-6, Sunday = 0)
	int tm_yday;	// Day in the year (0-365, 1 Jan = 0)
	int tm_isdst;	// Daylight saving time
};

typedef struct _PWR
{
	volatile unsigned int CR;
	volatile unsigned int CSR;
} _PWR;

typedef struct _RTC
{
	volatile unsigned int TR;
	volatile unsigned int DR;
	volatile unsigned int CR;
	volatile unsigned int ISR;
	volatile unsigned int PRER;
	volatile unsigned int WUTR;
	volatile unsigned int CALIBR;
	volatile unsigned int ALRMAR;
	volatile unsigned int ALRMBR;
	volatile unsigned int WPR;
	volatile unsigned int SSR;
	volatile unsigned int SHIFTR;
	volatile unsigned int TSTR;
	volatile unsigned int TSSSR;
	volatile unsigned int CALR;
	volatile unsigned int TAFCR;
	volatile unsigned int ALRMASSR;
	volatile unsigned int ALRMBSSR;
	volatile unsigned int BKPNR[19];
} _RTC;

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define PWR ((_PWR*)0x40007000)
#define RTC ((_RTC*)0x40002800)

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
char timetext[10];
short timepos;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int RtcConfigureClock (void)
{
	// These operations are defined in section 5.1.2 in the datasheet
	// Enable PWR clock
	RCC->APB1ENR |= 1 << 28;

	// Enable write to backup domain
	PWR->CR |= 1 << 8;

	// Enable the oscillator
	RCC->BDCR |= 1;

	// Wait for oscillator
	unsigned int start = PlatformGetTick ();
	while (!(RCC->BDCR & 2))
		if ((PlatformGetTick () - start) > 2)
			return 1;

	// Only reset if the current clock is different from LSE
	if ((RCC->BDCR & (3 << 8)) != (1 << 8))
	{
		// Get all the other register bits
		unsigned int reg = RCC->BDCR & ~((3 << 8) | (1 << 16));

		// Toggle BDRST to reset backup domain
		RCC->BDCR = reg | (1 << 16);
		RCC->BDCR = reg;

		// Set the clock source and enable rtc
		RCC->BDCR = reg | (1 << 8) | (1 << 15);
	}

	// Disable write to backup domain
	PWR->CR &= ~(1 << 8);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_rtc_open (struct FileNode *node, const char *filename)
{
	// Configure the RTC
	if (RtcConfigureClock ())
		return -EIO;

	// Read the time so that we may cat the devnode
	unsigned int reg = RTC->TR;

	timetext[0] = '0' + ((reg >> 20) & 0x03);
	timetext[1] = '0' + ((reg >> 16) & 0x0F);
	timetext[2] = ':';
	timetext[3] = '0' + ((reg >> 12) & 0x07);
	timetext[4] = '0' + ((reg >> 8) & 0x0F);
	timetext[5] = ':';
	timetext[6] = '0' + ((reg >> 4) & 0x07);
	timetext[7] = '0' + ((reg >> 0) & 0x0F);
	timetext[8] = '\n';
	timetext[9] = 0;

	timepos = 0;

	// Done
	return 0;
}

int dev_rtc_close (struct FileNode *node)
{
	return 0;
}

int dev_rtc_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	// Supplying this makes it possible to cat the devnode.
	// The output is the string that was assembled when the file
	// was opened, so the node has to be re-opened in order to
	// get a new time value when using this. Use ioctl for proper
	// operation.
	char *d = (char*) data;
	int i = 0;

	for (; timepos < sizeof(timetext) && i < nbytes; timepos++, i++)
		d[i] = timetext[timepos];

	return i;
}

int dev_rtc_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	return nbytes;
}

int dev_rtc_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	switch (code)
	{
	case RTCGETTIME:
		{
			struct rtc_time *t = (struct rtc_time*) data;
			unsigned int tr = RTC->TR;
			unsigned int dr = RTC->DR;

			t->tm_sec = (tr & 0x0F) + ((tr >> 4) & 0x07) * 10;
			t->tm_min = ((tr >> 8) & 0x0F) + ((tr >> 12) & 0x07) * 10;
			t->tm_hour = ((tr >> 16) & 0x0F) + ((tr >> 20) & 0x03) * 10;

			t->tm_mday = ((dr >> 0) & 0x0F) + ((dr >> 4) & 0x03) * 10;
			t->tm_mon = ((dr >> 8) & 0x0F) + ((dr >> 12) & 0x01) * 10;
			t->tm_wday = ((dr >> 13) & 0x07);
			t->tm_year = ((dr >> 16) & 0x0F) + ((dr >> 20) & 0x0F) * 10;

			t->tm_mon--;
			t->tm_wday--;
		}
		return 0;
	case RTCSETTIME:
		{
			struct rtc_time *t = (struct rtc_time*) data;
			unsigned int tr = 0;
			unsigned int dr = 0;

			unsigned int tm_mon = t->tm_mon + 1;
			unsigned int tm_wday = t->tm_wday + 1;

			tr |= t->tm_sec % 10;
			tr |= (t->tm_sec / 10) << 4;

			tr |= (t->tm_min % 10) << 8;
			tr |= (t->tm_min / 10) << 12;

			tr |= (t->tm_hour % 10) << 16;
			tr |= (t->tm_hour / 10) << 20;

			dr |= t->tm_mday % 10;
			dr |= (t->tm_mday / 10) << 4;

			dr |= (tm_mon % 10) << 8;
			dr |= (tm_mon / 10) << 12;

			dr |= tm_wday << 13;

			dr |= (t->tm_year % 10) << 16;
			dr |= (t->tm_year / 10) << 20;

			// Debug
			//Print ("TR %x\n", tr);
			//Print ("DR %x\n", dr);

			// Enable write by setting DBP
			PWR->CR |= 1 << 8;

			// Write key
			RTC->WPR = 0xCA;
			RTC->WPR = 0x53;

			// Set INIT bit in the ISR register
			RTC->ISR |= 1 << 7;

			// Wait for INITS
			unsigned int wait = 0x00FFFFFF;
			while (wait && !(RTC->ISR & (1 << 6))) wait--;

			if (!wait)
				Print ("RTC Timeout\n");

			// Load time, date and time format
			RTC->TR = tr;
			RTC->DR = dr;
			RTC->CR &= ~(1 << 6);

			// Exit init mode
			RTC->ISR &= ~(1 << 7);
			RTC->WPR = 0;
			PWR->CR &= ~(1 << 8);
		}
		return 0;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (rtc,
	dev_rtc_open,
	dev_rtc_close,
	dev_rtc_read,
	dev_rtc_write,
	dev_rtc_ioctl,
	"rtc");
