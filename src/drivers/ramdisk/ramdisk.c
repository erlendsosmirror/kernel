/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "mem/string.h"
#include "print/print.h"
#include "ioctl/ioctl.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
unsigned int ramdisk_data[1024*1/4];
unsigned int ramdisk_open = 0;
unsigned int ramdisk_pos;

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_ramdisk_open (struct FileNode *node, const char *filename)
{
	if (ramdisk_open)
		return -EBUSY;

	MemSet (ramdisk_data, 0, sizeof (ramdisk_data));
	ramdisk_pos = 0;
	return 0;
}

int dev_ramdisk_close (struct FileNode *node)
{
	ramdisk_open = 0;
	return 0;
}

int dev_ramdisk_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	unsigned char *p = (unsigned char*) ramdisk_data;

	if (ramdisk_pos > sizeof(ramdisk_data) || (ramdisk_pos + nbytes) > sizeof(ramdisk_data))
	{
		Print ("Indexing error on read\n");
		return 0;
	}

	MemCpy (data, p + ramdisk_pos, nbytes);
	return nbytes;
}

int dev_ramdisk_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	unsigned char *p = (unsigned char*) ramdisk_data;

	if (ramdisk_pos > sizeof(ramdisk_data) || (ramdisk_pos + nbytes) > sizeof(ramdisk_data))
	{
		Print ("Indexing error on write\n");
		return nbytes;
	}

	MemCpy (p + ramdisk_pos, data, nbytes);
	return nbytes;
}

int dev_ramdisk_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	switch (code)
	{
	case 0:
		return 0;
	case 1: // GET_SECTOR_COUNT 1
		*(unsigned int*)data = sizeof (ramdisk_data) / 512;
		return 0;
	case 2: // GET_SECTOR_SIZE 2
		*(unsigned short*)data = 512;
		return 0;
	case 3: // GET_BLOCK_SIZE 3
		*(unsigned int*)data = 1;
		return 0;
	case SPIFSETSECT:
		ramdisk_pos = (unsigned int) data * 512;
		return 0;
	default:
		// Unknown ioctl: Return "Inappropriate ioctl for device"
		return -ENOTTY;
	}

	// Default return value
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (ramdisk,
	dev_ramdisk_open,
	dev_ramdisk_close,
	dev_ramdisk_read,
	dev_ramdisk_write,
	dev_ramdisk_ioctl,
	"ramdisk");
