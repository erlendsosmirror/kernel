/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "posix/errno.h"
#include "ioctl/ioctl.h"
#include "mem/mem.h"
#include "mem/string.h"
#include "panic/panic.h"
#include "print/print.h"
#include "drivers/lcd/lcd.h"
#include "drivers/fbcon/fbcon.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
const unsigned short fbcon_colors[] = {TFT_BLACK, TFT_RED, TFT_GREEN, TFT_YELLOW,
	TFT_BLUE, TFT_MAGENTA, TFT_CYAN, TFT_WHITE};

FBConData fbcon;
char fbconInitialized;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
#if FBCON_OPTION_CURSOR
void fbcon_setblink (int marked)
{
	// Do not redraw if there is no change
	if (marked == fbcon.cursormarked)
		return;

	// Position and character to alter
	int x = fbcon.posx / fbcon.charwidth;
	int y = fbcon.posy / fbcon.charheight;
	char c = fbcon.data[y * fbcon.width_chars + x];

	// Print with correct colors
	if (marked)
		LCDChar (c, fbcon.posx, fbcon.posy, FBCON_CHARSIZE, fbcon.bcolor, fbcon.fcolor);
	else
		LCDChar (c, fbcon.posx, fbcon.posy, FBCON_CHARSIZE, fbcon.fcolor, fbcon.bcolor);

	// Store marked state
	fbcon.cursormarked = marked;
}

void fbcon_timercallback (TimerHandle *h)
{
	fbcon_setblink (!fbcon.cursormarked);
}
#endif

void fbcon_processcolor (void)
{
	char *str = fbcon.colarg;
	int state = 0;

	while (*str)
	{
		int arg = *str - '0';

		switch (state)
		{
		case 0:
			if (*str == '0')
			{
				fbcon.fcolor = 0xFFFF;
				fbcon.bcolor = 0;
			}
			else if (*str == '3')
				state = 1;
			else if (*str == '4')
				state = 2;
			break;
		case 1:
			if (arg >= 0 && arg <= 7)
				fbcon.fcolor = fbcon_colors[arg];

			state = 0;
			break;
		case 2:
			if (arg >= 0 && arg <= 7)
				fbcon.bcolor = fbcon_colors[arg];

			state = 0;
			break;
		}

		str++;
	}
}

void fbcon_processcursor (void)
{
	// Extract line and column argument
	char *tok = strtok (fbcon.colarg, ";");
	int line = atoi (tok);
	tok = strtok (0, ";");
	int column = atoi (tok);

	// Decrement and multiply for screen position
	line--;
	line *= fbcon.charheight;
	column--;
	column *= fbcon.charwidth;

	// Clamp bounds
	if (line < 0)
		line = 0;

	if (line > fbcon.height - fbcon.charheight)
		line = fbcon.height - fbcon.charheight;

	if (column < 0)
		column = 0;

	if (column > fbcon.width - fbcon.charwidth)
		column = fbcon.width - fbcon.charwidth;

	// Clear cursor
#if FBCON_OPTION_CURSOR
	fbcon_setblink (0);
#endif

	// Set position
	fbcon.posx = column;
	fbcon.posy = line;
}

void fbcon_processescape (char c)
{
	if (fbcon.inescape == 27)
		fbcon.inescape = c;
	else if (fbcon.inescape == 91) // 0x5b / '['
	{
		if (c == 65) // 'A' Uparrow
		{
			if (fbcon.posy >= fbcon.charheight)
				fbcon.posy -= fbcon.charheight;
		}
		else if (c == 68) // 'D' Leftarrow
		{
			if (fbcon.posx >= fbcon.charwidth)
				fbcon.posx -= fbcon.charwidth;
		}
		else if (c == 67) // 'C' Rightarrow
		{
			if (fbcon.posx < fbcon.width - fbcon.charwidth)
				fbcon.posx += fbcon.charwidth;
		}
		else if (c == 66) // 'B' Downarrow
		{
			if (fbcon.posy < (fbcon.height - fbcon.charheight))
				fbcon.posy += fbcon.charheight;
		}
		else if (c == 'H') // Cursor position without arguments
		{
			fbcon.posx = 0;
			fbcon.posy = 0;
			fbcon.inescape = 0;
		}
		else // Default if not recognized after '[', assume arguments
		{
			fbcon.inescape = 1;
			fbcon.colarg[0] = c;
			return;
		}

		fbcon.inescape = 0;
#if FBCON_OPTION_CURSOR
		fbcon_setblink (1);
#endif
	}
	else if (fbcon.inescape > 6) // Prevent errors
		fbcon.inescape = 0;
	else if (c == 'J') // Clear screen, assumes n=2 (clear entire)
	{
		LCDFill (0);
#if FBCON_OPTION_CURSOR
		MemSet (fbcon.data, ' ', fbcon.width_chars * fbcon.height_chars);
#endif
		fbcon.inescape = 0;
	}
	else if (c == 'm') // Select Graphic Rendition
	{
		fbcon.colarg[fbcon.inescape] = 0;
		fbcon.inescape = 0;
		fbcon_processcolor ();
	}
	else if (c == 'H') // Cursor position with arguments
	{
		fbcon.colarg[fbcon.inescape] = 0;
		fbcon.inescape = 0;
		fbcon_processcursor ();
	}
	else // Unknown, store it until we stored too many or found end
	{
		fbcon.colarg[fbcon.inescape] = c;
		fbcon.inescape++;
	}
}

void fbcon_write (char *buf, int nbytes)
{
	for (char *end = buf + nbytes; *buf && buf != end; buf++)
	{
		if (fbcon.inescape)
			fbcon_processescape (*buf);
		else
		{
#if FBCON_OPTION_CURSOR
			fbcon_setblink (0);
#endif

			switch (*buf)
			{
			case '\r':
				fbcon.posx = 0;
				break;
			case '\n':
				fbcon.posx = 0;

				if (fbcon.posy < (fbcon.height - fbcon.charheight))
					fbcon.posy += fbcon.charheight;
#if FBCON_OPTION_WRAPAROUND
				else
					fbcon.posy = 0;
#elif FBCON_OPTION_SCROLL
				else
				{
					MemCpy (fbcon.data, fbcon.data + fbcon.width_chars, fbcon.width_chars * (fbcon.height_chars-1));
					MemSet (fbcon.data + (fbcon.width_chars*(fbcon.height_chars-1)), ' ', fbcon.width_chars);

					for (int y = 0; y < fbcon.height_chars; y++)
					{
						int yy = y * fbcon.charheight;
						int yi = y * fbcon.width_chars;

						for (int x = 0; x < fbcon.width_chars; x++)
							LCDChar (fbcon.data[yi+x], x * fbcon.charwidth, yy, FBCON_CHARSIZE, TFT_WHITE, TFT_BLACK);
					}
				}
#endif
				break;
			case 8:
				if (fbcon.posx >= fbcon.charwidth)
					fbcon.posx -= fbcon.charwidth;
				break;
			case 27:
				fbcon.inescape = 27;
				break;
			default:
				if (*buf >= 32)
				{
					LCDChar (*buf, fbcon.posx, fbcon.posy, FBCON_CHARSIZE, fbcon.fcolor, fbcon.bcolor);
#if FBCON_OPTION_CURSOR
					fbcon.data[(fbcon.posy / fbcon.charheight) * fbcon.width_chars + (fbcon.posx / fbcon.charwidth)] = *buf;
#endif
				}

				if (fbcon.posx < (fbcon.width - fbcon.charwidth))
					fbcon.posx += fbcon.charwidth;

#if FBCON_OPTION_CURSOR
				fbcon_setblink (1);
#endif
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Driver functions
///////////////////////////////////////////////////////////////////////////////
int dev_fbcon_open (struct FileNode *node, const char *filename)
{
	// Set a reference to the chain device
	static struct FileNode fnode;
	node->priv = &fnode;

	// Prevent re-init
	if (fbconInitialized)
		return 0;

	fbconInitialized = 1;

	// Make sure the display is initialized and cleared
	LCDInit ();
	LCDFill (0);

	// Erase data structure
	MemSet (&fbcon, 0, sizeof (FBConData));

	// Get size of screen and calculate related values
	fbcon.charwidth = 6;
	fbcon.charheight = 8;
	unsigned int w, h;
	LCDGetMetrics (&w, &h);
	fbcon.width = w;
	fbcon.height = h;
	fbcon.width_chars = fbcon.width / fbcon.charwidth;
	fbcon.height_chars = fbcon.height / fbcon.charheight;

	// Set foreground color
	fbcon.fcolor = 0xFFFF;

#if FBCON_OPTION_CURSOR
	// Attempt to allocate memory
	fbcon.data = (char*) MemAlloc (fbcon.width_chars * fbcon.height_chars, 0);

	// Check allocation
	if (!fbcon.data)
		Panic ("No fbcon data");

	// Erase with spaces
	MemSet (fbcon.data, ' ', fbcon.width_chars * fbcon.height_chars);

	// Start cursor timer
	fbcon.cursortimer.Callback = &fbcon_timercallback;
	fbcon.cursortimer.interval = 600;
	fbcon.cursortimer.flags = TIMER_FLAG_REPEATING;
	TimerRegister (&fbcon.cursortimer);
#endif

	// Open some default chain device
	int ret = vfs_open (&fnode, "/", "/dev/tty0", 0, 0);

	if (ret)
	{
		// If it failed, open /dev/null as a fallback, so that we can skip null checks
		Print ("Failed to open default device\n");

		ret = vfs_open (&fnode, "/", "/dev/null", 0, 0);

		if (ret)
			Panic ("No fallback");
	}

	return 0;
}

int dev_fbcon_close (struct FileNode *node)
{
	return 0;
}

int dev_fbcon_read (struct FileNode *node, void *data, unsigned int nbytes)
{
	struct FileNode *f = (struct FileNode*) node->priv;
	return f->dev->read (f, data, nbytes);
}

int dev_fbcon_write (struct FileNode *node, void *data, unsigned int nbytes)
{
	struct FileNode *f = (struct FileNode*) node->priv;
	int n = f->dev->write (f, data, nbytes);

	if (!fbcon.donotprint)
		fbcon_write ((char*)data, n);

	return n;
}

int dev_fbcon_ioctl (struct FileNode *node, unsigned int code, void *data)
{
	switch (code)
	{
	case TTYSETCBLINK:
		if (data && !TimerIsRegistered (&fbcon.cursortimer))
			TimerRegister (&fbcon.cursortimer);
		else if (!data && TimerIsRegistered (&fbcon.cursortimer))
			TimerUnRegister (&fbcon.cursortimer);

		return 0;
	case TTYSETMODE:
		if (data)
			// Direct mode
			fbcon.donotprint = 1;
		else
			// Normal mode
			fbcon.donotprint = 0;

		break;
	default:
		// Unknown ioctls are indicated by the chain device
		break;
	}

	struct FileNode *f = (struct FileNode*) node->priv;
	return f->dev->ioctl (f, code, data);
}

///////////////////////////////////////////////////////////////////////////////
// Driver declarations
///////////////////////////////////////////////////////////////////////////////
MODULE_DESCRIPTOR (fbcon,
	dev_fbcon_open,
	dev_fbcon_close,
	dev_fbcon_read,
	dev_fbcon_write,
	dev_fbcon_ioctl,
	"fbcon");
