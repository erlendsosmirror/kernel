/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

.syntax unified
.cpu cortex-m4
.fpu fpv4-sp-d16
.thumb

.section .text.SchedContext
.global SchedContext
.type SchedContext, %function
SchedContext:					@ r0-r3, r12, lr, pc, xPSR already stacked
	mrs r0, psp					@ Fetch the thread stack pointer

	tst r14, #0x10				@ Stack fpu regs if used
	it eq
	vstmdbeq r0!, {s16-s31}		@ If lazy stacking, s0-s15 and fpscr are stacked now

	stmdb r0!, {r4-r11, r14}	@ Stack core regs, and save the stack
	ldr	r3, =currentthc
	ldr	r2, [r3]
	str r0, [r2]

.global SchedContextNext
SchedContextNext:
	bl SchedSelectNext			@ Call scheduler selection function

	ldr	r3, =currentthc			@ r0 becomes the stacktop member of currentthc
	ldr r1, [r3]
	ldr r0, [r1]

	ldr r2, [r1, #4]			@ Use correct privilege level
	msr control, r2

	ldr r3, =0xe000ed9c			@ Get address of the MPU_RBAR register
	add r1, r1, #8				@ Point r1 to our data in ThreadControl
	tst r2, #0x01				@ If unprivileged, set mpu regs
	itt ne
	ldmne r1, {r4-r11}			@ Copy using the alias registers
	stmne r3, {r4-r11}

	ldmia r0!, {r4-r11, r14}	@ Unstack core regs

	tst r14, #0x10				@ Unstack fpu regs if used
	it eq
	vldmiaeq r0!, {s16-s31}

	msr psp, r0					@ Set stack pointer and return
	bx r14						@ Unstack r0-r3, r12, lr, pc, xPSR
.size SchedContext, .-SchedContext
