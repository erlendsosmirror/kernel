/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "arch/stm32f407/rcc.h"
#include "mem/mem.h"
#include "print/print.h"
#include "drivers/gpio/gpio-stm32f407.h"
#include "../config/live/platform.h"

///////////////////////////////////////////////////////////////////////////////
// Register sets
///////////////////////////////////////////////////////////////////////////////
typedef struct _FMCSD
{
	volatile unsigned int SDCR1;
	volatile unsigned int SDCR2;
	volatile unsigned int SDTR1;
	volatile unsigned int SDTR2;
	volatile unsigned int SDCMR;
	volatile unsigned int SDRTR;
	volatile unsigned int SDSR;
} _FMCSD;

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define FMCSD ((_FMCSD*) 0xA0000140)

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void PlatformInitSDRAM (void)
{
	// Enable clocks: IO CDEFGH
	RCC->AHB1ENR |= 0xFC;
	RCC->AHB3ENR |= 1;

	// Pins
	GPIOInit (GPIOC, PLATFORM_FSMC_PINS_PORTC,
		GPIO_AF | GPIO_SPEED_VERYHIGH | GPIO_ALTERNATE(12));

	GPIOInit (GPIOD, PLATFORM_FSMC_PINS_PORTD,
		GPIO_AF | GPIO_SPEED_VERYHIGH | GPIO_ALTERNATE(12));

	GPIOInit (GPIOE, PLATFORM_FSMC_PINS_PORTE,
		GPIO_AF | GPIO_SPEED_VERYHIGH | GPIO_ALTERNATE(12));

	GPIOInit (GPIOF, PLATFORM_FSMC_PINS_PORTF,
		GPIO_AF | GPIO_SPEED_VERYHIGH | GPIO_ALTERNATE(12));

	GPIOInit (GPIOG, PLATFORM_FSMC_PINS_PORTG,
		GPIO_AF | GPIO_SPEED_VERYHIGH | GPIO_ALTERNATE(12));

	GPIOInit (GPIOH, PLATFORM_FSMC_PINS_PORTH,
		GPIO_AF | GPIO_SPEED_VERYHIGH | GPIO_ALTERNATE(12));

	// Program memory device features
	FMCSD->SDCR1 =
		(0 << 13) |	// RPIPE 2
		(1 << 12) |	// RBURST 1
		(2 << 10) |	// SDCLK HCLK/2 (168 MHz / 2 = 84 MHz)
		(0 << 9) |	// WP 0
		(2 << 7) |	// CAS 3
		(1 << 6) |	// NB 1 (4 banks)
		(1 << 4) |	// MWID 1 (16-bit)
		(1 << 2) |	// NR 1 (12-bit)
		(0 << 0);	// NC 0 (8-bit)
	FMCSD->SDCR2 = FMCSD->SDCR1;

	// Program memory device timing: TRP and TRC in SDTR1
	FMCSD->SDTR1 =
		(2 << 24) |		// Row to column delay, t_rcd (activate to read/write) (3)
		(2 << 20) |		// Row precharge delay, t_rp (3)
		(3 << 16) |		// Recovery delay (4)
		(10 << 12) |	// Row cycle delay (11)
		(15 << 8) |		// Self refresh time (16)
		(11 << 4) |		// Exit self-refresh delay (12)
		(1 << 0);		// Load mode register to active (2)
	FMCSD->SDTR2 = FMCSD->SDTR1;

	// Set mode bits to 001 and configure target bank bits in SDCMR, starts clock
	FMCSD->SDCMR = (0 << 9) | (0 << 5) | (0 << 4) | (1 << 3) | 1;

	// Wait for power up delay (should check datasheet)
	volatile unsigned int delay = 1680000;
	while (delay) delay--;

	// Set mode bits to 010 (precharge all) command
	FMCSD->SDCMR = (0 << 9) | (0 << 5) | (0 << 4) | (1 << 3) | 2;

	// Set mode bits to 011 and number of consecutive auto-refresh commands.
	FMCSD->SDCMR = (0 << 9) | (8 << 5) | (0 << 4) | (1 << 3) | 3;

	// Load mode register command
	unsigned int mode = (2 << 4);
	FMCSD->SDCMR = (mode << 9) | (8 << 5) | (0 << 4) | (1 << 3) | 4;

	// Program refresh rate
	FMCSD->SDRTR = 448 << 1;
}
