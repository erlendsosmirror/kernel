/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

.syntax unified
.cpu cortex-m4
.fpu softvfp
.thumb

.section .text.SchedStartAsm
.global SchedStartAsm
.type SchedStartAsm, %function
SchedStartAsm:
	cpsie i
	cpsie f
	svc 63
	nop
.size SchedStartAsm, .-SchedStartAsm

.section .text.SchedStartSvc
.global SchedStartSvc
.type SchedStartSvc, %function
SchedStartSvc:
								@ TODO: Start systick irq here
	ldr r0, =kernelstack		@ Reset kernel stack
	msr msp, r0
	ldr	r3, =currentthc			@ Get current thread
	ldr r1, [r3]
	ldr r0, [r1]				@ Get stacktop and unstack regs
	ldmia r0!, {r4-r11, r14}
	msr psp, r0					@ Set thread stack.
	mov r0, #0					@ Use privileged mode for the first process started.
	msr	basepri, r0				@ It will always be a kernel thead so no need to bother
	msr control, r0				@ with the MPU yet.
	bx r14						@ The CPU unstacks the rest of the regs here.
.size SchedStartSvc, .-SchedStartSvc

.section .text.SchedEnterCritical
.global SchedEnterCritical
.type SchedEnterCritical, %function
SchedEnterCritical:
	cpsid i
	bx lr
.size SchedEnterCritical, .-SchedEnterCritical

.section .text.SchedExitCritical
.global SchedExitCritical
.type SchedExitCritical, %function
SchedExitCritical:
	cpsie i
	bx lr
.size SchedExitCritical, .-SchedExitCritical
