/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

.syntax unified
.cpu cortex-m4
.fpu softvfp
.thumb

.section .text.KernelResetEntry
.global KernelResetEntry
.type KernelResetEntry, %function
KernelResetEntry:
	ldr r0, =kernelstack
	msr msp, r0

	ldr r0, =_ramstart
	ldr r1, =_ramend
	mov r2, #0

KernelResetEntryZero:
	str r2, [r0], #4
	cmp r0, r1
	bcc KernelResetEntryZero

	ldr r0, =kernelstackstart
	ldr r1, =kernelstack
	ldr r2, =0xDEADBEEF

KernelResetEntryStack:
	str r2, [r0], #4
	cmp r0, r1
	bcc KernelResetEntryStack

	ldr r0, =_dataloadfrom
	ldr r1, =_datastart
	ldr r2, =_dataend

KernelResetEntryCopy:
	ldr r3, [r0], #4
	str r3, [r1], #4
	cmp r1, r2
	bcc KernelResetEntryCopy

	bl KernelInit

KernelResetEntryError:
	b KernelResetEntryError
	nop
.size KernelResetEntry, .-KernelResetEntry
