/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Include
///////////////////////////////////////////////////////////////////////////////
#include "sched/threadcontrol.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void InitializeStack (struct ThreadControl *t)
{
	unsigned int *top = t->stacktop;

	top--; *top = 0x01000000; // xPSR: Set the thumb bit in the xPSR
	top--; *top = (unsigned int) t->entrypoint; // PC: Set to entry point
	top--; *top = 0x00000000; // LR: Return address
	top--; *top = 0; // R12
	top--; *top = 0; // R3
	top--; *top = t->param3; // R2: Normally environ
	top--; *top = t->param2; // R1: Normally argv
	top--; *top = t->param1; // R0: Normally argc
	top--; *top = 0xFFFFFFFD; // Return value used: To thread mode, no float, state and exec from PSP
	top--; *top = 0; // R11
	top--; *top = 0; // R10
	top--; *top = 0; // R9
	top--; *top = 0; // R8
	top--; *top = 0; // R7
	top--; *top = 0; // R6
	top--; *top = 0; // R5
	top--; *top = 0; // R4

	t->stacktop = top;
}

void SchedSignalStackSetup (struct ThreadControl *thc)
{
	unsigned int *top = thc->stacktop;

	thc->stackstored = top;

	top--; *top = 0x01000000; // xPSR: Set the thumb bit in the xPSR
	top--; *top = (unsigned int) thc->sigh; // PC: Set to entry point
	top--; *top = (unsigned int) thc->sigt; // LR: Return address
	top--; *top = 0; // R12
	top--; *top = 0; // R3
	top--; *top = 0; // R2
	top--; *top = 0; // R1
	top--; *top = thc->param1; // R0
	top--; *top = 0xFFFFFFFD; // Return value used: To thread mode, no float, state and exec from PSP
	top--; *top = 0; // R11
	top--; *top = 0; // R10
	top--; *top = 0; // R9
	top--; *top = 0; // R8
	top--; *top = 0; // R7
	top--; *top = 0; // R6
	top--; *top = 0; // R5
	top--; *top = 0; // R4

	thc->stacktop = top;
	thc->sigstate = 0;

	// Clear the signal pending bit
	thc->sigpending &= ~(1 << (thc->param1 - 1));
}

void SchedYield (void)
{
#define NVIC_INTCTRL (*((volatile unsigned int*) 0xE000ED04))
	NVIC_INTCTRL = 1 << 28;
	__asm volatile("dsb");
	__asm volatile("isb");
}
