/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * This is where the CPU starts executing when it encounters an SVC
 * instruction. It essentially just runs the correct function in the dispatch
 * table.
 */

.section .text
.syntax unified
.cpu cortex-m4
.thumb

.weak FaultHandler
.type FaultHandler, %function
FaultHandler:
	tst lr, #4				@ Test bit 2 of lr, true when we came from PSP
	bne loadfrompsp
	mrs r0, msp				@ *stack = MSP
	mov r1, #0				@ stacktype = 0
	b PanicInternal
loadfrompsp:
	mrs r0, psp				@ *stack = PSP
	mov r1, #1				@ stacktype  = PANIC_STACKTYPE_USER
	stmdb sp!, {lr}			@ Stack return register
	bl SchedCrashRecover	@ Attempt crash recovery
	ldmia sp!, {lr}			@ Unstack return reg
	b SchedContextNext		@ Jump into scheduler and restore a different process
.size FaultHandler, .-FaultHandler

.weak HardFaultHandler
.type HardFaultHandler, %function
HardFaultHandler:
	mov r2, #1
	b FaultHandler
.size HardFaultHandler, .-HardFaultHandler

.weak MemManageHandler
.type MemManageHandler, %function
MemManageHandler:
	mov r2, #2
	b FaultHandler
.size MemManageHandler, .-MemManageHandler

.weak BusFaultHandler
.type BusFaultHandler, %function
BusFaultHandler:
	mov r2, #3
	b FaultHandler
.size BusFaultHandler, .-BusFaultHandler

.weak UsageFaultHandler
.type UsageFaultHandler, %function
UsageFaultHandler:
	mov r2, #4
	b FaultHandler
.size UsageFaultHandler, .-UsageFaultHandler
