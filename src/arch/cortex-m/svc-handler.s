/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * This is where the CPU starts executing when it encounters an SVC
 * instruction. It essentially just runs the correct function in the dispatch
 * table.
 */

.section .text
.syntax unified
.cpu cortex-m4
.thumb

.weak SVCHandler
.type SVCHandler, %function
SVCHandler:
	tst lr, #4					@ Load the correct stack pointer to r1
	ite eq
	mrseq r1, msp
	mrsne r1, psp

	ldr r2, [r1, #24]			@ Add 6 words (6*4 = 24) to point to the stacked pc
	ldrb r2, [r2, #-2]			@ Get the stacked pc
								@ Point to the svc immediate, endianness makes it 2
								@ Load the svc argument

	and r2, r2, #0x3F			@ Make sure to keep index within jump table bounds
	ldr r3, =svcJumpTable		@ Load address of jump table
	ldr r3, [r3, r2, lsl #2]	@ Load entry by shifting argument two times

	ldr r0, =currentthc			@ Load current thread
	ldr r0, [r0]

	push {r1, lr}
	blx r3						@ Branch and link to entry
	pop {r1, lr}
	str r0, [r1]
	bx lr
.size SVCHandler, .-SVCHandler
