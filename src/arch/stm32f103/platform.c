/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sched/scheduler.h"
#include "arch/stm32f103/rcc.h"
#include "mem/mem.h"
#include "print/print.h"
#include "../config/live/platform.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define AIRCR (*((volatile unsigned int*)0xE000ED0C))

#define SHP ((volatile unsigned char*)0xE000ED18)
#define NVIC_IPR ((volatile unsigned char*)0xE000E400)
#define NVIC_ISER ((volatile unsigned int*)0xE000E100)

///////////////////////////////////////////////////////////////////////////////
// External variables
///////////////////////////////////////////////////////////////////////////////
extern unsigned int _dataend;
extern unsigned int _ramend;

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
volatile unsigned int systemTick;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void PlatformSetInterruptPriority (int irqnum, int priority)
{
	if (irqnum < 0)
	{
		// Cortex-M interrupt
		// Note that SHP is byte addressable, and we have 4 priority bits
		SHP[(irqnum & 0x0F) - 4] = (unsigned char) priority << 4;
	}
	else
	{
		// STM32 interrupt
		// Note that IPR is byte addressable, and we have 4 priority bits
		NVIC_IPR[irqnum] = (unsigned char) priority << 4;
	}
}

void PlatformEnableInterrupt (int irqnum)
{
	NVIC_ISER[irqnum >> 5] = 1 << (irqnum & 0x1F);
}

unsigned int PlatformGetSystemFrequency (void)
{
	switch (RCC->CFGR & 0x3)
	{
	case 0: return 16000000; // HSI
	case 1: return HSE_CLK_FREQ; // HSE
	default:
		{
			unsigned int pllmul = (RCC->CFGR >> 18) & 0x0F;
			unsigned int plldiv = (RCC->CFGR >> 17) & 1;

			pllmul += 2;
			plldiv += 1;
			unsigned int pllsrc = (!(RCC->CFGR & (1 << 16))) ? 16000000 : HSE_CLK_FREQ;

			return (pllsrc / plldiv) * pllmul;
		}
	}
}

const unsigned char scaletable[16] =
	{0, 0, 0, 0, 1, 2, 3, 4, // APB1 and APB2
	1, 2, 3, 4, 6, 7, 8, 9}; // AHB

unsigned int PlatformPCLK1 (void)
{
	unsigned int freq = PlatformGetSystemFrequency ();
	unsigned int coreclock = freq >> scaletable[(RCC->CFGR >> 4) & 0x0F];
	return coreclock >> scaletable[(RCC->CFGR >> 8) & 0x07];
}

unsigned int PlatformPCLK2 (void)
{
	unsigned int freq = PlatformGetSystemFrequency ();
	unsigned int coreclock = freq >> scaletable[(RCC->CFGR >> 4) & 0x0F];
	return coreclock >> scaletable[(RCC->CFGR >> 11) & 0x07];
}

void PlatformInitTick (void)
{
	// Get the current clock
	unsigned int clk = PlatformGetSystemFrequency ();

	// Calculate core clock
	static const unsigned char table[16] =
		{0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 6, 7, 8, 9};

	unsigned int cclk = clk >> table[(RCC->CFGR >> 4) & 0x0F];

	// Initialize tick
	SysTick->RVR = cclk / 1000 - 1;
	SysTick->CVR = 0;
	SysTick->CSR = 7;
}

int PlatformClockConfig (int latency, int sysclksrc, unsigned int clocks)
{
	// Check that the desired clock source is on (HSI, HSE, PLL)
	static const unsigned int bits[3] = {0x00000002, 0x00020000, 0x02000000};

	if (!(RCC->CR & bits[sysclksrc]))
		return 1;

	// If the new flash latency is greater, set it now
	if (latency > (FLASH->ACR & 0x07))
	{
		FLASH->ACR |= latency;

		if ((FLASH->ACR & 0x07) != latency)
			return 1;
	}

	// Now set the clocks
	RCC->CFGR = (RCC->CFGR & ~0x0000FFFF) | clocks | sysclksrc;

	// Wait for the correct clock to be applied
	unsigned int checkfor = (unsigned int) sysclksrc << 2;
	unsigned int tickstart = systemTick;

	while ((RCC->CFGR & 0x0C) != checkfor)
		if ((systemTick - tickstart) > 5000)
			return 2;

	// If the flash latency is to be decreased
	if (latency < (FLASH->ACR & 0x07))
	{
		FLASH->ACR |= latency;

		if ((FLASH->ACR & 0x07) != latency)
			return 1;
	}

	return 0;
}

void PlatformIncrementTick (void)
{
	systemTick++;
	SchedSystickHandler ();
}

void PlatformInitialize (void)
{
	// Reset main clock (RCC) to HSI source
	RCC->CR = 0x00000083;
	RCC->CFGR = 0x00000080;
	RCC->CIR = 0;

	// Init internal flash interface: caches and prefetch enabled
	FLASH->ACR |= 0x00000032;

	// Configure 4 bits of pre-emption priority
	AIRCR = 0x05FA0300;

	// Set priorities of faults and systick
	PlatformSetInterruptPriority (-1, 13);	// SysTick
	PlatformSetInterruptPriority (-2, 15);	// PendSV
	PlatformSetInterruptPriority (-5, 14);	// SVCall
	PlatformSetInterruptPriority (-10, 0);	// UsageFault
	PlatformSetInterruptPriority (-11, 0);	// BusFault
	PlatformSetInterruptPriority (-12, 0);	// MemManage

	// Configure HSE
	RCC->CR |= 0x00010000;
	while (!(RCC->CR & 0x00020000));

	// Configure PLL: M 9, no div, HSE source
	RCC->CR &= ~0x01000000;
	while (RCC->CR & 0x02000000);
	RCC->CFGR |= (7 << 18) | (0 << 17) | (1 << 16);
	RCC->CR |= 0x01000000;
	while (!(RCC->CR & 0x02000000));

	// Reset system tick value
	systemTick = 0;

	// Configure Clock (flash latency, system clock source, APB1 | APB2)
	PlatformClockConfig (2, 2, 4 << 8 | 0 << 11 | 0 << 4);

	// Enable DMA clocks
	RCC->AHBENR |= 3;
}

void PlatformInitMemory (void)
{
	// Add internal memory
#if PLATFORM_USE_INTERNAL_MEMORY
	static struct MemBank bank;
	unsigned int bank0start = (unsigned int) &_dataend;
	unsigned int bank0end = (unsigned int) &_ramend;

	bank0start = (bank0start + 7) & ~3;
	unsigned int bank0size = bank0end - bank0start;

	MemBankInit (&bank, bank0start, bank0size);
	MemAddBank (&bank);

	Print ("Added %ib addr 0x%x\n", bank0size, bank0start);
#endif
}

void PlatformStoreCrashRegs (struct ThreadControl *ctc)
{
	// Get registers
	unsigned int hfsr = *((volatile unsigned long *)(0xE000ED2C));
	unsigned int cfsr = *((volatile unsigned long *)(0xE000ED28));
	unsigned int bfar = *((volatile unsigned long *)(0xE000ED38));
	unsigned int mmar = *((volatile unsigned long *)(0xE000ED34));

	// Re-use params and sigmask
	ctc->param1 = hfsr;
	ctc->param2 = cfsr;
	ctc->param3 = bfar;
	ctc->sigmask = mmar;
}
