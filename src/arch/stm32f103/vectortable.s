/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

.syntax unified
.cpu cortex-m3
.fpu softvfp
.thumb

.section .text.UnknownHandler
.global UnknownHandler
.type UnknownHandler, %function
UnknownHandler:
	b UnknownHandler
.size UnknownHandler, .-UnknownHandler

.section .vectortable.VectorTable,"a",%progbits
.global VectorTable
.type VectorTable, %object
.size VectorTable, .-VectorTable
VectorTable:
	.word 0x00000000
	.word KernelResetEntry				@ Reset
	.word UnknownHandler				@ Non-maskable interrupt
	.word HardFaultHandler				@ Hard fault
	.word MemManageHandler				@ Memory management fault
	.word BusFaultHandler				@ Bus fault
	.word UsageFaultHandler				@ Usage fault
	.word 0x00000000					@ Reserved
	.word 0x00000000					@ Reserved
	.word 0x00000000					@ Reserved
	.word 0x00000000					@ Reserved
	.word SVCHandler					@ SVC
	.word UnknownHandler				@ Debug monitor
	.word 0x00000000					@ Reserved
	.word SchedContext					@ PendSV
	.word PlatformIncrementTick			@ SysTick
	.word WWDG_IrqHandler				@ WWDG (Window watchdog)	IRQ0
	.word PVD_IrqHandler				@ PVD						IRQ1
	.word TAMP_STAMP_IrqHandler			@ TAMP_STAMP				IRQ2
	.word RTC_WKUP_IrqHandler			@ RTC_WKUP					IRQ3
	.word FLASH_IrqHandler				@ FLASH						IRQ4
	.word RCC_IrqHandler				@ RCC						IRQ5
	.word EXTI0_IrqHandler				@ EXTI0						IRQ6
	.word EXTI1_IrqHandler				@ EXTI1						IRQ7
	.word EXTI2_IrqHandler				@ EXTI2						IRQ8
	.word EXTI3_IrqHandler				@ EXTI3						IRQ9
	.word EXTI4_IrqHandler				@ EXTI4						IRQ10
	.word DMA1CH1_IrqHandler			@ DMA1_Stream1				IRQ11
	.word DMA1CH2_IrqHandler			@ DMA1_Stream2				IRQ12
	.word DMA1CH3_IrqHandler			@ DMA1_Stream3				IRQ13
	.word DMA1CH4_IrqHandler			@ DMA1_Stream4				IRQ14
	.word DMA1CH5_IrqHandler			@ DMA1_Stream5				IRQ15
	.word DMA1CH6_IrqHandler			@ DMA1_Stream6				IRQ16
	.word DMA1CH7_IrqHandler			@ DMA1_Stream7				IRQ17
	.word ADC_IrqHandler				@ ADC						IRQ18
	.word CAN1TX_IrqHandler				@ CAN1_TX					IRQ19
	.word CAN1RX0_IrqHandler			@ CAN1_RX0					IRQ20
	.word CAN1RX1_IrqHandler			@ CAN1_RX1					IRQ21
	.word CAN1SCE_IrqHandler			@ CAN1_SCE					IRQ22
	.word EXTI95_IrqHandler				@ EXTI9_5					IRQ23
	.word TIM1BRKTIM9_IrqHandler		@ TIM1_BRK_TIM9				IRQ24
	.word TIM1UPTIM10_IrqHandler		@ TIM1_UP_TIM10				IRQ25
	.word TIM1TRGCOMTIM11_IrqHandler	@ TIM1_TRG_COM_TIM11		IRQ26
	.word TIM1CC_IrqHandler				@ TIM1_CC					IRQ27
	.word TIM2_IrqHandler				@ TIM2						IRQ28
	.word TIM3_IrqHandler				@ TIM3						IRQ29
	.word TIM4_IrqHandler				@ TIM4						IRQ30
	.word I2C1EV_IrqHandler				@ I2C1_EV					IRQ31
	.word I2C1ER_IrqHandler				@ I2C1_ER					IRQ32
	.word I2C2EV_IrqHandler				@ I2C2_EV					IRQ33
	.word I2C2ER_IrqHandler				@ I2C2_ER					IRQ34
	.word SPI1_IrqHandler				@ SPI1						IRQ35
	.word SPI2_IrqHandler				@ SPI2						IRQ36
	.word UART1_IrqHandler				@ USART1					IRQ37
	.word UART2_IrqHandler				@ USART2					IRQ38
	.word UART3_IrqHandler				@ USART3					IRQ39
	.word EXTI1510_IrqHandler			@ EXTI15_10					IRQ40
	.word RTCALARM_IrqHandler			@ RTC_Alarm					IRQ41
	.word OTGFSWKUP_IrqHandler			@ OTG_FS WKUP				IRQ42
	.word TIM8BRKTIM12_IrqHandler		@ TIM8_BRK_TIM12			IRQ43
	.word TIM8UPTIM13_IrqHandler		@ TIM8_UP_TIM13				IRQ44
	.word TIM8TRGCOMTIM14_IrqHandler	@ TIM8_TRG_COM_TIM14		IRQ45
	.word TIM8CC_IrqHandler				@ TIM8_CC					IRQ46
	.word ADC3_IrqHandler				@ ADC3						IRQ47
	.word FSMC_IrqHandler				@ FSMC						IRQ48
	.word SDIO_IrqHandler				@ SDIO						IRQ49
	.word TIM5_IrqHandler				@ TIM5						IRQ50
	.word SPI3_IrqHandler				@ SPI3						IRQ51
	.word UART4_IrqHandler				@ UART4						IRQ52
	.word UART5_IrqHandler				@ UART5						IRQ53
	.word TIM6DAC_IrqHandler			@ TIM6_DAC					IRQ54
	.word TIM7_IrqHandler				@ TIM7						IRQ55
	.word DMA2CH1_IrqHandler			@ DMA2_Stream1				IRQ56
	.word DMA2CH2_IrqHandler			@ DMA2_Stream2				IRQ57
	.word DMA2CH3_IrqHandler			@ DMA2_Stream3				IRQ58
	.word DMA2CH4_IrqHandler			@ DMA2_Stream4				IRQ59

.weak WWDG_IrqHandler;				.thumb_set WWDG_IrqHandler,				UnknownHandler
.weak PVD_IrqHandler;				.thumb_set PVD_IrqHandler,				UnknownHandler
.weak TAMP_STAMP_IrqHandler;		.thumb_set TAMP_STAMP_IrqHandler,		UnknownHandler
.weak RTC_WKUP_IrqHandler;			.thumb_set RTC_WKUP_IrqHandler,			UnknownHandler
.weak FLASH_IrqHandler;				.thumb_set FLASH_IrqHandler,			UnknownHandler
.weak RCC_IrqHandler;				.thumb_set RCC_IrqHandler,				UnknownHandler
.weak EXTI0_IrqHandler;				.thumb_set EXTI0_IrqHandler,			UnknownHandler
.weak EXTI1_IrqHandler;				.thumb_set EXTI1_IrqHandler,			UnknownHandler
.weak EXTI2_IrqHandler;				.thumb_set EXTI2_IrqHandler,			UnknownHandler
.weak EXTI3_IrqHandler;				.thumb_set EXTI3_IrqHandler,			UnknownHandler
.weak EXTI4_IrqHandler;				.thumb_set EXTI4_IrqHandler,			UnknownHandler
.weak DMA1CH1_IrqHandler;			.thumb_set DMA1CH1_IrqHandler,			UnknownHandler
.weak DMA1CH2_IrqHandler;			.thumb_set DMA1CH2_IrqHandler,			UnknownHandler
.weak DMA1CH3_IrqHandler;			.thumb_set DMA1CH3_IrqHandler,			UnknownHandler
.weak DMA1CH4_IrqHandler;			.thumb_set DMA1CH4_IrqHandler,			UnknownHandler
.weak DMA1CH5_IrqHandler;			.thumb_set DMA1CH5_IrqHandler,			UnknownHandler
.weak DMA1CH6_IrqHandler;			.thumb_set DMA1CH6_IrqHandler,			UnknownHandler
.weak DMA1CH7_IrqHandler;			.thumb_set DMA1CH7_IrqHandler,			UnknownHandler
.weak ADC_IrqHandler;				.thumb_set ADC_IrqHandler,				UnknownHandler
.weak CAN1TX_IrqHandler;			.thumb_set CAN1TX_IrqHandler,			UnknownHandler
.weak CAN1RX0_IrqHandler;			.thumb_set CAN1RX0_IrqHandler,			UnknownHandler
.weak CAN1RX1_IrqHandler;			.thumb_set CAN1RX1_IrqHandler,			UnknownHandler
.weak CAN1SCE_IrqHandler;			.thumb_set CAN1SCE_IrqHandler,			UnknownHandler
.weak EXTI95_IrqHandler;			.thumb_set EXTI95_IrqHandler,			UnknownHandler
.weak TIM1BRKTIM9_IrqHandler;		.thumb_set TIM1BRKTIM9_IrqHandler,		UnknownHandler
.weak TIM1UPTIM10_IrqHandler;		.thumb_set TIM1UPTIM10_IrqHandler,		UnknownHandler
.weak TIM1TRGCOMTIM11_IrqHandler;	.thumb_set TIM1TRGCOMTIM11_IrqHandler,	UnknownHandler
.weak TIM1CC_IrqHandler;			.thumb_set TIM1CC_IrqHandler,			UnknownHandler
.weak TIM2_IrqHandler;				.thumb_set TIM2_IrqHandler,				UnknownHandler
.weak TIM3_IrqHandler;				.thumb_set TIM3_IrqHandler,				UnknownHandler
.weak TIM4_IrqHandler;				.thumb_set TIM4_IrqHandler,				UnknownHandler
.weak I2C1EV_IrqHandler;			.thumb_set I2C1EV_IrqHandler,			UnknownHandler
.weak I2C1ER_IrqHandler;			.thumb_set I2C1ER_IrqHandler,			UnknownHandler
.weak I2C2EV_IrqHandler;			.thumb_set I2C2EV_IrqHandler,			UnknownHandler
.weak I2C2ER_IrqHandler;			.thumb_set I2C2ER_IrqHandler,			UnknownHandler
.weak SPI1_IrqHandler;				.thumb_set SPI1_IrqHandler,				UnknownHandler
.weak SPI2_IrqHandler;				.thumb_set SPI2_IrqHandler,				UnknownHandler
.weak UART1_IrqHandler;				.thumb_set UART1_IrqHandler,			UnknownHandler
.weak UART2_IrqHandler;				.thumb_set UART2_IrqHandler,			UnknownHandler
.weak UART3_IrqHandler;				.thumb_set UART3_IrqHandler,			UnknownHandler
.weak EXTI1510_IrqHandler;			.thumb_set EXTI1510_IrqHandler,			UnknownHandler
.weak RTCALARM_IrqHandler;			.thumb_set RTCALARM_IrqHandler,			UnknownHandler
.weak OTGFSWKUP_IrqHandler;			.thumb_set OTGFSWKUP_IrqHandler,		UnknownHandler
.weak TIM8BRKTIM12_IrqHandler;		.thumb_set TIM8BRKTIM12_IrqHandler,		UnknownHandler
.weak TIM8UPTIM13_IrqHandler;		.thumb_set TIM8UPTIM13_IrqHandler,		UnknownHandler
.weak TIM8TRGCOMTIM14_IrqHandler;	.thumb_set TIM8TRGCOMTIM14_IrqHandler,	UnknownHandler
.weak TIM8CC_IrqHandler;			.thumb_set TIM8CC_IrqHandler,			UnknownHandler
.weak ADC3_IrqHandler;				.thumb_set ADC3_IrqHandler,				UnknownHandler
.weak FSMC_IrqHandler;				.thumb_set FSMC_IrqHandler,				UnknownHandler
.weak SDIO_IrqHandler;				.thumb_set SDIO_IrqHandler,				UnknownHandler
.weak TIM5_IrqHandler;				.thumb_set TIM5_IrqHandler,				UnknownHandler
.weak SPI3_IrqHandler;				.thumb_set SPI3_IrqHandler,				UnknownHandler
.weak UART4_IrqHandler;				.thumb_set UART4_IrqHandler,			UnknownHandler
.weak UART5_IrqHandler;				.thumb_set UART5_IrqHandler,			UnknownHandler
.weak TIM6DAC_IrqHandler;			.thumb_set TIM6DAC_IrqHandler,			UnknownHandler
.weak TIM7_IrqHandler;				.thumb_set TIM7_IrqHandler,				UnknownHandler
.weak DMA2CH1_IrqHandler;			.thumb_set DMA2CH1_IrqHandler,			UnknownHandler
.weak DMA2CH2_IrqHandler;			.thumb_set DMA2CH2_IrqHandler,			UnknownHandler
.weak DMA2CH3_IrqHandler;			.thumb_set DMA2CH3_IrqHandler,			UnknownHandler
.weak DMA2CH4_IrqHandler;			.thumb_set DMA2CH4_IrqHandler,			UnknownHandler
