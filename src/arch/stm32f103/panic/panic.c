/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "panic/panic.h"
#include "print/print.h"
#include "sched/scheduler.h"
#include "process/processcontrol.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
const char *CFSRtoString (unsigned int cfsr_bitindex)
{
	static const char a[] = "Inst access violation";
	static const char b[] = "Data access violation";
	static const char c[] = "Fault on unstacking";
	static const char d[] = "Fault on stacking";
	static const char e[] = "Fault float lazy state";
	static const char f[] = "MMAR valid address";

	static const char g[] = "Instruction bus error";
	static const char h[] = "Precise data bus error";
	static const char i[] = "Imprecise data bus error";
	static const char j[] = "BusFault on unstacking";
	static const char k[] = "BusFault on stacking";
	static const char l[] = "Fault float lazy state";
	static const char m[] = "BFAR valid address";

	static const char n[] = "Undefined instruction";
	static const char o[] = "Invalid state";
	static const char p[] = "Invalid PC load";
	static const char q[] = "No coprocessor";
	static const char r[] = "Unaligned access";
	static const char s[] = "Divide by zero";

	static const char t[] = "Unknown fault";

	static const char *ptrs[] =
		{a, b, t, c,
		d, e, t, f,
		g, h, i, j, k, l, t, m,
		n, o, p, q,
		t, t, t, t,
		r, s, t, t,
		t, t, t, t};

	return ptrs[cfsr_bitindex];
}

#if PANIC_ENABLE_TTY
void PanicInternalTTY (unsigned int *stack, int stacktype, int faulttype, unsigned int *regs)
{
	// Print header
	Print ("Crash detected!\n");
	Print (stacktype == PANIC_STACKTYPE_USER ? "Userspace\n" : "Kernel space\n");

	// Fault type
	const char *type = "Unknown handler call\n";

	if (faulttype == PANIC_FAULTTYPE_KERNEL) type = "Kernel panic\n";
	if (faulttype == PANIC_FAULTTYPE_HARDFAULT) type = "HardFault\n";
	if (faulttype == PANIC_FAULTTYPE_MEMMANAGE) type = "MemManage\n";
	if (faulttype == PANIC_FAULTTYPE_BUSFAULT) type = "BusFault\n";
	if (faulttype == PANIC_FAULTTYPE_USAGEFAULT) type = "UsageFault\n";

	Print (type);

	// If fault type was a "manual" kernel panic
	if (faulttype == PANIC_FAULTTYPE_KERNEL)
	{
		Print ((const char*) stack);
	}
	else
	{
		// Print all regular registers
		Print ("r0  0x%x\n", stack[0]);
		Print ("r1  0x%x\n", stack[1]);
		Print ("r2  0x%x\n", stack[2]);
		Print ("r3  0x%x\n", stack[3]);
		Print ("r12 0x%x\n", stack[4]);
		Print ("lr  0x%x\n", stack[5]);
		Print ("pc  0x%x\n", stack[6]);
		Print ("psr 0x%x\n", stack[7]);

		// Print hardfault status reg, configurable fault status reg and bus fault address reg
		Print ("hfsr 0x%x\n", regs[0]);
		Print ("cfsr 0x%x\n", regs[1]);
		Print ("bfar 0x%x\n", regs[2]);
		Print ("mmar 0x%x\n", regs[3]);

		// Print type
		if (regs[1] & 0x000000FF) Print ("Memory Management Fault\n");
		if (regs[1] & 0x0000FF00) Print ("Bus Fault\n");
		if (regs[1] & 0xFFFF0000) Print ("Usage Fault\n");

		// Print CFSR members
		for (int i = 0; i < 32; i++)
		{
			if (regs[1] & (1 << i))
			{
				Print (CFSRtoString (i));
				Print ("\n");
			}
		}
	}
}
#endif

void PanicInternal (unsigned int *stack, int stacktype, int faulttype)
{
	// Disable interrupts
	SchedEnterCritical ();

	// Store hardfault status reg, configurable fault status reg and bus fault address reg
	unsigned int hfsr = *((volatile unsigned int*) 0xE000ED2C);
	unsigned int cfsr = *((volatile unsigned int*) 0xE000ED28);
	unsigned int bfar = *((volatile unsigned int*) 0xE000ED38);
	unsigned int mmar = *((volatile unsigned int*) 0xE000ED34);

	unsigned int regs[4] = {hfsr, cfsr, bfar, mmar};

	// Print to lcd/tty
#if PANIC_ENABLE_TTY
	PanicInternalTTY (stack, stacktype, faulttype, regs);
#endif

	// Loop forever
	while (1);
}

void PanicPrintReport (struct ThreadControl *ctc)
{
#if PANIC_ENABLE_TTY
	unsigned int regs[4] = {ctc->param1, ctc->param2, ctc->param3, ctc->sigmask};
	PanicInternalTTY (ctc->stacktop, PANIC_STACKTYPE_USER, ctc->sigpending, regs);
	Print ("Adjusted PC %x\n", ctc->stacktop[6] - ctc->pctrl->text);
#else
	(void)ctc;
#endif
}
