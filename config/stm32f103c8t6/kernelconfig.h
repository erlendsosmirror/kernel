/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Maximum number of nodes in the node table
#define NODETABLE_SIZE 16

// Maximum number of virtual file systems
#define VFS_MAX_DRIVERS 4

// Maximum path size
#define VFS_MAX_PATH 64

// Maximum mount point name/path size
#define VFS_MOUNTPOINT_MAX_SIZE 16

// Maximum mount point prepend size
#define VFS_PREPEND_MAX_SIZE 4

// Maximum number of file descriptors per process
#define MAX_NODES 8

// Specifies if the stdin/stdout is to be reopened in init and the filename
#define INIT_REOPENSTDIO 0
#define INIT_REOPEN_FILENAME "/dev/fbcon"

// Enables (1) or disables (0) the two different panic solutions
// Both may be enabled at the same time
#define PANIC_ENABLE_TTY 1
#define PANIC_ENABLE_LCD 0

// Run a custom "main" instead of launching the root shell, for use in
// minimal configurations
#define INIT_CUSTOM_MINIMAL_MAIN 1
