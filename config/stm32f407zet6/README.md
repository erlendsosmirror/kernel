This configuration is for the "Minimum System Development" board for the
144-pin stm32f407zet6. The board comes with a clock battery, a 25Q16 serial
flash and various pin headers.
