/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Memory init options
///////////////////////////////////////////////////////////////////////////////
#define PLATFORM_USE_LCD 1
#define PLATFORM_USE_INTERNAL_MEMORY 1
#define PLATFORM_USE_EXTERNAL_MEMORY 0
#define PLATFORM_EXTERNAL_MEMORY_ADDR 0xD0000000
#define PLATFORM_EXTERNAL_MEMORY_SIZE (8 * 1024 * 1024)

///////////////////////////////////////////////////////////////////////////////
// Platform FSMC config
///////////////////////////////////////////////////////////////////////////////

// Platform FSMC config
#define PLATFORM_LCD_FSMC 0

// Clock configuration for the LCD
#define PLATFORM_LCD_CLOCK_CONFIG 0x00010511

// Not used
#define PLATFORM_FSMC_PINS_PORTC 0

// OE and WE (PD4 and PD5), NE1 (PD7), A18 (PD13)
// D2 D3 D13 D14 D15 D0 D1
#define PLATFORM_FSMC_PINS_PORTD (PIN(4) | PIN(5) | PIN(7) | PIN(13) | PIN(0) | \
	PIN(1) | PIN(8) | PIN(9) | PIN(10) | PIN(14) | PIN(15))

// D4 D5 D6 D7 D8 D9 D10 D11 D12
#define PLATFORM_FSMC_PINS_PORTE (PIN(7) | PIN(8) | PIN(9) | PIN(10) | \
	PIN(11) | PIN(12) | PIN(13) | PIN(14) | PIN(15))

// Not used
#define PLATFORM_FSMC_PINS_PORTF 0

// Not used
#define PLATFORM_FSMC_PINS_PORTG 0

// Not used
#define PLATFORM_FSMC_PINS_PORTH 0

///////////////////////////////////////////////////////////////////////////////
// HSE Clock Frequency
///////////////////////////////////////////////////////////////////////////////
#define HSE_CLK_FREQ 8000000

///////////////////////////////////////////////////////////////////////////////
// PLL Initial Configuration
///////////////////////////////////////////////////////////////////////////////
#define PLL_M 8
#define PLL_N 336
#define PLL_P 0 // Check datasheet! 0 = DIV/2
#define PLL_SRC 1
#define PLL_Q 7
