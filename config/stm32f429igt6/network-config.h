/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NETWORK_CONFIG
#define NETWORK_CONFIG

///////////////////////////////////////////////////////////////////////////////
// Address configuration
///////////////////////////////////////////////////////////////////////////////

// The "MAC" address of this device
#define LAYER2_ADDRESS 0x27

// Default gateway, normal notation 192.1
#define IP_DEFAULT_GATEWAY 0xC001

// Default address of this node
#define IP_DEFAULT_ADDRESS 5

///////////////////////////////////////////////////////////////////////////////
// Interface init
///////////////////////////////////////////////////////////////////////////////
#define NET_INIT \
	static Layer2KTTY interface0; \
	static Layer2KTTY interface1; \
	static Layer2KTTY interface2; \
	KTTYInit (&interface0, "tty1"); \
	KTTYInit (&interface1, "tty2"); \
	KTTYInit (&interface2, "tty5"); \
	AddInterface ((Layer2*) &interface0); \
	AddInterface ((Layer2*) &interface1); \
	AddInterface ((Layer2*) &interface2);

///////////////////////////////////////////////////////////////////////////////
// Stack component configuration
///////////////////////////////////////////////////////////////////////////////

// Address Resolution Protocol
#define ARP_TIMEOUT 1000
#define ARP_USE_TABLE 1
#define ARP_TABLE_SIZE 5

// Allocation component
#define FRAMEPOOL_POOLSIZE 10
#define FRAMEPOOL_ERASE 0

// Driver component
#define RS485_TXFIFO_SIZE 10
#define RS485_RXFIFO_SIZE 10
#define RS485_RXBUFF_SIZE 256

// Link speed component
#define LAYER2LINKSPEED_USE_DEFAULT_SPEEDS 1

// Switch component
#define SWITCH_LISTSIZE 32

///////////////////////////////////////////////////////////////////////////////
// Special headers
///////////////////////////////////////////////////////////////////////////////

// Timing header to use
#define NETWORK_DELAY_INC "platform/platform.h"

///////////////////////////////////////////////////////////////////////////////
// Debug setup
///////////////////////////////////////////////////////////////////////////////

// Include required for the selected print function
#define NETWORK_DEBUG_INC "print/print.h"

// Print macros for strings and string+integer
#define NETWORK_PRINTS(str) Print(str)
#define NETWORK_PRINTI(str,i) Print(str,i)

// Per-module debug enable
#define NETWORK_DEBUG_ARP 0
#define NETWORK_DEBUG_FRAMEPOOL 0
#define NETWORK_DEBUG_LINKSPEED 0
#define NETWORK_DEBUG_INTERFACE 0
#define NETWORK_DEBUG_SWITCH 0
#define NETWORK_DEBUG_IP 0
#define NETWORK_DEBUG_OTHER 0

// Switch messages
#define DEBUG_SWITCH_ADDLIST 1
#define DEBUG_SWITCH_UPDATELIST 1
#define DEBUG_SWITCH_DELETELIST 1
#define DEBUG_SWITCH_BROADCASTING 1

// Linkspeed
#define DEBUG_LINKSPEED_DISCOVERYSEND 1
#define DEBUG_LINKSPEED_STATEIS3 1
#define DEBUG_LINKSPEED_TESTFAILED 1
#define DEBUG_LINKSPEED_YIELDONDISCOVER 1
#define DEBUG_LINKSPEED_SENDINGLIST 1
#define DEBUG_LINKSPEED_YIELDONLISTREPLY 1
#define DEBUG_LINKSPEED_SENDINGSETSPEED 1
#define DEBUG_LINKSPEED_SETTINGSPEED 1
#define DEBUG_LINKSPEED_TEST 1
#define DEBUG_LINKSPEED_TESTREPLY 1
#define DEBUG_LINKSPEED_SPEEDCONFIRMED 1
#define DEBUG_LINKSPEED_TIMEOUT 1

#endif

